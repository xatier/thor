/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_FRAMEWORK_EXECUTOR_H_
#define ZILLIANS_FRAMEWORK_EXECUTOR_H_

#include <cstdint>

#include <atomic>
#include <deque>
#include <string>
#include <memory>
#include <thread>

#include <boost/any.hpp>
#include <boost/filesystem/path.hpp>
#include <boost/noncopyable.hpp>

#include <log4cxx/logger.h>

#include "utility/FactoryRegistry.h"

namespace zillians { namespace framework {

struct invocation_request;
class  executor_remote;

class executor : boost::noncopyable
{
public:
    explicit  executor(std::int32_t new_id, const std::string& logger_name);
    virtual  ~executor() = 0;

    std::int32_t get_id() const noexcept;

    bool is_verbose() const noexcept;
    void set_verbose(bool new_verbose = true) noexcept;

    virtual bool set_hint(const std::string& key, const boost::any& value);

    virtual bool initialize();
    virtual bool finalize();

    virtual bool start();
    virtual bool stop();

private:
    void looping();

protected:
    virtual void do_work() = 0;

protected:
    bool               verbose;
    log4cxx::LoggerPtr logger;

private:
    std::int32_t      id;
    std::atomic<bool> stop_requested;
    std::thread       worker;
};

class executor_rt : public executor
{
private:
    using path_t = boost::filesystem::path;

public:
    enum
    {
        TYPE_UNKNOWN   ,
        TYPE_X86_ST = 1,
        TYPE_X86_MT    ,
        TYPE_CUDA      ,
    };

    enum
    {
        EXPORT_ALL   ,
        EXPORT_SERVER,
        EXPORT_CLIENT,
    };

             executor_rt(std::int32_t new_id, const std::string& logger_name);
    virtual ~executor_rt() = 0;

    virtual bool initialize() override;
    virtual bool finalize() override;

    virtual bool start() override;
    virtual bool stop() override;

    virtual const char* get_binary_file_suffix() const = 0;

    virtual void set_arguments(const std::vector<std::wstring>& new_args) = 0;

    virtual bool call(const std::string& func_name) = 0;
    virtual bool call(std::int64_t func_id) = 0;
    virtual bool call(std::int64_t session_id, invocation_request&& request) = 0;

    virtual bool call_and_wait(std::int64_t       func_id  ) = 0;
    virtual bool call_and_wait(const std::string& func_name) = 0;

    virtual int get_exit_code() = 0; // wait if not yet

    int  get_export_type() const noexcept;
    void set_export_type(int new_export_type) noexcept;

    const path_t& get_ast_path() const noexcept;
    void          set_ast_path(const path_t& new_ast_path);

    const path_t& get_dll_path() const noexcept;
    void          set_dll_path(const path_t& new_dll_path);

    const std::vector<path_t>& get_dep_pathes() const noexcept;
    void                       set_dep_pathes(const std::vector<path_t>& new_dep_pathes);

    framework::executor_remote& get_executor_remote() const;

protected:
    int export_type = EXPORT_ALL;

    path_t              ast_path;
    path_t              dll_path;
    std::vector<path_t> dep_pathes;

    std::unique_ptr<framework::executor_remote> executor_remote;
};

using executor_rt_registry = factory_registry<executor_rt, int>;

} } // namespace 'zillians::framework'

#endif /* ZILLIANS_FRAMEWORK_EXECUTOR_H_ */
