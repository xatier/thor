/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_FRAMEWORK_PROXIES_X86_H_
#define ZILLIANS_FRAMEWORK_PROXIES_X86_H_

#include <cstddef>
#include <cstdint>

#include <deque>
#include <memory>
#include <string>
#include <utility>
#include <vector>

#include "utility/UUIDUtil.h"
#include "thor/lang/Domain.h"

// class forward declarations
namespace thor {

namespace lang {

class Object;

}

namespace container {

template<typename> class Vector;

}

}

namespace zillians { namespace framework { namespace x86_detail {

class object_manager;

} } }

namespace zillians { namespace framework { namespace proxies { namespace x86 {

const std::vector<std::wstring>& get_arguments();

void exit(std::int32_t exit_code);

void* allocate_object(std::size_t size, std::int64_t type_id);
void   destroy_objects(void* ptr);

void* dyn_cast(void* ptr, std::int64_t target_type_id);

std::pair<const wchar_t*, std::size_t> get_string(std::int64_t str_id);

std::pair<std::unique_ptr<std::deque<std::int8_t>>, std::int64_t> steal_replication_data();

std::int64_t     add_invocation(std::int64_t func_id, void* ret_ptr);
std::int64_t     add_invocation(std::int64_t func_id, std::int64_t session_id, std::unique_ptr<std::deque<std::int8_t>>&& replication_data);
std::int64_t reserve_invocation(std::int64_t func_id);
bool          commit_invocation(std::int64_t inv_id);
bool           abort_invocation(std::int64_t inv_id);

bool set_invocation_return_ptr(std::int64_t inv_id, void* ptr);

std::int64_t append_invocation_parameter(std::int64_t inv_id, std::int64_t offset, bool         value);
std::int64_t append_invocation_parameter(std::int64_t inv_id, std::int64_t offset, std::int8_t  value);
std::int64_t append_invocation_parameter(std::int64_t inv_id, std::int64_t offset, std::int16_t value);
std::int64_t append_invocation_parameter(std::int64_t inv_id, std::int64_t offset, std::int32_t value);
std::int64_t append_invocation_parameter(std::int64_t inv_id, std::int64_t offset, std::int64_t value);
std::int64_t append_invocation_parameter(std::int64_t inv_id, std::int64_t offset, float        value);
std::int64_t append_invocation_parameter(std::int64_t inv_id, std::int64_t offset, double       value);
std::int64_t append_invocation_parameter(std::int64_t inv_id, std::int64_t offset, void*        value); // for thor.lang.Object

bool postpone_invocation_dependency_to(std::int64_t inv_id);
bool set_invocation_depend_on_count(std::int64_t inv_id, std::int32_t count);
bool set_invocation_depend_on(std::int64_t inv_id, std::int64_t depend_on_inv_id);

std::int64_t get_current_invocation_function_id();
std::int64_t get_current_invocation_session_id();
const char*  get_current_invocation_parameters();

std::int64_t get_type_id(::thor::lang::Object* obj);

using remote_adaptor_t      = void(::thor::lang::ReplicationDecoder*);
using object_creator_t      = ::thor::lang::Object*();
using object_serializer_t   = bool(::thor::lang::ReplicationEncoder*, ::thor::lang::Object*);
using object_deserializer_t = bool(::thor::lang::ReplicationDecoder*, ::thor::lang::Object*);

remote_adaptor_t     * get_remote_adaptor     (std::int64_t func_id);
object_creator_t     * get_object_creator     (std::int64_t type_id);
object_serializer_t  * get_object_serializer  (std::int64_t type_id);
object_deserializer_t* get_object_deserializer(std::int64_t type_id);

std::int64_t gc_get_generation_id();
void         gc_get_active_objects(thor::container::Vector<thor::lang::Object*>& objs);

void gc_shared_root_set_add   (void* ptr, x86_detail::object_manager* object_manager = nullptr);
void gc_shared_root_set_remove(void* ptr, x86_detail::object_manager* object_manager = nullptr);

void gc_global_root_set_add   (void* ptr);
void gc_global_root_set_remove(void* ptr);

zillians::UUID domain_listen(const std::wstring& endpoint, ::thor::lang::Domain::ConnCallback* conn_cb, ::thor::lang::Domain::ConnCallback* disconn_cb, ::thor::lang::Domain::ErrCallback* err_cb);
zillians::UUID domain_connect(const std::wstring& endpoint, ::thor::lang::Domain::ConnCallback* conn_cb, ::thor::lang::Domain::ConnCallback* disconn_cb, ::thor::lang::Domain::ErrCallback* err_cb);
bool           domain_cancel(const zillians::UUID& connection_id);

::thor::lang::Domain* get_domain_object(std::int64_t session_id);
::thor::lang::Domain* add_domain_object(std::int64_t session_id, ::thor::lang::Domain* domain);

} } } }

#endif /* ZILLIANS_FRAMEWORK_PROXIES_X86_H_ */
