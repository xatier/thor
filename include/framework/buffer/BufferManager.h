/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_FRAMEWORK_BUFFERMANAGER_H_
#define ZILLIANS_FRAMEWORK_BUFFERMANAGER_H_

#include <unordered_map>

#include <boost/tuple/tuple.hpp>

#include "core/IntTypes.h"
#include "core/ScalablePoolAllocator.h"
#include "core/Singleton.h"
#include "framework/buffer/BufferConstants.h"

namespace zillians { namespace framework { namespace buffer {

/**
 * BufferManager is responsible for all memory allocations inside ExecutionEngine.
 *
 * TODO: allow dynamic resizing of the buffer reserved
 */
class BufferManager : public Singleton<BufferManager, SingletonInitialization::automatic>
{
public:
	BufferManager();
	virtual ~BufferManager();

public:
	bool reserve(buffer_location_t::type location, std::size_t size);
	bool allocate(buffer_location_t::type location, std::size_t size, /*OUT*/ byte** buffer);
	void deallocate(buffer_location_t::type location, byte* buffer);
	bool commit(buffer_location_t::type location);
	bool commit();

#ifdef BUILD_WITH_NETWORK_ADDON
public:
	bool query_mr_lkey(buffer_location_t::type location, byte* buffer, int dev, /*OUT*/ uint32& lkey);
#endif
	byte* query_mirrored_ptr(buffer_location_t::type location, byte* buffer);

private:
	void cleanup(buffer_location_t::type location);

private:
	struct BufferPool
	{
		BufferPool() : total_reserved(0), total_allocated(0), to_reserve(0), max_single_allocation_size(std::numeric_limits<std::size_t>::max()) { }
		std::vector<ScalablePoolAllocator*> allocators;
		std::vector< boost::tuple<byte*, byte*, std::size_t> > reserved_segments;
		std::size_t total_reserved;
		std::size_t total_allocated;
		std::size_t to_reserve;
		std::size_t max_single_allocation_size;
	};

	std::unordered_map<uint32, BufferPool*> mReservedBuffers;

private:
	static log4cxx::LoggerPtr mLogger;
};

} } }

#endif /* ZILLIANS_FRAMEWORK_BUFFERMANAGER_H_ */
