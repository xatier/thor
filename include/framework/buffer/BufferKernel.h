/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include "framework/buffer/BufferConstants.h"

#ifndef ZILLIANS_FRAMEWORK_BUFFERKERNEL_H_
#define ZILLIANS_FRAMEWORK_BUFFERKERNEL_H_

namespace zillians { namespace framework { namespace buffer {

typedef void* (*malloc_func)(std::size_t size);
typedef void  (*free_func)  (void* ptr);
typedef bool  (*memcpy_func)(void* dest, const void* src, std::size_t size);

// global look-up table (from buffer location to actual malloc/free/memcpy function)
// this table is exported so that memory functions can be dynamically registered and used and link dependency can be avoided
// see framework/buffer/init/ for more buffer kernel init functions
extern malloc_func malloc_func_table[buffer_location_t::number_of_locations];
extern free_func   free_func_table  [buffer_location_t::number_of_locations];
extern memcpy_func memcpy_func_table[buffer_location_t::number_of_locations][buffer_location_t::number_of_locations];

} } }

#endif /* ZILLIANS_FRAMEWORK_BUFFERKERNEL_H_ */
