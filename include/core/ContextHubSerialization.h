/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_CONTEXTHUBSERIALIZATION_H_
#define ZILLIANS_CONTEXTHUBSERIALIZATION_H_

#include <boost/serialization/base_object.hpp>
#include <boost/serialization/export.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/list.hpp>
#include <boost/serialization/utility.hpp>
#include <boost/mpl/size.hpp>
#include <boost/mpl/at.hpp>
#include <boost/mpl/int.hpp>
#include <boost/mpl/vector.hpp>

#include "core/ContextHub.h"

namespace zillians {

struct ContextHubSerializationBase
{
    template<typename Archive>
    void serialize(Archive& ar, const unsigned int version)
    {
        UNUSED_ARGUMENT(ar);
        UNUSED_ARGUMENT(version);

        // dummy serialization
    }
};

template<int N, typename Types>
struct ContextHubSerializationImpl : public ContextHubSerializationImpl<N-1, Types>
{
    ContextHubSerializationImpl(ContextHub<ContextOwnership::transfer>& h) : ContextHubSerializationImpl<N-1,Types>(h), hub(h)
    { }

    template<class Archive>
    void save(Archive& ar, const unsigned int version) const
    {
        UNUSED_ARGUMENT(version);

        typedef typename boost::mpl::at<Types, boost::mpl::int_<N - 1> >::type T;
        ar & boost::serialization::base_object<ContextHubSerializationImpl<N-1,Types>>(*this);

        T* t = hub.get<T>();
        ar & t;
    }

    template<class Archive>
    void load(Archive& ar, const unsigned int version)
    {
        UNUSED_ARGUMENT(version);

        typedef typename boost::mpl::at<Types, boost::mpl::int_<N - 1> >::type T;
        ar & boost::serialization::base_object<ContextHubSerializationImpl<N-1,Types>>(*this);

        T* t = NULL;
        ar & t;
        hub.set<T>(t);
    }

    BOOST_SERIALIZATION_SPLIT_MEMBER()

    ContextHub<ContextOwnership::transfer>& hub;
};

template<typename Types>
struct ContextHubSerializationImpl<0, Types> : public ContextHubSerializationBase
{
    ContextHubSerializationImpl(ContextHub<ContextOwnership::transfer>& h) : hub(h)
    { }

    template<typename Archive>
    void serialize(Archive& ar, const unsigned int version)
    {
        UNUSED_ARGUMENT(version);

        ar & boost::serialization::base_object<ContextHubSerializationBase>(*this);
    }

    ContextHub<ContextOwnership::transfer>& hub;
};

template<typename Types>
struct ContextHubSerialization
{
    ContextHubSerialization(ContextHub<ContextOwnership::transfer>& h) : hub(h), t(h)
    { }

    template<typename Archive>
    void serialize(Archive& ar, const unsigned int version)
    {
        UNUSED_ARGUMENT(version);

        ar & t;
    }

    ContextHub<ContextOwnership::transfer>& hub;
    ContextHubSerializationImpl<boost::mpl::size<Types>::value, Types> t;
};

}

//BOOST_CLASS_EXPORT_GUID(zillians::ContextHubSerializationBase, "ContextHubSerializationBase")

#endif /* ZILLIANS_CONTEXTHUBSERIALIZATION_H_ */
