/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_TREE_ASTNODEHELPER_H_
#define ZILLIANS_LANGUAGE_TREE_ASTNODEHELPER_H_

#include <fstream>

#include <boost/mpl/vector.hpp>
#include <boost/mpl/size.hpp>
#include <boost/mpl/at.hpp>
#include <boost/mpl/int.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/filesystem.hpp>
#include <boost/logic/tribool.hpp>

#include "language/Architecture.h"
#include "language/tree/ASTNodeFactory.h"
#include "language/tree/basic/Type.h"
#include "language/tree/basic/DeclType.h"
#include "language/context/ManglingStageContext.h"
#include "language/context/ParserContext.h"
#include "language/context/ResolverContext.h"
#include "language/context/TransformerContext.h"
#include "language/stage/parser/context/SourceInfoContext.h"
#include "language/tree/visitor/ASTGraphvizGenerator.h"

namespace zillians { namespace language { namespace tree {

namespace detail {

typedef boost::mpl::vector<
    language::stage::SourceInfoContext,
    language::NameManglingContext,
    language::DefaultValueFrom
> ContextToCloneTypeList;

struct CloneReferenceContext { };

struct CloneRefMapper
{
    template<typename NodeType>
    void operator&(NodeType*& ref)
    {
        if(ref)
        {
            // FIXME the result of 'get<CloneReferenceContext>' should be 'ASTNode*' instead of 'CloneReferenceContext*'

            ASTNode* cloned_ref = (ASTNode*) (ref->template get<CloneReferenceContext>());
            if(cloned_ref)
            {
                BOOST_ASSERT(isa<NodeType>(cloned_ref) && "incompatible node");

                ref = cast<NodeType>(cloned_ref);
            }
        }
    }
};

template<typename T, bool RecursiveVisit = true, bool IncludeSelf = true>
struct ForeachVisitor : public visitor::GenericDoubleVisitor
{
    CREATE_INVOKER(foreachInvoker, apply);

    ForeachVisitor(std::function<void(T&)>& functor, std::function<bool(ASTNode&)>& visit_condition) : depth(0), functor(functor), visit_condition(visit_condition)
    {
        REGISTER_ALL_VISITABLE_ASTNODE(foreachInvoker);
    }

    void apply(ASTNode& node)
    {
        if(visit_condition && !visit_condition(node))
            return;

        if(isa<T>(&node))
        {
            if(IncludeSelf || depth)
                functor(*cast<T>(&node));
        }

        if(RecursiveVisit || (!RecursiveVisit && !depth))
        {
            ++depth;
            revisit(node);
            --depth;
        }
    }

    int depth;
    std::function<void(T&)>& functor;
    std::function<bool(ASTNode&)>& visit_condition;
};

template<typename ContextT>
inline void contextCloneUpdateReferenceImpl(ASTNode& from, ASTNode& to)
{
    static CloneRefMapper m;

    ContextT* ctx = new ContextT(*from.template get<ContextT>());
    ctx->template update_reference<CloneRefMapper>(m);
    to.template set<ContextT, ContextOwnership::transfer>(ctx);
}

template<>
inline void contextCloneUpdateReferenceImpl<ResolvedType>(ASTNode& from, ASTNode& to)
{
    static CloneRefMapper m;

    Type* ref = ResolvedType::get(&from);
    ResolvedType::update_reference(m, ref);
    ResolvedType::set(&to, ref);
}

template<int N, typename ContextTypeList>
struct ContextCloneImpl
{
    typedef typename boost::mpl::at<ContextTypeList, boost::mpl::int_<N-1> >::type ContextT;
    static void clone(ASTNode* from, ASTNode* to)
    {
        if(ContextT::get(from))
        {
            contextCloneUpdateReferenceImpl<ContextT>(*from, *to);
        }
        ContextCloneImpl<N-1, ContextTypeList>::clone(from, to);
    }
};

template<typename ContextTypeList>
struct ContextCloneImpl<0, ContextTypeList>
{
    static void clone(ASTNode* from, ASTNode* to)
    {
        UNUSED_ARGUMENT(from);
        UNUSED_ARGUMENT(to);
    }
};

}

struct ASTNodeHelper
{
    template<typename T, bool RecursiveVisit = true, bool IncludeSelf = true>
    static void foreachApply(ASTNode& node, std::function<void(T&)> functor, std::function<bool(ASTNode&)> visit_condition = std::function<bool(ASTNode&)>())
    {
        typename detail::ForeachVisitor<T, RecursiveVisit, IncludeSelf> visitor(functor, visit_condition);
        visitor.visit(node);
    }

    template<typename T, typename ContextTypeList = detail::ContextToCloneTypeList, bool RecursiveClone = true>
    static T* clone(T* from)
    {
        T* to = from->clone();
        cloneContext<T, ContextTypeList, RecursiveClone>(from, to);
        return to;
    }

    template<typename T, typename ContextTypeList = detail::ContextToCloneTypeList, bool RecursiveClone = true>
    static void cloneContext(T* from, T* to)
    {
        if(RecursiveClone)
        {
            // collect from nodes
            std::vector<ASTNode*> fromNodes;
            foreachApply<ASTNode>(*from, [&fromNodes](ASTNode& node){
                fromNodes.push_back(&node);
            });
            // collect to nodes
            std::vector<ASTNode*> toNodes;
            foreachApply<ASTNode>(*to, [&toNodes](ASTNode& node){
                toNodes.push_back(&node);
            });
            BOOST_ASSERT(fromNodes.size() == toNodes.size());

            // associate the clone reference
            for(size_t i=0; i != fromNodes.size(); ++i)
            {
                ASTNode* fromNode = fromNodes[i];
                ASTNode* toNode   = toNodes[i];
                fromNode->template set<detail::CloneReferenceContext, ContextOwnership::keep>((detail::CloneReferenceContext*)toNode);
            }

            // copy
            for(size_t i=0; i != fromNodes.size(); ++i)
            {
                ASTNode* fromNode = fromNodes[i];
                ASTNode* toNode   = toNodes[i];
                detail::ContextCloneImpl<boost::mpl::size<ContextTypeList>::value, ContextTypeList>::clone(fromNode, toNode);
            }

            // clean-up the clone reference
            for(size_t i=0; i != fromNodes.size(); ++i)
            {
                ASTNode* fromNode = fromNodes[i];
                fromNode->template set<detail::CloneReferenceContext, ContextOwnership::keep>(NULL);
            }
        }
        else
        {
            detail::ContextCloneImpl<boost::mpl::size<ContextTypeList>::value, ContextTypeList>::clone(from, to);
        }
    }

    static void tryPropogateSourceInfo(ASTNode& to, const ASTNode& from);
    static void propogateSourceInfo(ASTNode& to, const ASTNode& from);
    static void propogateSourceInfoRecursively(ASTNode& to, const ASTNode& from);
    static void propogateArchitecture(Declaration& node);

    template<typename NodeType, typename... ArgTypes>
    static NodeType* createWithSourceInfo(const ASTNode& ref_node, ArgTypes&&... args)
    {
        NodeType*const node = new NodeType(std::forward<ArgTypes>(args)...);

        propogateSourceInfo(*node, ref_node);

        return node;
    }

    //////////////////////////////////////////////////////////////////////////
    // canonical type
    //////////////////////////////////////////////////////////////////////////
    static Type* getCanonicalType(const ASTNode* node);
    static ASTNode* getCanonicalSymbol(const ASTNode* node);

    static bool isInheritedFrom(const ClassDecl* node_derived, const ClassDecl* node_base);
    static boost::tribool isInheritedFromEx(const ClassDecl* node_derived, const ClassDecl* node_base);

    /**
     * @brief Identify a node has a specified ancestor
     * @param child 
     * @param ancestor
     * @return \p true if \p ancestor <b>is a ancestor</b> of child.
     *         \p false else.
     */
    static bool hasAncestor(ASTNode* child, ASTNode* ancestor);

    static bool hasOwnerNamedScope(const ASTNode* node);
    static ASTNode* getOwnerNamedScope(const ASTNode* node);
    static ASTNode* insertIntoOwnerNameScope(const ASTNode& ref_node, FunctionDecl& func_decl);

    static bool hasSystemLinkage(const Annotatable* node);
    static bool hasNativeLinkage(const Declaration* node);
    static bool isExported(const Annotatable* node);

    enum class ExportKind {
    	NoExport,
    	GeneralExport,
    	ClientExport,
    	ServerExport,
    };
    static ExportKind getExportKind(const Annotatable* node);
    static ExportKind mergeExportKind(ExportKind lhs, ExportKind rhs);
    static bool parseCudaAnnotation(Annotation* cuda_annotation, std::vector<std::wstring>& isa);
    static const ASTNode* getDebugAnnotationAttachPoint(const ASTNode* node);

    static bool isFuncParam(VariableDecl* node);
    static bool isDeclStmtVarDecl(const VariableDecl* node);
    static bool isInvalidDebugAnnotationAttachPoint(const ASTNode* node);
    static bool isCallIdentifier(Identifier* node);
    static bool isSameFunctionSignature(FunctionDecl& func1, FunctionDecl& func2);
    static std::wstring getNodeName(ASTNode* node, bool FQN = false);
    static std::wstring getTypeString(ASTNode* node, bool FQN = false);
    static void toSource(char* path);
    static void visualize(char* path);
    static void visualize(ASTNode* node, const boost::filesystem::path& path);
    static TypeSpecifier* buildResolvableTypeSpecifier(Declaration* decl);

    /**
     * @brief Get type specifier from type declaration (or another type specifier)
     * @param node given declaration node
     * @return type specifier pointing to the given declaration node
     */
    static TypeSpecifier* getTypeSpecifierFrom(ASTNode* node);

    /**
     * @brief Create type specifier from type declaration (or another type specifier)
     * @param node given declaration node
     * @return type specifier pointing to the given declaration node
     */
    static TypeSpecifier*      createTypeSpecifierFrom(const          Type* type          );
    static FunctionSpecifier*  createTypeSpecifierFrom(const  FunctionType* type_func     );
    static MultiSpecifier*     createTypeSpecifierFrom(const     MultiType* type_multi    );
    static NamedSpecifier*     createTypeSpecifierFrom(const    RecordType* type_record   );
    static NamedSpecifier*     createTypeSpecifierFrom(const      EnumType* type_enum     );
    static PrimitiveSpecifier* createTypeSpecifierFrom(const PrimitiveType* type_primitive);

    static Expression* getDummyValue(const Type& type);

    static bool isDynamicClass(ClassDecl& node);
    static std::list<ClassDecl*> getDirectBases(ClassDecl& node);

    static std::vector<Package*> getParentScopePackages(const ASTNode* node);

    template< typename Collector>
    static std::wstring getExpressiveDescImpl(Identifier* id, Collector& collector)
    {
        if(auto templated_id = cast<TemplatedIdentifier>(id))
        {
            for(auto* templated : templated_id->templated_type_list)
            {
                if(templated->specialized_type)
                {
                    collector.push_back(templated);
                }
            }
        }

        return id->toString();
    }

    template< typename Collector>
    static std::wstring getExpressiveDescImpl(TypeSpecifier* ts, Collector& collector)
    {
        if(Type* resolved_type = ResolvedType::get(ts))
        {
            if(TypenameDecl* decl = resolved_type->getAsTypenameDecl())
            {
                if(decl->specialized_type)
                {
                    collector.push_back(decl);
                }
            }
        }

        return ts->toString();
    }

    template< typename Collector>
    static std::wstring getExpressiveDescImpl(FunctionDecl* func, Collector& collector)
    {
        std::wstring desc;

        desc += getExpressiveDescImpl(func->name, collector);

        desc += L'(';

        {
            bool has_param = false;

            for(auto* param : func->parameters)
            {
                if(has_param)
                {
                    desc += L',';
                }
                else
                {
                    has_param = true;
                }

                desc += getExpressiveDescImpl(param->type, collector);
            }
        }

        desc += L"):";

        if(func->type)
        {
            desc += getExpressiveDescImpl(func->type, collector);
        }
        else
        {
            desc += L"<not-resolved>";
        }

        return desc;
    }

    static std::wstring& getExpressiveDescSuffix(std::wstring& desc, const std::vector<TypenameDecl*>& typenames);
    static std::wstring getExpressiveDesc(FunctionDecl& func);

    static bool isOnThorLang(const ASTNode* node);
    static Expression* useThorLangId(const ASTNode* node, Identifier* id_in_thor_lang);
    static Identifier* createThorLangId(const ASTNode* node, Identifier* id_in_thor_lang);
    static NamedSpecifier* createThorLangTs(const ASTNode* node, Identifier* id_in_thor_lang);

    static ASTNode* getDeclarationOwner(Declaration& decl);

    /**
     * @brief Calculate the conversion rank from an Expression to a TypeSpecifier.
     * @param from Source Expression
     * @param to Target TypeSpecifier
     * @return 
     * 
     * @li @c ConversionRank::ExactMatch if two type is exactly same
     * @li @c ConversionRank::Promotion if @p from can be promote to @p to.
     *
     * Smaller numeric types can be promote to larger numeric types, for example,
     * @c int8 to @c int16 or @c int64 or @c float32 is promotion.
     *
     * @li @c ConversionRank::StandardConversion if @p from can be standard to @p to.
     *
     * Larger numeric types can be standard converse to smaller numeric types, for example,
     * @c float32 to @c int16 or @c int64 is standard conversion.
     *
     * @note Derived class to Base class is standard conversion, too.
     *
     * @li @c ConversionRank::NotMatch if @p from can **NOT** be convert to @p to.
     *
     * @li @c ConversionRank::UnknownYet if the result can not be determined yet.
     *
     */
    static Type::ConversionRank getConversionRank(Expression& from, TypeSpecifier& to    , const Type::ConversionPolicy policy = Type::ConversionPolicy::Implicitly);
    static Type::ConversionRank getConversionRank(Expression& from, Type&          toType, const Type::ConversionPolicy policy = Type::ConversionPolicy::Implicitly);

private:
    static bool isNamedScope(ASTNode* node);
    static bool isDebugAnnotationAttachPoint(const ASTNode* node);
    static const ASTNode* getSplitReferenceAttachPoint(const ASTNode* node);
    static const ASTNode* getOwnerDebugAnnotationAttachPoint(const ASTNode* node);

    ASTNodeHelper();
    ~ASTNodeHelper();
};

} } }

#endif /* ZILLIANS_LANGUAGE_TREE_ASTNODEHELPER_H_ */
