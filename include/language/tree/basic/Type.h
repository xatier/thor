/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_TREE_TYPE_H_
#define ZILLIANS_LANGUAGE_TREE_TYPE_H_

#include <string>
#include <vector>

#include <boost/preprocessor/cat.hpp>
#include <boost/preprocessor/seq/for_each.hpp>

#include "language/tree/ASTNode.h"
#include "language/tree/RelinkablePtr.h"
#include "language/tree/UniqueName.h"

namespace zillians { namespace language { namespace tree {

struct Internal;
struct PrimitiveType;
struct PointerType;
struct ReferenceType;
struct FunctionType;
struct DeclType;
struct RecordType;
struct EnumType;
struct TypenameType;
struct TypedefType;
struct PrimitiveKind;

//////////////////////////////////////////////////////////////////////////////
// Type
//////////////////////////////////////////////////////////////////////////////

#define THOR_SCRIPT_TYPE_SEQ     \
    (PointerType  )              \
    (PrimitiveType)              \
    (ReferenceType)              \
    (MultiType    )              \
    (FunctionType )              \
    (DeclType     )              \
    (RecordType   )              \
    (EnumType     )              \
    (TypenameType )              \
    (TypedefType  )

struct Type: public ASTNode
{
    friend class boost::serialization::access;

    /**
     * Ranks of Conversion.
     *
     * The order of enums IS CRITICAL! The integer number of the enums is used
     * for sorting and comparing the priority of function candidates. Be
     * careful while changing or adding new member to this enum, and make sure
     * all the test cases pass.
     */
    enum class ConversionRank
    {
        ExactMatch,
        Promotion,
        StandardConversion,
        NotMatch,
        UnknownYet,
    };

    enum class ConversionPolicy
    {
        Implicitly,
        Explicitly,
    } ;

    AST_NODE_HIERARCHY(Type, ASTNode);

    virtual bool isEqualImpl(const ASTNode& rhs) const override = 0;
    virtual bool replaceUseWith(const ASTNode& from,
                                const ASTNode& to,
                                bool update_parent = true) override = 0;

    template<typename Archive>
    void serialize(Archive& ar, const unsigned int)
    {
        AST_NODE_SERIALIZE(
            ar,
            (unique_name)
            (resolved_type)
        );
    }

    virtual Type* clone() const = 0;
    virtual bool isSame(const Type& rhs) const = 0;
    virtual bool isLessThan(const Type& rhs) const = 0;
    virtual std::wstring toString() const = 0;
    virtual size_t getTypeClassSerialNumber() const = 0;
    virtual ConversionRank getConversionRank(const Type& target, const ConversionPolicy policy = Type::ConversionPolicy::Implicitly) const = 0;

    static FunctionType*  getBy(FunctionDecl& func_decl);
    static PrimitiveType* getBy(PrimitiveKind kind, const ASTNode& parent_ref);

#define THOR_SCRIPT_DECL_TYPE_ACCESSOR(r, data, elem)         \
          bool  BOOST_PP_CAT(   is, elem) () const;           \
          elem* BOOST_PP_CAT(getAs, elem) ()      ;           \
    const elem* BOOST_PP_CAT(getAs, elem) () const;

    BOOST_PP_SEQ_FOR_EACH(THOR_SCRIPT_DECL_TYPE_ACCESSOR, _, THOR_SCRIPT_TYPE_SEQ)

#undef THOR_SCRIPT_DECL_TYPE_ACCESSOR

    template<typename elem_type>       elem_type* getAs()      ;
    template<typename elem_type> const elem_type* getAs() const;

    bool isVoidType         () const;
    bool isBoolType         () const;
    bool isIntegerType      () const;
    bool isFloatType        () const;
    bool isArithmeticCapable() const;

    Declaration * getAsDecl        () const;
    ClassDecl   * getAsClassDecl   () const;
    EnumDecl    * getAsEnumDecl    () const;
    TypenameDecl* getAsTypenameDecl() const;
    TypedefDecl * getAsTypedefDecl () const;

    const unique_name_t& getUniqueName() const noexcept;

    bool isCanonical() const;
    virtual Type* getCanonicalType() const = 0;

    virtual PrimitiveType* getArithmeticCompatibleType() const;

    struct TypeLess
    {
        bool operator()(Type* l, Type* r);
    };

    unique_name_t unique_name;
    relinkable_ptr<Type> resolved_type;
protected:
    Type();

private:
};

//////////////////////////////////////////////////////////////////////////////
// PointerType
//////////////////////////////////////////////////////////////////////////////

struct PointerType : public Type
{
    friend class boost::serialization::access;

    AST_NODE_HIERARCHY(PointerType, Type);

    virtual bool isEqualImpl(const ASTNode& rhs) const;
    virtual bool replaceUseWith(const ASTNode& from,
                                const ASTNode& to,
                                bool update_parent = true);

    template<typename Archive>
    void serialize(Archive& ar, const unsigned int)
    {
        AST_NODE_SERIALIZE(
            ar,
            (pointee_type)
        );
    }

    virtual std::wostream& toSource(std::wostream& output, unsigned indent/* = 0*/) const override; using BaseNode::toSource;
    virtual PointerType* clone() const;
    virtual bool isSame(const Type& rhs) const;
    virtual bool isLessThan(const Type& rhs) const;
    virtual std::wstring toString() const;
    virtual size_t getTypeClassSerialNumber() const;
    virtual ConversionRank getConversionRank(const Type& target, const ConversionPolicy policy = Type::ConversionPolicy::Implicitly) const override;

    Type* getPointeeType() const;
    void setPointeeType(Type* pointeeType);
    virtual PointerType* getCanonicalType() const override;

protected:
    PointerType();

private:
    friend Internal;
    PointerType(Type* pointeeType);
    Type* pointee_type;
} ;

//////////////////////////////////////////////////////////////////////////////
// ReferenceType
//////////////////////////////////////////////////////////////////////////////

struct ReferenceType : public Type
{
    friend class boost::serialization::access;

    AST_NODE_HIERARCHY(ReferenceType, Type);

    virtual bool isEqualImpl(const ASTNode& rhs) const;
    virtual bool replaceUseWith(const ASTNode& from,
                                const ASTNode& to,
                                bool update_parent = true);

    template<typename Archive>
    void serialize(Archive& ar, const unsigned int)
    {
        AST_NODE_SERIALIZE(
            ar,
            (pointee_type)
        );
    }

    virtual std::wostream& toSource(std::wostream& output, unsigned indent/* = 0*/) const override; using BaseNode::toSource;
    virtual ReferenceType* clone() const;
    virtual bool isSame(const Type& rhs) const;
    virtual bool isLessThan(const Type& rhs) const;
    virtual std::wstring toString() const;
    virtual size_t getTypeClassSerialNumber() const;
    virtual ConversionRank getConversionRank(const Type& target, const ConversionPolicy policy = Type::ConversionPolicy::Implicitly) const override;

    Type* getPointeeType() const;
    void setPointeeType(Type* pointeeType);
    virtual ReferenceType* getCanonicalType() const override;

protected:
    ReferenceType();

private:
    friend Internal;
    ReferenceType(Type* pointeeType);
    Type* pointee_type;
} ;

struct MultiType : public Type
{
    friend class boost::serialization::access;

    AST_NODE_HIERARCHY(MultiType, Type);

    virtual bool isEqualImpl(const ASTNode& rhs) const override;
    virtual bool replaceUseWith(const ASTNode& from, const ASTNode& to, bool update_parent = true) override;

    virtual std::wostream& toSource(std::wostream& output, unsigned indent/* = 0*/) const override; using BaseNode::toSource;
    virtual MultiType* clone() const override;

    virtual bool isSame(const Type& rhs) const override;
    virtual bool isLessThan(const Type& rhs) const override;
    virtual std::wstring toString() const override;
    virtual std::size_t getTypeClassSerialNumber() const override;
    virtual ConversionRank getConversionRank(const Type& target, const ConversionPolicy policy = Type::ConversionPolicy::Implicitly) const override;

    virtual MultiType* getCanonicalType() const override;

    template<typename Archive>
    void serialize(Archive& ar, const unsigned)
    {
        AST_NODE_SERIALIZE(
            ar,
            (types)
        );
    }

    std::vector<Type*> types;

protected:
    MultiType() = default;

private:
    friend Internal;

    MultiType(const std::vector<Type*>& types);
    MultiType(std::vector<Type*>&& types);

};

} } }

#endif /* ZILLIANS_LANGUAGE_TREE_TYPE_H_ */
