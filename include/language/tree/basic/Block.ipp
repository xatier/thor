/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_TREE_BLOCK_IPP_
#define ZILLIANS_LANGUAGE_TREE_BLOCK_IPP_

#include <boost/range/adaptor/reversed.hpp>

#include "language/tree/basic/Block.h"

namespace zillians { namespace language { namespace tree {

template<typename T>
void Block::prependObjects(const T& object_list)
{
    for (Statement* object: object_list | boost::adaptors::reversed)
    {
        BOOST_ASSERT(object && "null pointer exception");

        prependObject(object);
    }
}

template<typename T>
void Block::appendObjects(const T& object_list)
{
    for (Statement* object: object_list)
    {
        BOOST_ASSERT(object && "null pointer exception");

        appendObject(object);
    }
}

template<typename T>
bool Block::insertObjectsBefore(Statement* before, T& object_list, bool replace_before /*= false*/)
{
    if(!before)
    {
        prependObjects(object_list);
        return true;
    }
    else
    {
        auto it = std::find(objects.begin(), objects.end(), before);
        if(it != objects.end())
        {
            for(auto* object : object_list)
                object->parent = this;
            objects.insert(it, object_list.begin(), object_list.end());

            if(replace_before)
                objects.erase(it);

            return true;
        }
        else
        {
            return false;
        }
    }
}

template<typename T>
bool Block::insertObjectsAfter(Statement* after, T& object_list, bool replace_after /*= false*/)
{
    if(!after)
    {
        appendObjects(object_list);
        return true;
    }
    else
    {
        auto it = std::find(objects.begin(), objects.end(), after);
        if(it != objects.end())
        {
            for(auto* object : object_list)
                object->parent = this;
            ++it;
            objects.insert(it, object_list.begin(), object_list.end());

            if(replace_after)
                objects.erase(it);

            return true;
        }
        else
        {
            return false;
        }
    }
}

template<typename Archive>
void Block::serialize(Archive& ar, const unsigned int)
{
    AST_NODE_SERIALIZE(
        ar,
        (arch       )
        (is_implicit)
        (objects    )
    );
}

template<typename Archive>
void NormalBlock::serialize(Archive& ar, const unsigned int)
{
    AST_NODE_SERIALIZE(
        ar,
    );
}

template<typename Archive>
void AsyncBlock::serialize(Archive& ar, const unsigned int)
{
    AST_NODE_SERIALIZE(
        ar,
        (target)
        (block )
        (grid  )
    );
}

template<typename Archive>
void AtomicBlock::serialize(Archive& ar, const unsigned int)
{
    AST_NODE_SERIALIZE(
        ar,
        (stm_policy)
    );
}

template<typename Archive>
void FlowBlock::serialize(Archive& ar, const unsigned int)
{
    AST_NODE_SERIALIZE(
        ar,
    );
}

template<typename Archive>
void LockBlock::serialize(Archive& ar, const unsigned int)
{
    AST_NODE_SERIALIZE(
        ar,
        (rw_lock_or_simple_mutex)
    );
}

template<typename Archive>
void PipelineBlock::serialize(Archive& ar, const unsigned int)
{
    AST_NODE_SERIALIZE(
        ar,
        (async_or_sync)
    );
}

} } }

#endif /* ZILLIANS_LANGUAGE_TREE_BLOCK_IPP_ */
