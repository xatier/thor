/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_UNIQUENAME_H_
#define ZILLIANS_LANGUAGE_UNIQUENAME_H_

#include <string>
#include <vector>
#include <utility>

// Forward declarations
namespace zillians { namespace language { namespace tree {

struct ClassDecl;
struct FunctionDecl;
struct Declaration;
struct VariableDecl;
struct Package;
struct EnumDecl;
struct TypenameDecl;
struct TypedefDecl;

struct Type;
struct PrimitiveKind;
struct PrimitiveType;
struct RecordType;
struct FunctionType;
struct MultiType;
struct TypenameType;
struct EnumType;
struct TypedefType;

using unique_name_t = std::string;

// Here are rules how a unique name was generated, elements in uppercase
// are identifiers, no blanks in unique name:

// <simple_id>                  ::= value of 'SimpleIdentifier::toString()'
// <template_id>                ::= <simple_id> '<' +( <type_unique_name> | '@1' | '@2' | '@3' and so on... ) '>'
// <identifier>                 ::= <simple_id> | <template_id>

// <type_unique_name>           ::= (
//                                   <primitive_type_unique_name>
//                                   | <primitive_kind_unique_name>
//                                   | <record_type_unique_name>
//                                   | <enum_type_unique_name>
//                                   | <function_type_unique_name>
//                                   | <mulitype_unique_name>
//                                   | <typedef_type_unique_name>
//                                  )
// <type_unique_name_list>      ::= <type_unique_name> *( ',' <type_unique_name> )

// <primitive_type_unique_name> ::= <primitive_kind_unique_name>
// <primitive_kind_unique_name> ::= valud of 'PrimitiveKind::toString()'
// <record_type_unique_name>    ::= <class_decl_unique_name>
// <enum_type_unique_name>      ::= <enum_decl_unique_name>
// <typedef_type_unique_name>   ::= <typedef_decl_unique_name>
// <typname_type_unique_name>   ::= <typename_decl_unique_name>
// <function_type_unique_name>  ::= [ <record_type_unique_name> '.' ] <multitype_unique_name> ':' <type_unique_name>
// <multitype_unique_name>      ::= '(' [ <type_unique_name_list> ] ')'

// <decl_unique_name>           ::= <package_unique_name> '.'
//                                  (
//                                   <class_decl_unique_name>
//                                   | <function_decl_unique_name>
//                                   | <enum_decl_unique_name>
//                                   | <typedef_decl_unique_name>
//                                   | <variable_decl_unique_name>
//                                  )

// <package_unique_name>        ::= 'p:_' *( '.p:' <simple_id> )
//                                  ^ this is root package
// <class_decl_unique_name>     ::= <package_unique_name> '$c:' <identifier>
// <enum_decl_unique_name>      ::= <package_unique_name> '$e:' <simple_id>
// <function_decl_unique_name>  ::= ( <class_decl_unique_name> | <package_unique_name> ) '$f:' <identifier>
//                                  '(' +( <type_unique_name> | '@1' | '@2' | '@3' and so on... ) ')'
// <typename_decl_unique_name>  ::= ( <class_decl_unique_name> | <function_decl_unique_name> ) '$tn:' <simple_id>
// <typedef_decl_unique_name>   ::= <package_unique_name> '$td:' <simple_id>
// <variable_decl_unique_name>  ::= ( <class_decl_unique_name> | <function_decl_unique_name> ) '$v:' <simple_id>

// create unique name from declarations, returns empty string indicate FAILURE
unique_name_t create_unique_name(const Package&     , bool with_header = true);
unique_name_t create_unique_name(const Declaration& , bool with_header = true);
unique_name_t create_unique_name(const ClassDecl&   , bool with_header = true);
unique_name_t create_unique_name(const EnumDecl&    , bool with_header = true);
unique_name_t create_unique_name(const FunctionDecl&, bool with_header = true);
unique_name_t create_unique_name(const VariableDecl&, bool with_header = true);
unique_name_t create_unique_name(const TypedefDecl& , bool with_header = true);
unique_name_t create_unique_name(const TypenameDecl&, bool with_header = true);

// create unique name from types, returns empty string indicate FAILURE
unique_name_t create_unique_name(const Type&         );
unique_name_t create_unique_name(const RecordType&   );
unique_name_t create_unique_name(const EnumType&     );
unique_name_t create_unique_name(const FunctionType& );
unique_name_t create_unique_name(const MultiType&    );
unique_name_t create_unique_name(const PrimitiveKind&);
unique_name_t create_unique_name(const PrimitiveType&);
unique_name_t create_unique_name(const TypedefType&  );
unique_name_t create_unique_name(const TypenameType& );

std::vector<unique_name_t> extract_param_type_names(const unique_name_t& function_type_unique_name);
unique_name_t              extract_return_type_name(const unique_name_t& funciton_type_unique_name);
// for method types, return their owner class type's unique name
unique_name_t              extract_class_type_name(const unique_name_t& unique_name);
std::vector<unique_name_t> extract_subtype_names(const unique_name_t& mutitype_unique_name);

// get the owner unique name of a declaration.
// empty unique name indicate failure
unique_name_t extract_owner_unique_name(const unique_name_t& decl_unique_name);

enum class unique_name_category_t
{
    PACKAGE, CLASS, ENUM, FUNCTION, VARIABLE, TYPEDEF,
    NONE // failure
};
unique_name_category_t get_unique_name_category(const unique_name_t& unique_name);

bool is_type_unique_name(const unique_name_t& name);
bool is_multitype_unique_name(const unique_name_t& name);
bool is_function_type_unique_name(const unique_name_t& name);

} } } // namespace 'zillians::language::tree'

#endif
