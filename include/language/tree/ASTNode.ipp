/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_TREE_ASTNODE_IPP_
#define ZILLIANS_LANGUAGE_TREE_ASTNODE_IPP_

#include <memory>
#include <string>
#include <type_traits>

#include <boost/variant.hpp>

#include "utility/Traits.h"
#include "utility/UUIDUtil.h"

#include "language/tree/ASTNode.h"

namespace zillians { namespace language { namespace tree {

struct PrimitiveKind;

namespace detail {

template<typename Type, typename ...RemainTypes>
bool isa_impl<Type, RemainTypes...>::test(const ASTNode* ptr) noexcept
{
    if (ptr->checkType(std::remove_cv<Type>::type::stype()))
        return true;
    else
        return isa_impl<RemainTypes...>::test(ptr);
}

template<typename Type, typename ...RemainTypes>
bool isa_impl<Type, RemainTypes...>::test(const ASTNode& ref) noexcept
{
    if (ref.checkType(std::remove_cv<Type>::type::stype()))
        return true;
    else
        return isa_impl<RemainTypes...>::test(ref);
}

template<typename T>
struct is_primitive_ast_member : std::integral_constant<
    bool,
    std::is_integral      <T               >::value ||
    std::is_floating_point<T               >::value ||
    std::is_enum          <T               >::value ||
    std::is_same          <T, PrimitiveKind>::value ||
    std::is_same          <T, std::string  >::value ||
    std::is_same          <T, UUID         >::value
>{};

}

inline const char* ASTNode::instanceName() const noexcept
{
    return "ASTNode";
}

inline bool ASTNode::checkType(int type) const noexcept
{
    return type == ASTNode::stype();
}

inline bool ASTNode::isEqual(const ASTNode& rhs) const
{
    const auto lhs_tag =     _tag();
    const auto rhs_tag = rhs._tag();

    if (lhs_tag != rhs_tag)
        return false;
    else
        return isEqualImpl(rhs);
}

inline bool ASTNode::isEqualImpl(const ASTNode& rhs) const
{
    return true;
}

struct ASTNode::ReplaceUseWithVisitor : public boost::static_visitor<bool>
{
    ReplaceUseWithVisitor(const ASTNode& from, const ASTNode& to, bool update_parent)
        : from(from)
        , to(to)
        , update_parent(update_parent)
    {
    }

    template<typename U>
    result_type operator()(U&& t) const
    {
        return ASTNode::replaceUseWithEval(t, from, to, update_parent);
    }

    const ASTNode& from         ;
    const ASTNode& to           ;
    bool           update_parent;
};

template<typename VariantType>
struct ASTNode::IsEqualVisitor : public boost::static_visitor<bool>
{
    explicit IsEqualVisitor(const VariantType& rhs)
        : rhs(rhs)
    {
    }

    template<typename T>
    result_type operator()(const T& lhs_value) const
    {
        BOOST_ASSERT(boost::get<const T>(&rhs) != nullptr && "LHS and RHS contain different kind of value, this should be handled out of this visitor");

        const auto& rhs_value = boost::get<const T>(rhs);

        return ASTNode::isEqualEval(lhs_value, rhs_value);
    }

    const VariantType& rhs;
};

template<typename T0, typename T1>
inline bool ASTNode::isEqualEval(const std::pair<T0, T1>& lhs, const std::pair<T0, T1>& rhs)
{
    if (!isEqualEval(lhs.first , rhs.first ))
        return false;

    if (!isEqualEval(lhs.second, rhs.second))
        return false;

    return true;
}

template<typename ...Ts>
inline bool ASTNode::isEqualEval(const boost::tuple<Ts...>& lhs, const boost::tuple<Ts...>& rhs)
{
    return isEqualEvalTuple(lhs, rhs);
}

inline bool ASTNode::isEqualEvalTuple(const boost::tuples::null_type& lhs, const boost::tuples::null_type& rhs)
{
    return true;
}

template<typename HeadType, typename TailType>
inline bool ASTNode::isEqualEvalTuple(const boost::tuples::cons<HeadType, TailType>& lhs, const boost::tuples::cons<HeadType, TailType>& rhs)
{
    if (!isEqualEval(lhs.get_head(), rhs.get_head()))
        return false;

    return isEqualEvalTuple(lhs.get_tail(), rhs.get_tail());
}

template<typename ...Ts>
inline bool ASTNode::isEqualEval(const boost::variant<Ts...>& lhs, const boost::variant<Ts...>& rhs)
{
    typedef IsEqualVisitor<boost::variant<Ts...>> VisitorType;

    if (lhs.which() != rhs.which())
        return false;

    return boost::apply_visitor(VisitorType(rhs), lhs);
}

template<typename T, typename std::enable_if<traits::has_begin_end<T>::value>::type * /*= nullptr*/>
inline bool ASTNode::isEqualEval(const T& lhs, const T& rhs)
{
    if (lhs.size() != rhs.size())
        return false;

          auto lhs_i   = lhs.begin();
    const auto lhs_end = lhs.end  ();

          auto rhs_i   = rhs.begin();
    const auto rhs_end = rhs.end  ();

    for (; lhs_i != lhs_end; ++lhs_i, ++rhs_i)
    {
        BOOST_ASSERT(rhs_i != rhs_end && "unexpected end of iteration, have you ensure the size of LHS and RHS is the same");

        if (!isEqualEval(*lhs_i, *rhs_i))
            return false;
    }

    return true;
}

template<typename T, typename std::enable_if<std::is_pointer<T>::value>::type * /*= nullptr*/>
inline bool ASTNode::isEqualEval(const T& lhs, const T& rhs)
{
    if (lhs != nullptr && rhs != nullptr)
        return isEqualEval(*lhs, *rhs);
    else
        return lhs == rhs;
}

template<typename T, typename std::enable_if<isNode<T>::value>::type * /*= nullptr*/>
inline bool ASTNode::isEqualEval(const T& lhs, const T& rhs)
{
    return lhs.isEqual(rhs);
}

template<typename T, typename std::enable_if<!std::is_pointer<T>::value && !traits::has_begin_end<T>::value && !isNode<T>::value>::type * /*= nullptr*/>
inline bool ASTNode::isEqualEval(const T& lhs, const T& rhs)
{
    static_assert(detail::is_primitive_ast_member<T>::value, "not verified value type");

    return lhs == rhs;
}

inline bool ASTNode::replaceUseWith(const ASTNode& from, const ASTNode& to, bool update_parent /*= true*/)
{
    return false;
}

template<typename T0, typename T1>
inline bool ASTNode::replaceUseWithEval(std::pair<T0, T1>& t, const ASTNode& from, const ASTNode& to, bool update_parent)
{
    bool result = false;

    result |= replaceUseWithEval(t.first , from, to, update_parent);
    result |= replaceUseWithEval(t.second, from, to, update_parent);

    return result;
}

template<typename ...Ts>
inline bool ASTNode::replaceUseWithEval(boost::tuple<Ts...>& t, const ASTNode& from, const ASTNode& to, bool update_parent)
{
    return replaceUseWithEvalTuple(t, from, to, update_parent);
}

template<typename ...Ts>
inline bool ASTNode::replaceUseWithEval(boost::variant<Ts...>& t, const ASTNode& from, const ASTNode& to, bool update_parent)
{
    bool result = false;

    result |= boost::apply_visitor(ReplaceUseWithVisitor(from, to, update_parent), t);

    return result;
}

template<typename T>
inline bool ASTNode::replaceUseWithEval(T*& t, const ASTNode& from, const ASTNode& to, bool update_parent)
{
    if (t == &from) // pointer compare
    {
        t = static_cast<T*>(const_cast<ASTNode*>(&to));

        if (update_parent)
        {
            t->parent = from.parent;
            const_cast<ASTNode&>(from).parent = nullptr;
        }
    }

    return true;
}

template<typename T, typename std::enable_if<traits::has_begin_end<T>::value>::type * /* = nullptr*/>
inline bool ASTNode::replaceUseWithEval(T& t, const ASTNode& from, const ASTNode& to, bool update_parent)
{
    bool result = false;

    for (auto &item: t)
    {
        result |= replaceUseWithEval(item, from, to, update_parent);
    }

    return result;
}

template<typename T, typename std::enable_if<isNode<T>::value>::type * /* = nullptr*/>
inline bool ASTNode::replaceUseWithEval(T& t, const ASTNode& from, const ASTNode& to, bool update_parent)
{
    return t.replaceUseWith(from, to, update_parent);
}

template<typename T, typename std::enable_if<!traits::has_begin_end<T>::value && !isNode<T>::value>::type * /* = nullptr*/>
inline bool ASTNode::replaceUseWithEval(T& t, const ASTNode& from, const ASTNode& to, bool update_parent)
{
    static_assert(detail::is_primitive_ast_member<T>::value, "not verified value type");

    return false;
}

inline bool ASTNode::replaceUseWithEvalTuple(boost::tuples::null_type t, const ASTNode& from, const ASTNode& to, bool update_parent)
{
    return false;
}

template<typename HeadType, typename TailType>
inline bool ASTNode::replaceUseWithEvalTuple(boost::tuples::cons<HeadType, TailType>& t, const ASTNode& from, const ASTNode& to, bool update_parent)
{
    if (replaceUseWithEval(t.get_head(), from, to, update_parent))
        return true;

    return replaceUseWithEvalTuple(t.get_tail(), from, to, update_parent);
}

} } }

#endif /* ZILLIANS_LANGUAGE_TREE_ASTNODE_IPP_ */

