/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_TREE_SYSTEMCALLEXPR_H_
#define ZILLIANS_LANGUAGE_TREE_SYSTEMCALLEXPR_H_

#include <initializer_list>
#include <ostream>
#include <vector>

#include <boost/serialization/access.hpp>

#include "language/tree/ASTNode.h"
#include "language/tree/expression/Expression.h"

namespace zillians { namespace language { namespace tree {

struct SystemCallExpr : public Expression
{
    friend class boost::serialization::access;

    AST_NODE_HIERARCHY(SystemCallExpr, Expression);

    enum class CallType
    {
        UNKNOWN,
        ASYNC  ,
        REMOTE ,
    };

    static SystemCallExpr* createAsync();
    static SystemCallExpr* createRemote();

private:
    SystemCallExpr();

public:
    explicit SystemCallExpr(CallType type);

    template<typename RangeType = std::initializer_list<Expression*>>
    SystemCallExpr(CallType type, const RangeType& parameters);

    void appendParameter(Expression* parameter);
    void prependParameter(Expression* parameter);

    template<typename RangeType = std::initializer_list<Expression*>> void appendParameters(const RangeType& new_parameters);
    template<typename RangeType = std::initializer_list<Expression*>> void prependParameters(const RangeType& new_parameters);

    virtual bool hasValue() const override;
    virtual bool isRValue() const override;

    virtual bool isEqualImpl(const ASTNode& rhs) const override;
    virtual bool replaceUseWith(const ASTNode& from, const ASTNode& to, bool update_parent = true) override;

    virtual SystemCallExpr* clone() const override;

    using BaseNode::toSource;
    virtual std::wostream& toSource(std::wostream& output, unsigned indent = 0) const override;

    template<typename Archive>
    void serialize(Archive& ar, const unsigned int)
    {
        AST_NODE_SERIALIZE(
            ar,
            (call_type )
            (parameters)
        );
    }

    CallType                 call_type;
    std::vector<Expression*> parameters;
};

} } }

#include "language/tree/expression/SystemCallExpr.ipp"

#endif /* ZILLIANS_LANGUAGE_TREE_SYSTEMCALLEXPR_H_ */
