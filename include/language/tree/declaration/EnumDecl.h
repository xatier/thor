/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_TREE_ENUMDECL_H_
#define ZILLIANS_LANGUAGE_TREE_ENUMDECL_H_

#include "language/resolver/SymbolTable.h"
#include "language/tree/basic/DeclType.h"
#include "language/tree/basic/Identifier.h"
#include "language/tree/declaration/Declaration.h"
#include "language/tree/declaration/VariableDecl.h"
#include "language/tree/expression/Expression.h"

namespace zillians { namespace language { namespace tree {

struct PrimitiveKind;

struct EnumDecl : public Declaration
{
    friend class boost::serialization::access;

    AST_NODE_HIERARCHY(EnumDecl, Declaration);

    explicit EnumDecl(Identifier* name);

    void addEnumeration(VariableDecl* decl);
    void addEnumeration(SimpleIdentifier* tag, Expression* value = NULL);

    VariableDecl* findEnumerator(int64 value) const;
    PrimitiveKind getUnderlyingType() const;

    virtual bool isEqualImpl(const ASTNode& rhs) const;
    virtual bool replaceUseWith(const ASTNode& from, const ASTNode& to, bool update_parent = true);

    virtual std::wstring toString() const override;
    virtual EnumDecl* clone() const;
    virtual std::wostream& toSource(std::wostream& output, unsigned indent/* = 0*/) const override; using BaseNode::toSource;

    template<typename Archive>
    void serialize(Archive& ar, const unsigned int version)
    {
        UNUSED_ARGUMENT(version);

        ar & boost::serialization::base_object<Declaration>(*this);
        ar & values;
        ar & enum_type_;
    }

    virtual EnumType* getType() const override;
    virtual EnumType* getCanonicalType() const override;

    std::vector<VariableDecl*> values;

    SymbolTable symbol_table;

protected:
    EnumDecl() { }

private:
    EnumType* enum_type_;
};

} } }

#endif /* ZILLIANS_LANGUAGE_TREE_ENUMDECL_H_ */
