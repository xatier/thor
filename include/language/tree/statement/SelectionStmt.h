/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_TREE_SELECTIONSTMT_H_
#define ZILLIANS_LANGUAGE_TREE_SELECTIONSTMT_H_

#include "language/tree/ASTNode.h"
#include "language/tree/statement/Statement.h"

namespace zillians { namespace language { namespace tree {

struct Selection : public ASTNode
{
    friend class boost::serialization::access;

    AST_NODE_HIERARCHY(Selection, ASTNode);

    Selection();
    Selection(Expression* cond, Block* block);

    virtual bool isEqualImpl(const ASTNode& rhs) const override;
    virtual bool replaceUseWith(const ASTNode& from, const ASTNode& to, bool update_parent = true) override;

    virtual std::wostream& toSource(std::wostream& output, unsigned indent/* = 0*/) const override; using BaseNode::toSource;
    virtual Selection* clone() const override;

    template<typename Archive>
    void serialize(Archive& ar, const unsigned int)
    {
        AST_NODE_SERIALIZE(
            ar,
            (cond )
            (block)
        );
    }

    Expression* cond;
    Block*      block;
};

struct SelectionStmt : public Statement
{
    friend class boost::serialization::access;

    AST_NODE_HIERARCHY(SelectionStmt, Statement);

    template<typename Archive>
    void serialize(Archive& ar, const unsigned int version)
    {
        UNUSED_ARGUMENT(version);

        ar & boost::serialization::base_object<Statement>(*this);
    }

    virtual SelectionStmt* clone() const override = 0;
};

struct IfElseStmt : public SelectionStmt
{
    friend class boost::serialization::access;

    AST_NODE_HIERARCHY(IfElseStmt, SelectionStmt);

    explicit IfElseStmt(Selection* if_branch);
             IfElseStmt(Selection* if_branch, Block* else_branch);

    void addElseIfBranch(Selection* branch);
    void setElseBranch(Block* block);

    virtual bool isEqualImpl(const ASTNode& rhs) const;
    virtual bool replaceUseWith(const ASTNode& from, const ASTNode& to, bool update_parent = true);

    virtual IfElseStmt* clone() const;

    virtual std::wostream& toSource(std::wostream& output, unsigned indent/* = 0*/) const override; using BaseNode::toSource;

    template<typename Archive>
    void serialize(Archive& ar, const unsigned int)
    {
        AST_NODE_SERIALIZE(
            ar,
            (if_branch      )
            (elseif_branches)
            (else_block     )
        );
    }

    Selection*              if_branch;
    std::vector<Selection*> elseif_branches;
    Block*                  else_block;

protected:
    IfElseStmt();
};

struct SwitchStmt : public SelectionStmt
{
    friend class boost::serialization::access;

    AST_NODE_HIERARCHY(SwitchStmt, SelectionStmt);

    explicit SwitchStmt(Expression* node);

    void addCase(Selection* branch);
    void setDefaultCase(Block* block);

    virtual bool isEqualImpl(const ASTNode& rhs) const;
    virtual bool replaceUseWith(const ASTNode& from, const ASTNode& to, bool update_parent = true);

    virtual SwitchStmt* clone() const;

    virtual std::wostream& toSource(std::wostream& output, unsigned indent/* = 0*/) const override; using BaseNode::toSource;

    template<typename Archive>
    void serialize(Archive& ar, const unsigned int)
    {
        AST_NODE_SERIALIZE(
            ar,
            (node         )
            (cases        )
            (default_block)
        );
    }

    Expression*             node;
    std::vector<Selection*> cases;
    Block*                  default_block;

protected:
    SwitchStmt();
};

} } }

#endif /* ZILLIANS_LANGUAGE_TREE_SELECTIONSTMT_H_ */
