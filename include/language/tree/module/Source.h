/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_TREE_SOURCE_H_
#define ZILLIANS_LANGUAGE_TREE_SOURCE_H_

#include "language/tree/ASTNode.h"
#include "language/tree/basic/Identifier.h"
#include "language/tree/declaration/Declaration.h"
#include "language/tree/module/Import.h"
#include "language/tree/module/Internal.h"

namespace zillians { namespace language { namespace tree {

struct Source : public ASTNode
{
    friend class boost::serialization::access;

    AST_NODE_HIERARCHY(Source, ASTNode);

private:
    Source();

public:
    explicit Source(const std::string& filename, bool is_imported = false);

    bool isImported() const;

    void addImport(Import* import);

    void addDeclare(Declaration* declare);

    virtual bool isEqualImpl(const ASTNode& rhs) const;

    virtual bool replaceUseWith(const ASTNode& from, const ASTNode& to, bool update_parent = true);

    virtual Source* clone() const;

    virtual std::wostream& toSource(std::wostream& output, unsigned indent/* = 0*/) const override; using BaseNode::toSource;

    template<typename Archive>
    void serialize(Archive& ar, const unsigned int)
    {
        AST_NODE_SERIALIZE(
            ar,
            (is_imported)
            (filename   )
            (imports    )
            (declares   )
        );
    }

    bool is_imported;
    std::string filename;
    std::vector<Import*> imports;
    std::vector<Declaration*> declares;

};

} } }

#endif /* ZILLIANS_LANGUAGE_TREE_SOURCE_H_ */
