/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_TREE_CONFIG_H_
#define ZILLIANS_LANGUAGE_TREE_CONFIG_H_

#include <iosfwd>

#include "language/tree/ASTNode.h"

namespace zillians { namespace language { namespace tree {

struct Config : public ASTNode
{
    AST_NODE_HIERARCHY(Config, ASTNode);

    virtual bool isEqualImpl(const ASTNode& rhs) const override;
    virtual bool replaceUseWith(const ASTNode& from, const ASTNode& to, bool update_parent = true) override;

    virtual Config* clone() const;

    virtual std::wostream& toSource(std::wostream& output, unsigned indent = 0) const;

    template<typename Archive>
    void serialize(Archive& ar, const unsigned int version)
    {
        AST_NODE_SERIALIZE(
            ar,
            (emit_debug_info)
            (opt_level      )
        );
    }

    bool     emit_debug_info = false;
    unsigned opt_level       = 0;
};

} } }

#endif /* ZILLIANS_LANGUAGE_TREE_CONFIG_H_ */
