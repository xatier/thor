/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_TREE_PACKAGE_H_
#define ZILLIANS_LANGUAGE_TREE_PACKAGE_H_

#include "language/resolver/SymbolTable.h"
#include "language/tree/ASTNode.h"
#include "language/tree/UniqueName.h"
#include "language/tree/basic/Annotations.h"
#include "language/tree/basic/Identifier.h"
#include "language/tree/module/Internal.h"
#include "language/tree/module/Source.h"
#include "utility/Foreach.h"

namespace zillians { namespace language { namespace tree {

/**
 * Package is used to represent the hierarchical structure of a program.
 * Every ASTNode, except Package and Program, must be contained by a Package.
 */
struct Package : public Annotatable
{
    friend class boost::serialization::access;

    AST_NODE_HIERARCHY(Package, Annotatable);

private:
    Package();

public:
    explicit Package(SimpleIdentifier* _id);

    bool hasChild() const;
    bool hasSource() const;

    const unique_name_t& getUniqueName() const noexcept;

    Package* findPackage(const std::wstring& name);

    void addPackage(Package* package);

    void addSource(Source* source);

    bool isRoot() const;
    Package* getRoot();

    void markImported(bool is_imported);

    virtual bool isEqualImpl(const ASTNode& rhs) const;

    virtual bool replaceUseWith(const ASTNode& from, const ASTNode& to, bool update_parent = true);

    virtual Package* clone() const;

    virtual std::wostream& toSource(std::wostream& output, unsigned indent/* = 0*/) const override; using BaseNode::toSource;

    bool merge(Package& rhs);

    template<typename Archive>
    void serialize(Archive& ar, const unsigned int version)
    {
        AST_NODE_SERIALIZE(
            ar,
            (id         )
            (unique_name)
            (children   )
            (sources    )
        );
    }

    SimpleIdentifier* id;
    unique_name_t unique_name;
    std::vector<Package*> children;
    std::vector<Source*> sources;

    SymbolTable symbol_table;

    void setImportStatus(const SymbolTable::ImportStatus::value tag);
    SymbolTable::ImportStatus::value getImportStatus() const;
};

} } }

#endif /* ZILLIANS_LANGUAGE_TREE_PACKAGE_H_ */
