/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_TREE_VISITOR_GENERAL_NODEINFOVISITOR_H_
#define ZILLIANS_LANGUAGE_TREE_VISITOR_GENERAL_NODEINFOVISITOR_H_

#include "core/Visitor.h"
#include "language/tree/visitor/GenericDoubleVisitor.h"

namespace zillians { namespace language { namespace tree { namespace visitor {

struct NodeInfoVisitor : Visitor<ASTNode, void, VisitorImplementation::recursive_dfs>
{
    CREATE_INVOKER(infoInvoker, info);

    NodeInfoVisitor(int32 max_depth = 20) : current_depth(0), max_depth(max_depth)
    {
        REGISTER_ALL_VISITABLE_ASTNODE(infoInvoker);
    }

    void info(ASTNode& node)
    {
        if(node.parent) tryVisit(*node.parent);
    }

    void info(Identifier& node)
    {
        if(node.parent) tryVisit(*node.parent);

        if(stream.str().length() > 0)
            stream << L".";

        stream << node.toString();
    }

    void info(Import& node)
    {
        if(node.parent) tryVisit(*node.parent);

        if(stream.str().length() > 0)
            stream << L".";

        if(node.alias)
        {
            if(node.alias->isEmpty())
            {
                stream << "[alias: . => " << node.ns->toString() << "]";
            }
            else
            {
                stream << "[alias: " << node.alias->toString() << " => " << node.ns->toString() << "]";
            }
        }
        else
        {
            stream << "[import: " << node.ns->toString() << "]";
        }
    }

    void info(Package& node)
    {
        if(node.parent) tryVisit(*node.parent);

        if(stream.str().length() > 0)
            stream << L".";

        if(node.isRoot())
            stream << L"[root_package]";
        else
            stream << node.id->toString();
    }

    void info(NormalBlock& node)
    {
        if(node.parent) tryVisit(*node.parent);

        if(stream.str().length() > 0)
            stream << L".";

        stream << L"[block]";
    }

    void info(AsyncBlock& node)
    {
        if(node.parent) tryVisit(*node.parent);

        if(stream.str().length() > 0)
            stream << L".";

        stream << L"[async_blocl]";
    }

    void info(AtomicBlock& node)
    {
        if(node.parent) tryVisit(*node.parent);

        if(stream.str().length() > 0)
            stream << L".";

        stream << L"[atomic_block]";
    }

    void info(LockBlock& node)
    {
        if(node.parent) tryVisit(*node.parent);

        if(stream.str().length() > 0)
            stream << L".";

        stream << L"[lock_block]";
    }

    void info(PipelineBlock& node)
    {
        if(node.parent) tryVisit(*node.parent);

        if(stream.str().length() > 0)
            stream << L".";

        stream << L"[pipeline_blocl]";
    }

    void info(TypeSpecifier& node)
    {
        if(node.parent) tryVisit(*node.parent);

        if(stream.str().length() > 0)
            stream << L".";

        stream << node.toString();
    }

    void info(ClassDecl& node)
    {
        if(node.parent) tryVisit(*node.parent);

        if(stream.str().length() > 0)
            stream << L".";

        stream << node.name->toString();
    }

    void info(EnumDecl& node)
    {
        if(node.parent) tryVisit(*node.parent);

        if(stream.str().length() > 0)
            stream << L".";

        stream << node.name->toString();
    }

    void info(FunctionDecl& node)
    {
        if(node.parent) tryVisit(*node.parent);

        if(stream.str().length() > 0)
            stream << L".";

        stream << node.name->toString();
    }

    void info(TypedefDecl& node)
    {
        if(node.parent) tryVisit(*node.parent);

        if(stream.str().length() > 0)
            stream << L".";

        stream << node.name->toString();
    }

    void info(TypenameDecl& node)
    {
        if(node.parent) tryVisit(*node.parent);

        if(stream.str().length() > 0)
            stream << L".";

        stream << node.name->toString();
    }

    void info(Statement& node)
    {
        if(node.parent) tryVisit(*node.parent);

        if(stream.str().length() > 0)
            stream << L".";

        stream << L"[stmt]";
    }

    void info(BranchStmt& node)
    {
        if(node.parent) tryVisit(*node.parent);

        if(stream.str().length() > 0)
            stream << L".";

        stream << L"[branch_stmt]";
    }

    void info(DeclarativeStmt& node)
    {
        if(node.parent) tryVisit(*node.parent);

        if(stream.str().length() > 0)
            stream << L".";

        stream << L"[decl_stmt]";
    }

    void info(ExpressionStmt& node)
    {
        if(node.parent) tryVisit(*node.parent);

        if(stream.str().length() > 0)
            stream << L".";

        stream << L"[expr_stmt]";
    }

    void info(ForStmt& node)
    {
        if(node.parent) tryVisit(*node.parent);

        if(stream.str().length() > 0)
            stream << L".";

        stream << L"[for_stmt]";
    }

    void info(ForeachStmt& node)
    {
        if(node.parent) tryVisit(*node.parent);

        if(stream.str().length() > 0)
            stream << L".";

        stream << L"[foreach_stmt]";
    }

    void info(WhileStmt& node)
    {
        if(node.parent) tryVisit(*node.parent);

        if(stream.str().length() > 0)
            stream << L".";

        stream << L"[while_stmt]";
    }

    void info(IfElseStmt& node)
    {
        if(node.parent) tryVisit(*node.parent);

        if(stream.str().length() > 0)
            stream << L".";

        stream << L"[ifelse_stmt]";
    }

    void info(SwitchStmt& node)
    {
        if(node.parent) tryVisit(*node.parent);

        if(stream.str().length() > 0)
            stream << L".";

        stream << L"[switch_stmt]";
    }

    void info(Expression& node)
    {
        if(node.parent) tryVisit(*node.parent);

        if(stream.str().length() > 0)
            stream << L".";

        stream << L"[expr]";
    }

    void info(UnaryExpr& node)
    {
        if(node.parent) tryVisit(*node.parent);

        if(stream.str().length() > 0)
            stream << L".";

        stream << L"[unary_expr]";
    }

    void info(BinaryExpr& node)
    {
        if(node.parent) tryVisit(*node.parent);

        if(stream.str().length() > 0)
            stream << L".";

        stream << L"[binary_expr]";
    }

    void info(TernaryExpr& node)
    {
        if(node.parent) tryVisit(*node.parent);

        if(stream.str().length() > 0)
            stream << L".";

        stream << L"[ternary_expr]";
    }

    void info(CallExpr& node)
    {
        if(node.parent) tryVisit(*node.parent);

        if(stream.str().length() > 0)
            stream << L".";

        stream << L"[call_expr]";
    }

    void info(ArrayExpr& node)
    {
        if(node.parent) tryVisit(*node.parent);

        if(stream.str().length() > 0)
            stream << L".";

        stream << L"[vector_expr]";
    }

    void info(CastExpr& node)
    {
        if(node.parent) tryVisit(*node.parent);

        if(stream.str().length() > 0)
            stream << L".";

        stream << L"[cast_expr]";
    }

    void info(IdExpr& node)
    {
        if(node.parent) tryVisit(*node.parent);

        if(stream.str().length() > 0)
            stream << L".";

        stream << L"[id_expr]";
    }

    void info(LambdaExpr& node)
    {
        if(node.parent) tryVisit(*node.parent);

        if(stream.str().length() > 0)
            stream << L".";

        stream << L"[lambda_expr]";
    }

    void info(MemberExpr& node)
    {
        if(node.parent) tryVisit(*node.parent);

        if(stream.str().length() > 0)
            stream << L".";

        stream << L"[member_expr]";
    }

    void info(BlockExpr& node)
    {
        if(node.parent) tryVisit(*node.parent);

        if(stream.str().length() > 0)
            stream << L".";

        stream << L"[block_expr]";
    }

    void reset()
    {
        stream.str(L"");
    }

    void tryVisit(ASTNode& node)
    {
        ++current_depth;
        if(current_depth < max_depth)
            visit(node);
        --current_depth;
    }

    int32 current_depth;
    int32 max_depth;
    std::wstringstream stream;
};

} } } }

#endif /* ZILLIANS_LANGUAGE_TREE_VISITOR_GENERAL_NODEINFOVISITOR_H_ */
