/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_TREE_VISITOR_GENERAL_NAMEMANGLINGVISITOR_H_
#define ZILLIANS_LANGUAGE_TREE_VISITOR_GENERAL_NAMEMANGLINGVISITOR_H_

#include "core/Visitor.h"

#include "language/tree/ASTNodeFwd.h"

namespace zillians { namespace language { namespace tree { namespace visitor {

/**
 * NameManglingVisitor implements a small set of Itanium C++ ABI name mangling rules
 *
 * @see http://sourcery.mentor.com/public/cxx-abi/abi.html#mangling
 */
struct NameManglingVisitor : Visitor<ASTNode, void, VisitorImplementation::recursive_dfs>
{
    /**
     * TemplatedIdentifierContext is attached to TypenameDecl and used to store the template index and substitution index for template parameters
     */
    struct TemplatedIdentifierContext
    {
        TemplatedIdentifierContext() : template_index(0), substitution_index(-1) { }
        int template_index;
        int substitution_index;
    };

    /**
     * ClassManglingContext is attached to ClassDecl and used to store the substitution index for pointer to ClassDecl type
     */
    struct ClassManglingContext
    {
        ClassManglingContext() : pointer_type_mangling_index(-1) { }
        int pointer_type_mangling_index;
    };

    CREATE_INVOKER(mangleInvoker, mangle);

    NameManglingVisitor();

    void mangle(ASTNode& node);
    void mangle(SimpleIdentifier& node);
    void mangle(TemplatedIdentifier& node);
    void mangle(NestedIdentifier& node);
    void mangle(TypeSpecifier& node);
    void mangle(FunctionSpecifier& node);
    void mangle(MultiSpecifier& node);
    void mangle(NamedSpecifier& node);
    void mangle(PrimitiveSpecifier& node);
    void mangle(Package& node);
    void mangle(Source& node);
    void mangle(EnumDecl& node);
    void mangle(TypenameDecl& node);
    void mangle(TypedefDecl& node);
    void mangle(ClassDecl& node);
    void mangle(FunctionDecl& node);
    void mangle(VariableDecl& node);

public:
    std::string encode(const std::wstring ucs4);

public:
    void reset();
    void mangleClassAsScope(bool flag);

private:
    static bool isEqualPackage(Package* lhs, ASTNode* table_item);
    int findSubstitution(ASTNode* component);
    int findSubstitution(ClassDecl* component, bool fully_match);
    int findSubstitution(TemplatedIdentifier* component);
    int addSubstitution(ASTNode* component);
    std::string getSubstitution(const std::string& catalog, int index);

public:
    std::stringstream stream;

private:
    std::vector<ASTNode*> substitution_table;
    std::vector<std::function<void()>> cleanups;

    int current_nested_depth;
    int max_nested_depth;

    std::stack<int> current_nested_depth_stack;
    std::stack<int> max_nested_depth_stack;

    bool class_as_scope_mode;
    bool mangle_identifier_name;
};

} } } }

#endif /* ZILLIANS_LANGUAGE_TREE_VISITOR_GENERAL_NAMEMANGLINGVISITOR_H_ */
