/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_TREE_VISITOR_GENERICDOUBLEVISITOR_H_
#define ZILLIANS_LANGUAGE_TREE_VISITOR_GENERICDOUBLEVISITOR_H_

#include <boost/preprocessor/seq/enum.hpp>
#include <boost/preprocessor/seq/for_each.hpp>

#include "core/Visitor.h"

#include "language/tree/ASTNodeFwd.h"

#define REGISTER_ALL_VISITABLE_ASTNODE(invoker) \
        REGISTER_VISITABLE(invoker, BOOST_PP_SEQ_ENUM(THOR_SCRIPT_AST_QUALIFIED_NODE_NAMES))

namespace zillians { namespace language { namespace tree { namespace visitor {

// TODO change visitor implementation into a templated parameter (due to lack of support in GCC 4.4)
struct GenericDoubleVisitor : Visitor<ASTNode, void, VisitorImplementation::recursive_dfs>
{
    struct ApplyVisitor : Visitor<ASTNode, void, VisitorImplementation::recursive_dfs>
    {
        Visitor<ASTNode, void, VisitorImplementation::recursive_dfs>* user_visitor;

        CREATE_INVOKER(applyInvoker, apply);

        ApplyVisitor();

#define GENERIC_DOUBLE_VISITOR_APPLY_DECL(r, _, elem)  void apply(elem & node);

BOOST_PP_SEQ_FOR_EACH(GENERIC_DOUBLE_VISITOR_APPLY_DECL, _, THOR_SCRIPT_AST_NODE_NAMES)

#undef  GENERIC_DOUBLE_VISITOR_APPLY_DECL
    };

    ApplyVisitor revisitor;

    GenericDoubleVisitor();

    void revisit(ASTNode& node);
    void terminateRevisit();
};

} } } }


#endif /* ZILLIANS_LANGUAGE_TREE_VISITOR_GENERICDOUBLEVISITOR_H_ */
