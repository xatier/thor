/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_ACTION_DECLARATION_DECLARATIONACTIONS_H_
#define ZILLIANS_LANGUAGE_ACTION_DECLARATION_DECLARATIONACTIONS_H_

#include <utility>
#include <vector>

#include "language/action/detail/SemanticActionsDetail.h"
#include "language/action/basic/BasicActions.h"

namespace zillians { namespace language { namespace action {

struct global_decl
{
    DEFINE_ATTRIBUTES(tree::Declaration*)
    DEFINE_LOCALS()

    BEGIN_ACTION(init)
    {
#ifdef DEBUG
        printf("global_decl param(0) type = %s\n", typeid(_param_t(0)).name());
        printf("global_decl param(1) type = %s\n", typeid(_param_t(1)).name());
#endif
        _result = _param(1);
        if(_result && _param(0).is_initialized())
            _result->setAnnotations(*_param(0));
    }
    END_ACTION
};

struct variable_decl_stem
{
    DEFINE_ATTRIBUTES(tree::VariableDecl*)
    DEFINE_LOCALS(LOCATION_TYPE)

    BEGIN_ACTION(init)
    {
#ifdef DEBUG
        printf("variable_decl_stem param(0) type = %s\n", typeid(_param_t(0)).name());
        printf("variable_decl_stem param(1) type = %s\n", typeid(_param_t(1)).name());
#endif
        Identifier*                            ident       = _param(0);
        TypeSpecifier*                         type        = _param(1).is_initialized() ? *_param(1) : NULL;
        Expression*                            initializer = nullptr;
        Declaration::VisibilitySpecifier::type visibility  = Declaration::VisibilitySpecifier::PUBLIC;
        bool                                   is_member   = false;
        bool                                   is_static   = false;
        bool                                   is_const    = false;
        BIND_CACHED_LOCATION(_result = new VariableDecl(ident, type, is_member, is_static, is_const, visibility, initializer));
    }
    END_ACTION
};

struct param_decl_with_init
{
    DEFINE_ATTRIBUTES(tree::VariableDecl*)
    DEFINE_LOCALS()

    BEGIN_ACTION(init)
    {
#ifdef DEBUG
        printf("param_decl_with_init param(0) type = %s\n", typeid(_param_t(0)).name());
        printf("param_decl_with_init param(1) type = %s\n", typeid(_param_t(1)).name());
#endif
        _result = _param(1);
        if(_result && _param(0).is_initialized())
            _result->setAnnotations(*_param(0));
        if(_result && _param(2).is_initialized())
            _result->setInitializer(*_param(2));
    }
    END_ACTION
};

struct variable_decl_with_init
{
    DEFINE_ATTRIBUTES(tree::VariableDecl*)
    DEFINE_LOCALS()

    BEGIN_ACTION(init)
    {
#ifdef DEBUG
        printf("variable_decl_with_init param(0) type = %s\n", typeid(_param_t(0)).name());
        printf("variable_decl_with_init param(1) type = %s\n", typeid(_param_t(1)).name());
#endif
        _result = _param(0);
        if(_result && _param(1).is_initialized())
            _result->setInitializer(*_param(1));
    }
    END_ACTION
};

struct variable_decl_stmt
{
    DEFINE_ATTRIBUTES(tree::Declaration*)
    DEFINE_LOCALS()
};

struct const_decl
{
    DEFINE_ATTRIBUTES(tree::Declaration*)
    DEFINE_LOCALS()

    BEGIN_ACTION(init)
    {
#ifdef DEBUG
        printf("const_decl param(0) type = %s\n", typeid(_param_t(0)).name());
        printf("const_decl param(1) type = %s\n", typeid(_param_t(1)).name());
#endif
        _result = _param(0);
        if(_result)
        {
            cast<VariableDecl>(_result)->is_const = true;
            cast<VariableDecl>(_result)->setInitializer(_param(1));
        }
    }
    END_ACTION
};

struct function_decl
{
    DEFINE_ATTRIBUTES(tree::Declaration*)
    DEFINE_LOCALS(LOCATION_TYPE)

    BEGIN_ACTION(init)
    {
#ifdef DEBUG
        printf("function_decl param(0) type = %s\n", typeid(_param_t(0)).name());
        printf("function_decl param(1) type = %s\n", typeid(_param_t(1)).name());
        printf("function_decl param(2) type = %s\n", typeid(_param_t(2)).name());
        printf("function_decl param(3) type = %s\n", typeid(_param_t(3)).name());
        printf("function_decl param(4) type = %s\n", typeid(_param_t(4)).name());
#endif

        std::vector<VariableDecl*>*            parameters  = _param(2).is_initialized() ? &(*_param(2)) : nullptr;
        TypeSpecifier*                         type        = _param(3).is_initialized() ? *_param(3) : nullptr;
        Block*                                 block       = _param(4).is_initialized() ? *_param(4) : nullptr;
        Declaration::VisibilitySpecifier::type visibility  = Declaration::VisibilitySpecifier::PUBLIC;
        bool                                   is_task     = _param(0);
        bool                                   is_member   = false;
        bool                                   is_static   = false;
        bool                                   is_virtual  = false;
        bool                                   is_override = false;

        auto* identifier = _param(1);
        auto* function = new FunctionDecl(
            identifier, type, is_member, is_static, is_virtual, is_override, visibility, block
        );

        if(is_task)
            function->is_task = true;

        if(parameters)
        {
            for(auto& param : *parameters)
                function->appendParameter(param);
        }

        BIND_CACHED_LOCATION(_result = function);
    }
    END_ACTION
};

struct member_function_decl
{
    DEFINE_ATTRIBUTES(tree::Declaration*)
    DEFINE_LOCALS(LOCATION_TYPE)

    BEGIN_ACTION(init)
    {
#ifdef DEBUG
        printf("member_function_decl param(0) type = %s\n", typeid(_param_t(0)).name());
        printf("member_function_decl param(1) type = %s\n", typeid(_param_t(1)).name());
#endif
        _result = _param(1);
        if(_param(0).is_initialized())
        {
            switch(*_param(0))
            {
            case 0:  cast<FunctionDecl>(_result)->is_virtual = true; break;
            case 1:  cast<FunctionDecl>(_result)->is_override = true; break;
            default: UNREACHABLE_CODE(); break;
            }
        }
    }
    END_ACTION
};

struct typedef_decl
{
    DEFINE_ATTRIBUTES(tree::Declaration*)
    DEFINE_LOCALS(LOCATION_TYPE)

    BEGIN_ACTION(init)
    {
#ifdef DEBUG
        printf("typedef_decl param(0) type = %s\n", typeid(_param_t(0)).name());
        printf("typedef_decl param(1) type = %s\n", typeid(_param_t(1)).name());
#endif
        BIND_CACHED_LOCATION(_result = new TypedefDecl(_param(0), _param(1)));
    }
    END_ACTION
};

struct class_decl
{
    DEFINE_ATTRIBUTES(tree::Declaration*)
    DEFINE_LOCALS(LOCATION_TYPE)

    BEGIN_ACTION(init)
    {
#ifdef DEBUG
        printf("class_decl param(0) type = %s\n", typeid(_param_t(0)).name());
        printf("class_decl param(1) type = %s\n", typeid(_param_t(1)).name());
        printf("class_decl param(2) type = %s\n", typeid(_param_t(2)).name());
        printf("class_decl param(3) type = %s\n", typeid(_param_t(3)).name());
#endif
        Identifier* ident = _param(0);
        Identifier* extends_from_ident = _param(1).is_initialized() ? *_param(1) : NULL;
        NamedSpecifier* extends_from = NULL;
        if(extends_from_ident)
            BIND_CACHED_LOCATION(extends_from = new NamedSpecifier(extends_from_ident));
        BIND_CACHED_LOCATION(_result = new ClassDecl(ident, false));
        if(extends_from)
        {
            cast<ClassDecl>(_result)->setBase(extends_from);
        }

        if(_param(2).is_initialized())
        {
            for(auto& i : *_param(2))
            {
                NamedSpecifier* type = new NamedSpecifier(i); BIND_CACHED_LOCATION(type);
                cast<ClassDecl>(_result)->addInterface(type);
            }
        }

        if(_param(3).is_initialized())
        {
            for(auto& i : *_param(3))
            {
                if(auto* variable = cast<VariableDecl>(i))
                {
                    cast<ClassDecl>(_result)->addVariable(variable);
                    variable->is_member = true;
                }
                else if(auto* function  = cast<FunctionDecl>(i))
                {
                    cast<ClassDecl>(_result)->addFunction(function);
                    function->is_member = true;
                }
            }
        }
    }
    END_ACTION
};

struct class_member_decl
{
    DEFINE_ATTRIBUTES(tree::Declaration*)
    DEFINE_LOCALS()

    BEGIN_ACTION(init)
    {
#ifdef DEBUG
        printf("class_member_decl param(0) type = %s\n", typeid(_param_t(0)).name());
        printf("class_member_decl param(1) type = %s\n", typeid(_param_t(1)).name());
        printf("class_member_decl param(2) type = %s\n", typeid(_param_t(2)).name());
        printf("class_member_decl param(3) type = %s\n", typeid(_param_t(3)).name());
#endif
        Annotations*                           annotation_list = _param(0).is_initialized() ? *_param(0) : nullptr;
        Declaration::VisibilitySpecifier::type visibility      = _param(1).is_initialized() ? *_param(1) : Declaration::VisibilitySpecifier::DEFAULT;
        bool                                   is_static       = _param(2).is_initialized();
        _result = _param(3);
        if(_result)
        {
            _result->setAnnotations(annotation_list);
            if(auto* variable = cast<VariableDecl>(_result))
            {
                variable->visibility = visibility;
                variable->is_static  = is_static;
            }
            else if(auto* function = cast<FunctionDecl>(_result))
            {
                function->visibility = visibility;
                function->is_static  = is_static;
            }
        }
    }
    END_ACTION
};

struct interface_decl
{
    DEFINE_ATTRIBUTES(tree::Declaration*)
    DEFINE_LOCALS(LOCATION_TYPE)

    BEGIN_ACTION(init)
    {
#ifdef DEBUG
        printf("interface_decl param(0) type = %s\n", typeid(_param_t(0)).name());
        printf("interface_decl param(1) type = %s\n", typeid(_param_t(1)).name());
        printf("interface_decl param(2) type = %s\n", typeid(_param_t(2)).name());
#endif
        BIND_CACHED_LOCATION(_result = new ClassDecl(_param(0), true));
        if(_param(1).is_initialized())
        {
            for(auto& i : *_param(1))
            {
                NamedSpecifier* type = new NamedSpecifier(i); BIND_CACHED_LOCATION(type);
                cast<ClassDecl>(_result)->addInterface(type);
            }
        }
        for(auto& i : _param(2))
        {
            auto* function = cast<FunctionDecl>(i);

            cast<ClassDecl>(_result)->addFunction(function);
            function->is_member = true;
        }
    }
    END_ACTION
};

struct interface_member_function_decl
{
    DEFINE_ATTRIBUTES(tree::Declaration*)
    DEFINE_LOCALS(LOCATION_TYPE)

    BEGIN_ACTION(init)
    {
#ifdef DEBUG
        printf("interface_member_function_decl param(0) type = %s\n", typeid(_param_t(0)).name());
        printf("interface_member_function_decl param(1) type = %s\n", typeid(_param_t(1)).name());
        printf("interface_member_function_decl param(2) type = %s\n", typeid(_param_t(2)).name());
        printf("interface_member_function_decl param(3) type = %s\n", typeid(_param_t(3)).name());
        printf("interface_member_function_decl param(4) type = %s\n", typeid(_param_t(4)).name());
        printf("interface_member_function_decl param(5) type = %s\n", typeid(_param_t(5)).name());
#endif
        Annotations*                           annotation_list = _param(0).is_initialized() ? *_param(0) : NULL;
        Declaration::VisibilitySpecifier::type visibility      = _param(1).is_initialized() ? *_param(1) : Declaration::VisibilitySpecifier::DEFAULT;
        std::vector<VariableDecl*>*            parameters      = _param(4).is_initialized() ? &(*_param(4)) : NULL;
        bool                                   is_member       = true;
        bool                                   is_static       = false;
        bool                                   is_virtual      = true;
        bool                                   is_override     = false;
        bool                                   is_task         = _param(2);

        auto* identifier = _param(3);
        auto* member_function = new FunctionDecl(
            identifier, _param(5), is_member, is_static, is_virtual, is_override, visibility
        );
        member_function->setAnnotations(annotation_list);

        if(is_task)
            member_function->is_task = is_task;

        if(parameters)
        {
            for(auto& param : *parameters)
                member_function->appendParameter(param);
        }

        BIND_CACHED_LOCATION(_result = member_function);
    }
    END_ACTION
};

struct enum_decl
{
    DEFINE_ATTRIBUTES(tree::Declaration*)
    DEFINE_LOCALS(LOCATION_TYPE)

    BEGIN_ACTION(init)
    {
#ifdef DEBUG
        printf("enum_decl param(0) type = %s\n", typeid(_param_t(0)).name());
        printf("enum_decl param(1) type = %s\n", typeid(_param_t(1)).name());
#endif
        BIND_CACHED_LOCATION(_result = new EnumDecl(_param(0)));
        for(auto& i : _param(1))
        {
            boost::optional<Annotations*>& optional_annos  = boost::fusion::at_c<0>(i);
            boost::optional<Expression*>&  optional_result = boost::fusion::at_c<2>(i);
            Annotations*                   annotations     = optional_annos.is_initialized() ? *optional_annos : NULL;
            SimpleIdentifier*              tag             = boost::fusion::at_c<1>(i);
            Expression*                    value           = optional_result.is_initialized() ? *optional_result : NULL;
            VariableDecl* decl = new VariableDecl(tag, new PrimitiveSpecifier(PrimitiveKind::INT32_TYPE), true, true, true, Declaration::VisibilitySpecifier::DEFAULT, value);
            if(annotations != NULL)
            {
                decl->setAnnotations(annotations);
            }
            BIND_CACHED_LOCATION(decl);
            cast<EnumDecl>(_result)->addEnumeration(decl);
        }
    }
    END_ACTION
};

struct typename_decl
{
    DEFINE_ATTRIBUTES(tree::TypenameDecl*)
    DEFINE_LOCALS(LOCATION_TYPE)

    BEGIN_ACTION(init)
    {
         Annotations*      annotations      = NULL;
         Identifier*       ident            = _param(1);
         TypeSpecifier*    default_type     = NULL;
         TypeSpecifier*    specialized_type = NULL;

         if(_param(0).is_initialized())
         {
             annotations = *_param(0);
         }
         if(_param(2).is_initialized())
         {
             switch( std::get<0>(**_param(2)) )
             {
             case 0u: default_type = std::get<1>(**_param(2)); break;
             case 1u: specialized_type = std::get<1>(**_param(2)); break;
             }
             delete *_param(2);
         } 

         TypenameDecl* typename_decl = new TypenameDecl(ident, specialized_type, default_type);
         typename_decl->setAnnotations(annotations);
         BIND_CACHED_LOCATION(_result = typename_decl);
    }
    END_ACTION
};

} } }

#endif /* ZILLIANS_LANGUAGE_ACTION_DECLARATION_DECLARATIONACTIONS_H_ */
