/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_ACTION_BASIC_IDENTIFIERACTIONS_H_
#define ZILLIANS_LANGUAGE_ACTION_BASIC_IDENTIFIERACTIONS_H_

#include "language/action/detail/SemanticActionsDetail.h"

namespace zillians { namespace language { namespace action {

struct identifier
{
    DEFINE_ATTRIBUTES(tree::SimpleIdentifier*)
    DEFINE_LOCALS(LOCATION_TYPE)

    BEGIN_ACTION(init)
    {
#ifdef DEBUG
        printf("identifier param(0) type = %s\n", typeid(_param_t(0)).name());
#endif
        BIND_CACHED_LOCATION(_result = new SimpleIdentifier(_param(0)));
    }
    END_ACTION
};

struct new_or_delete_id
{
    DEFINE_ATTRIBUTES(tree::Identifier*)
    DEFINE_LOCALS(LOCATION_TYPE)

    BEGIN_ACTION(init)
    {
#ifdef DEBUG
        printf("identifier param(0) type = %s\n", typeid(_param_t(0)).name());
#endif

        switch(_param(0).which())
        {
        case 0:
            {
                using fusion_vec_t = boost::fusion::vector2<int, boost::optional<std::vector<TypenameDecl*> > >;
                auto& typename_decls_optional = boost::fusion::at_c<1>(boost::get<fusion_vec_t>(_param(0)));
                if (!typename_decls_optional.is_initialized())
                {
                    BIND_CACHED_LOCATION(_result = new SimpleIdentifier(L"new"));
                    return;
                }

                auto* templated_id = new TemplatedIdentifier(
                    TemplatedIdentifier::Usage::FORMAL_PARAMETER,
                    new SimpleIdentifier(L"new")
                );
                BOOST_FOREACH(auto i, *typename_decls_optional)
                {
                    templated_id->append(i);
                }

                BIND_CACHED_LOCATION(_result = templated_id);
            }
            return;
        case 1:
            BIND_CACHED_LOCATION(_result = new SimpleIdentifier(L"delete"));
            return;
        default:
            BOOST_ASSERT(false && "should not reach here");
        }
    }
    END_ACTION
};

struct nested_simple_name
{
    DEFINE_ATTRIBUTES(tree::Identifier*)
    DEFINE_LOCALS(LOCATION_TYPE)

    BEGIN_ACTION(init)
    {
#ifdef DEBUG
        printf("nested_simple_name param(0) type = %s\n", typeid(_param_t(0)).name());
        printf("nested_simple_name param(1) type = %s\n", typeid(_param_t(1)).name());
#endif
        if(_param(1).size() == 0)
            _result = _param(0);
        else
        {
            BIND_CACHED_LOCATION(_result = new NestedIdentifier());
            cast<NestedIdentifier>(_result)->appendIdentifier(_param(0));
            for(auto& i :_param(1))
                cast<NestedIdentifier>(_result)->appendIdentifier(i);
        }
    }
    END_ACTION
};

struct nested_name
{
    DEFINE_ATTRIBUTES(tree::Identifier*)
    DEFINE_LOCALS(LOCATION_TYPE)

    BEGIN_ACTION(init)
    {
#ifdef DEBUG
        printf("nested_name param(0) type = %s\n", typeid(_param_t(0)).name());
        printf("nested_name param(1) type = %s\n", typeid(_param_t(1)).name());
#endif
        if(_param(1).size() == 0)
            _result = _param(0);
        else
        {
            BIND_CACHED_LOCATION(_result = new NestedIdentifier());
            cast<NestedIdentifier>(_result)->appendIdentifier(_param(0));
            for(auto& i : _param(1))
                cast<NestedIdentifier>(_result)->appendIdentifier(i);
        }
    }
    END_ACTION
};

struct id_expression
{
    DEFINE_ATTRIBUTES(tree::Identifier*)
    DEFINE_LOCALS(LOCATION_TYPE)

    BEGIN_ACTION(init)
    {
#ifdef DEBUG
        printf("id_expression param(0) type = %s\n", typeid(_param_t(0)).name());
#endif
        switch ( _param(0).which() )
        {
        case 1:
            _result = boost::get<SimpleIdentifier*>( _param(0) );
            break;
        case 0:
            _result = boost::get<Identifier*>( _param(0) );
            break;
        }
    }
    END_ACTION
};

struct simple_template_id
{
    DEFINE_ATTRIBUTES(tree::Identifier*)
    DEFINE_LOCALS(LOCATION_TYPE)

    BEGIN_ACTION(init)
    {
#ifdef DEBUG
        printf("simple_template_id param(0) type = %s\n", typeid(_param_t(0)).name());
        printf("simple_template_id param(1) type = %s\n", typeid(_param_t(1)).name());
#endif
        BIND_CACHED_LOCATION(
            _result = new TemplatedIdentifier(
                          TemplatedIdentifier::Usage::ACTUAL_ARGUMENT,
                          _param(0)
                      )
        );
        for(auto& i : _param(1))
        {
            SimpleIdentifier* sid = new SimpleIdentifier(L"_");
            BIND_CACHED_LOCATION(sid);

            TypenameDecl* typename_decl = new TypenameDecl(sid, i, NULL);
            BIND_CACHED_LOCATION(typename_decl);

            cast<TemplatedIdentifier>(_result)->append(typename_decl);
        }
    }
    END_ACTION
};

struct template_arg_identifier
{
    DEFINE_ATTRIBUTES(tree::Identifier*)
    DEFINE_LOCALS(LOCATION_TYPE)

    BEGIN_ACTION(init)
    {
#ifdef DEBUG
        printf("template_arg_identifier param(0) type = %s\n", typeid(_param_t(0)).name());
        printf("template_arg_identifier param(1) type = %s\n", typeid(_param_t(1)).name());
#endif
        if(_param(1).is_initialized())
        {
            BIND_CACHED_LOCATION(
                _result = new TemplatedIdentifier(
                              TemplatedIdentifier::Usage::ACTUAL_ARGUMENT,
                              _param(0)
                          )
            );
            for(auto& i : *_param(1))
            {
                SimpleIdentifier* sid = new SimpleIdentifier(L"_");
                BIND_CACHED_LOCATION(sid);

                TypenameDecl* typename_decl = new TypenameDecl(sid, i, NULL);
                BIND_CACHED_LOCATION(typename_decl);

                cast<TemplatedIdentifier>(_result)->append(typename_decl);
            }
        }
        else
            _result = _param(0);
    }
    END_ACTION
};

struct template_param_identifier
{
    DEFINE_ATTRIBUTES(tree::Identifier*)
    DEFINE_LOCALS(LOCATION_TYPE)

    BEGIN_ACTION(init)
    {
#ifdef DEBUG
        printf("template_param_identifier param(0) type = %s\n", typeid(_param_t(0)).name());
        printf("template_param_identifier param(1) type = %s\n", typeid(_param_t(1)).name());
#endif
        if(_param(1).is_initialized())
        {
            BIND_CACHED_LOCATION(
                _result = new TemplatedIdentifier(
                                TemplatedIdentifier::Usage::FORMAL_PARAMETER,
                                _param(0)
                          )
            );
            for(auto& i : *(_param(1)))
            {
                cast<TemplatedIdentifier>(_result)->append(i);
            }
        }
        else
        {
            BIND_CACHED_LOCATION(_result = _param(0));
        }
    }
    END_ACTION
};

struct template_param_list
{
    DEFINE_ATTRIBUTES(std::vector<tree::TypenameDecl*>)
    DEFINE_LOCALS()
};

} } }

#endif /* ZILLIANS_LANGUAGE_ACTION_BASIC_IDENTIFIERACTIONS_H_ */
