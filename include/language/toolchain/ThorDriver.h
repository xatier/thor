/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_THORDRIVER_H_
#define ZILLIANS_LANGUAGE_THORDRIVER_H_

#include <functional>

#include <boost/filesystem.hpp>

#include "utility/Filesystem.h"
#include "language/ThorConfiguration.h"

namespace zillians { namespace language { namespace tree {

struct Tangle;

} } }

namespace zillians { namespace language {

class ThorDriver
{
public:
    int main(int argc, const char** argv);
    int run(int argc, const char** argv);

private:
    typedef std::vector<std::string> CmdOptions;

    int runCommandProject(CmdOptions args);
    int runCommandProjectCreate(CmdOptions args);
    int runCommandBuild(CmdOptions args);
    int runCommandGenerate(CmdOptions args);
    int runCommandGenerateBundle(CmdOptions args);
    int runCommandGenerateDoc(CmdOptions args);
    int runCommandGenerateStub(CmdOptions args);
    int runCommandRun(CmdOptions args);
    int runCommandExtract();

private:
    static CmdOptions::iterator findCmdEnd(CmdOptions& options, const CmdOptions& cmds);
    boost::filesystem::path findSharedLibrary(const std::string& name, const std::string& lpath);
    boost::filesystem::path findLibrary(const std::string& name, const std::string& lpath);

private:
    tree::Tangle* mergeAllTangles();
    int generateStub(const std::vector<std::string>& stubTypes);
    int generateCppClientStub();
    int generateJsClientStub();
    int generateServerStub();

private:
    int build(bool emit_debug_info, unsigned opt_level);
    int unbundle();
    int make(bool emit_debug_info, unsigned opt_level);
    int bundle();
    int jitCodeGen();
    int link();
    int doc();
    int prepareEnvironment();
    int run(bool run_as_server, bool run_as_client, const std::string& entry, const std::vector<std::string>& args, const std::string& domain, int dev_id);

private:
    ThorBuildConfiguration config;
};

} }

#endif /* ZILLIANS_LANGUAGE_THORDRIVER_H_ */
