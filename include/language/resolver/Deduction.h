/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_DEDUCTION_H_
#define ZILLIANS_LANGUAGE_DEDUCTION_H_

#include <map>
#include <memory>
#include <vector>

#include <boost/logic/tribool.hpp>
#include <boost/operators.hpp>
#include <boost/tuple/tuple.hpp>

#include "language/tree/ASTNodeFwd.h"
#include "language/tree/basic/Type.h"

namespace zillians { namespace language { namespace resolution { namespace deduction {

struct Rank : public boost::totally_ordered<Rank>
{
    tree::Type::ConversionRank conv_rank;
    size_t nested_level;

    explicit Rank(tree::Type::ConversionRank conv, size_t level = 0);

    // '<' is better, if a < b, means conversion a is better than conversion b
    bool operator<(const Rank& rhs) const;
    bool operator==(const Rank& rhs) const;
};

typedef std::vector<Rank> RankList;

struct SingleInfo
{
    enum MatchState
    {
        IN_PROGRESS,
        NOT_MATCH  ,
        FULL_MATCH ,
    };

    enum DeductSource
    {
        EXPLICIT,
        IMPLICIT,
        LITERAL , // For "null"
    };

    struct DeductInfo
    {
        tree::Type* type;
        DeductSource by;
        Rank rank;
    };

    SingleInfo(tree::ASTNode* candidate, MatchState state);

    tree::ASTNode* candidate;
    MatchState state;
    RankList ranks;
    std::map<tree::TypenameDecl*, DeductInfo> deducted_types;
};

class Accepter
{
public:
    template<typename MapType>
    explicit Accepter(MapType& deducted_types) : deducted_types(deducted_types) {}

    virtual std::pair<bool, Rank> on_deducted(const std::size_t nested_level, tree::Type& rhs_type, tree::TypenameDecl& rhs_td) = 0;
    virtual std::pair<boost::tribool, Rank> on_complete_types(const std::size_t nested_level, tree::ASTNode& lhs, tree::ASTNode& rhs) = 0;

protected:
    decltype(SingleInfo::deducted_types)& deducted_types;
};

class DeductAccepter : public Accepter
{
public:
    template<typename MapType>
    explicit DeductAccepter(MapType& deducted_types) : Accepter(deducted_types) {}

    virtual std::pair<bool, Rank>           on_deducted(const std::size_t nested_level, tree::Type& rhs_type, tree::TypenameDecl& rhs_td);
    virtual std::pair<boost::tribool, Rank> on_complete_types(const std::size_t nested_level, tree::ASTNode& lhs, tree::ASTNode& rhs);
};

class MatchAccepter : public Accepter
{
public:
    template<typename MapType>
    explicit MatchAccepter(MapType& deducted_types, const std::vector<tree::TypenameDecl*>& lhs_tds, const std::vector<tree::TypenameDecl*>& rhs_tds)
        : Accepter(deducted_types)
        , lhs_tds(lhs_tds)
        , rhs_tds(rhs_tds)
    {}

    virtual std::pair<bool, Rank> on_deducted(const std::size_t nested_level, tree::Type& rhs_type, tree::TypenameDecl& rhs_td);
    virtual std::pair<boost::tribool, Rank> on_complete_types(const std::size_t nested_level, tree::ASTNode& lhs, tree::ASTNode& rhs);

private:
    const std::vector<tree::TypenameDecl*>& lhs_tds;
    const std::vector<tree::TypenameDecl*>& rhs_tds;
};

class TypeFollower
{
public:
    static TypeFollower from_type(tree::Type& type);
    static TypeFollower from_ts(tree::TypeSpecifier& ts);
    static TypeFollower from_ts_with_custom_lookup_node(tree::TypeSpecifier& ts, tree::ASTNode& node);

private:
    explicit TypeFollower();
    explicit TypeFollower(tree::TypeSpecifier& ts, tree::Type& canonical_type);

public:
    bool is_ready() const;

    tree::TypeSpecifier& get_ts() const;
    tree::Type& get_canonical_type() const;

    tree::TemplatedIdentifier* get_ts_tid() const;
    tree::TemplatedIdentifier* get_type_tid() const;

private:
    tree::TypeSpecifier* ts;
    tree::Type* canonical_type;
};

bool is_template_declaration(tree::ASTNode* candidate);
bool is_partial_specialized(tree::Identifier& id);

bool is_more_or_equally_specialized(tree::TypenameDecl& lhs, tree::TypenameDecl& rhs);
bool is_more_or_equally_specialized(tree::TemplatedIdentifier& lhs, tree::TemplatedIdentifier& rhs);
bool is_more_or_equally_specialized(tree::Declaration& lhs, tree::Declaration& rhs);
bool is_more_or_equally_specialized(const SingleInfo& lhs, const SingleInfo& rhs);

bool is_better_or_equally_conversion(const RankList& lhs_ranks, const RankList& rhs_ranks);

boost::tribool is_better_or_equally_matched_for_conversion_rank(const SingleInfo& lhs, const SingleInfo& rhs);
boost::tribool is_better_or_equally_matched_for_deduction_rank(const SingleInfo& lhs, const SingleInfo& rhs);
boost::tribool is_better_or_equally_matched_for_deducted_types(const SingleInfo& lhs, const SingleInfo& rhs);
bool is_better_or_equally_matched(const SingleInfo& lhs, const SingleInfo& rhs);

boost::tribool is_identifier_matched(tree::Identifier& use, tree::Identifier& decl, const bool is_call = false);

bool is_specialization_matched(const tree::TemplatedIdentifier& decl_tid, const decltype(SingleInfo::deducted_types)& deducted_types);
bool is_specialization_matched(const tree::TemplatedIdentifier& decl_tid, const tree::TemplatedIdentifier& use_tid, const decltype(SingleInfo::deducted_types)& deducted_types);

bool add_deducted_type_by_explicit(decltype(SingleInfo::deducted_types)& deducted_types, tree::TypenameDecl* decl, tree::Type* type);
bool add_deducted_type_by_implicit(Accepter& accepter, tree::TypenameDecl* decl, tree::Type* type, Rank& rank, decltype(Rank::nested_level) nested_level);
bool add_deducted_type_by_literal(decltype(SingleInfo::deducted_types)& deducted_types, tree::TypenameDecl* decl, Rank& rank, decltype(Rank::nested_level) nested_level);

tree::TypenameDecl* get_canonical_typename_decl(tree::TypenameDecl* v);
tree::TypenameDecl* get_typename_decl(tree::TypeSpecifier* specifier);

boost::tribool set_conversion_rank(tree::ASTNode* decl_type, tree::ASTNode* use_type, const decltype(Rank::nested_level) nested_level, Accepter& accepter, Rank& rank);

template<typename NodeType>
std::pair<bool, boost::tribool> deduct_recursively_impl(const TypeFollower& use, const TypeFollower& decl, Accepter& accepter, Rank& rank, const decltype(Rank::nested_level) nested_level);

boost::tribool deduct_template_id(const TypeFollower& use, const TypeFollower& decl, Accepter& accepter, Rank& rank, const decltype(Rank::nested_level) nested_level, const tree::Type::ConversionRank success_rank);
boost::tribool deduct_null(tree::TypeSpecifier* decl_param_ts, decltype(SingleInfo::deducted_types)& deducted_types, Rank& rank, const decltype(Rank::nested_level) nested_level);
boost::tribool deduct_recursively(const TypeFollower& use, const TypeFollower& decl, Accepter& accepter, Rank& rank, const decltype(Rank::nested_level) nested_level);

boost::tuple<bool, std::size_t, decltype(SingleInfo::deducted_types)> explicit_specialize(const std::vector<tree::TypenameDecl*>& use_tds, const std::vector<tree::TypenameDecl*>& decl_tds);
std::pair<boost::tribool, RankList> implicit(const std::vector<tree::VariableDecl*>& decl_params, const std::vector<tree::Expression*>& use_exprs, const std::vector<tree::Type*>& use_types, decltype(SingleInfo::deducted_types)& deducted_types);
void fill_by_default_types(decltype(SingleInfo::deducted_types)& deducted_types, const tree::TemplatedIdentifier& del_template_id);
boost::tribool match_template_recursively(tree::FunctionDecl& generic_decl, tree::FunctionDecl& specialized_decl);

boost::tribool is_same_specialization(tree::ClassDecl& specialization_a, tree::ClassDecl& specialization_b);

} } } }

#endif /* ZILLIANS_LANGUAGE_DEDUCTION_H_ */
