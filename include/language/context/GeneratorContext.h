/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_GENERATORCONTEXT_H_
#define ZILLIANS_LANGUAGE_GENERATORCONTEXT_H_

#include <llvm/Config/config.h>
#if LLVM_VERSION_MAJOR >= 3 && LLVM_VERSION_MINOR >= 3
#include <llvm/IR/Module.h>
#else
#include <llvm/Module.h>
#endif

#include <boost/filesystem.hpp>

#include "language/Architecture.h"
#include "language/ThorConfiguration.h"

namespace zillians { namespace language {

struct GeneratorContext
{
	enum Target {
		BUILD_FOR_X86,
		BUILD_FOR_CUDA,
		BUILD_FOR_OPENCL,
	};

    GeneratorContext()
        : active_config(nullptr)
        , active_config_index(-1)
        , emit_debug_info(false)
        , opt_level(0)
    {
        global_config.loadDefault();
    }

    GeneratorContext(const boost::filesystem::path& project_path)
        : active_config(nullptr)
        , active_config_index(-1)
        , emit_debug_info(false)
        , opt_level(0)
    {
        global_config.load(project_path);
    }

	struct Configuration {
		explicit Configuration(Architecture arch) : arch(arch), context(NULL), module(NULL)
		{ }

		Architecture arch;

	    llvm::LLVMContext* context;
	    llvm::Module* module;

	    std::string asm_file;
	    std::string object_file;
	};

    ThorBuildConfiguration global_config;
    std::vector<Configuration*> target_config;
    int active_config_index;
    Configuration* active_config;
    bool emit_debug_info;
    unsigned opt_level;

};

bool hasGeneratorContext();
GeneratorContext& getGeneratorContext();
void setGeneratorContext(GeneratorContext* context);

} }

#endif /* ZILLIANS_LANGUAGE_GENERATORCONTEXT_H_ */
