/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_STAGE_IMPORT_GRAMMAR_CXX_PIMPL_GRAMMAR_H_
#define ZILLIANS_LANGUAGE_STAGE_IMPORT_GRAMMAR_CXX_PIMPL_GRAMMAR_H_

#include <boost/fusion/container/vector.hpp>
#include <boost/spirit/include/karma_grammar.hpp>
#include <boost/spirit/include/karma_rule.hpp>

#include "language/stage/import/TSInfo.h"
#include "language/stage/import/grammar/CxxIdentifierGrammar.h"
#include "language/stage/import/grammar/CxxOptionalScopeGrammar.h"
#include "language/stage/import/grammar/ExpectedIterator.h"

namespace zillians { namespace language { namespace stage { namespace import { namespace grammar {

template<typename iterator>
struct cxx_pimpl_getter_decl_grammar : public boost::spirit::karma::grammar<iterator, boost::fusion::vector<ts_class*, bool>()>
{
    cxx_pimpl_getter_decl_grammar();

    boost::spirit::karma::rule<iterator, boost::fusion::vector<ts_class*, bool>()>   start;

    boost::spirit::karma::rule<iterator, boost::fusion::vector<ts_class*, bool>()>   pimpl;
    cxx_optional_scope_grammar<iterator>                                             optional_scope;
};

template<typename iterator>
struct cxx_pimpl_getter_def_grammar : public boost::spirit::karma::grammar<iterator, ts_class*()>
{
    cxx_pimpl_getter_def_grammar();

    boost::spirit::karma::rule<iterator, ts_class*()>   start;

    boost::spirit::karma::rule<iterator, ts_class*()>   pimpl;
    cxx_pimpl_getter_decl_grammar<iterator>             pimpl_decl;
    boost::spirit::karma::rule<iterator, void()>        pimpl_body;
};

template<typename iterator>
struct cxx_pimpl_setter_decl_grammar : public boost::spirit::karma::grammar<iterator, boost::fusion::vector<ts_class*, bool>()>
{
    cxx_pimpl_setter_decl_grammar();

    boost::spirit::karma::rule<iterator, boost::fusion::vector<ts_class*, bool>()>   start;
    boost::spirit::karma::rule<iterator, boost::fusion::vector<ts_class*, bool>()>   pimpl;

    cxx_optional_scope_grammar<iterator>                                             optional_scope;
};

template<typename iterator>
struct cxx_pimpl_setter_def_grammar : public boost::spirit::karma::grammar<iterator, ts_class*()>
{
    cxx_pimpl_setter_def_grammar();

    boost::spirit::karma::rule<iterator, ts_class*()>   start;

    boost::spirit::karma::rule<iterator, ts_class*()>   pimpl;
    cxx_pimpl_setter_decl_grammar<iterator>             pimpl_decl;
    boost::spirit::karma::rule<iterator, void()>        pimpl_body;
};

template<typename iterator>
struct cxx_pimpl_creator_decl_grammar : public boost::spirit::karma::grammar<iterator, ts_class*()>
{
    cxx_pimpl_creator_decl_grammar();

    boost::spirit::karma::rule<iterator, ts_class*()>   start;
    boost::spirit::karma::rule<iterator, ts_class*()>   pimpl;

    cxx_qualified_identifier_grammar<iterator>          qualified_identifier;
};

TS_IMPORT_GRAMMAR_EXTERN_TEMPLATES(cxx_pimpl_getter_decl_grammar)
TS_IMPORT_GRAMMAR_EXTERN_TEMPLATES(cxx_pimpl_getter_def_grammar)
TS_IMPORT_GRAMMAR_EXTERN_TEMPLATES(cxx_pimpl_setter_decl_grammar)
TS_IMPORT_GRAMMAR_EXTERN_TEMPLATES(cxx_pimpl_setter_def_grammar)
TS_IMPORT_GRAMMAR_EXTERN_TEMPLATES(cxx_pimpl_creator_decl_grammar)

} } } } }

#endif /* ZILLIANS_LANGUAGE_STAGE_IMPORT_GRAMMAR_CXX_PIMPL_GRAMMAR_H_ */
