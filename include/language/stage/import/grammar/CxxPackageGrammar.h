/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_STAGE_IMPORT_GRAMMAR_CXX_PACKAGE_GRAMMAR_H_
#define ZILLIANS_LANGUAGE_STAGE_IMPORT_GRAMMAR_CXX_PACKAGE_GRAMMAR_H_

#include <string>

#include <boost/spirit/include/karma_grammar.hpp>
#include <boost/spirit/include/karma_rule.hpp>

#include "language/stage/import/TSInfo.h"
#include "language/stage/import/grammar/CxxClassGrammar.h"
#include "language/stage/import/grammar/CxxFunctionGrammar.h"
#include "language/stage/import/grammar/CxxGuardSymbolGrammar.h"
#include "language/stage/import/grammar/CxxNamespaceGrammar.h"
#include "language/stage/import/grammar/CxxIncludeGrammar.h"
#include "language/stage/import/grammar/ExpectedIterator.h"

namespace zillians { namespace language { namespace stage { namespace import { namespace grammar {

template<typename iterator>
struct cxx_package_fwd_grammar : public boost::spirit::karma::grammar<iterator, ts_package*()>
{
    cxx_package_fwd_grammar();

    boost::spirit::karma::rule<iterator, ts_package*()>    start;

    boost::spirit::karma::rule<iterator, ts_package*()>    package;

    cxx_guard_symbol_grammar<iterator>                     guard_symbol;

    cxx_namespace_begin_grammar<iterator>                  ns_begin;
    cxx_namespace_end_grammar<iterator>                    ns_end;

    cxx_class_fwd_grammar<iterator>                        cls;
    boost::spirit::karma::rule<iterator, ts_function*()>   function;
};

template<typename iterator>
struct cxx_package_decl_grammar : public boost::spirit::karma::grammar<iterator, ts_package*()>
{
    cxx_package_decl_grammar();

    boost::spirit::karma::rule<iterator, ts_package*()>    start;

    boost::spirit::karma::rule<iterator, ts_package*()>    package;

    cxx_guard_symbol_grammar<iterator>                     guard_symbol;

    cxx_namespace_begin_grammar<iterator>                  ns_begin;
    cxx_namespace_end_grammar<iterator>                    ns_end;

    boost::spirit::karma::rule<iterator, std::string()>    wrapped_header;
    cxx_include_grammar<iterator>                          include;
    cxx_refers_grammar<iterator>                           refers;

    boost::spirit::karma::rule<iterator, ts_class*()>      cls;
    cxx_class_decl_grammar<iterator>                       cls_impl;

    boost::spirit::karma::rule<iterator, ts_function*()>   function;
    cxx_function_decl_grammar<iterator>                    function_impl;
};

template<typename iterator>
struct cxx_package_def_grammar : public boost::spirit::karma::grammar<iterator, ts_package*()>
{
    cxx_package_def_grammar();

    boost::spirit::karma::rule<iterator, ts_package*()>   start;

    boost::spirit::karma::rule<iterator, ts_package*()>   package;

    boost::spirit::karma::rule<iterator, ts_package*()>   builtins;

    cxx_namespace_begin_grammar<iterator>                 ns_begin;
    cxx_namespace_end_grammar<iterator>                   ns_end;

    cxx_include_grammar<iterator>                         include;
    cxx_refers_grammar<iterator>                          refers;

    cxx_class_def_grammar<iterator>                       cls;
    cxx_function_def_grammar<iterator>                    function;
};

TS_IMPORT_GRAMMAR_EXTERN_TEMPLATES(cxx_package_fwd_grammar)
TS_IMPORT_GRAMMAR_EXTERN_TEMPLATES(cxx_package_decl_grammar)
TS_IMPORT_GRAMMAR_EXTERN_TEMPLATES(cxx_package_def_grammar)

} } } } }

#endif /* ZILLIANS_LANGUAGE_STAGE_IMPORT_GRAMMAR_CXX_PACKAGE_GRAMMAR_H_ */
