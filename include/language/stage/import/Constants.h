/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_STAGE_IMPORT_CONSTANTS_H_
#define ZILLIANS_LANGUAGE_STAGE_IMPORT_CONSTANTS_H_

#include <string>

namespace zillians { namespace language { namespace stage { namespace import {

// C++ type names
extern const char* CXX_VOID_NAME            ;
extern const char* CXX_BOOL_NAME            ;
extern const char* CXX_INT8_NAME            ;
extern const char* CXX_INT16_NAME           ;
extern const char* CXX_INT32_NAME           ;
extern const char* CXX_INT64_NAME           ;
extern const char* CXX_FLOAT32_NAME         ;
extern const char* CXX_FLOAT64_NAME         ;
extern const char* CXX_THOR_LANG_OBJECT_NAME;
extern const char* CXX_THOR_LANG_STRING_NAME;
extern const char* CXX_INVALID_TYPE_NAME    ;

// Thor type names
extern const char* TS_VOID_NAME            ;
extern const char* TS_BOOL_NAME            ;
extern const char* TS_INT8_NAME            ;
extern const char* TS_INT16_NAME           ;
extern const char* TS_INT32_NAME           ;
extern const char* TS_INT64_NAME           ;
extern const char* TS_FLOAT32_NAME         ;
extern const char* TS_FLOAT64_NAME         ;
extern const char* TS_THOR_LANG_OBJECT_NAME;
extern const char* TS_THOR_LANG_STRING_NAME;
extern const char* TS_INVALID_TYPE_NAME    ;

// filenames
extern const char* CXX_HEADER_FILENAME          ;
extern const char* CXX_HEADER_FILENAME_GUARD    ;
extern const char* CXX_HEADER_FWD_FILENAME      ;
extern const char* CXX_HEADER_FWD_FILENAME_GUARD;
extern const char* CXX_SOURCE_FILENAME          ;
extern const char* TS_SOURCE_FILENAME           ;

// for Pimpl
extern const std::string CXX_PIMPL_SUFFIX                ;
extern const std::string CXX_VARIABLE_OWN_IMPL_NAME      ;
extern const std::string CXX_VARIABLE_IMPL_NAME          ;
extern const std::string CXX_VARIABLE_LOCAL_OWN_IMPL_NAME;
extern const std::string CXX_VARIABLE_LOCAL_IMPL_NAME    ;
extern const std::string CXX_FUNCTION_CREATOR_PIMPL_NAME ;
extern const std::string CXX_FUNCTION_GET_PIMPL_NAME     ;
extern const std::string CXX_FUNCTION_SET_PIMPL_NAME     ;

// for builtin helpers
extern const std::string CXX_BUILTIN_SUFFIX                 ;
extern const std::string CXX_BUILTIN_FUNCTION_TO_STRING_NAME;
extern const std::string CXX_BUILTIN_FUNCTION_TO_STRING_IMPL;

// for temporary variables
extern const std::string CXX_LOCAL_VARIABLE_SUFFIX;
extern const std::string CXX_MEMBER_VARIABLE_SUFFIX;

} } } }

#endif /* ZILLIANS_LANGUAGE_STAGE_IMPORT_CONSTANTS_H_ */
