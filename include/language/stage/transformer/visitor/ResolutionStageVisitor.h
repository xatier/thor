/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_STAGE_VISITOR_RESOLUTIONSTAGEVISITOR_H_
#define ZILLIANS_LANGUAGE_STAGE_VISITOR_RESOLUTIONSTAGEVISITOR_H_

#include <set>
#include <tuple>
#include <type_traits>
#include <vector>
#include <unordered_set>
#include <map>

#include <boost/optional.hpp>
#include <boost/logic/tribool_fwd.hpp>

#include "language/stage/transformer/detail/ObjectReplicationHelper.h"
#include "language/stage/transformer/detail/SymbolTableChain.h"
#include "language/tree/visitor/GenericDoubleVisitor.h"
#include "language/resolver/Resolver.h"
#include "language/resolver/Specialization.h"
#include "language/resolver/SymbolTable.h"

namespace zillians { namespace language { namespace stage { namespace visitor {

/**
 * ResolutionStageVisitor is the visitation helper for ResolutionStage
 *
 * ResolutionStageVisitor will visit every node in the AST that needs to be resolved as type or symbol by using Resolver
 *
 * @see ResolutionStage, Resolver
 * @todo implement resolution cache
 */
struct ResolutionStageVisitor : public tree::visitor::GenericDoubleVisitor
{
    struct PersistantState
    {
        std::set<tree::ClassDecl*>                     class_content_ready_set;
        detail::object_replication::verification_state verification_state;
        std::unordered_set<tree::ASTNode*>             failed_nodes;
        resolution::specialization::Grouper            specialization_grouper;
    };

    CREATE_INVOKER(resolveInvoker, resolve);

    explicit ResolutionStageVisitor(PersistantState& state);

    void resolve(tree::ASTNode& node);

    void resolve(tree::Annotations& node);
    void resolve(tree::NumericLiteral& node);
    void resolve(tree::ObjectLiteral& node);
    template<
        typename NodeType,
        typename std::enable_if<
            std::is_same<NodeType, tree::TypeIdLiteral    >::value ||
            std::is_same<NodeType, tree::SymbolIdLiteral  >::value ||
            std::is_same<NodeType, tree::FunctionIdLiteral>::value
        >::type * = nullptr
    >
    void resolve(NodeType& node);
    void resolve(tree::TemplatedIdentifier& node);
    void resolve(tree::Block& node);
    void resolve(tree::Source& node);
    void resolve(tree::TypeSpecifier& node);
    void resolve(tree::FunctionSpecifier& node);
    void resolve(tree::MultiSpecifier& node);
    void resolve(tree::NamedSpecifier& node);
    void resolve(tree::PrimitiveSpecifier& node);
    void resolve(tree::ClassDecl& node);
    void resolve(tree::FunctionDecl& node);
    void resolve(tree::EnumDecl& node);
    void resolve(tree::VariableDecl& node);
    void resolve(tree::BranchStmt& node);
    void resolve(tree::DeclarativeStmt& node);
    void resolve(tree::ExpressionStmt& node);
    void resolve(tree::ForeachStmt& node);
    void resolve(tree::UnaryExpr& node);
    void resolve(tree::BinaryExpr& node);
    void resolve(tree::TernaryExpr& node);
    void resolve(tree::CallExpr& node);
    void resolve(tree::ArrayExpr& node);
    void resolve(tree::MemberExpr& node);
    void resolve(tree::IdExpr& node);
    void resolve(tree::LambdaExpr& node);
    void resolve(tree::BlockExpr& node);
    void resolve(tree::CastExpr& node);
    void resolve(tree::IsaExpr& node);
    void resolve(tree::TieExpr& node);
    void resolve(tree::StringizeExpr& node);
    void resolve(tree::SystemCallExpr& node);

    std::size_t getResolvedCount();
    std::size_t getFailedCount();
    std::size_t getUnknownCount();

    bool isResolved(tree::ASTNode* node);
    bool isFailed(tree::ASTNode* node);
    bool isUnknown(tree::ASTNode* node);

    void reset();

    bool hasTransforms();
    void applyTransforms();

private:
    void markResolved(tree::ASTNode* node);
    void markFailed(tree::ASTNode* node);
    void markFailedSymbol(tree::ASTNode* node, tree::Identifier* id, bool log_error = true);
    void markFailedType(tree::ASTNode* node, bool log_error = true);
    void markFailedExpr(tree::ASTNode* node, tree::ASTNode* resolved);
    void markUnknown(tree::ASTNode* node);

    SymbolTable::FindResult findSymbol(tree::Identifier& id, tree::ASTNode* depend);

    boost::tribool resolveType(tree::ASTNode& attach, tree::Identifier& id, tree::ASTNode* depend = NULL);
    boost::tribool resolveSymbol(tree::ASTNode& attach, tree::Identifier& id, tree::ASTNode* depend = NULL);

    template<typename Resolver, typename OnResolved, typename OnFailure>
    boost::tribool resolveImpl(Resolver& resolver, OnResolved onResolved, OnFailure onFailure, tree::ASTNode& attach, tree::Identifier& id, tree::ASTNode* depend);

    bool is_valid_symbol_for_call(tree::ASTNode& node, tree::Identifier& id);
    bool is_valid_symbol_for_top_expr(tree::ASTNode& node);

private:
    void propogateType(tree::ASTNode& to, tree::ASTNode& from);
    void propogateType(tree::ASTNode& to, tree::TypeSpecifier& from);
    void propogateSymbol(tree::ASTNode& to, tree::ASTNode& from);

    template<typename NodeType>
    void resolveExprImpl(NodeType& node);

    std::tuple<boost::tribool, bool, tree::Type*> getLogicalExprType(tree::UnaryExpr& expr);
    std::tuple<boost::tribool, bool, tree::Type*> getArithmeticOrBinaryExprType(tree::UnaryExpr& expr);
    std::tuple<boost::tribool, bool, tree::Type*> getAssignExprType(tree::BinaryExpr& expr);
    std::tuple<boost::tribool, bool, tree::Type*> getArithmeticOrBianryExprType(tree::BinaryExpr& expr);
    std::tuple<boost::tribool, bool, tree::Type*> getComparisonOrLogicalExprType(tree::BinaryExpr& expr);

    std::tuple<boost::tribool, bool, tree::Type*> getExprType(tree::UnaryExpr& expr);
    std::tuple<boost::tribool, bool, tree::Type*> getExprType(tree::BinaryExpr& expr);

    /**
     * @brief check whether an assign operator can infer variable type
     * @return the VariableDecl of the variable if follow condition satisfied
     *
     * - lhs is primary expression
     *
     * - lhs has resolved symbol
     *
     * - lhs resolve symbol to an variable declaration
     *
     * - lhs's resolved variable declaration has NO type yet
     *
     * - rhs is not a null literal
     */
    boost::optional<tree::VariableDecl*> canInferVariableType(tree::BinaryExpr& node);
    void inferVariableType(tree::BinaryExpr& node);
    void returnTypeInference(tree::BranchStmt& node);
    void tryResolveEnumValues(tree::EnumDecl& node);

private:
    template<typename NodeType>
    void insert_symbol(NodeType& node);

    void prepare_symbol_table(tree::ClassDecl& cls_decl);
    void prepare_symbol_table_impl(tree::ClassDecl& cls_decl);

    bool is_class_content_ready(tree::ClassDecl& cls_decl) const;

    SymbolTableChain symbol_table_chain;
    std::set<tree::ClassDecl*>& class_content_ready_set;
    detail::object_replication::verification_state& verification_state;

private:
    void put_call_parameters(tree::ASTNode& owner, tree::ASTNode& self, const std::vector<tree::Expression*>& parameters);
    void drop_call_parameters(tree::ASTNode& owner, tree::ASTNode& self);
    const std::vector<tree::Expression*>* get_call_parameters(tree::ASTNode& owner, tree::ASTNode& self);

    std::map<std::pair<tree::ASTNode*, tree::ASTNode*>, const std::vector<tree::Expression*>*> call_parameters_map;

private:
    // operator overload restruct
    void replaceByMethodCall(tree::BinaryExpr& node);
    void replaceByMethodCall(tree::UnaryExpr& node);

public:
    resolution::Instantiater instantiater;
    resolution::specialization::Grouper& specialization_grouper;

    std::size_t resolved_count;
    std::size_t unresolved_count;

private:
    std::vector<std::function<void()>> transforms;
    std::unordered_set<tree::ASTNode*> resolved_nodes;
    std::unordered_set<tree::ASTNode*>& failed_nodes;
    std::unordered_set<tree::ASTNode*> unknown_nodes;
};

} } } }

#endif /* ZILLIANS_LANGUAGE_STAGE_VISITOR_RESOLUTIONSTAGEVISITOR_H_ */
