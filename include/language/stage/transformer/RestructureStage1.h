/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_STAGE_TRANSFORMER_RESTRUCTURESTAGE1_H_
#define ZILLIANS_LANGUAGE_STAGE_TRANSFORMER_RESTRUCTURESTAGE1_H_

#include "language/stage/CompoundStage.h"

namespace zillians { namespace language { namespace stage {

/**
 * RestructureStage1 will be executed after resolution stage
 */
class RestructureStage1 : public CompoundSubStage
{
public:
    struct RestructureReport
    {
        void reset()
        {
            *this = RestructureReport();
        }

        bool hasProgress()
        {
            return         split_reference_count > 0 ||
                               assign_null_count > 0 ||
                      system_api_transform_count > 0 ||
                           typeid_resolved_count > 0 ||
                              new_restruct_count > 0 ||
                              isa_restruct_count > 0 ||
                    lambda_class_construct_count > 0 ||
                        generated_function_count > 0 ||
                            async_restruct_count > 0;
        }

        unsigned        split_reference_count = 0;
        unsigned            assign_null_count = 0;
        unsigned   system_api_transform_count = 0;
        unsigned        typeid_resolved_count = 0;
        unsigned           new_restruct_count = 0;
        unsigned           isa_restruct_count = 0;
        unsigned lambda_class_construct_count = 0;
        unsigned     generated_function_count = 0;
        unsigned         async_restruct_count = 0;
    };

public:
    RestructureStage1();
    virtual ~RestructureStage1();

public:
    virtual const char* name();
    virtual std::pair<shared_ptr<po::options_description>, shared_ptr<po::options_description>> getOptions();
    virtual bool parseOptions(po::variables_map& vm);
    virtual bool execute(bool& continue_execution);

public:
    virtual bool hasProgress();

private:
    bool debug;
    bool dump_graphviz;
    bool no_system_bundle;
    std::string dump_graphviz_dir;

    RestructureStage1::RestructureReport restruct_report;
};

} } }

#endif /* ZILLIANS_LANGUAGE_STAGE_TRANSFORMER_RESTRUCTURESTAGE1_H_ */
