/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */


#ifndef ZILLIANS_LANGUAGE_STAGE_VISITOR_DETAIL_ENUM_DEPENDENCY_WALKER_H_
#define ZILLIANS_LANGUAGE_STAGE_VISITOR_DETAIL_ENUM_DEPENDENCY_WALKER_H_

#include <vector>
#include <tuple>
#include <initializer_list>

#include "language/tree/expression/Expression.h"
#include "language/tree/declaration/VariableDecl.h"


namespace zillians { namespace language { namespace stage { namespace visitor {

namespace detail {

struct EnumDependencyWalker
{
    using Enumerator = language::tree::VariableDecl;
    using Result = std::tuple<
        bool   // all depended enum values(if any) were determined
        , bool // circular dependency was found
        , bool // expression had been folded
    >;

private:
    using Path = std::vector<Enumerator*>;

    // helper functons
    static bool has_value(Enumerator* enumerator);

    template < typename ParentExpr >
    Result walk_by_steps(ParentExpr* parent, std::initializer_list<language::tree::Expression*> sub_exprs);

    Result walk_impl(language::tree::Expression* expr);

private:
    Path walked;

public:
    Enumerator* self_referenced;

    Result walk(Enumerator* enumerator);
};

} // namespace detail

} } } } // namespace zillians::language::stage::visitor

#endif
