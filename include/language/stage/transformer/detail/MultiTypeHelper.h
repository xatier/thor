/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_STAGE_VISITOR_DETAIL_MULTI_TYPE_HELPER_H_
#define ZILLIANS_LANGUAGE_STAGE_VISITOR_DETAIL_MULTI_TYPE_HELPER_H_

#include <boost/logic/tribool.hpp>

namespace zillians { namespace language {

namespace tree {

struct Type;

struct BinaryExpr;
struct BlockExpr;
struct CallExpr;
struct TieExpr;

struct BranchStmt;

}

namespace stage { namespace visitor { namespace multi_type_helper {

tree::TieExpr& findEndingTieExpr(tree::BlockExpr& expr);
bool isValidReturn(const tree::Type& type);

boost::tribool isMergable(const tree::TieExpr&   lhs_expr, const tree::BlockExpr& rhs_expr);
boost::tribool isMergable(const tree::TieExpr&   lhs_expr, const tree::CallExpr&  rhs_expr);
boost::tribool isMergable(const tree::TieExpr&   lhs_expr, const tree::TieExpr&   rhs_expr);
boost::tribool isMergable(const tree::BlockExpr& lhs_expr, const tree::BlockExpr& rhs_expr);
boost::tribool isMergable(const tree::BlockExpr& lhs_expr, const tree::CallExpr&  rhs_expr);
boost::tribool isMergable(const tree::BlockExpr& lhs_expr, const tree::TieExpr&   rhs_expr);

/**
 *  Transforms "a, b = multi_value_func();" into the following form:
 *  @code
 *  <block-expr: multi-type-restructure> {
 *      var a_temp;
 *      var b_temp;
 *
 *      <unpack-expr>(multi_value_func(), a_temp, b_temp);
 *
 *      a = a_temp, b = b_temp;
 *  };
 *  @endcode
 */
void unpackCall(tree::BinaryExpr& node, tree::TieExpr& lhs_expr, tree::CallExpr& rhs_expr);

/**
 *  Transforms intermediate result of "(a, b = foo()) = bar();":
 *  @code
 *  <block-expr: multi-type-restructure> {
 *      // this BlockExpr is transformed from "(a, b = foo())"
 *      var a_temp;
 *      var b_temp;
 *
 *      <unpack-expr>(foo(), a_temp, b_temp);
 *
 *      (a = a_temp), (b = b_temp);
 *  } = bar();
 *  @endcode
 *  into
 *  @code
 *  <block-expr: multi-type-restructure> {
 *      // this BlockExpr is transformed from "(a, b = foo()) = bar()"
 *      var a_temp;
 *      var b_temp;
 *
 *      <unpack-expr>(foo(), a_temp, b_temp);
 *
 *      var bar_temp_0;
 *      var bar_temp_1;
 *
 *      <unpack-expr>(bar(), bar_temp_0, bar_temp_1);
 *
 *      ((a = a_temp) = bar_temp_0), ((b = b_temp) = bar_temp_1);
 *  };
 *  @endcode
 */
void unpackCall(tree::BinaryExpr& node, tree::BlockExpr& lhs_expr, tree::CallExpr& rhs_expr);

/**
 *  Transforms return statement of 'return foo();' into:
 *  @code
 *  return <block-expr: multi-type-restructure> {
 *      var a;
 *      var b;
 *
 *      <unpack-expr>(foo(), a, b);
 *
 *      a, b;
 *  }
 *  @endcode
 */
void unpackCall(tree::BranchStmt& node, tree::CallExpr& ret_expr);

/**
 *  Transforms "a, b = 1, 2;" into "(a = 1), (b = 2);"
 */
void mergeExpr(tree::BinaryExpr& node, tree::TieExpr& lhs_expr, tree::TieExpr& rhs_expr);

/**
 *  Transforms intermediate result of "a, b = c, d = foo();":
 *  @code
 *  a, b = <block-expr: multi-type-restructure> {
 *      // this BlockExpr is transformed from "c, d = foo()"
 *      var c_temp;
 *      var d_temp;
 *
 *      <unpack-expr>(foo(), c_temp, d_temp);
 *
 *      (c = c_temp), (d = d_temp);
 *  };
 *  @endcode
 *  into
 *  @code
 *  <block-expr: multi-type-restructure> {
 *      // this BlockExpr is transformed from "a, b = c, d = foo()"
 *      var c_temp;
 *      var d_temp;
 *
 *      <unpack-expr>(foo(), c_temp, d_temp);
 *
 *      (a = c = c_temp), (b = d = d_temp);
 *  };
 *  @endcode
 */
void mergeExpr(tree::BinaryExpr& node, tree::TieExpr& lhs_expr, tree::BlockExpr& rhs_expr);

/**
 *  Transforms intermediate result of "(a, b = foo()) = c, d;":
 *  @code
 *  <block-expr: multi-type-restructure> {
 *      // this BlockExpr is transformed from "(a, b = foo())"
 *      var a_temp;
 *      var b_temp;
 *
 *      <unpack-expr>(foo(), a_temp, b_temp);
 *
 *      (a = a_temp), (b = b_temp);
 *  } = c, d;
 *  @endcode
 *  into
 *  @code
 *  <block-expr: multi-type-restructure> {
 *      // this BlockExpr is transformed from "(a, b = foo())"
 *      var a_temp;
 *      var b_temp;
 *
 *      <unpack-expr>(foo(), a_temp, b_temp);
 *
 *      ((a = a_temp) = c), ((b = b_temp) = d);
 *  };
 *  @endcode
 */
void mergeExpr(tree::BinaryExpr& node, tree::BlockExpr& lhs_expr, tree::TieExpr& rhs_expr);

/**
 *  Transforms intermediate result of "(a, b = foo()) = (c, d = bar());":
 *  @code
 *  <block-expr: multi-type-restructure> {
 *      // this BlockExpr is transformed from "(a, b = foo())"
 *      var a_temp;
 *      var b_temp;
 *
 *      <unpack-expr>(foo(), a_temp, b_temp);
 *
 *      (a = a_temp), (b = b_temp);
 *  } = <block-expr: multi-type-restructure> {
 *      // this BlockExpr is transformed from "(c, d = bar())"
 *      var c_temp;
 *      var d_temp;
 *
 *      <unpack-expr>(foo(), c_temp, d_temp);
 *
 *      (c = c_temp), (d = d_temp);
 *  };
 *  @endcode
 *  into
 *  @code
 *  <block-expr: multi-type-restructure> {
 *      // this BlockExpr is transformed from "(a, b = foo()) = (c, d = bar())"
 *      var a_temp;
 *      var b_temp;
 *
 *      <unpack-expr>(foo(), a_temp, b_temp);
 *
 *      var c_temp;
 *      var d_temp;
 *
 *      <unpack-expr>(foo(), c_temp, d_temp);
 *
 *      ((a = a_temp) = (c = c_temp)), ((b = b_temp), (d = d_temp));
 *  };
 *  @endcode
 */
void mergeExpr(tree::BinaryExpr& node, tree::BlockExpr& lhs_expr, tree::BlockExpr& rhs_expr);

} } }

} }

#endif /* ZILLIANS_LANGUAGE_STAGE_VISITOR_DETAIL_MULTI_TYPE_HELPER_H_ */
