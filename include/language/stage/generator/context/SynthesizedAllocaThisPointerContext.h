/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_STAGE_SYNTHESIZEDALLOCATHISPOINTERCONTEXT_H_
#define ZILLIANS_LANGUAGE_STAGE_SYNTHESIZEDALLOCATHISPOINTERCONTEXT_H_

#include "language/tree/ASTNodeFactory.h"
#include "language/stage/generator/detail/LLVMHeaders.h"

namespace zillians { namespace language { namespace stage {

/**
 * This context stored the this pointer via alloca, which differs from the one from parameter.
 */
struct SynthesizedAllocaThisPointerContext
{
	SynthesizedAllocaThisPointerContext() : llvm_this_pointer(NULL)
	{ }

    SynthesizedAllocaThisPointerContext(llvm::Value* llvm_this_pointer) : llvm_this_pointer(llvm_this_pointer)
    { }

    static SynthesizedAllocaThisPointerContext* get(tree::ASTNode* node)
    {
        return node->get<SynthesizedAllocaThisPointerContext>();
    }

    static void set(tree::ASTNode* node, SynthesizedAllocaThisPointerContext* ctx)
    {
        node->set<SynthesizedAllocaThisPointerContext>(ctx);
    }

    static void reset(tree::ASTNode* node)
    {
    	node->reset<SynthesizedAllocaThisPointerContext>();
    }

    llvm::Value* llvm_this_pointer;
};

#define GET_SYNTHESIZED_ALLOCA_THIS_POINTER(x) \
    ((SynthesizedAllocaThisPointerContext::get((x))) ? SynthesizedAllocaThisPointerContext::get((x))->llvm_this_pointer : NULL)

#define SET_SYNTHESIZED_ALLOCA_THIS_POINTER(x, v)  \
    { \
        if(SynthesizedAllocaThisPointerContext::get(x)) \
        { \
            SynthesizedAllocaThisPointerContext::get((x))->llvm_this_pointer = v; \
        } \
        else \
            SynthesizedAllocaThisPointerContext::set(x, new SynthesizedAllocaThisPointerContext(v)); \
    }


} } }

#endif /* ZILLIANS_LANGUAGE_STAGE_SYNTHESIZEDALLOCATHISPOINTERCONTEXT_H_ */
