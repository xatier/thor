/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_STAGE_VISITOR_LLVMGENERATORPREAMBLESTAGEVISITOR_H_
#define ZILLIANS_LANGUAGE_STAGE_VISITOR_LLVMGENERATORPREAMBLESTAGEVISITOR_H_

#include <cstdint>

#include <string>
#include <vector>

#include <llvm/LLVMContext.h>
#include <llvm/Module.h>
#include <llvm/IRBuilder.h>
#include <llvm/Constant.h>
#include <llvm/Function.h>

#include "utility/StringUtil.h"

#include "core/IntTypes.h"

#include "language/context/ManglingStageContext.h"
#include "language/tree/visitor/GenericDoubleVisitor.h"
#include "language/stage/generator/context/SynthesizedFunctionContext.h"
#include "language/stage/generator/context/SynthesizedVTableContext.h"
#include "language/stage/generator/context/SynthesizedValueContext.h"

namespace zillians { namespace language { namespace stage { namespace visitor {

/**
 * LLVMGeneratorStagePreambleVisitor is used to generate llvm::Function object for all functions prior to actual code generation
 *
 * We have to generate llvm::Function object prior to actual code generation because the LLVMGeneratorVisitor visits the tree in a top-down approach,
 * so it's common case that the callee is visited after the caller, which requires llvm::Function object to create llvm::CallInst
 *
 * @see LLVMGeneratorStageVisitor
 */
struct LLVMGeneratorStagePreambleVisitor : public tree::visitor::GenericDoubleVisitor
{
    typedef std::pair<tree::ClassDecl*, uint64> traversed_node_t;

    CREATE_INVOKER(generateInvoker, generate);

    LLVMGeneratorStagePreambleVisitor(llvm::LLVMContext& context, llvm::Module& module);

    void generate(tree::ASTNode& node);
    void generate(tree::VariableDecl& node);
    void generate(tree::Tangle& node);
    void generate(tree::ClassDecl& node);

private:
    void generateGlobalVariable(tree::VariableDecl& var, const std::string& mangled_name);
    void generateStaticVariable(tree::VariableDecl& var);

private:
    std::string getThunkName(const tree::FunctionDecl& derived_func, std::int64_t class_offset, std::int64_t return_offset);

    void            generateVTable(tree::ClassDecl& node);
    void            generateVTableSegment(const tree::ClassDecl& node, const virtual_table::segment_info_type& segment_info, std::vector<llvm::Constant*>& entries, std::vector<uint32>& subtable_starts);
    llvm::Constant* generateVTableEntry(std::int64_t class_offset, const virtual_table::entry_info_type& entry_info);

    llvm::Constant* getOrInsertPureVirtual();
    llvm::Function* getOrInsertFunction(const tree::FunctionDecl& decl);
    llvm::Function* getOrInsertThunk(std::int64_t class_offset, std::int64_t return_offset, tree::FunctionDecl& origin_decl, tree::FunctionDecl& impl_decl);

private:
    llvm::LLVMContext &mContext;
    llvm::Module& mModule;
    llvm::IRBuilder<> mBuilder;
    LLVMHelper mHelper;

    unsigned mFunctionAddrSpace;
    unsigned mGlobalAddrSpace;
    unsigned mConstAddrSpace;
};

} } } }

#endif /* ZILLIANS_LANGUAGE_STAGE_VISITOR_LLVMGENERATORPREAMBLESTAGEVISITOR_H_ */
