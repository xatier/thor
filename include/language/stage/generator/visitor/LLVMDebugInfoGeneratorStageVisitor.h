/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_STAGE_VISITOR_LLVMDEBUGINFOGENERATORSTAGEVISITOR_H_
#define ZILLIANS_LANGUAGE_STAGE_VISITOR_LLVMDEBUGINFOGENERATORSTAGEVISITOR_H_

#include <vector>

#include <llvm/LLVMContext.h>
#include <llvm/Module.h>
#include <llvm/IRBuilder.h>
#include <llvm/DIBuilder.h>

#include "utility/Filesystem.h"
#include "language/tree/visitor/GenericDoubleVisitor.h"
#include "language/stage/generator/detail/LLVMHelper.h"
#include "language/stage/generator/context/DebugInfoContext.h"
#include "utility/UnicodeUtil.h"

namespace zillians { namespace language { namespace stage { namespace visitor {


struct LLVMDebugInfoGeneratorStageVisitor: public tree::visitor::GenericDoubleVisitor
{
    CREATE_INVOKER(generateInvoker, generate);

    LLVMDebugInfoGeneratorStageVisitor(llvm::LLVMContext& context, llvm::Module& current_module, llvm::DIBuilder& factory);

    virtual ~LLVMDebugInfoGeneratorStageVisitor();

    void generate(tree::ASTNode& node);
    void generate(tree::FunctionType& node);
    void generate(tree::Source& node);
    void generate(tree::CallExpr& node);
    void generate(tree::TypeSpecifier& node);
    void generate(tree::EnumDecl& node);
    void generate(tree::ClassDecl& node);
    void generate(tree::FunctionDecl& node);
    void generate(tree::VariableDecl& node);

private:
    bool createDIType(tree::TypeSpecifier& node, llvm::DIType& type, uint32& bits, uint32& alignment, llvm::dwarf::dwarf_constants& encoding);
    void createClassMembersDebugInfo(tree::ClassDecl& node, std::vector<llvm::Value*>& elements, DebugInfoContext* debuginfo_context);
    llvm::DIType createSubroutineType(tree::FunctionDecl& node, llvm::DIFile file);
    llvm::DIType createMemberVariableDebugInfo(tree::VariableDecl& node, DebugInfoContext* context);
    void generateGlobalVariableDebugInfo(tree::VariableDecl& node);
    void generateStaticVariableDebugInfo(tree::VariableDecl& node, bool is_class_data_member);
    void generateVariableDebugInfo(tree::VariableDecl& node, llvm::dwarf::llvm_dwarf_constants variable_type, int argument_position = 0);
    llvm::DIVariable createVariableDebugInfoDeclare(llvm::StringRef name, llvm::dwarf::llvm_dwarf_constants variable_type, llvm::DIType type, llvm::Value* llvm_value,
            llvm::BasicBlock* block, DebugInfoContext* debug_info, SourceInfoContext* source_info, int argument_position = 0);
    bool getPrimitiveTypeInfo(tree::PrimitiveKind kind, uint32& bits, uint32& alignment, llvm::dwarf::dwarf_constants& encoding);
    bool createStructPointerType(tree::ClassDecl& class_decl, llvm::DIType& debug_info_type, uint32& bits, uint32& alignment, llvm::dwarf::dwarf_constants& encoding);
    bool createMultiType(tree::MultiSpecifier& node, llvm::DIType& debug_info_type, uint32& bits, uint32& alignment, llvm::dwarf::dwarf_constants& encoding);
    bool createPrimitiveType(tree::PrimitiveKind kind, llvm::DIType& debug_info_type, uint32& bits, uint32& alignment, llvm::dwarf::dwarf_constants& encoding);
    void insertDebugLocationForBlock(tree::ASTNode& node);
    DebugInfoContext* getOrInheritDebugInfo(tree::ASTNode& node);
    void insertDebugLocation(tree::ASTNode& node);
    void insertDebugLocationForIntermediateValues(tree::ASTNode& node);

private:
    LLVMHelper mHelper;
    llvm::IRBuilder<> mBuilder;

    llvm::LLVMContext& context;
    llvm::Module& current_module;

    llvm::DIBuilder& factory;
};

}}}}

#endif /* ZILLIANS_LANGUAGE_STAGE_VISITOR_LLVMDEBUGINFOGENERATORSTAGEVISITOR_H_ */
