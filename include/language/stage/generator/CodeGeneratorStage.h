/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_STAGE_GENERATOR_CODEGENERATORSTAGE_H_
#define ZILLIANS_LANGUAGE_STAGE_GENERATOR_CODEGENERATORSTAGE_H_

#include "language/stage/CompoundStage.h"

namespace zillians { namespace language { namespace stage {

/**
 * CodeGeneratorStage is responsible for generating assembly into linkable object file for all architectures
 */
class CodeGeneratorStage : public CompoundSubStage
{
public:
	CodeGeneratorStage();
    virtual ~CodeGeneratorStage();

public:
    virtual const char* name();
    virtual std::pair<shared_ptr<po::options_description>, shared_ptr<po::options_description>> getOptions();
    virtual bool parseOptions(po::variables_map& vm);
    virtual bool execute(bool& continue_execution);

private:
    bool buildAssemblyCode();
    bool buildAssemblyCodeLLVM();
#ifdef BUILD_WITH_NVVM
    bool buildAssemblyCodeNVVM();
#endif
public:
    virtual bool hasProgress();

private:
    std::string asm_file;
    bool verbose;
};

} } }



#endif /* ZILLIANS_LANGUAGE_STAGE_GENERATOR_CODEGENERATORSTAGE_H_ */
