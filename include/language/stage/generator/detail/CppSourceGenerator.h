/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_STAGE_DETAIL_CPPGENERATORHELPER_H_
#define ZILLIANS_LANGUAGE_STAGE_DETAIL_CPPGENERATORHELPER_H_

#include <functional>
#include <iostream>
#include <utility>
#include <set>
#include <vector>

#include "language/tree/ASTNodeFwd.h"

namespace zillians { namespace language { namespace stage {

class CppSourceGenerator
{
public:
    using BaseDerivePair = std::pair<tree::Declaration*, tree::Declaration*>;
    using WithInTemplateQuate = bool;
    using AddStar = bool;
    using Stream = std::wostream;

public:
    CppSourceGenerator(bool cuda_mode = false);

public:

    void setWrite(Stream* new_stream);

    std::vector<tree::Declaration*> sortDeclByInheritence(std::set<tree::Declaration*>& classDecls, std::set<BaseDerivePair>& baseDerivedPairs);
    void genVisibility(tree::Declaration::VisibilitySpecifier::type v);
    void genNonNSQualifiedName(const bool addStar, tree::Identifier* id);
    void genNamespaceQualifier(tree::ASTNode* node);
    void genNamespaceQualifiedType(const bool addStar, tree::TypeSpecifier* type);
    void genNamespaceQualifiedType(const bool addStar, tree::FunctionSpecifier* type);
    void genNamespaceQualifiedType(const bool addStar, tree::NamedSpecifier* type);
    void genNamespaceQualifiedType(const bool addStar, tree::PrimitiveSpecifier* type);
    void genStatic(bool isStatic);
    void genVirtual(bool isVirtual);
    void genDataMember(tree::VariableDecl* var);
    void genCppClassName(tree::Identifier* id);
    void genCppMemberFunctionTemplateQuaifier(tree::Identifier* id);
    void genCppSimpleName(tree::Identifier* id);
    void genParameter(tree::VariableDecl* var);
    void genParameters(std::vector<tree::VariableDecl*>& parameters);
    void genMemberFunction(tree::FunctionDecl* func);
    void genBases(tree::NamedSpecifier* base, std::vector<tree::NamedSpecifier*>& implements);
    void openCppNamespace(tree::Declaration* decl);
    void closeCppNamespace(tree::Declaration* decl);
    void genPreIncludeForwardDeclaration(std::set<tree::Declaration*>& classDecls);
    void genForwardDeclaration(std::set<tree::Declaration*>& classDecls);
    void genEnumDefinitions(std::set<tree::EnumDecl*>& enumDecls);
    void genClassDecls(std::vector<tree::Declaration*>& classDecls);
    void genPrimitiveType();
    void genClassExplicitInstantiation(std::set<tree::Declaration*>& instantiationSet);
    void genFunctionExplicitInstantiation(std::set<tree::Declaration*>& instantiationSet);
    void genIncludeImplementation(std::set<tree::Declaration*>& nativeClasses);
    void genClassNameQualifiedFunctionName(tree::FunctionDecl* funcDecl);

private:
    bool isCtorOrDtor(tree::FunctionDecl* func);
    void genClassDecl(tree::ClassDecl* classDecl);

private:

    bool cuda_mode;
    Stream* output;
};

} } }


#endif /* ZILLIANS_LANGUAGE_STAGE_DETAIL_CPPGENERATORHELPER_H_ */
