/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_COMOPOUND_STAGE_H_
#define ZILLIANS_LANGUAGE_COMOPOUND_STAGE_H_

#include "core/ContextHub.h"

#include "language/stage/Stage.h"

namespace zillians { namespace language { namespace stage {

class CompoundSubStage : public Stage
{
public:
	virtual bool hasProgress() = 0;
};

namespace detail {

    template <typename ... Stages>
    struct StageAppender;

    template <typename T, typename ... Stages>
    struct StageAppender<T, Stages...> : public StageAppender<Stages...>
    {
        typedef T                           CurrentStageType;
        typedef StageAppender<Stages...>    NextStageAppender;

        static void append(std::vector<shared_ptr<CompoundSubStage> >& stages)
        {
            shared_ptr<CurrentStageType> sub_stage(new CurrentStageType());
            stages.push_back(sub_stage);

            NextStageAppender::append(stages);
        }
    };

    template<>
    struct StageAppender<>
    {
        static void append(std::vector<shared_ptr<CompoundSubStage> >& stages) {}
    };
}

namespace po = boost::program_options;

template <typename ... SubStages>
class CompoundStage : public CompoundSubStage
{
public:
    CompoundStage() 
    { 
        detail::StageAppender<SubStages...>::append(mStages);
    }
    virtual ~CompoundStage() { }

    virtual const char* name() 
    {
        return "compound_stage";
    }

    virtual bool hasProgress() {
        return mHasProgress;
    }

    virtual std::pair<shared_ptr<po::options_description>, shared_ptr<po::options_description>> getOptions() 
    {
        shared_ptr<po::options_description> option_public(new po::options_description());
        shared_ptr<po::options_description> option_private(new po::options_description());

        for(auto stage : mStages)
        {
             auto options = stage->getOptions();
             for(auto& option : options.first->options()) option_public->add(option);
             for(auto& option : options.second->options()) option_private->add(option);
        }

        return std::make_pair(option_public, option_private);
    }

    virtual bool parseOptions(po::variables_map& map)
    {
        bool result = true;

        for(auto stage : mStages)
        {
            if (stage->parseOptions(map) == false)
            {
                result = false;
            }
        }
        return result;
    }

    virtual bool execute(bool& continue_execution)
    {
        bool result = true;

        bool has_progress = true;
        mHasProgress = false;
        while (has_progress && result)
        {
            has_progress = false;
            for(auto stage : mStages)
            {
                if (stage->execute(continue_execution) == false)
                {
                    result = false;
                    break;
                }

                if (stage->hasProgress())
                {
                    mHasProgress = true;
                    has_progress = true;
                }
            }
        }

        return result;
    }

private:
    bool mHasProgress = false;
	po::variables_map mVariablesMap;
	std::vector<shared_ptr<CompoundSubStage>> mStages;
};

} } }

#endif /* ZILLIANS_LANGUAGE_COMOPOUND_STAGE_H_ */
