/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_STAGE_SEMANTICVERIFICATIONCONTEXT_H_
#define ZILLIANS_LANGUAGE_STAGE_SEMANTICVERIFICATIONCONTEXT_H_

#include "language/tree/ASTNodeFactory.h"

#include <string>

namespace zillians { namespace language { namespace stage {

template<class T>
struct ASTNodeContext
{
    static bool is_bound(tree::ASTNode* node)
    {
        return get(node);
    }

    static T* get(tree::ASTNode* node)
    {
        return node->get<T>();
    }

    static T* bind(tree::ASTNode* node)
    {
        T* x = get(node);
        if(!x)
            set(node, x = new T());
        return x;
    }

    static void unbind(tree::ASTNode* node)
    {
        set(node, NULL);
    }

private:
    static void set(tree::ASTNode* node, T* ctx)
    {
        node->set<T>(ctx);
    }
};

/////////////////////////////////////////////////////////////////////
/// s0

/////////////////////////////////////////////////////////////////////
/// s1

// MISSING_CASE
struct SemanticVerificationEnumKeyContext_HasVisited : public ASTNodeContext<SemanticVerificationEnumKeyContext_HasVisited>
{ };

} } }

#endif /* ZILLIANS_LANGUAGE_STAGE_SEMANTICVERIFICATIONCONTEXT_H_ */
