/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_STAGE_VISITOR_NAMEVERIFICATIONVISITOR_H_
#define ZILLIANS_LANGUAGE_STAGE_VISITOR_NAMEVERIFICATIONVISITOR_H_

#include "language/tree/ASTNodeFwd.h"
#include "language/stage/verifier/detail/NameShadowVerifier.h"
#include "language/tree/visitor/GenericDoubleVisitor.h"

namespace zillians { namespace language { namespace stage { namespace visitor {

struct NameVerificationVisitor : public tree::visitor::GenericDoubleVisitor
{
    CREATE_INVOKER(verifyInvoker, verify);

    NameVerificationVisitor(NameShadowVerifier& nameShadowVerifier)
        : nameShadowVerifier(&nameShadowVerifier)
    {
        REGISTER_ALL_VISITABLE_ASTNODE(verifyInvoker);
    }

    void verify(tree::ASTNode& node)
    {
        revisit(node);
    }

    void verify(tree::Package& node)
    {
        nameShadowVerifier->enter(node);

        revisit(node);

        nameShadowVerifier->leave(node);
    }

    void verify(tree::Block& node)
    {
        nameShadowVerifier->enter(node);

        revisit(node);

        nameShadowVerifier->leave(node);
    }

    void verify(tree::ClassDecl& node)
    {
        nameShadowVerifier->enter(node);

        revisit(node);

        nameShadowVerifier->leave(node);
    }

    void verify(tree::EnumDecl& node)
    {
        nameShadowVerifier->enter(node);

        revisit(node);

        nameShadowVerifier->leave(node);
    }

    void verify(tree::FunctionDecl& node)
    {
        nameShadowVerifier->enter(node);

        revisit(node);

        nameShadowVerifier->leave(node);
    }

    void verify(tree::VariableDecl& node)
    {
        nameShadowVerifier->enter(node);

        revisit(node);

        nameShadowVerifier->leave(node);
    }

    void verify(tree::TypenameDecl& node)
    {
        nameShadowVerifier->enter(node);

        revisit(node);

        nameShadowVerifier->leave(node);
    }

    NameShadowVerifier* nameShadowVerifier;
};

} } } }

#endif /* ZILLIANS_LANGUAGE_STAGE_VISITOR_NAMEVERIFICATIONVISITOR_H_ */
