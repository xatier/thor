/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_RANGE_UTIL_ZIP_RANGE_H_
#define ZILLIANS_RANGE_UTIL_ZIP_RANGE_H_

#include <boost/iterator/zip_iterator.hpp>
#include <boost/range/begin.hpp>
#include <boost/range/end.hpp>
#include <boost/range/iterator_range.hpp>
#include <boost/tuple/tuple.hpp>

namespace zillians {

template<typename ...range_types>
auto make_zip_range(range_types&&... ranges)
    -> decltype(boost::make_iterator_range(
        boost::make_zip_iterator(
            boost::make_tuple(
                boost::begin(ranges)... // workaround for ADL failure, see https://svn.boost.org/trac/boost/ticket/8571
            )
        ),
        boost::make_zip_iterator(
            boost::make_tuple(
                boost::end(ranges)... // workaround for ADL failure, see https://svn.boost.org/trac/boost/ticket/8571
            )
        )
    ))
{
    return boost::make_iterator_range(
        boost::make_zip_iterator(
            boost::make_tuple(
                boost::begin(ranges)... // workaround for ADL failure, see https://svn.boost.org/trac/boost/ticket/8571
            )
        ),
        boost::make_zip_iterator(
            boost::make_tuple(
                boost::end(ranges)... // workaround for ADL failure, see https://svn.boost.org/trac/boost/ticket/8571
            )
        )
    );
}

}

#endif/*ZILLIANS_RANGE_UTIL_ZIP_RANGE_H_*/
