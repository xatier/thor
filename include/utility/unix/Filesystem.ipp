/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_FILESYSTEM_IPP_
#define ZILLIANS_FILESYSTEM_IPP_

#include <cstdlib>

#include <utility>

#include <boost/range/as_literal.hpp>
#include <boost/range/begin.hpp>
#include <boost/range/end.hpp>
#include <boost/algorithm/string/find_iterator.hpp>
#include <boost/algorithm/string/classification.hpp>
#include <boost/algorithm/string/finder.hpp>

namespace zillians {

auto Filesystem::find_program_by_name(const std::string& program_name) -> boost::filesystem::path 
{
    using path_type = boost::filesystem::path;

    // Check some degenerate cases
    if (program_name.length() == 0) // no program
        return path_type();

    // Use the given path verbatim if it contains any slashes; this matches
    // the behavior of sh(1) and friends.
    if (program_name.find('/') != std::string::npos)
        return path_type(program_name);

    // At this point, the file name is valid and does not contain slashes. Search
    // for it through the directories specified in the PATH environment variable.

    // Get the path. If its empty, we can't do anything to find it.
    const auto *path_var = std::getenv("PATH");
    if (path_var == nullptr)
        return path_type();

    // Now we have a colon separated list of directories to search; try them.
    const auto path_str = boost::as_literal(path_var);
    auto token = boost::algorithm::make_split_iterator(
        path_str,
        boost::algorithm::token_finder(
            boost::is_any_of(":"), boost::algorithm::token_compress_on
        )
    );

    for (decltype(token) end; token != end; ++token)
    {
        auto possible_executable = path_type(boost::begin(*token), boost::end(*token)) / program_name;
        if (is_regular_file(possible_executable) && !access(possible_executable.c_str(), X_OK))
            return std::move(possible_executable);
    }

    return path_type();
}

} // namespace zillians

#endif // ZILLIANS_FILESYSTEM_IPP_
