/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_ARCHIVE_H_
#define ZILLIANS_ARCHIVE_H_

#include <zlib/minizip/zip.h>
#include <zlib/minizip/unzip.h>

namespace zillians {

typedef voidp zip_file_t;

enum ArchiveMode
{
    ARCHIVE_FILE_COMPRESS,
    ARCHIVE_FILE_DECOMPRESS,
};

struct ArchiveItem
{
    ArchiveItem()
    { init(); }

    void init()
    {
        std::memset(&zip_info, 0, sizeof(zip_fileinfo));
        // TODO fill the dates and other fields
    }

    std::vector<unsigned char> buffer;

    // The filename is the name represented in the archive.
    std::string filename;

    union
    {
        zip_fileinfo zip_info;
        unz_file_info64 unzip_info;
    };
};


class Archive
{
public:
    Archive(const std::string& archive_name, ArchiveMode mode);
    virtual ~Archive();

public:
    /**
     * Open the archive, which will create an empty one.
     */
    bool open();

    /**
     * Close the archive
     */
    bool close();

    /**
     * Add series of buffer into the archive with the file name written in the archive file structure.
     *
     * @param archive_item : the structure specify the buffer and filename to store into the archive
     * @return True if success; otherwise, false
     */
    bool add(ArchiveItem& archive_item);

    /**
     * Add a file into the archive in which the file name is the same as the input parameter
     *
     * @param filename : the file name which could be used to access local file
     * @return True if success; otherwise, false
     */
    bool add(const std::string& filename);

    /**
     * Extract all files in the archive, represented as a list of ArchiveItem_t
     *
     * @param archive_items : return a list of archive items
     * @return True if success; otherwise, false
     */
    bool extractAll(std::vector<ArchiveItem>& archive_items);

    /**
     * Extract all files in the archive to the specific folder, also return a list of ArchiveItem_t
     *
     * @param archive_items: return a list of archive items
     * @param folder_path : the folder to place the extracted files
     * @return True if success; otherwise, false
     */
    bool extractAllToFolder(std::vector<ArchiveItem>& archive_items, const std::string& folder_path = "");

    /**
     * Set the compress level. Range is 0~9.
     *
     * @param level : level of compression
     */
    void setCompressLevel(int level);

private:
    bool extractCurrentFile(ArchiveItem& archive_item);

private:
    zip_file_t mArchive;
    std::string mArchiveName;
    ArchiveMode mArchiveMode;

    // Only work for compression
    int mCompressLevel;
};

}

#endif
