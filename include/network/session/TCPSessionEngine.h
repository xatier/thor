/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_NETWORK_SESSION_TCPSESSIONENGINE_H_
#define ZILLIANS_NETWORK_SESSION_TCPSESSIONENGINE_H_

#include <cstdint>

#include <functional>
#include <memory>
#include <utility>
#include <vector>

#include <boost/lexical_cast.hpp>
#include <boost/system/error_code.hpp>

#include "utility/MemoryUtil.h"
#include "utility/StringUtil.h"

#include "threading/Scheduler.h"

#include "network/session/TCPSession.h"

namespace zillians { namespace network { namespace session {

struct TCPSessionEngine
{
    typedef TCPSession                   session_t;
    typedef boost::asio::ip::tcp         protocol_t;

    typedef protocol_t::endpoint         endpoint_t;
    typedef protocol_t::acceptor         acceptor_t;
    typedef protocol_t::resolver         resolver_t;
    typedef boost::system::error_code    error_code_t;

#define RETURN_ON_FAILURE(var)     if (var) return var

    TCPSessionEngine() : current_scheduler_index(0), acceptor_ptr(), resolver_ptr()
    { }

    void addScheduler()
    {
        const auto& scheduler = std::make_shared<Scheduler>();
        addScheduler(scheduler);
    }

    void addScheduler(const shared_ptr<Scheduler>& scheduler)
    {
        schedulers.push_back(scheduler);
    }

    Scheduler& nextScheduler()
    {
        if(schedulers.size() == 0)
        {
            addScheduler();
        }

        uint64 current_index = current_scheduler_index % schedulers.size();
        ++current_scheduler_index;

        return *schedulers[current_index].get();
    }

    TCPSession* createSession()
    {
        TCPSession* p = new TCPSession(nextScheduler());
        return p;
    }

    error_code_t listen(const protocol_t& type, uint16 port)
    {
        error_code_t ec;

        acceptor_t& actor = acceptor();

        actor.open(type, ec);
        RETURN_ON_FAILURE(ec);

        // re-use the address (in some system, the listening port is not free immediately after program exit, so we have to re-use the port)
        boost::asio::socket_base::reuse_address option(true);
        actor.set_option(option, ec);
        RETURN_ON_FAILURE(ec);

        actor.bind(endpoint_t(type, port), ec);
        RETURN_ON_FAILURE(ec);

        actor.listen(SOMAXCONN, ec);

        return ec;
    }

    error_code_t cancel()
    {
        error_code_t ec;

        acceptor_t& actor = acceptor();

        actor.close(ec);

        return ec;
    }

    error_code_t accept(TCPSession* session)
    {
        boost::system::error_code ec;
        acceptor_t& actor = acceptor();

        actor.accept(*session, ec);

        return ec;
    }

    template <typename Handler>
    void acceptAsync(TCPSession* session, Handler handler)
    {
        acceptor_t& actor = acceptor();

        actor.get_io_service().dispatch([=] {
            boost::system::error_code error;
            acceptor().async_accept(*session, handler);
        });
    }

    error_code_t connect(TCPSession* session, const protocol_t& type, const std::string& endpoint_name)
    {
        std::vector<std::string> tokens = zillians::StringUtil::tokenize(endpoint_name, ":", false);

        if(tokens.size() != 2)
            throw std::invalid_argument("given endpoint format is not correct, should be \"host_name:service_name\"");

        return connect(session, type, tokens[0], tokens[1]);
    }

    error_code_t connect(TCPSession* session, const protocol_t& type, const std::string& host_name, const std::string& service_name)
    {
        boost::system::error_code ec;

        resolver_t::query query(type, host_name, service_name);
        resolver_t::iterator it = resolver().resolve(query, ec);
        RETURN_ON_FAILURE(ec);

        session->connect(*it, ec);

        return ec;
    }

    template <typename Handler>
    void connectAsync(TCPSession* session, const protocol_t& type, const std::string& endpoint_name, Handler&& handler)
    {
        std::vector<std::string> tokens = zillians::StringUtil::tokenize(endpoint_name, ":", false);

        if(tokens.size() != 2)
            throw std::invalid_argument("given endpoint format is not correct, should be \"host_name:service_name\"");

        connectAsync(session, type, tokens[0], tokens[1], std::forward<Handler>(handler));
    }

    template <typename Handler>
    void connectAsync(TCPSession* session, const protocol_t& type, const std::string& host_name, std::uint16_t port, Handler&& handler)
    {
        auto port_name = boost::lexical_cast<std::string>(port);

        connectAsync(session, type, host_name, std::move(port_name), std::forward<Handler>(handler));
    }

    template <typename Handler>
    void connectAsync(TCPSession* session, const protocol_t& type, const std::string& host_name, const std::string& service_name, Handler&& handler)
    {
        session->get_io_service().dispatch([=] {
            boost::system::error_code error;
            typename resolver_t::query query(type, host_name, service_name);
            typename resolver_t::iterator it = resolver().resolve(query, error);
            if(error)
                handler(error);

            session->async_connect(*it, boost::bind(handler, _1));
        });
    }

    acceptor_t& acceptor()
    {
        if(acceptor_ptr == nullptr)
            acceptor_ptr = make_unique<acceptor_t>(nextScheduler());
        return *acceptor_ptr;
    }

    resolver_t& resolver()
    {
        if(resolver_ptr == nullptr)
            resolver_ptr = make_unique<resolver_t>(nextScheduler());
        return *resolver_ptr;
    }

#undef RETURN_ON_FAILURE

private:
    int current_scheduler_index;
    std::vector<shared_ptr<Scheduler>> schedulers; // Need to be destroyed before accepter and resolver, since it refers them
    std::unique_ptr<acceptor_t> acceptor_ptr;
    std::unique_ptr<resolver_t> resolver_ptr;
};

} } }

#endif /* ZILLIANS_NETWORK_SESSION_TCPSESSIONENGINE_H_ */
