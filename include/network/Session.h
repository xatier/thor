/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_NETWORK_SESSION_H
#define ZILLIANS_NETWORK_SESSION_H

#include <cstddef>
#include <cstdint>

#include <mutex>
#include <string>
#include <functional>

#include <boost/asio.hpp>
#include <boost/system/error_code.hpp>

namespace zillians { namespace network {

class listener;

class session : public boost::asio::ip::tcp::socket
{
private:
    friend class listener;

    struct header_type
    {
        std::size_t data_size;
    };

public:
    enum transport_type
    {
        TCP_V4, TCP_V6
    };

    class buffer_type : private std::vector<char>
    {
    private:
        using base_type = std::vector<char>;

    public:
        buffer_type() = default;
        buffer_type(const buffer_type&) = default;
        buffer_type(buffer_type&&) = default;
        buffer_type(std::size_t size);

        void append(const void* data, std::size_t size);

        template <typename Target>
        Target& as(std::size_t offset = 0)
        {
            return *reinterpret_cast<Target*>(data() + offset);
        }

        using base_type::resize;
        using base_type::reserve;
        using base_type::data;
        using base_type::size;
    };

    using protocol_type = boost::asio::ip::tcp;
    using resolver_type = protocol_type::resolver;
    using base_type     = protocol_type::socket;

public:
    session(boost::asio::io_service& service);
    ~session();

    boost::system::error_code close();

    // type alias
    using connect_callback_signature = void(session*, const boost::system::error_code&);
    using send_callback_signature    = void(session*, const boost::system::error_code&, std::size_t);
    using receive_callback_signature = void(session*, const boost::system::error_code&, std::size_t);

    using connect_handler_type = std::function<connect_callback_signature>;
    using send_handler_type    = std::function<send_callback_signature>;
    using receive_handler_type = std::function<receive_callback_signature>;

    template <typename ConstantBufferSequence>
    void write(const ConstantBufferSequence& buffers)
    {
        // write header first, then the data
        const header_type header{boost::asio::buffer_size(buffers)};

        boost::system::error_code error;
        const auto transferred_count = boost::asio::write(
            *this,
            boost::asio::buffer(&header, sizeof(header)), error
        );

        if (error || transferred_count != sizeof(header))
        {
            return;
        }

        boost::asio::write(*this, buffers, error);
    }

    // asynchronize operations
    void async_connect(transport_type type, const std::string& host, std::uint16_t port, connect_handler_type handler);
    void async_read(buffer_type* buffer, receive_handler_type handler);

private:
    // read operations
    void async_read_header_completed(header_type* header,
                                     buffer_type* buffer,
                                     receive_handler_type handler,
                                     const boost::system::error_code& error, std::size_t bytes_transferred);

    // attribute modifiers
    bool set_minimal_send(std::size_t bytes);
    bool set_minimal_recv(std::size_t bytes);
    bool set_no_delay(bool no_delay);
};

} }

#endif
