/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

/**
 * This code is copied from Chris Knohlohff who is the author of boost asio.
 * Basically he implements stackless coroutine on top of asio. This can be very useful
 * for implementing application with lots asynchronous operations.
 *
 * We modify the naming convention and the macro name to meet our coding standard.
 *
 * @ref http://blog.think-async.com/2009/07/wife-says-i-cant-believe-it-works.html
 * @ref http://blog.think-async.com/2009/08/composed-operations-coroutines-and-code.html
 * @ref http://blog.think-async.com/2009/08/secret-sauce-revealed.html
 */

#ifndef ZILLIANS_COROUTINE_H_
#define ZILLIANS_COROUTINE_H_

#include <boost/asio.hpp>
#include <boost/asio/error.hpp>

namespace zillians {

class Coroutine
{
public:
    Coroutine() : mInternalCoroutineState(0)
    { }

public:
    /**
     * reset the coroutine state to initial value (so the coroutine state is re-initialized for new asynchronous transaction)
     *
     * @note CAUTION!! DANGEROUS!! ZAC ONLY!! Unless you understand coroutine completely, DO NOT TRY IT without supervision!
     */
    void reset() { mInternalCoroutineState = 0; }

private:
    friend class CoroutineRef;
    int mInternalCoroutineState;
};

class CoroutineRef
{
public:
    CoroutineRef(Coroutine& c) :
        mInternalCoroutineState(c.mInternalCoroutineState)
    {
    }
    CoroutineRef(Coroutine* c) :
        mInternalCoroutineState(c->mInternalCoroutineState)
    {
    }
    operator int() const
    {
        return mInternalCoroutineState;
    }
    int operator= (int state)
    {
        return mInternalCoroutineState = state;
    }

public:
    /**
     * reset the coroutine state to initial value (so the coroutine state is re-initialized for new asynchronous transaction)
     *
     * @note CAUTION!! DANGEROUS!! ZAC ONLY!! Unless you understand coroutine completely, DO NOT TRY IT without supervision!
     */
    void reset() { mInternalCoroutineState = 0; }

protected:
    int& mInternalCoroutineState;
};

#define CoroutineReenter(c) \
  switch (CoroutineRef _coro_value = c)

#define CoroutineEntry \
  extern void you_forgot_to_add_the_entry_label(); \
  bail_out_of_coroutine: break; \
  case 0

#define CoroutineYield \
  if ((_coro_value = __LINE__) == 0) \
  { \
    case __LINE__: ; \
    (void)&you_forgot_to_add_the_entry_label; \
  } \
  else \
    for (bool _coro_bool = false;; _coro_bool = !_coro_bool) \
      if (_coro_bool) \
        goto bail_out_of_coroutine; \
      else

}

#endif /* COROUTINE_H_ */
