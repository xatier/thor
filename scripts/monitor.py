#!/usr/bin/python
#
# Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
#
# This file is part of Thor.
# Thor is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License, version 3,
# as published by the Free Software Foundation.
#
# Thor is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Thor.  If not, see <http://www.gnu.org/licenses/>.
#
# If you want to develop any commercial services or closed-source products with
# Thor, to adapt sources of Thor in your own projects without
# disclosing sources, purchasing a commercial license is mandatory.
#
# For more information, please contact Zillians, Inc.
# <thor@zillians.com>
#

import sys
import subprocess
import time
import getopt


def help():
    print("""
    Usage:

        -h
        --help              help

        -c arg
        --command=arg       running command

        -t value
        --timeout=value     specify the timeout of a process (default: 60 sec)

        -d value
        --delay=value       the delay launching time between two processes (default: 3 sec)

        -h value
        --hang-code=value   define the error code for process hang (default: 188)
          """)

def main(argv):

    print('[Script] monitor.py' + ''.join(argv))

    try:
        opts, args = getopt.getopt(argv, "hc:t:", ["help", "command=", "timeout="])
    except getopt.GetoptError:
        help()
        exit(2)

    command = []
    timeout = 120 # in second
    delay = 3 # in second
    hang_code = 188
    for opt, arg in opts:
        if opt in ("-h", "--help"):
            help()
            exit(0)
        elif opt in ("-c", "--command"):
            command.append( arg )
        elif opt in ("-t", "--timeout"):
            timeout = arg
        elif opt in ("-d", "--delay"):
            delay = arg
        else:
            help()
            exit(0)

    if len(command) == 0:
        help()
        exit(2)

    # Run commands. The definition of success is that all commands should succeed
    processes = []
    for c in command:
        processes.append(subprocess.Popen(c, shell=True))
        time.sleep(delay)

    last = time.time()
    result = {}
    while True:
        for i, p in enumerate(processes):
            if p in result:
                continue

            p.poll()

            if p.returncode != None:
                result[p] = p.returncode
                print('[Monitor.py] executeion ({}) exited with return code:{}'.format(command[i], result[p]))

            if time.time() - last > timeout:
                result[p] = hang_code
                print('[Monitor.py] executeion ({}) hanged'.format(command[i]))

        # if any process report error, we finish
        fail_report = [x for x in list(result.values()) if x != 0]
        if len(fail_report) > 0:
            break

        # all success
        if len([x for x in list(result.values()) if x == 0]) == len(processes):
            break

    # No matter the process is alive or not, try to terminate it
    # to finish those unfinished processes
    for p in processes:
        try:
            p.terminate()
        except:
            pass

    # Report result
    if len(fail_report):
        error_count = len([x for x in fail_report if x != hang_code])
        hang_count = len([x for x in fail_report if x == hang_code])
        print('[Monitor] Error: {} Hang: {}'.format(error_count, hang_count))
        exit(fail_report[0])
    else:
        print('[Monitor] Success!!')
        exit(0)

if __name__ == "__main__":
    main(sys.argv[1:])
