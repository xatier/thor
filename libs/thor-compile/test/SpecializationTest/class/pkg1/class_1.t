/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
import .= dummy;
import .= pkg0;

// generic versions
@static_test { specialization_of = "" } class cls_1  <T   > {}
@static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_TEMPLATE_CLASS_DECLARATIONS" } }
@static_test { specialization_of = "" } class cls_1_d<T   > {}

// incorrect specialized versions of "cls_1_d"
@static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_TEMPLATE_CLASS_DECLARATIONS" } }
@static_test { specialization_of = "" } class cls_1_d<T: int8           > {}
@static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_TEMPLATE_CLASS_DECLARATIONS" } }
@static_test { specialization_of = "" } class cls_1_d<T: int8 , U       > {}
@static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_TEMPLATE_CLASS_DECLARATIONS" } }
@static_test { specialization_of = "" } class cls_1_d<T       , U: int32> {}
@static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_TEMPLATE_CLASS_DECLARATIONS" } }
@static_test { specialization_of = "" } class cls_1_d<T: int8 , U: int32> {}

// specialized versions of "cls_1"
@static_test { specialization_of = "cls_1<T>" } class cls_1<T:             int8         > {}
@static_test { specialization_of = "cls_1<T>" } class cls_1<U:             int32        > {}
@static_test { specialization_of = "cls_1<T>" } class cls_1<V:             cls_0        > {}
@static_test { specialization_of = "cls_1<T>" } class cls_1<W:       Dummy<int8 >       > {}
@static_test { specialization_of = "cls_1<T>" } class cls_1<X: Dummy<Dummy<int32> >     > {}
@static_test { specialization_of = "cls_1<T>" } class cls_1<Y:    function(     ): void > {}
@static_test { specialization_of = "cls_1<T>" } class cls_1<Z:    function(int8 ): int32> {}
