#!/bin/bash
#
# Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
#
# This file is part of Thor.
# Thor is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License, version 3,
# as published by the Free Software Foundation.
#
# Thor is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Thor.  If not, see <http://www.gnu.org/licenses/>.
#
# If you want to develop any commercial services or closed-source products with
# Thor, to adapt sources of Thor in your own projects without
# disclosing sources, purchasing a commercial license is mandatory.
#
# For more information, please contact Zillians, Inc.
# <thor@zillians.com>
#

TEMP_FILE_A=`mktemp`
TEMP_FILE_B=`mktemp`

TS_COMPILE=$1
MODE=$3

COUNTER=1
for ARG in "$@"; do
    if [ $ARG == $TS_COMPILE -o $ARG == $MODE ]; then
        continue
    fi
    echo "file #$COUNTER.."
    echo "filename: $ARG"
    if [ ! -f "$ARG" ]; then
        echo "ERROR: file not found!"
        exit 1
    fi
    if [ $MODE -eq 1 ]; then
        $TS_COMPILE $ARG --root-dir=`dirname $ARG` --mode-parse --debug-parser-ast-with-loc
        ERROR_CODE="$?"
        if [ $ERROR_CODE -ne 0 ]; then
            echo "ERROR: construct/dump ast fail!"
            exit 1
        fi
        continue
    fi
    if [ $MODE -eq 2 ]; then
        $TS_COMPILE $ARG --root-dir=`dirname $ARG` --mode-xform --debug-restructure-stage
        ERROR_CODE="$?"
        if [ $ERROR_CODE -ne 0 ]; then
            echo "ERROR: transform ast fail!"
            exit 1
        fi
        continue
    fi
    if [ $MODE -eq 3 ]; then
        $TS_COMPILE $ARG --root-dir=`dirname $ARG` --mode-parse
        ERROR_CODE="$?"
        if [ $ERROR_CODE -ne 0 ]; then
            echo "ERROR: parse fail!"
            exit 1
        fi
        continue
    fi
    $TS_COMPILE $ARG --root-dir=`dirname $ARG` --mode-parse --debug-parser |& grep -v "[DEBUG]" > $TEMP_FILE_A
    ERROR_CODE="${PIPESTATUS[0]}" # NOTE: need PIPESTATUS because piped grep always succeeds
    if [ $ERROR_CODE -ne 0 ]; then
        cat $TEMP_FILE_A # NOTE: for convenience of reference
        echo "ERROR: parse fail!"
        echo "using temp file: $TEMP_FILE_A (contents shown above)" # NOTE: useful for diagnosing error
        exit 1
    fi

    let COUNTER=$COUNTER+1;
    echo
done

echo "success! (test.sh)"
exit 0

