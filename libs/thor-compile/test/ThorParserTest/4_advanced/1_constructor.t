/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
class my_class
{
    public function new() // NOTE: no return type necessary
    {
    }

    public function new(a:int32, b:float64) // NOTE: no return type necessary
    {
    }

	public function new<T>() {}
	public function new<T>(t: T) {}
	public function new<T, M>() {}
	public function new<T, M>(m: M, t: T) {}
}

function f():void
{
    var a:my_class = new my_class();
    var b:my_class = new my_class(13, 3.14159);
	var c:my_class = new my_class<int32>();
	var d:my_class = new my_class<int32>(10);
	var e:my_class = new my_class<int32, float32>();
	var e:my_class = new my_class<int32, float32>(10, 10.20);
}
