#
# Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
#
# This file is part of Thor.
# Thor is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License, version 3,
# as published by the Free Software Foundation.
#
# Thor is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Thor.  If not, see <http://www.gnu.org/licenses/>.
#
# If you want to develop any commercial services or closed-source products with
# Thor, to adapt sources of Thor in your own projects without
# disclosing sources, purchasing a commercial license is mandatory.
#
# For more information, please contact Zillians, Inc.
# <thor@zillians.com>
#

SET(sources
    "${CMAKE_CURRENT_SOURCE_DIR}/0_modifier.t"
    "${CMAKE_CURRENT_SOURCE_DIR}/1_constructor.t"
    "${CMAKE_CURRENT_SOURCE_DIR}/2_annotation.t"
    "${CMAKE_CURRENT_SOURCE_DIR}/3_instanceof.t"
    "${CMAKE_CURRENT_SOURCE_DIR}/4_typedef.t"
    "${CMAKE_CURRENT_SOURCE_DIR}/5_lambda.t"
    "${CMAKE_CURRENT_SOURCE_DIR}/6_template.t"
    "${CMAKE_CURRENT_SOURCE_DIR}/7_container.t"
#    "${CMAKE_CURRENT_SOURCE_DIR}/8_va_arg.t"
    "${CMAKE_CURRENT_SOURCE_DIR}/9_interface.t"
)

thor_compile_add_parser_test(advanced       0 ${sources})
thor_compile_add_parser_test(ast-advanced   1 ${sources})
thor_compile_add_parser_test(xform-advanced 2 ${sources})
thor_compile_add_parser_test(auto-advanced  3 ${sources})

UNSET(sources)
