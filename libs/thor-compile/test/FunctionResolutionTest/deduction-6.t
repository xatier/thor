/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
class CLS<X, Y>
{
}

//class CLSC<X, Y:int32> extends CLS<X, X>
//{
//}

@static_test { resolution="complex_f_1" }
function complex_f<A, B, C>(a:A, b:B, c:C):void { }

@static_test { resolution="complex_f_2" }
function complex_f<X, Y>(a:CLS<X,Y>, b:X, c:Y):void { }

@static_test { resolution="complex_f_3" }
function complex_f<X>(a:CLS<X,X>, b:X, c:X):void { }

@static_test { resolution="complex_f_4" }
function complex_f<X,Y>(a:CLS<X,Y>, b:X, c:CLS<Y,Y>):void { }
 
function ForTest() : void
{
	var i:int32;
	var j:int64;
	var k:CLS<int64,int32>;
	var l:CLS<int32,int64>;
	var m:CLS<int32,int32>;
	var n:CLS<int64,int64>;
	
	@static_test { expect_resolution="complex_f_1" }
	complex_f(i, j, k);

	@static_test { expect_resolution="complex_f_2" }
	complex_f(k, j, i);

	@static_test { expect_resolution="complex_f_1" }
    complex_f(k, j, j);

	@static_test { expect_resolution="complex_f_2" }
	complex_f(l, i, j);

	@static_test { expect_resolution="complex_f_1" }
	complex_f(l, j, j);

	@static_test { expect_resolution="complex_f_3" }
	complex_f(m, i, i);
	
	@static_test { expect_resolution="complex_f_1" }
	complex_f(m, i, j);

	@static_test { expect_resolution="complex_f_1" }
	complex_f(m, j, j);

	@static_test { expect_resolution="complex_f_4" }
	complex_f(n, j, n);

	@static_test { expect_resolution="complex_f_1" }
    complex_f(n, i, n);
	
	@static_test { expect_resolution="complex_f_4" }
	complex_f(k, j, m);
}
