/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
// overload functions
function overload( i: int32 ): void { }

function overload( f: float64 ): void { }


// template functions
function temp<T>( t: T ): void { }

function temp2<U,V>( u: U, v: V ): void { }


// compiler deduct the template arguments
function deduct_by_argument( param: int32 ): void
{
    switch(param)
    {
    case 0:
        return overload( 123 );
    case 1:
        return overload( 4.56 );
    case 2:
        return temp( 123 );
    case 3:
        return temp2( 123, 456 );
    case 4:
        return temp2( 123, 4.56 );
    }
}


// user specify the template arguments
function specify_template_argument( param: int32 ): void
{
    switch(param)
    {
    case 1:
        return temp<float64>( 123 );
    case 2:
        return temp2<int32>( 123, 4.56 );
    case 3:
        return temp2<int32,float64>( 123, 456 );
    }
}
