#
# Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
#
# This file is part of Thor.
# Thor is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License, version 3,
# as published by the Free Software Foundation.
#
# Thor is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Thor.  If not, see <http://www.gnu.org/licenses/>.
#
# If you want to develop any commercial services or closed-source products with
# Thor, to adapt sources of Thor in your own projects without
# disclosing sources, purchasing a commercial license is mandatory.
#
# For more information, please contact Zillians, Inc.
# <thor@zillians.com>
#

SET(THOR_LANG_OBJECT_FILE "${CMAKE_CURRENT_SOURCE_DIR}/thor/lang/Object.t")
SET(DUMMY_CLS_OBJECT_FILE "${CMAKE_CURRENT_SOURCE_DIR}/dummy/dummy.t"     )

thor_compile_add_simple_resolution_test(function none-template                                 )
thor_compile_add_simple_resolution_test(function one-template                                  )
thor_compile_add_simple_resolution_test(function two-template          ${THOR_LANG_OBJECT_FILE})
thor_compile_add_simple_resolution_test(function class                                         )
thor_compile_add_simple_resolution_test(function nested-template                               )
thor_compile_add_simple_resolution_test(function deduction-1                                   )
thor_compile_add_simple_resolution_test(function deduction-2           ${DUMMY_CLS_OBJECT_FILE})
thor_compile_add_simple_resolution_test(function deduction-3                                   )
thor_compile_add_simple_resolution_test(function deduction-4                                   )
thor_compile_add_simple_resolution_test(function deduction-5                                   )
thor_compile_add_simple_resolution_test(function deduction-6                                   )
thor_compile_add_simple_resolution_test(function deduction-7                                   )
thor_compile_add_simple_resolution_test(function deduction-8                                   )
thor_compile_add_simple_resolution_test(function default-parameter                             )
thor_compile_add_simple_resolution_test(function ambiguous-deduction-1                         )
thor_compile_add_simple_resolution_test(function conversion                                    )
thor_compile_add_simple_resolution_test(function use-self                                      )

SET(DUMMY_CLS_OBJECT_FILE)
SET(THOR_LANG_OBJECT_FILE)
