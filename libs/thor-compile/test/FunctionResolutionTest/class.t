/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
interface I0
{
    @static_test { resolution="I0::x" }
    function x() : void;
}

interface I1
{
    @static_test { resolution="I1::y" }
    @static_test { expect_message={ level="LEVEL_ERROR", id="FUNCTION_CANT_RESOLVE_CANDIDATES", parameters={ candidate="function y():void" } } }
    function y() : void;
}

interface I2 extends I0, I1
{
    @static_test { resolution="I2::y" }
    function y() : void;
}


@static_test { resolution="::a" }
function a() : void {}

class Base implements I2
{
    @static_test { resolution="Base::a" }
    function a() : void {}
    @static_test { resolution="Base::b" }
    @static_test { expect_message={ level="LEVEL_ERROR", id="FUNCTION_CANT_RESOLVE_CANDIDATES", parameters={ candidate="function b():void" } } }
    function b() : void
    {
        @static_test { expect_resolution="I2::y" }
        y();
        @static_test { expect_resolution="Base::a" }
        this.a();
        @static_test { expect_resolution="Base::a" }
        a();
    }
}

class Derived extends Base
{
    @static_test { resolution="Derived::c" }
    function c() : void { }
}

class Extended extends Derived
{
    @static_test { resolution="Extended::c" }
    function c() : void
    {
        @static_test { expect_resolution="Derived::c" }
        super.c();
        @static_test { expect_resolution="Extended::c" }
        this.c();
    }
}

function main() : void
{
    var v : Base;
    v.b();

    var v1 : Derived;
    @static_test { expect_resolution="Base::b" } v1.b();
    @static_test { expect_resolution="I0::x" } v1.x();
    @static_test { expect_resolution="I2::y" } v1.y();

    var v2 : I1;
    //@static_test { expect_resolution="", expect_message={level="LEVEL_ERROR", id="UNDEFINED_SYMBOL_INFO", parameters={id="g"}}  } v2.x();
    @static_test { expect_resolution="I1::y" } v2.y();

    @static_test { expect_message={ level="LEVEL_ERROR", id="FUNCTION_CANT_RESOLVE", parameters={ id="b(int32)" } } }
    @static_test { expect_message={ level="LEVEL_ERROR", id="UNDEFINED_SYMBOL_INFO", parameters={ id="b" } } }
    v1.b(1234);
    @static_test { expect_message={ level="LEVEL_ERROR", id="FUNCTION_CANT_RESOLVE", parameters={ id="y(int32)" } } }
    @static_test { expect_message={ level="LEVEL_ERROR", id="UNDEFINED_SYMBOL_INFO", parameters={ id="y" } } }
    v2.y(1234);
}
