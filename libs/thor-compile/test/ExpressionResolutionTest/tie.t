/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
var a : int32 = 0;
var b : int32 = 0;
var c : int8  = 0;
var d : int16 = 0;

@native function callee() : (int32, int64);

function test_trivial()
{
    @static_test { expect_type = "(int32,int32)" } a, b;
    @static_test { expect_type = "(int32,int8)"  } a, c;
    @static_test { expect_type = "(int32,int16)" } a, d;

    @static_test { expect_type = "(int32,int64)" } callee();
}

function test_simple()
{
    @static_test { expect_type = "(int32,int32)" } a, b = a, b;
    @static_test { expect_type = "(int32,int32)" } a, b = a, c;
    @static_test { expect_type = "(int32,int32)" } a, b = a, d;

    @static_test { expect_type = "(int32,int8)"  } a, c = a, b;
    @static_test { expect_type = "(int32,int8)"  } a, c = a, c;
    @static_test { expect_type = "(int32,int8)"  } a, c = a, d;

    @static_test { expect_type = "(int32,int16)" } a, d = a, b;
    @static_test { expect_type = "(int32,int16)" } a, d = a, c;
    @static_test { expect_type = "(int32,int16)" } a, d = a, d;

    @static_test { expect_type = "(int32,int32)" } a, b = callee();
    @static_test { expect_type = "(int32,int8)"  } a, c = callee();
    @static_test { expect_type = "(int32,int16)" } a, d = callee();
}

function test_nested_1()
{
    @static_test { expect_type = "(int32,int32)" } (a, b = a, b) = a, b;
    @static_test { expect_type = "(int32,int32)" } (a, b = a, b) = a, c;
    @static_test { expect_type = "(int32,int32)" } (a, b = a, b) = a, d;

    @static_test { expect_type = "(int32,int8)"  } (a, c = a, c) = a, b;
    @static_test { expect_type = "(int32,int8)"  } (a, c = a, c) = a, c;
    @static_test { expect_type = "(int32,int8)"  } (a, c = a, c) = a, d;

    @static_test { expect_type = "(int32,int16)" } (a, d = a, d) = a, b;
    @static_test { expect_type = "(int32,int16)" } (a, d = a, d) = a, c;
    @static_test { expect_type = "(int32,int16)" } (a, d = a, d) = a, d;

    @static_test { expect_type = "(int32,int32)" } (a, b = callee()) = a, b;
    @static_test { expect_type = "(int32,int32)" } (a, b = callee()) = a, c;
    @static_test { expect_type = "(int32,int32)" } (a, b = callee()) = a, d;

    @static_test { expect_type = "(int32,int8)"  } (a, c = callee()) = a, b;
    @static_test { expect_type = "(int32,int8)"  } (a, c = callee()) = a, c;
    @static_test { expect_type = "(int32,int8)"  } (a, c = callee()) = a, d;

    @static_test { expect_type = "(int32,int16)" } (a, d = callee()) = a, b;
    @static_test { expect_type = "(int32,int16)" } (a, d = callee()) = a, c;
    @static_test { expect_type = "(int32,int16)" } (a, d = callee()) = a, d;
}

function test_nested_2()
{
    @static_test { expect_type = "(int32,int32)" } a, b = (a, b = a, b);
    @static_test { expect_type = "(int32,int32)" } a, b = (a, c = a, c);
    @static_test { expect_type = "(int32,int32)" } a, b = (a, d = a, d);

    @static_test { expect_type = "(int32,int8)"  } a, c = (a, b = a, b);
    @static_test { expect_type = "(int32,int8)"  } a, c = (a, c = a, c);
    @static_test { expect_type = "(int32,int8)"  } a, c = (a, d = a, d);

    @static_test { expect_type = "(int32,int16)" } a, d = (a, b = a, b);
    @static_test { expect_type = "(int32,int16)" } a, d = (a, c = a, c);
    @static_test { expect_type = "(int32,int16)" } a, d = (a, d = a, d);

    @static_test { expect_type = "(int32,int32)" } a, b = (a, b = callee());
    @static_test { expect_type = "(int32,int32)" } a, b = (a, c = callee());
    @static_test { expect_type = "(int32,int32)" } a, b = (a, d = callee());

    @static_test { expect_type = "(int32,int8)"  } a, c = (a, b = callee());
    @static_test { expect_type = "(int32,int8)"  } a, c = (a, c = callee());
    @static_test { expect_type = "(int32,int8)"  } a, c = (a, d = callee());

    @static_test { expect_type = "(int32,int16)" } a, d = (a, b = callee());
    @static_test { expect_type = "(int32,int16)" } a, d = (a, c = callee());
    @static_test { expect_type = "(int32,int16)" } a, d = (a, d = callee());
}

function test_nested_3()
{
    @static_test { expect_type = "(int32,int32)" } (a, b = a, b) = (a, b = a, b);
    @static_test { expect_type = "(int32,int32)" } (a, b = a, c) = (a, c = a, c);
    @static_test { expect_type = "(int32,int32)" } (a, b = a, d) = (a, d = a, d);

    @static_test { expect_type = "(int32,int8)"  } (a, c = a, b) = (a, b = a, b);
    @static_test { expect_type = "(int32,int8)"  } (a, c = a, c) = (a, c = a, c);
    @static_test { expect_type = "(int32,int8)"  } (a, c = a, d) = (a, d = a, d);

    @static_test { expect_type = "(int32,int16)" } (a, d = a, b) = (a, b = a, b);
    @static_test { expect_type = "(int32,int16)" } (a, d = a, c) = (a, c = a, c);
    @static_test { expect_type = "(int32,int16)" } (a, d = a, d) = (a, d = a, d);

    @static_test { expect_type = "(int32,int32)" } (a, b = a, b) = (a, b = callee());
    @static_test { expect_type = "(int32,int32)" } (a, b = a, c) = (a, c = callee());
    @static_test { expect_type = "(int32,int32)" } (a, b = a, d) = (a, d = callee());

    @static_test { expect_type = "(int32,int8)"  } (a, c = a, b) = (a, b = callee());
    @static_test { expect_type = "(int32,int8)"  } (a, c = a, c) = (a, c = callee());
    @static_test { expect_type = "(int32,int8)"  } (a, c = a, d) = (a, d = callee());

    @static_test { expect_type = "(int32,int16)" } (a, d = a, b) = (a, b = callee());
    @static_test { expect_type = "(int32,int16)" } (a, d = a, c) = (a, c = callee());
    @static_test { expect_type = "(int32,int16)" } (a, d = a, d) = (a, d = callee());

    @static_test { expect_type = "(int32,int32)" } (a, b = callee()) = (a, b = a, b);
    @static_test { expect_type = "(int32,int32)" } (a, b = callee()) = (a, c = a, c);
    @static_test { expect_type = "(int32,int32)" } (a, b = callee()) = (a, d = a, d);

    @static_test { expect_type = "(int32,int8)"  } (a, c = callee()) = (a, b = a, b);
    @static_test { expect_type = "(int32,int8)"  } (a, c = callee()) = (a, c = a, c);
    @static_test { expect_type = "(int32,int8)"  } (a, c = callee()) = (a, d = a, d);

    @static_test { expect_type = "(int32,int16)" } (a, d = callee()) = (a, b = a, b);
    @static_test { expect_type = "(int32,int16)" } (a, d = callee()) = (a, c = a, c);
    @static_test { expect_type = "(int32,int16)" } (a, d = callee()) = (a, d = a, d);
}
