/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
function test_lsh(): void
{
    var b  :bool;
    var i8 :int8;
    var i16:int16;
    var i32:int32;
    var i64:int64;
    var f32:float32;
    var f64:float64;

    @static_test { expect_type = "int32"   } b   << b  ;
    @static_test { expect_type = "int32"   } b   << i8 ;
    @static_test { expect_type = "int32"   } b   << i16;
    @static_test { expect_type = "int32"   } b   << i32;
    @static_test { expect_type = "int32"   } b   << i64;
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_BINARY_OP", parameters = {lhs_type = "bool", rhs_type = "float32", operator_str = "<<"} } }
    @static_test { expect_type = ""        } b   << f32;
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_BINARY_OP", parameters = {lhs_type = "bool", rhs_type = "float64", operator_str = "<<"} } }
    @static_test { expect_type = ""        } b   << f64;

    @static_test { expect_type = "int32"   } i8  << b  ;
    @static_test { expect_type = "int32"   } i8  << i8 ;
    @static_test { expect_type = "int32"   } i8  << i16;
    @static_test { expect_type = "int32"   } i8  << i32;
    @static_test { expect_type = "int32"   } i8  << i64;
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_BINARY_OP", parameters = {lhs_type = "int8", rhs_type = "float32", operator_str = "<<"} } }
    @static_test { expect_type = ""        } i8  << f32;
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_BINARY_OP", parameters = {lhs_type = "int8", rhs_type = "float64", operator_str = "<<"} } }
    @static_test { expect_type = ""        } i8  << f64;

    @static_test { expect_type = "int32"   } i16 << b  ;
    @static_test { expect_type = "int32"   } i16 << i8 ;
    @static_test { expect_type = "int32"   } i16 << i16;
    @static_test { expect_type = "int32"   } i16 << i32;
    @static_test { expect_type = "int32"   } i16 << i64;
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_BINARY_OP", parameters = {lhs_type = "int16", rhs_type = "float32", operator_str = "<<"} } }
    @static_test { expect_type = ""        } i16 << f32;
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_BINARY_OP", parameters = {lhs_type = "int16", rhs_type = "float64", operator_str = "<<"} } }
    @static_test { expect_type = ""        } i16 << f64;

    @static_test { expect_type = "int32"   } i32 << b  ;
    @static_test { expect_type = "int32"   } i32 << i8 ;
    @static_test { expect_type = "int32"   } i32 << i16;
    @static_test { expect_type = "int32"   } i32 << i32;
    @static_test { expect_type = "int32"   } i32 << i64;
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_BINARY_OP", parameters = {lhs_type = "int32", rhs_type = "float32", operator_str = "<<"} } }
    @static_test { expect_type = ""        } i32 << f32;
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_BINARY_OP", parameters = {lhs_type = "int32", rhs_type = "float64", operator_str = "<<"} } }
    @static_test { expect_type = ""        } i32 << f64;

    @static_test { expect_type = "int64"   } i64 << b  ;
    @static_test { expect_type = "int64"   } i64 << i8 ;
    @static_test { expect_type = "int64"   } i64 << i16;
    @static_test { expect_type = "int64"   } i64 << i32;
    @static_test { expect_type = "int64"   } i64 << i64;
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_BINARY_OP", parameters = {lhs_type = "int64", rhs_type = "float32", operator_str = "<<"} } }
    @static_test { expect_type = ""        } i64 << f32;
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_BINARY_OP", parameters = {lhs_type = "int64", rhs_type = "float64", operator_str = "<<"} } }
    @static_test { expect_type = ""        } i64 << f64;

    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_BINARY_OP", parameters = {lhs_type = "float32", rhs_type = "bool"   , operator_str = "<<"} } }
    @static_test { expect_type = ""        } f32 << b  ;
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_BINARY_OP", parameters = {lhs_type = "float32", rhs_type = "int8"   , operator_str = "<<"} } }
    @static_test { expect_type = ""        } f32 << i8 ;
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_BINARY_OP", parameters = {lhs_type = "float32", rhs_type = "int16"  , operator_str = "<<"} } }
    @static_test { expect_type = ""        } f32 << i16;
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_BINARY_OP", parameters = {lhs_type = "float32", rhs_type = "int32"  , operator_str = "<<"} } }
    @static_test { expect_type = ""        } f32 << i32;
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_BINARY_OP", parameters = {lhs_type = "float32", rhs_type = "int64"  , operator_str = "<<"} } }
    @static_test { expect_type = ""        } f32 << i64;
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_BINARY_OP", parameters = {lhs_type = "float32", rhs_type = "float32", operator_str = "<<"} } }
    @static_test { expect_type = ""        } f32 << f32;
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_BINARY_OP", parameters = {lhs_type = "float32", rhs_type = "float64", operator_str = "<<"} } }
    @static_test { expect_type = ""        } f32 << f64;

    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_BINARY_OP", parameters = {lhs_type = "float64", rhs_type = "bool"   , operator_str = "<<"} } }
    @static_test { expect_type = ""        } f64 << b  ;
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_BINARY_OP", parameters = {lhs_type = "float64", rhs_type = "int8"   , operator_str = "<<"} } }
    @static_test { expect_type = ""        } f64 << i8 ;
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_BINARY_OP", parameters = {lhs_type = "float64", rhs_type = "int16"  , operator_str = "<<"} } }
    @static_test { expect_type = ""        } f64 << i16;
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_BINARY_OP", parameters = {lhs_type = "float64", rhs_type = "int32"  , operator_str = "<<"} } }
    @static_test { expect_type = ""        } f64 << i32;
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_BINARY_OP", parameters = {lhs_type = "float64", rhs_type = "int64"  , operator_str = "<<"} } }
    @static_test { expect_type = ""        } f64 << i64;
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_BINARY_OP", parameters = {lhs_type = "float64", rhs_type = "float32", operator_str = "<<"} } }
    @static_test { expect_type = ""        } f64 << f32;
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_BINARY_OP", parameters = {lhs_type = "float64", rhs_type = "float64", operator_str = "<<"} } }
    @static_test { expect_type = ""        } f64 << f64;
}

function test_rsh(): void
{
    var b  :bool;
    var i8 :int8;
    var i16:int16;
    var i32:int32;
    var i64:int64;
    var f32:float32;
    var f64:float64;

    @static_test { expect_type = "int32"   } b   >> b  ;
    @static_test { expect_type = "int32"   } b   >> i8 ;
    @static_test { expect_type = "int32"   } b   >> i16;
    @static_test { expect_type = "int32"   } b   >> i32;
    @static_test { expect_type = "int32"   } b   >> i64;
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_BINARY_OP", parameters = {lhs_type = "bool", rhs_type = "float32", operator_str = ">>"} } }
    @static_test { expect_type = ""        } b   >> f32;
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_BINARY_OP", parameters = {lhs_type = "bool", rhs_type = "float64", operator_str = ">>"} } }
    @static_test { expect_type = ""        } b   >> f64;

    @static_test { expect_type = "int32"   } i8  >> b  ;
    @static_test { expect_type = "int32"   } i8  >> i8 ;
    @static_test { expect_type = "int32"   } i8  >> i16;
    @static_test { expect_type = "int32"   } i8  >> i32;
    @static_test { expect_type = "int32"   } i8  >> i64;
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_BINARY_OP", parameters = {lhs_type = "int8", rhs_type = "float32", operator_str = ">>"} } }
    @static_test { expect_type = ""        } i8  >> f32;
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_BINARY_OP", parameters = {lhs_type = "int8", rhs_type = "float64", operator_str = ">>"} } }
    @static_test { expect_type = ""        } i8  >> f64;

    @static_test { expect_type = "int32"   } i16 >> b  ;
    @static_test { expect_type = "int32"   } i16 >> i8 ;
    @static_test { expect_type = "int32"   } i16 >> i16;
    @static_test { expect_type = "int32"   } i16 >> i32;
    @static_test { expect_type = "int32"   } i16 >> i64;
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_BINARY_OP", parameters = {lhs_type = "int16", rhs_type = "float32", operator_str = ">>"} } }
    @static_test { expect_type = ""        } i16 >> f32;
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_BINARY_OP", parameters = {lhs_type = "int16", rhs_type = "float64", operator_str = ">>"} } }
    @static_test { expect_type = ""        } i16 >> f64;

    @static_test { expect_type = "int32"   } i32 >> b  ;
    @static_test { expect_type = "int32"   } i32 >> i8 ;
    @static_test { expect_type = "int32"   } i32 >> i16;
    @static_test { expect_type = "int32"   } i32 >> i32;
    @static_test { expect_type = "int32"   } i32 >> i64;
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_BINARY_OP", parameters = {lhs_type = "int32", rhs_type = "float32", operator_str = ">>"} } }
    @static_test { expect_type = ""        } i32 >> f32;
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_BINARY_OP", parameters = {lhs_type = "int32", rhs_type = "float64", operator_str = ">>"} } }
    @static_test { expect_type = ""        } i32 >> f64;

    @static_test { expect_type = "int64"   } i64 >> b  ;
    @static_test { expect_type = "int64"   } i64 >> i8 ;
    @static_test { expect_type = "int64"   } i64 >> i16;
    @static_test { expect_type = "int64"   } i64 >> i32;
    @static_test { expect_type = "int64"   } i64 >> i64;
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_BINARY_OP", parameters = {lhs_type = "int64", rhs_type = "float32", operator_str = ">>"} } }
    @static_test { expect_type = ""        } i64 >> f32;
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_BINARY_OP", parameters = {lhs_type = "int64", rhs_type = "float64", operator_str = ">>"} } }
    @static_test { expect_type = ""        } i64 >> f64;

    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_BINARY_OP", parameters = {lhs_type = "float32", rhs_type = "bool"   , operator_str = ">>"} } }
    @static_test { expect_type = ""        } f32 >> b  ;
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_BINARY_OP", parameters = {lhs_type = "float32", rhs_type = "int8"   , operator_str = ">>"} } }
    @static_test { expect_type = ""        } f32 >> i8 ;
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_BINARY_OP", parameters = {lhs_type = "float32", rhs_type = "int16"  , operator_str = ">>"} } }
    @static_test { expect_type = ""        } f32 >> i16;
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_BINARY_OP", parameters = {lhs_type = "float32", rhs_type = "int32"  , operator_str = ">>"} } }
    @static_test { expect_type = ""        } f32 >> i32;
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_BINARY_OP", parameters = {lhs_type = "float32", rhs_type = "int64"  , operator_str = ">>"} } }
    @static_test { expect_type = ""        } f32 >> i64;
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_BINARY_OP", parameters = {lhs_type = "float32", rhs_type = "float32", operator_str = ">>"} } }
    @static_test { expect_type = ""        } f32 >> f32;
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_BINARY_OP", parameters = {lhs_type = "float32", rhs_type = "float64", operator_str = ">>"} } }
    @static_test { expect_type = ""        } f32 >> f64;

    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_BINARY_OP", parameters = {lhs_type = "float64", rhs_type = "bool"   , operator_str = ">>"} } }
    @static_test { expect_type = ""        } f64 >> b  ;
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_BINARY_OP", parameters = {lhs_type = "float64", rhs_type = "int8"   , operator_str = ">>"} } }
    @static_test { expect_type = ""        } f64 >> i8 ;
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_BINARY_OP", parameters = {lhs_type = "float64", rhs_type = "int16"  , operator_str = ">>"} } }
    @static_test { expect_type = ""        } f64 >> i16;
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_BINARY_OP", parameters = {lhs_type = "float64", rhs_type = "int32"  , operator_str = ">>"} } }
    @static_test { expect_type = ""        } f64 >> i32;
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_BINARY_OP", parameters = {lhs_type = "float64", rhs_type = "int64"  , operator_str = ">>"} } }
    @static_test { expect_type = ""        } f64 >> i64;
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_BINARY_OP", parameters = {lhs_type = "float64", rhs_type = "float32", operator_str = ">>"} } }
    @static_test { expect_type = ""        } f64 >> f32;
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_BINARY_OP", parameters = {lhs_type = "float64", rhs_type = "float64", operator_str = ">>"} } }
    @static_test { expect_type = ""        } f64 >> f64;
}
