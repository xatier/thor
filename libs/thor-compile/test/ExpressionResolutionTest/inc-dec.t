/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
function test_inc(): void
{
    var b  :bool;
    var i8 :int8;
    var i16:int16;
    var i32:int32;
    var i64:int64;
    var f32:float32;
    var f64:float64;

    @static_test { expect_type = "bool"    } ++ b     ;
    @static_test { expect_type = "int8"    } ++ i8    ;
    @static_test { expect_type = "int16"   } ++ i16   ;
    @static_test { expect_type = "int32"   } ++ i32   ;
    @static_test { expect_type = "int64"   } ++ i64   ;
    @static_test { expect_type = "float32" } ++ f32   ;
    @static_test { expect_type = "float64" } ++ f64   ;
}

function test_dec(): void
{
    var b  :bool;
    var i8 :int8;
    var i16:int16;
    var i32:int32;
    var i64:int64;
    var f32:float32;
    var f64:float64;

    @static_test { expect_type = "bool"    } -- b     ;
    @static_test { expect_type = "int8"    } -- i8    ;
    @static_test { expect_type = "int16"   } -- i16   ;
    @static_test { expect_type = "int32"   } -- i32   ;
    @static_test { expect_type = "int64"   } -- i64   ;
    @static_test { expect_type = "float32" } -- f32   ;
    @static_test { expect_type = "float64" } -- f64   ;
}
