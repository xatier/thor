/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
// should success
enum E1
{
    SRC = cast<int32>(E2.DOWN),
    DEST = 2
}

enum E2
{
    DOWN = cast<int32>(E3.RIGHT),
    UP = cast<int32>(E1.DEST) + 1
}

enum E3
{
    RIGHT = cast<int32>(E2.UP)
}

// should failure
enum E4
{
    SRC = cast<int32>(E5.DEST),

    @static_test { expect_message={ level="LEVEL_ERROR", id="CIRCULAR_ENUM_DEPEND" } }
    @static_test { expect_message={ level="LEVEL_ERROR", id="CIRCULAR_ENUM_DEPEND" } }
    @static_test { expect_message={ level="LEVEL_ERROR", id="CIRCULAR_ENUM_DEPEND" } }
    DOWN = cast<int32>(E5.DEST)
}

enum E5
{
    @static_test { expect_message={ level="LEVEL_ERROR", id="CIRCULAR_ENUM_DEPEND" } }
    @static_test { expect_message={ level="LEVEL_ERROR", id="CIRCULAR_ENUM_DEPEND" } }
    @static_test { expect_message={ level="LEVEL_ERROR", id="CIRCULAR_ENUM_DEPEND" } }
    @static_test { expect_message={ level="LEVEL_ERROR", id="CIRCULAR_ENUM_DEPEND" } }
    @static_test { expect_message={ level="LEVEL_ERROR", id="CIRCULAR_ENUM_DEPEND" } }
    @static_test { expect_message={ level="LEVEL_ERROR", id="CIRCULAR_ENUM_DEPEND" } }
    DEST = cast<int32>(E6.RIGHT),

    @static_test { expect_message={ level="LEVEL_ERROR", id="CIRCULAR_ENUM_DEPEND" } }
    @static_test { expect_message={ level="LEVEL_ERROR", id="CIRCULAR_ENUM_DEPEND" } }
    @static_test { expect_message={ level="LEVEL_ERROR", id="CIRCULAR_ENUM_DEPEND" } }
    UP = cast<int32>(E4.DOWN)
}

enum E6
{
    @static_test { expect_message={ level="LEVEL_ERROR", id="CIRCULAR_ENUM_DEPEND" } }
    @static_test { expect_message={ level="LEVEL_ERROR", id="CIRCULAR_ENUM_DEPEND" } }
    @static_test { expect_message={ level="LEVEL_ERROR", id="CIRCULAR_ENUM_DEPEND" } }
    @static_test { expect_message={ level="LEVEL_ERROR", id="CIRCULAR_ENUM_DEPEND" } }
    RIGHT = cast<int32>(E6.UP),

    @static_test { expect_message={ level="LEVEL_ERROR", id="CIRCULAR_ENUM_DEPEND" } }
    @static_test { expect_message={ level="LEVEL_ERROR", id="CIRCULAR_ENUM_DEPEND" } }
    @static_test { expect_message={ level="LEVEL_ERROR", id="CIRCULAR_ENUM_DEPEND" } }
    @static_test { expect_message={ level="LEVEL_ERROR", id="CIRCULAR_ENUM_DEPEND" } }
    UP = cast<int32>(E5.UP)
}
