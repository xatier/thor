/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <functional>
#include <vector>
#include <utility>

#include <boost/assert.hpp>
#include <boost/algorithm/cxx11/all_of.hpp>
#include <boost/range/adaptor/transformed.hpp>
#include <boost/range/algorithm_ext/push_back.hpp>
#include <boost/range/begin.hpp>
#include <boost/range/end.hpp>

#include "utility/Functional.h"

#include "language/tree/ASTNode.h"
#include "language/tree/UniqueName.h"
#include "language/tree/basic/DeclType.h"
#include "language/tree/basic/PrimitiveType.h"
#include "language/tree/basic/Type.h"
#include "language/tree/declaration/ClassDecl.h"
#include "language/tree/declaration/Declaration.h"
#include "language/tree/module/Internal.h"
#include "language/tree/module/Package.h"

#include "language/stage/tree_refactor/detail/UniqueNameCacher.h"

namespace zillians { namespace language {

namespace {

template<typename map_type, typename key_type>
typename map_type::mapped_type find_value_or_null(map_type& map, key_type&& key)
{
    const auto& pos = map.find(std::forward<key_type>(key));

    if (pos == boost::end(map))
        return nullptr;

    return pos->second;
}

template<typename map_type, typename node_type, typename unique_name_type>
bool insert_with_unique_name(map_type& map, node_type& node, unique_name_type&& unique_name)
{
    // if node is from parser, there will be no unique name ready.
    // But we can safely ignore them!
    if (unique_name.empty())
        return false;

    const auto& insertion_result = map.emplace(std::forward<unique_name_type>(unique_name), &node);
    const auto& is_inserted      = insertion_result.second;

    BOOST_ASSERT(is_inserted && "duplicated unique name!?");

    return is_inserted;
}

template<typename map_type, typename node_type>
bool insert_with_unique_name(map_type& map, node_type& node)
{
    const auto& unique_name = node.getUniqueName();

    return insert_with_unique_name(map, node, unique_name);
}

template<typename target_type = tree::Type>
bool lookup_and_store_type(unique_name_cacher& cacher, const tree::unique_name_t& name, target_type*& ptr_ref)
{
    if (name.empty())
    {
        ptr_ref = nullptr;

        return true;
    }

    auto*const found_type = cacher.lookup_type(name);

    if (found_type == nullptr)
        return false;

    auto*const found_target_type = tree::cast<target_type>(found_type);

    if (found_target_type == nullptr)
        return false;

    ptr_ref = found_target_type;

    return true;
}

template<typename target_type = tree::Type>
bool lookup_and_store_types(unique_name_cacher& cacher, const std::vector<tree::unique_name_t>& names, std::vector<target_type*>& ptrs_ref)
{
    ptrs_ref.resize(names.size());

          auto  names_i    = boost::begin(names);
    const auto& names_end  = boost::end  (names);
          auto  ptrs_ref_i = boost::begin(ptrs_ref);

    for (; names_i != names_end; ++names_i, ++ptrs_ref_i)
    {
        const auto&  name    = *names_i;
              auto*& ptr_ref = *ptrs_ref_i;

        BOOST_ASSERT(!name.empty() && "there should be valid unique names");

        if (name.empty())
            return false;

        if (!lookup_and_store_type(cacher, name, ptr_ref))
            return false;

        BOOST_ASSERT(ptr_ref != nullptr && "unexpected result of lookup_and_store_type");
    }

    return true;
}

}

unique_name_cacher::unique_name_cacher(tree::Internal& internal)
    : internal(&internal)
    , mapping_decl()
    , mapping_pkg ()
    , mapping_type()
{
    constexpr tree::PrimitiveKind::kind primitive_kinds[] = {
        tree::PrimitiveKind::VOID_TYPE   ,
        tree::PrimitiveKind::BOOL_TYPE   ,
        tree::PrimitiveKind::INT8_TYPE   ,
        tree::PrimitiveKind::INT16_TYPE  ,
        tree::PrimitiveKind::INT32_TYPE  ,
        tree::PrimitiveKind::INT64_TYPE  ,
        tree::PrimitiveKind::FLOAT32_TYPE,
        tree::PrimitiveKind::FLOAT64_TYPE,
    };

    for (const auto& primitive_kind : primitive_kinds)
    {
        BOOST_ASSERT(internal.getPrimitiveType(primitive_kind) != nullptr && "no primitive type!?");

        auto& primitive_type = *internal.getPrimitiveType(primitive_kind);
        auto  unique_name    = tree::create_unique_name(primitive_type);

        insert_with_unique_name(mapping_type, primitive_type, std::move(unique_name));
    }
}

tree::Declaration* unique_name_cacher::lookup_decl(const tree::unique_name_t& unique_name)
{
    if (auto*const decl = find_value_or_null(mapping_decl, unique_name))
        return decl;

    BOOST_ASSERT(false && "non-registered declaration!");
    return nullptr;
}

tree::Package* unique_name_cacher::lookup_package(const tree::unique_name_t& unique_name)
{
    if (auto*const pkg = find_value_or_null(mapping_pkg, unique_name))
        return pkg;

    BOOST_ASSERT(false && "non-registered package!");
    return nullptr;
}

tree::Type* unique_name_cacher::lookup_type(const tree::unique_name_t& unique_name)
{
    if (auto*const type = find_value_or_null(mapping_type, unique_name))
        return type;

    tree::Type* type_created = nullptr;

    if (tree::is_function_type_unique_name(unique_name))
        type_created = create_type_function(unique_name);
    else if (tree::is_multitype_unique_name(unique_name))
        type_created = create_type_multi(unique_name);
    else
        type_created = nullptr;

    if (type_created != nullptr)
    {
        const auto& insertion_result = mapping_type.emplace(unique_name, type_created);
        const auto& is_inserted      = insertion_result.second;

        BOOST_ASSERT(is_inserted && "duplicated type?");
    }
    else
    {
        BOOST_ASSERT(false && "non-registered type!");
    }

    return type_created;
}

bool unique_name_cacher::register_decl(tree::Declaration& decl)
{
    return insert_with_unique_name(mapping_decl, decl);
}

bool unique_name_cacher::register_package(tree::Package& pkg)
{
    return insert_with_unique_name(mapping_pkg, pkg);
}

bool unique_name_cacher::register_type(tree::Type& type)
{
    return insert_with_unique_name(mapping_type, type);
}

tree::FunctionType* unique_name_cacher::create_type_function(const tree::unique_name_t& unique_name)
{
    using boost::adaptors::transformed;

    const auto&  class_name  = tree::extract_class_type_name(unique_name);
    const auto& return_name  = tree::extract_return_type_name(unique_name);
    const auto&  param_names = tree::extract_param_type_names(unique_name);

    tree::RecordType*         class_type  = nullptr;
    tree::Type*              return_type  = nullptr;
    std::vector<tree::Type*>  param_types;

    const auto& is__class_type__found = lookup_and_store_type (*this,  class_name ,  class_type );
    const auto& is_return_type__found = lookup_and_store_type (*this, return_name , return_type );
    const auto& is__param_types_found = lookup_and_store_types(*this,  param_names,  param_types);

    const auto& is_all_found          = is__class_type__found &&
                                        is_return_type__found &&
                                        is__param_types_found
                                      ;

    BOOST_ASSERT(is_all_found && "not all type is found, please verify type re-creation is correct");

    if (!is_all_found)
        return nullptr;

    auto*const function_type = internal->addFunctionType(std::move(param_types), return_type, class_type);

    return function_type;
}

tree::MultiType* unique_name_cacher::create_type_multi(const tree::unique_name_t& unique_name)
{
    const auto&              sub_names = tree::extract_subtype_names(unique_name);
    std::vector<tree::Type*> sub_types;

    const auto& is_sub_types_found = lookup_and_store_types(*this, sub_names, sub_types);
    const auto& is_all_found       = is_sub_types_found;

    BOOST_ASSERT(is_all_found && "not all type is found, please verify type re-creation is correct");

    if (!is_all_found)
        return nullptr;

    auto*const multi_type  = internal->addMultiType(std::move(sub_types));

    return multi_type;
}

} }
