/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
#undef _GLIBCXX_ATOMIC_BUILTINS
#undef _GLIBCXX_USE_INT128

#include <cuda_runtime.h>
#include <stdio.h>
#include <thrust/host_vector.h>
#include <thrust/device_vector.h>
#include <algorithm>
#include <cstdlib>
#include <ctime>

using namespace std;

#define CUDA_DISPATCHER_THREADS_PER_BLOCK	32
#define CUDA_DISPATCHER_WARP_SIZE		32
#define CUDA_DISPATCHER_WARP_MASK		0x3F
#define CUDA_INVOCATION_PARAMETER_SIZE  	256
#define CUDA_INVOCATION_PARAMETER_LOAD_SIZE   	8

#if CUDA_INVOCATION_PARAMETER_SIZE % (CUDA_DISPATCHER_WARP_SIZE*CUDA_INVOCATION_PARAMETER_LOAD_SIZE) != 0
    #error "parameter size must be multiple of warp load size"
#endif

#if __INVOCATION_PARAMETER_ELEMENTS*CUDA_DISPATCHER_THREADS_PER_BLOCK > 16000
    #error "required shared memory size too large"
#endif

#define __INVOCATION_PARAMETER_ELEMENTS      (CUDA_INVOCATION_PARAMETER_SIZE/CUDA_INVOCATION_PARAMETER_LOAD_SIZE)

#define CEILING(x,y) (((x) + (y) - 1) / (y))

#define __TOTAL_PARAMETER_SIZE_PER_BLOCK  (CUDA_DISPATCHER_THREADS_PER_BLOCK*CUDA_INVOCATION_PARAMETER_SIZE)
#define __TOTAL_LOAD_SIZE_PER_WARP        (CUDA_DISPATCHER_WARP_SIZE * CUDA_INVOCATION_PARAMETER_LOAD_SIZE)
#define __TOTAL_LOADER_WARP_COUNT         (__TOTAL_PARAMETER_SIZE_PER_BLOCK/__TOTAL_LOAD_SIZE_PER_WARP)
#define __TOTAL_WARPS_PER_INVOCATION      (CUDA_INVOCATION_PARAMETER_SIZE/__TOTAL_LOAD_SIZE_PER_WARP)
#define __TOTAL_WARPS_PER_BLOCK           (CUDA_DISPATCHER_THREADS_PER_BLOCK/CUDA_DISPATCHER_WARP_SIZE)
#define __TOTAL_WORKING_ITEMS_PER_WARP    CEILING(__TOTAL_LOADER_WARP_COUNT,__TOTAL_WARPS_PER_BLOCK)

typedef char int8;
typedef short int16;
typedef int int32;
typedef long long int64;

template<int Size>
struct type_selector;

template<>
struct type_selector<1> {
	typedef int8 type;
};

template<>
struct type_selector<2> {
	typedef int16 type;
};

template<>
struct type_selector<4> {
	typedef int32 type;
};

template<>
struct type_selector<8> {
	typedef int64 type;
};

typedef type_selector<CUDA_INVOCATION_PARAMETER_LOAD_SIZE>::type element_type;


__device__ void load(int8* shm, int8* parameters, int32* shuffle_indices, int32 total_invocation)
{
	int tid = blockIdx.x * blockDim.x + threadIdx.x;
	//printf("blockIdx.x = %d, blockDim.x = %d, tid = %d\n", blockIdx.x, blockDim.x, tid);
	int32 warp_id = threadIdx.x / CUDA_DISPATCHER_WARP_SIZE;
	int32 wid = threadIdx.x % CUDA_DISPATCHER_WARP_SIZE;
	int32 base_id = blockIdx.x * blockDim.x;

	element_type* typed_shm = (element_type*)shm;
	element_type* typed_parameters = (element_type*)parameters;

#if __TOTAL_LOADER_WARP_COUNT == CUDA_DISPATCHER_WARP_SIZE && false
	int32 shuffled_index = -1;
	int32 current_row = base_id;


	#pragma unroll 32
	for(int j=0;j<__TOTAL_WORKING_ITEMS_PER_WARP;++j)
	{
		shuffled_index = shuffle_indices[current_row];

		if(current_row >= total_invocation)
			break;

		// load from global memory into shared memory
		int32 to_index = j * __INVOCATION_PARAMETER_ELEMENTS + threadIdx.x;
		int32 from_index = shuffled_index * __INVOCATION_PARAMETER_ELEMENTS + threadIdx.x;
		//printf("tid=%d, shuffled_index=%d, to_idx=%d, from_idx=%d\n", tid, shuffled_index, to_index, from_index);
		typed_shm[to_index] = typed_parameters[from_index];

		// load next item
		++current_row;
	}
#else
	int32 shuffled_index = -1;
	int32 last_row = -1;
	int32 working_index = warp_id;
	int32 current_row = base_id + working_index / __TOTAL_WARPS_PER_INVOCATION;

	#pragma unroll 32
	for(int j=0;j<__TOTAL_WORKING_ITEMS_PER_WARP;++j)
	{
		if(current_row != last_row)
			shuffled_index = shuffle_indices[current_row];

		if(current_row >= total_invocation)
			break;

		// load from global memory into shared memory
		int32 local_index = working_index % __TOTAL_WARPS_PER_INVOCATION;
		int32 to_index = working_index * CUDA_DISPATCHER_WARP_SIZE + wid;
		int32 from_index = shuffled_index * __INVOCATION_PARAMETER_ELEMENTS + local_index * CUDA_DISPATCHER_WARP_SIZE + wid;
		//printf("tid=%d, wid=%d, warp_id=%d, working_idx=%d, current_row=%d, shuffled_idx=%d, local_index=%d, to_idx=%d, from_idx=%d\n", tid, wid, warp_id, working_index, current_row, shuffled_index, local_index, to_index, from_index);
		typed_shm[to_index] = typed_parameters[from_index];

		// load next item
		working_index += __TOTAL_WARPS_PER_BLOCK;
		last_row = current_row;
		current_row = base_id + working_index / __TOTAL_WARPS_PER_INVOCATION;
	}
#endif
}

// use shared memory
__global__ void add_v2(element_type* parameters, int32* shuffle_indices, int total_invocation, element_type* result)
{
	__shared__ element_type shm[__INVOCATION_PARAMETER_ELEMENTS*CUDA_DISPATCHER_THREADS_PER_BLOCK];

	int tid = blockIdx.x * blockDim.x + threadIdx.x;
	load((char*)shm, (char*)parameters, shuffle_indices, total_invocation);
	//return;
	__syncthreads();
	if(tid < total_invocation)
	{
		element_type r = 0;
		for(int i=0;i<__INVOCATION_PARAMETER_ELEMENTS;++i)
		{
			//printf("tid=%d, i=%d, shm[i] = %d\n", tid, i, shm[threadIdx.x * __INVOCATION_PARAMETER_ELEMENTS + i]);
			r += shm[threadIdx.x * __INVOCATION_PARAMETER_ELEMENTS + i];
		}
		result[tid] = r;
	}
}

// use texture fetch

//

#define TOTAL_COUNT   65536
#define TOTAL_ITERATIONS 1000
int main()
{
	typedef type_selector<CUDA_INVOCATION_PARAMETER_LOAD_SIZE>::type element_type;

	thrust::host_vector<element_type> h_parameters(TOTAL_COUNT*__INVOCATION_PARAMETER_ELEMENTS);
	thrust::host_vector<int32> h_shuffle_indices(TOTAL_COUNT);
	thrust::host_vector<element_type> h_result(TOTAL_COUNT);

	for(int i=0;i<TOTAL_COUNT;++i)
	{
		for(int j=0;j<__INVOCATION_PARAMETER_ELEMENTS;++j)
		{
			h_parameters[i*__INVOCATION_PARAMETER_ELEMENTS+j] = i;
		}
	}
	for(int i=0;i<TOTAL_COUNT;++i)
	{
		h_shuffle_indices[i] = i;
	}
	for(int i=0;i<TOTAL_COUNT;++i)
	{
		h_result[i] = 0;
	}

//	srand(time(NULL));
//	random_shuffle(h_shuffle_indices.begin(), h_shuffle_indices.end());

	thrust::device_vector<element_type> d_parameters = h_parameters;
	thrust::device_vector<int32> d_shuffle_indices = h_shuffle_indices;
	thrust::device_vector<element_type> d_result = h_result;

	thrust::device_ptr<element_type> dp_parameters = d_parameters.data();
	thrust::device_ptr<int32> dp_shuffle_indices = d_shuffle_indices.data();
	thrust::device_ptr<element_type> dp_result = d_result.data();

	cudaEvent_t start, stop;
	cudaEventCreate(&start);
	cudaEventCreate(&stop);

	cudaEventRecord(start, 0);

	for(int iter=0;iter<TOTAL_ITERATIONS;++iter)
	{
	add_v2<<<CEILING(TOTAL_COUNT,CUDA_DISPATCHER_THREADS_PER_BLOCK), CUDA_DISPATCHER_THREADS_PER_BLOCK>>>(
		thrust::raw_pointer_cast(dp_parameters),
		thrust::raw_pointer_cast(dp_shuffle_indices),
		TOTAL_COUNT,
		thrust::raw_pointer_cast(dp_result));
	}

	cudaEventRecord(stop, 0);

//	cudaDeviceSynchronize();
	cudaEventSynchronize(stop);

	float elapsedTime;
	cudaEventElapsedTime(&elapsedTime , start, stop);

	h_result = d_result;

	/*
	for(int i=0;i<TOTAL_COUNT;++i)
	{
		printf("result[%d] = %d\n", i, h_result[i]);
	}
	*/

	printf("total time = %f ms\n", elapsedTime/(float)TOTAL_ITERATIONS);

	return 0;
}
