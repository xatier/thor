#
# Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
#
# This file is part of Thor.
# Thor is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License, version 3,
# as published by the Free Software Foundation.
#
# Thor is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Thor.  If not, see <http://www.gnu.org/licenses/>.
#
# If you want to develop any commercial services or closed-source products with
# Thor, to adapt sources of Thor in your own projects without
# disclosing sources, purchasing a commercial license is mandatory.
#
# For more information, please contact Zillians, Inc.
# <thor@zillians.com>
#
#==========================================================
# Supposed Make Target Chain Reaction:
#
# thor-framework-vw
#     --> thor-framework-generate-buffer
#         --> thor-framework-vw-buffers-XXX-service
#             --> XXXServiceBuffer.h
#
#==========================================================


set(__optional_include_dirs "")

if(ENABLE_FEATURE_NETWORK_ADDON)
    if(NOT DEFINED PROJECT_NETWORK_ADDON_SOURCE_DIR)
        message(ERROR "missing network addon source")
    endif()
    list(APPEND __optional_include_dirs ${PROJECT_NETWORK_ADDON_SOURCE_DIR}/include/)
endif()

if(TBB_FOUND)
    list(APPEND __optional_include_dirs ${TBB_INCLUDE_DIR}/) 
endif()

include_directories(
    ${THOR_SYSTEM_BUNDLE_INCLUDE_DIR}
	${TBB_INCLUDE_DIR}/
    )

set(src_files
    src/buffer/init/HostBufferKernelInit.cpp
    src/buffer/BufferKernel.cpp
    src/buffer/BufferManager.cpp
    src/buffer/BufferManagerWrapper.cpp
    src/buffer/BufferNetwork.cpp
    src/detail/GlobalDispatcherGenerator.cpp
    src/detail/GlobalOffsetsComputor.cpp
    src/Configuration.cpp
    src/Executor.cpp
    src/ExecutorRemote.cpp
    )

if(ENABLE_FEATURE_CUDA)
    list(APPEND src_files
        src/buffer/init/CudaBufferKernelInit.cpp
        )
endif()

add_library(thor-framework ${src_files})

unset(src_files)

set(__optional_dependencies "")
if(CUDA_FOUND AND ENABLE_FEATURE_CUDA)
    list(APPEND __optional_dependencies ${CUDA_LIBRARIES})
endif()

if(RDMA_FOUND AND ENABLE_FEATURE_RDMA AND ENABLE_FEATURE_NETWORK_ADDON)
    list(APPEND __optional_dependencies thor-network-addon-grid-infiniband)
endif() 

if(TBB_FOUND)
    list(APPEND __optional_dependencies ${TBB_LIBRARIES})
endif()

target_link_libraries(thor-framework 
    thor-common-core
    thor-common-utility
    thor-network
    thor-language-tree
    thor-language-serialization
    ${__optional_dependencies})

ADD_SUBDIRECTORY(test)
