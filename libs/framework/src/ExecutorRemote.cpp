/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <cassert>
#include <cstdint>

#include <deque>
#include <functional>
#include <mutex>
#include <sstream>
#include <queue>
#include <tuple>
#include <type_traits>

#include <boost/archive/binary_iarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/asio.hpp>
#include <boost/assert.hpp>
#include <boost/bimap.hpp>

#include <tbb/concurrent_hash_map.h>

#include "network/Session.h"
#include "framework/Executor.h"
#include "framework/InvocationRequest.h"
#include "framework/ExecutorRemote.h"

#include "thor/lang/Domain.h"

namespace zillians { namespace framework {

namespace {

    executor_remote::local_id_type extract_local_id(std::int64_t session_id)
    {
        constexpr auto local_id_total_bits = CHAR_BIT * sizeof(executor_remote::local_id_type);
        constexpr auto ones                = std::make_unsigned<std::int64_t>::type(-1);
        constexpr auto mask                = (ones << local_id_total_bits) >> local_id_total_bits;

        return session_id & mask;
    }
}

executor_remote::executor_remote(executor_rt& local)
    : executor(0, "framework.executor_remote")
    , next_local_id(0)
    , local(local)
{

}

void executor_remote::send_invocation_request(std::int64_t func_id, std::int64_t session_id, std::deque<std::int8_t>&& parameters)
{
    std::lock_guard<std::mutex> lock{tasks_mutex};
    pending_tasks.emplace(session_id, invocation_request{func_id, std::move(parameters)});
}

void executor_remote::do_work()
{
    send_invocation_requests_to_remote();
}

void executor_remote::send_invocation_requests_to_remote()
{
    decltype(pending_tasks) sending_tasks;
    {
        std::lock_guard<std::mutex> lock{tasks_mutex};
        sending_tasks.swap(pending_tasks);
    }

    for (;!sending_tasks.empty(); sending_tasks.pop())
    {
        const auto& current_task = sending_tasks.front();

        boost::asio::streambuf buffer;
        boost::archive::binary_oarchive archive(buffer);
        const auto& request = std::get<1>(current_task);
        archive & request;

        const auto& session_id = std::get<0>(current_task);
        auto*const  target = get_session(session_id);
        assert(target != nullptr && "invalid session id");

        target->write(buffer.data());
    }
}

network::session* executor_remote::get_session(std::int64_t session_id)
{
    std::lock_guard<std::mutex> lock{sessions_mutex};

    auto& local_id_to_session = local_id_and_sessions.left;
    const auto found = local_id_to_session.find(extract_local_id(session_id));
    if (found == local_id_to_session.end())
    {
        return nullptr;
    }

    return found->second;
}


network::session::buffer_type* executor_remote::create_read_buffer_for(network::session* session)
{
    decltype(read_buffers)::accessor accessor;

    using buffer_type = network::session::buffer_type;

    const bool success = read_buffers.insert(
        accessor,
        std::make_pair(session, std::make_shared<buffer_type>())
    );

    if (!success)
    {
        return nullptr;
    }

    return (accessor->second).get();
}

network::session::buffer_type* executor_remote::get_read_buffer(network::session* session)
{
    decltype(read_buffers)::const_accessor accessor;

    if (!read_buffers.find(accessor, session))
    {
        return nullptr;
    }

    return (accessor->second).get();
}

void executor_remote::receive_invocation_request(network::session* session, network::session::receive_handler_type handler)
{
    auto* read_buffer = get_read_buffer(session);
    assert(read_buffer != nullptr && "can not get read buffer for session.");

    if (read_buffer == nullptr)
    {
        return;
    }

    session->async_read(
        read_buffer, 
        std::bind(&executor_remote::process_read_data, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::move(handler))
    );
}

bool executor_remote::add(network::session* session)
{
    std::lock_guard<std::mutex> lock{sessions_mutex};

    auto& session_to_local_id = local_id_and_sessions.right;
    using value_type = decltype(local_id_and_sessions)::right_map::value_type;

    auto result = session_to_local_id.insert(
        value_type(session, ++next_local_id)
    );

    if (!result.second)
    {
        return false;
    }

    auto* created = create_read_buffer_for(session);
    assert(created != nullptr && "cannot create new read buffer for session(maybe duplicate?).");

    return true;
}

bool executor_remote::remove(network::session* session)
{
    std::lock_guard<std::mutex> lock{sessions_mutex};

    auto& session_to_local_id = local_id_and_sessions.right;
    return session_to_local_id.erase(session) && read_buffers.erase(session);
}

void executor_remote::process_read_data(
    network::session* session,
    const boost::system::error_code& error,
    std::size_t bytes_transferred,
    network::session::receive_handler_type handler
)
{
    if (error)
    {
        handler(session, error, 0);
        return;
    }

    const auto* read_buffer = get_read_buffer(session);
    if (bytes_transferred < read_buffer->size())
    {
        handler(session, make_error_code(boost::system::errc::operation_canceled), bytes_transferred);
        return;
    }

    assert(bytes_transferred == read_buffer->size());

    // transfer received bytes into invocation request object
    invocation_request from_remote;

    boost::asio::streambuf stream_buffer;
    std::ostream stream(&stream_buffer);
    stream.write(read_buffer->data(), read_buffer->size());

    boost::archive::binary_iarchive archive(stream_buffer);
    archive & from_remote;

    // perform invocation request
    local.call(get_session_id(session), std::move(from_remote));

    handler(session, error, bytes_transferred);
}

std::int64_t executor_remote::to_session_id(local_id_type local_id)
{
    static_assert(sizeof(std::int64_t) == 2*sizeof(local_id_type), "can not combine local ids into session id");

    std::int64_t session_id = executor::get_id();
    session_id <<= (CHAR_BIT * sizeof(local_id_type));
    session_id |= local_id;

    return session_id;
}

auto executor_remote::get_local_id(network::session* session) -> local_id_type
{
    std::lock_guard<std::mutex> lock{sessions_mutex};

    const auto& session_to_local_id = local_id_and_sessions.right;
    const auto found = session_to_local_id.find(session);
    if (found == session_to_local_id.end())
    {
        return -1;
    }

    return found->second; 
}

std::int64_t executor_remote::get_session_id(network::session* session)
{
    return to_session_id(get_local_id(session));
}

} } // namespace zillians::framework
