/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include "framework/buffer/init/CudaBufferKernelInit.h"
#include "core/Common.h"

#include <boost/preprocessor.hpp>
#include <cuda_runtime.h>

namespace zillians { namespace framework { namespace buffer {

namespace detail {

void* cuda_malloc_host_pinned(std::size_t size)
{
	void* ptr = NULL;
	unsigned int flags = cudaHostAllocPortable | cudaHostAllocMapped;
	if(LIKELY(cudaSuccess == cudaMallocHost(&ptr, size, flags)))
		return ptr;
	else
		return NULL;
}

void cuda_free_host_pinned(void* ptr)
{
	if(UNLIKELY(cudaSuccess == cudaFreeHost(ptr)))
	{
		// TODO log for error
	}
}


#define CUDA_MAX_DEVICES 8

#define CUDA_DEVICE_IDS (device_ptx_pinned_0)(device_ptx_pinned_1)(device_ptx_pinned_2)(device_ptx_pinned_3)(device_ptx_pinned_4)(device_ptx_pinned_5)(device_ptx_pinned_6)(device_ptx_pinned_7)

#define CUDA_MALLOC_DEVICE_PINNED_FUNC_IMPL(DEVICE_ID) \
void* cuda_malloc_ ## DEVICE_ID(std::size_t size) \
{ \
	void* ptr = NULL; \
	\
	int current_device; cudaGetDevice(&current_device); \
	int device_id = ((int)buffer_location_t::DEVICE_ID - (int)buffer_location_t::device_ptx_pinned_0) + 1; \
	if(current_device != device_id) cudaSetDevice(device_id); \
	\
	if(UNLIKELY(cudaSuccess != cudaMalloc(&ptr, size))) \
		ptr = NULL; \
	\
	if(current_device != device_id) cudaSetDevice(current_device); \
	return ptr; \
}

#define CUDA_MALLOC_DEVICE_PINNED_FUNC_IMPL_PP(r, data, elem) \
		CUDA_MALLOC_DEVICE_PINNED_FUNC_IMPL(elem)

BOOST_PP_SEQ_FOR_EACH(CUDA_MALLOC_DEVICE_PINNED_FUNC_IMPL_PP, _, CUDA_DEVICE_IDS)



#define CUDA_FREE_DEVICE_PINNED_FUNC_IMPL(DEVICE_ID) \
void cuda_free_ ## DEVICE_ID(void* ptr) \
{ \
	int current_device; cudaGetDevice(&current_device); \
	int device_id = ((int)buffer_location_t::DEVICE_ID - (int)buffer_location_t::device_ptx_pinned_0) + 1; \
	if(current_device != device_id) cudaSetDevice(device_id); \
	\
	if(UNLIKELY(cudaSuccess != cudaFree(ptr))) \
	{ \
	} \
	\
	if(current_device != device_id) cudaSetDevice(current_device); \
}

#define CUDA_FREE_DEVICE_PINNED_FUNC_IMPL_PP(r, data, elem) \
		CUDA_FREE_DEVICE_PINNED_FUNC_IMPL(elem)

BOOST_PP_SEQ_FOR_EACH(CUDA_FREE_DEVICE_PINNED_FUNC_IMPL_PP, _, CUDA_DEVICE_IDS)


template<buffer_location_t::type Dest, buffer_location_t::type Src, bool HostOrDeviceDest, bool HostOrDeviceSrc>
struct cuda_memcpy_helper_impl
{
	inline static bool perform(void* dest, const void* src, std::size_t size)
	{
		return false;
	}

	inline static bool to_register()
	{
		return false;
	}
};

template<buffer_location_t::type Dest, buffer_location_t::type Src>
struct cuda_memcpy_helper_impl<Dest, Src, true /*host*/, false /*device*/>
{
	inline static bool perform(void* dest, const void* src, std::size_t size)
	{
		if(UNLIKELY(cudaSuccess != cudaMemcpy(dest, src, size, cudaMemcpyDeviceToHost)))
			return false;
		return true;
	}

	inline static bool to_register()
	{
		return true;
	}
};

template<buffer_location_t::type Dest, buffer_location_t::type Src>
struct cuda_memcpy_helper_impl<Dest, Src, false /*host*/, true /*device*/>
{
	inline static bool perform(void* dest, const void* src, std::size_t size)
	{
		if(UNLIKELY(cudaSuccess != cudaMemcpy(dest, src, size, cudaMemcpyHostToDevice)))
			return false;
		return true;
	}

	inline static bool to_register()
	{
		return true;
	}
};

template<buffer_location_t::type Dest, buffer_location_t::type Src>
struct cuda_memcpy_helper_impl<Dest, Src, true /*device*/, true /*device*/>
{
	inline static bool perform(void* dest, const void* src, std::size_t size)
	{
		if(Dest != Src)
		{
			if(cudaSuccess != cudaMemcpyPeer(dest, (int)Dest - (int)buffer_location_t::device_ptx_pinned_0 + 1, src, (int)Src - (int)buffer_location_t::device_ptx_pinned_0 + 1, size))
				return false;
			return true;
		}
		else
		{
			if(UNLIKELY(cudaSuccess != cudaMemcpy(dest, src, size, cudaMemcpyDeviceToDevice)))
				return false;
			return true;
		}
	}

	inline static bool to_register()
	{
		return true;
	}
};

template<buffer_location_t::type Location>
struct is_host {
	enum { value = (Location == buffer_location_t::host_unpinned || Location == buffer_location_t::host_ptx_pinned) };
};

template<buffer_location_t::type Dest, buffer_location_t::type Src>
struct cuda_memcpy_helper
{
	inline static bool perform(void* dest, const void* src, std::size_t size)
	{
		return cuda_memcpy_helper_impl<Dest, Src, is_host<Dest>::value, is_host<Src>::value>::perform(dest, src, size);
	}

	inline static bool to_register()
	{
		return cuda_memcpy_helper_impl<Dest, Src, is_host<Dest>::value, is_host<Src>::value>::to_register();
	}
};

#define ALL_LOCATION_IDS (host_unpinned)(host_ptx_pinned)(device_ptx_pinned_0)(device_ptx_pinned_1)(device_ptx_pinned_2)(device_ptx_pinned_3)(device_ptx_pinned_4)(device_ptx_pinned_5)(device_ptx_pinned_6)(device_ptx_pinned_7)

#define MAKE_FULLY_QUALIFIED_BUFFER_LOCATION(X) \
		buffer_location_t::X

#define CUDA_MEMCPY_FUNC_PP_IMPL(r, product) \
bool BOOST_PP_CAT(cuda_memcpy_, BOOST_PP_CAT(BOOST_PP_SEQ_ELEM(0, product), BOOST_PP_CAT(_to_, BOOST_PP_SEQ_ELEM(1, product)))) (void *dest, const void *src, std::size_t size) { \
	return cuda_memcpy_helper<MAKE_FULLY_QUALIFIED_BUFFER_LOCATION(BOOST_PP_SEQ_ELEM(1, product)), MAKE_FULLY_QUALIFIED_BUFFER_LOCATION(BOOST_PP_SEQ_ELEM(0, product))>::perform(dest, src, size); \
}

BOOST_PP_SEQ_FOR_EACH_PRODUCT(CUDA_MEMCPY_FUNC_PP_IMPL, (ALL_LOCATION_IDS)(ALL_LOCATION_IDS))

}

void initCudaBufferKernels()
{
	malloc_func_table[buffer_location_t::host_ptx_pinned] = &detail::cuda_malloc_host_pinned;
	free_func_table[buffer_location_t::host_ptx_pinned] = &detail::cuda_free_host_pinned;

	// register all device malloc functions
#define CUDA_MALLOC_DEVICE_PINNED_FUNC_REGISTER_IMPL(DEVICE_ID) \
		malloc_func_table[buffer_location_t::DEVICE_ID] = &detail::cuda_malloc_ ## DEVICE_ID;

#define CUDA_MALLOC_DEVICE_PINNED_FUNC_REGISTER_PP(r, data, elem) \
		CUDA_MALLOC_DEVICE_PINNED_FUNC_REGISTER_IMPL(elem)

	BOOST_PP_SEQ_FOR_EACH(CUDA_MALLOC_DEVICE_PINNED_FUNC_REGISTER_PP, _, CUDA_DEVICE_IDS)

	// register all device free functions
#define CUDA_FREE_DEVICE_PINNED_FUNC_REGISTER_IMPL(DEVICE_ID) \
		free_func_table[buffer_location_t::DEVICE_ID] = &detail::cuda_free_ ## DEVICE_ID;

#define CUDA_FREE_DEVICE_PINNED_FUNC_REGISTER_PP(r, data, elem) \
		CUDA_FREE_DEVICE_PINNED_FUNC_REGISTER_IMPL(elem)

	BOOST_PP_SEQ_FOR_EACH(CUDA_FREE_DEVICE_PINNED_FUNC_REGISTER_PP, _, CUDA_DEVICE_IDS)

	// register all device/host memory copy functions
#define CUDA_MEMCPY_FUNC_REGISTER_PP(r, product) \
	if(detail::cuda_memcpy_helper<MAKE_FULLY_QUALIFIED_BUFFER_LOCATION(BOOST_PP_SEQ_ELEM(1, product)), MAKE_FULLY_QUALIFIED_BUFFER_LOCATION(BOOST_PP_SEQ_ELEM(0, product))>::to_register()) \
		memcpy_func_table[MAKE_FULLY_QUALIFIED_BUFFER_LOCATION(BOOST_PP_SEQ_ELEM(1, product))][MAKE_FULLY_QUALIFIED_BUFFER_LOCATION(BOOST_PP_SEQ_ELEM(0, product))] = \
		detail::BOOST_PP_CAT(cuda_memcpy_, BOOST_PP_CAT(BOOST_PP_SEQ_ELEM(0, product), BOOST_PP_CAT(_to_, BOOST_PP_SEQ_ELEM(1, product))));

	BOOST_PP_SEQ_FOR_EACH_PRODUCT(CUDA_MEMCPY_FUNC_REGISTER_PP, (ALL_LOCATION_IDS)(ALL_LOCATION_IDS))
}

} } }

