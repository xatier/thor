/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include "framework/buffer/support/MappedVariables.h"

namespace zillians { namespace framework { namespace buffer {

boost::thread_specific_ptr<MappedVariable<RuntimeApi, uint8> > MappedVariableContainer::mapped_variable_uint8;
boost::thread_specific_ptr<MappedVariable<RuntimeApi, uint16> > MappedVariableContainer::mapped_variable_uint16;
boost::thread_specific_ptr<MappedVariable<RuntimeApi, uint32> > MappedVariableContainer::mapped_variable_uint32;
boost::thread_specific_ptr<MappedVariable<RuntimeApi, uint64> > MappedVariableContainer::mapped_variable_uint64;
boost::thread_specific_ptr<MappedVariable<RuntimeApi, uint8*> > MappedVariableContainer::mapped_variable_pointer;

} } }
