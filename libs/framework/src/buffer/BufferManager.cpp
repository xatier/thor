/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include "framework/buffer/BufferManager.h"
#include "framework/buffer/BufferKernel.h"

#include "core/ScalablePoolAllocator.h"
#include "utility/Foreach.h"

#ifdef BUILD_WITH_NETWORK_ADDON
#ifdef BUILD_WITH_RDMA
#include "network/sys/infiniband/detail/IBFactory.h"
#include "network/sys/infiniband/detail/IBDeviceManager.h"
#endif
#endif

#include "framework/buffer/support/BufferAllocators.h"

namespace zillians { namespace framework { namespace buffer {

log4cxx::LoggerPtr BufferManager::mLogger(log4cxx::Logger::getLogger("BufferManager"));

namespace {

bool IsAccessibleFromHost(buffer_location_t::type location)
{
	switch(location)
	{
#ifdef BUILD_WITH_CUDA
	case buffer_location_t::device_ptx_pinned_0:
	case buffer_location_t::device_ptx_pinned_1:
	case buffer_location_t::device_ptx_pinned_2:
	case buffer_location_t::device_ptx_pinned_3:
	case buffer_location_t::device_ptx_pinned_4:
	case buffer_location_t::device_ptx_pinned_5:
	case buffer_location_t::device_ptx_pinned_6:
	case buffer_location_t::device_ptx_pinned_7:
		return false;
#endif
#ifdef BUILD_WITH_OPENCL
	case buffer_location_t::device_ocl_pinned_0:
	case buffer_location_t::device_ocl_pinned_1:
	case buffer_location_t::device_ocl_pinned_2:
	case buffer_location_t::device_ocl_pinned_3:
	case buffer_location_t::device_ocl_pinned_4:
	case buffer_location_t::device_ocl_pinned_5:
	case buffer_location_t::device_ocl_pinned_6:
	case buffer_location_t::device_ocl_pinned_7:
		return false;
#endif
	default:
		return true;
	}
}

}

//////////////////////////////////////////////////////////////////////////
BufferManager::BufferManager()
{
	mReservedBuffers[buffer_location_t::host_unpinned] = new BufferPool();
#ifdef BUILD_WITH_NETWORK_ADDON
#ifdef BUILD_WITH_RDMA
	mReservedBuffers[buffer_location_t::host_ib_pinned] = new BufferPool();
#endif
#endif
#ifdef BUILD_WITH_CUDA
	mReservedBuffers[buffer_location_t::host_ptx_pinned] = new BufferPool();
	mReservedBuffers[buffer_location_t::device_ptx_pinned_0] = new BufferPool();
	mReservedBuffers[buffer_location_t::device_ptx_pinned_1] = new BufferPool();
	mReservedBuffers[buffer_location_t::device_ptx_pinned_2] = new BufferPool();
	mReservedBuffers[buffer_location_t::device_ptx_pinned_3] = new BufferPool();
	mReservedBuffers[buffer_location_t::device_ptx_pinned_4] = new BufferPool();
	mReservedBuffers[buffer_location_t::device_ptx_pinned_5] = new BufferPool();
	mReservedBuffers[buffer_location_t::device_ptx_pinned_6] = new BufferPool();
	mReservedBuffers[buffer_location_t::device_ptx_pinned_7] = new BufferPool();
#endif
#ifdef BUILD_WITH_OPENCL
	mReservedBuffers[buffer_location_t::host_ocl_pinned] = new BufferPool();
	mReservedBuffers[buffer_location_t::device_ptx_pinned_0] = new BufferPool();
	mReservedBuffers[buffer_location_t::device_ptx_pinned_1] = new BufferPool();
	mReservedBuffers[buffer_location_t::device_ptx_pinned_2] = new BufferPool();
	mReservedBuffers[buffer_location_t::device_ptx_pinned_3] = new BufferPool();
	mReservedBuffers[buffer_location_t::device_ptx_pinned_4] = new BufferPool();
	mReservedBuffers[buffer_location_t::device_ptx_pinned_5] = new BufferPool();
	mReservedBuffers[buffer_location_t::device_ptx_pinned_6] = new BufferPool();
	mReservedBuffers[buffer_location_t::device_ptx_pinned_7] = new BufferPool();
#endif
}

BufferManager::~BufferManager()
{
	cleanup(buffer_location_t::host_unpinned);
#ifdef BUILD_WITH_NETWORK_ADDON
#ifdef BUILD_WITH_RDMA
	cleanup(buffer_location_t::host_ib_pinned);
#endif
#endif
#ifdef BUILD_WITH_CUDA
	cleanup(buffer_location_t::host_ptx_pinned);
	cleanup(buffer_location_t::device_ptx_pinned_0);
	cleanup(buffer_location_t::device_ptx_pinned_1);
	cleanup(buffer_location_t::device_ptx_pinned_2);
	cleanup(buffer_location_t::device_ptx_pinned_3);
	cleanup(buffer_location_t::device_ptx_pinned_4);
	cleanup(buffer_location_t::device_ptx_pinned_5);
	cleanup(buffer_location_t::device_ptx_pinned_6);
	cleanup(buffer_location_t::device_ptx_pinned_7);
#endif
#ifdef BUILD_WITH_OPENCL
	cleanup(buffer_location_t::host_ocl_pinned);
	cleanup(buffer_location_t::device_ocl_pinned_0);
	cleanup(buffer_location_t::device_ocl_pinned_0);
	cleanup(buffer_location_t::device_ocl_pinned_1);
	cleanup(buffer_location_t::device_ocl_pinned_2);
	cleanup(buffer_location_t::device_ocl_pinned_3);
	cleanup(buffer_location_t::device_ocl_pinned_4);
	cleanup(buffer_location_t::device_ocl_pinned_5);
	cleanup(buffer_location_t::device_ocl_pinned_6);
	cleanup(buffer_location_t::device_ocl_pinned_7);
#endif
}

//////////////////////////////////////////////////////////////////////////
bool BufferManager::reserve(buffer_location_t::type location, std::size_t size)
{
	if(size == 0) return false;

	BufferPool* pool = mReservedBuffers[location];
	BOOST_ASSERT(pool != NULL && "invalid location is used while reserving buffer");

	pool->to_reserve += size;

	return true;
}

bool BufferManager::allocate(buffer_location_t::type location, std::size_t size, /*OUT*/ byte** buffer)
{
	BufferPool* pool = mReservedBuffers[location];
	BOOST_ASSERT(pool != NULL && "invalid location is used while allocating buffer");

	byte* ptr = NULL;
	if( (pool->total_allocated - pool->total_reserved) > size && pool->max_single_allocation_size >= size )
	{
		// try to allocate by using every allocator on that location until success
		switch(location)
		{
		case buffer_location_t::host_unpinned:
		{
			for(auto* allocator : pool->allocators)
			{
				ptr = allocator->allocate(size);
				if(ptr) break;
			}
			break;
		}
#ifdef BUILD_WITH_NETWORK_ADDON
#ifdef BUILD_WITH_RDMA
		case buffer_location_t::host_ib_pinned:
		{
			for(auto allocator_index : zero_to(pool->allocators.size()))
			{
				ptr = pool->allocators[allocator_index]->allocate(size + BufferRawAllocator<buffer_location_t::host_ib_pinned>::reserved_size);
				if(ptr)
				{
					int device_count = BufferRawAllocator<buffer_location_t::host_ib_pinned>::device_count();
					byte* pool_base_ptr = pool->reserved_segments[allocator_index].get<0>() - BufferRawAllocator<buffer_location_t::host_ib_pinned>::reserved_size;
					for(int dev = 0; dev < device_count; ++dev)
					{
						((ibv_mr**)ptr)[dev] = ((ibv_mr**)pool_base_ptr)[dev];
					}
					ptr += BufferRawAllocator<buffer_location_t::host_ib_pinned>::reserved_size;
					break;
				}
			}
			break;
		}
#endif
#endif
#ifdef BUILD_WITH_CUDA
		case buffer_location_t::host_ptx_pinned:
		case buffer_location_t::device_ptx_pinned_0:
		case buffer_location_t::device_ptx_pinned_1:
		case buffer_location_t::device_ptx_pinned_2:
		case buffer_location_t::device_ptx_pinned_3:
		case buffer_location_t::device_ptx_pinned_4:
		case buffer_location_t::device_ptx_pinned_5:
		case buffer_location_t::device_ptx_pinned_6:
		case buffer_location_t::device_ptx_pinned_7:
		{
			int allocator_index = 0;
			for(auto* allocator : pool->allocators)
			{
				byte* ptr_ref = allocator->allocate(size);
				if(ptr_ref)
				{
					ptr = pool->reserved_segments[allocator_index].get<0>() + (ptr_ref - pool->reserved_segments[allocator_index].get<1>());
					break;
				}
				++allocator_index;
			}
			break;
		}
#endif
#ifdef BUILD_WITH_OPENCL
		case buffer_location_t::host_ocl_pinned:
		case buffer_location_t::device_ocl_pinned_0:
		case buffer_location_t::device_ocl_pinned_1:
		case buffer_location_t::device_ocl_pinned_2:
		case buffer_location_t::device_ocl_pinned_3:
		case buffer_location_t::device_ocl_pinned_4:
		case buffer_location_t::device_ocl_pinned_5:
		case buffer_location_t::device_ocl_pinned_6:
		case buffer_location_t::device_ocl_pinned_7:
		{
//			foreach(it, pool->allocators)
//			{
//				ptr = (*it)->allocate(size);
//				if(ptr) break;
//			}
			break;
		}
#endif
		default:
			BOOST_ASSERT(false && "request for buffer allocation at unknown buffer location");
			break;
		}
	}

	if(!ptr) ptr = (byte*)malloc_func_table[location](size);
	if(!ptr)
	{
		LOG4CXX_DEBUG(mLogger, "failed to allocate " << size << " bytes on location " << buffer_location_t::get_string(location));
		return false;
	}

	*buffer = ptr;

	return true;
}

void BufferManager::deallocate(buffer_location_t::type location, byte* buffer)
{
	BufferPool* pool = mReservedBuffers[location];
	BOOST_ASSERT(pool != NULL && "invalid location is used while allocating buffer");

	bool allocator_found = false;
	for(auto allocator_index : zero_to(pool->allocators.size()))
	{
		if(buffer >= pool->reserved_segments[allocator_index].get<0>() && buffer < pool->reserved_segments[allocator_index].get<0>() + pool->reserved_segments[allocator_index].get<2>())
		{
			allocator_found = true;
			switch(location)
			{
			case buffer_location_t::host_unpinned:
			{
				pool->allocators[allocator_index]->deallocate(buffer);
				break;
			}
#ifdef BUILD_WITH_NETWORK_ADDON
#ifdef BUILD_WITH_RDMA
			case buffer_location_t::host_ib_pinned:
			{
				byte* buffer_base = buffer - BufferRawAllocator<buffer_location_t::host_ib_pinned>::reserved_size;
				pool->allocators[allocator_index]->deallocate(buffer_base);
				break;
			}
#endif
#endif
#ifdef BUILD_WITH_CUDA
			case buffer_location_t::host_ptx_pinned:
			case buffer_location_t::device_ptx_pinned_0:
			case buffer_location_t::device_ptx_pinned_1:
			case buffer_location_t::device_ptx_pinned_2:
			case buffer_location_t::device_ptx_pinned_3:
			case buffer_location_t::device_ptx_pinned_4:
			case buffer_location_t::device_ptx_pinned_5:
			case buffer_location_t::device_ptx_pinned_6:
			case buffer_location_t::device_ptx_pinned_7:
			{
				byte* ptr_real = pool->reserved_segments[allocator_index].get<1>() + (buffer - pool->reserved_segments[allocator_index].get<0>());
				pool->allocators[allocator_index]->deallocate(ptr_real);
				break;
			}
#endif
#ifdef BUILD_WITH_OPENCL
			case buffer_location_t::host_ocl_pinned:
			case buffer_location_t::device_ocl_pinned_0:
			case buffer_location_t::device_ocl_pinned_1:
			case buffer_location_t::device_ocl_pinned_2:
			case buffer_location_t::device_ocl_pinned_3:
			case buffer_location_t::device_ocl_pinned_4:
			case buffer_location_t::device_ocl_pinned_5:
			case buffer_location_t::device_ocl_pinned_6:
			case buffer_location_t::device_ocl_pinned_7:
			{
				byte* ptr_real = pool->reserved_segments[allocator_index].get<1>() + (buffer - pool->reserved_segments[allocator_index].get<0>());
				pool->allocators[allocator_index]->deallocate(ptr_real);
				break;
			}
#endif
			default:
				BOOST_ASSERT(false && "request for buffer deallocation at unknown buffer location");
				break;
			}

			break;
		}
	}

	if(!allocator_found)
	{
		free_func_table[location](buffer);
	}
}

void BufferManager::cleanup(buffer_location_t::type location)
{
	BufferPool* pool = mReservedBuffers[location];
	BOOST_ASSERT(pool != NULL && "invalid location is used while allocating buffer");

	for(auto allocator_index : zero_to(pool->allocators.size()))
	{
		SAFE_DELETE(pool->allocators[allocator_index]);
		free_func_table[location](pool->reserved_segments[allocator_index].get<0>());
		if(!IsAccessibleFromHost(location))
		{
			free_func_table[buffer_location_t::host_unpinned](pool->reserved_segments[allocator_index].get<1>());
		}
	}

	pool->allocators.clear();
	pool->reserved_segments.clear();
	pool->total_allocated = pool->total_reserved = 0;
	pool->max_single_allocation_size = std::numeric_limits<std::size_t>::max();
}

bool BufferManager::commit(buffer_location_t::type location)
{
	BufferPool* pool = mReservedBuffers[location];
	BOOST_ASSERT(pool != NULL && "invalid location is used while allocating buffer");

	if(pool->to_reserve > 0)
	{
		std::vector< boost::tuple<byte*, byte*, std::size_t> > new_segments;
		bool insufficient_memory = false;
		bool page_limit_adjusted = false;
		std::size_t segment_needed = 1;
		std::size_t segment_size = pool->to_reserve;

		// try interactive allocation
		while(segment_needed > 0 && !insufficient_memory)
		{
			byte* ptr = (byte*)malloc_func_table[location](segment_size);
			byte* ptr_ref = NULL;

			if(!IsAccessibleFromHost(location))
			{
				ptr_ref = (byte*)malloc_func_table[buffer_location_t::host_unpinned](segment_size);
			}
			else
			{
				ptr_ref = ptr;
			}

			// if we failed to allocate the current segment size, the segment size could be too large to fit into a single page
			// (the size is defined by the system allocator at runtime)
			// then we reduce the segment size (that is, split into several smaller allocations) and try to allocate them one by one
			if(!ptr)
			{
				// we always try to half the segment size in case of reaching page size limit
				segment_needed *= 2; segment_size /= 2;

				// if the segment size is smaller than specific threshold, we might end up with insufficient memory
				if(segment_size < 4*1024 /*4KB*/)
				{
					insufficient_memory = true;
					break;
				}
			}
			else
			{
				// save the allocated pointer and size
				new_segments.push_back(boost::make_tuple(ptr, ptr_ref, segment_size));
				--segment_needed;
			}
		}

		// see if we have successfully allocated all segments
		if(insufficient_memory)
		{
			// if we don't have sufficient memory, we should clean up all allocation made this time
			for(auto& segment : new_segments)
			{
				free_func_table[location](segment.get<0>());
				if(!IsAccessibleFromHost(location))
				{
					free_func_table[buffer_location_t::host_unpinned](segment.get<1>());
				}
			}

			LOG4CXX_DEBUG(mLogger, "insufficient memory to reserve " << pool->to_reserve << " bytes on location " << buffer_location_t::get_string(location));

			return false;
		}
		else
		{
			// otherwise append the new segments into the reserved segments and create allocators on top of allocated segments
			for(auto& segment : new_segments)
			{
				pool->reserved_segments.push_back(segment);
				pool->allocators.push_back(new ScalablePoolAllocator(segment.get<1>(), segment.get<2>()));
				pool->total_allocated += segment.get<2>();
				pool->max_single_allocation_size = std::min(pool->max_single_allocation_size, segment_size);
			}

			pool->to_reserve = 0;

			return true;
		}
	}

	return true;
}

bool BufferManager::commit()
{
	bool result = true;

	result &= commit(buffer_location_t::host_unpinned);
#ifdef BUILD_WITH_NETWORK_ADDON
#ifdef BUILD_WITH_RDMA
	result &= commit(buffer_location_t::host_ib_pinned);
#endif
#endif
#ifdef BUILD_WITH_CUDA
	result &= commit(buffer_location_t::host_ptx_pinned);
	result &= commit(buffer_location_t::device_ptx_pinned_0);
	result &= commit(buffer_location_t::device_ptx_pinned_1);
	result &= commit(buffer_location_t::device_ptx_pinned_2);
	result &= commit(buffer_location_t::device_ptx_pinned_3);
	result &= commit(buffer_location_t::device_ptx_pinned_4);
	result &= commit(buffer_location_t::device_ptx_pinned_5);
	result &= commit(buffer_location_t::device_ptx_pinned_6);
	result &= commit(buffer_location_t::device_ptx_pinned_7);
#endif
#ifdef BUILD_WITH_OPENCL
	result &= commit(buffer_location_t::host_ocl_pinned);
	result &= commit(buffer_location_t::device_ocl_pinned_0);
	result &= commit(buffer_location_t::device_ocl_pinned_1);
	result &= commit(buffer_location_t::device_ocl_pinned_2);
	result &= commit(buffer_location_t::device_ocl_pinned_3);
	result &= commit(buffer_location_t::device_ocl_pinned_4);
	result &= commit(buffer_location_t::device_ocl_pinned_5);
	result &= commit(buffer_location_t::device_ocl_pinned_6);
	result &= commit(buffer_location_t::device_ocl_pinned_7);
#endif
	return result;
}

//////////////////////////////////////////////////////////////////////////

#ifdef BUILD_WITH_NETWORK_ADDON
#ifdef BUILD_WITH_RDMA
bool BufferManager::query_mr_lkey(buffer_location_t::type location, byte* buffer, int dev, /*OUT*/ uint32& lkey)
{
	BOOST_ASSERT(location == buffer_location_t::host_ib_pinned && "only ib_pinned buffer has lkey attribute");
	BOOST_ASSERT(dev >= 0 && dev < network::sys::IBDeviceManager::instance()->device_count() && "given device is not valid");

	if(dev < 0 || dev >= BufferRawAllocator<buffer_location_t::host_ib_pinned>::device_count())
	{
		LOG4CXX_ERROR(mLogger, "query lkey on invalid device id: " << dev);
		return false;
	}

	ibv_mr* mr = BufferRawAllocator<buffer_location_t::host_ib_pinned>::query_mr((void*)buffer, dev);
	lkey = mr->lkey;

	return true;
}
#endif
#endif

byte* BufferManager::query_mirrored_ptr(buffer_location_t::type location, byte* buffer)
{
	BufferPool* pool = mReservedBuffers[location];
	if(IsAccessibleFromHost(location))
	{
		for(auto allocator_index : zero_to(pool->allocators.size()))
		{
			if(buffer >= pool->reserved_segments[allocator_index].get<0>() && buffer < pool->reserved_segments[allocator_index].get<0>() + pool->reserved_segments[allocator_index].get<2>())
			{
				return pool->reserved_segments[allocator_index].get<1>() + (buffer - pool->reserved_segments[allocator_index].get<0>());
			}
		}
	}

	return NULL;
}

} } }
