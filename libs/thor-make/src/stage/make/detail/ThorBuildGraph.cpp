/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include "language/stage/make/detail/ThorBuildGraph.h"
#include "language/ThorConfiguration.h"
#include "utility/UnicodeUtil.h"

#include <boost/graph/topological_sort.hpp>
#include <boost/graph/strong_components.hpp>
#include <boost/graph/graphviz.hpp>
#include <boost/pending/property.hpp>

#include "language/tree/module/Package.h"
#include "language/stage/serialization/detail/ASTSerializationHelper.h"
#include "language/stage/make/detail/ThorDependencyParser.h"

enum edge_additional_prop_ref_t { edge_additional_prop_ref };
enum vertex_source_files_t { vertex_source_files };
enum vertex_output_files_t { vertex_output_files };

namespace boost {
    BOOST_INSTALL_PROPERTY(edge, additional_prop_ref);
    // this addition property is used to store a list of sources associated to the tangle vertex
    BOOST_INSTALL_PROPERTY(vertex, source_files);
    BOOST_INSTALL_PROPERTY(vertex, output_files);
}

namespace zillians { namespace language { namespace stage {

namespace {

// declare the tangle graph
// the tangle graph is a temporary data structure to store each strong-connected-component in dependency graph as a tangle vertex
// each tangle vertex is associated with a list of sources (which can be either *.t or *.ast)
typedef indirect_graph_traits<int /*vertex_property*/, int /*edge_property*/> IndirectTangleGraphTraits;

typedef boost::property<boost::vertex_index_t, long,
        boost::property<boost::vertex_color_t, boost::default_color_type,
        boost::property<boost::vertex_name_t, std::string,
        boost::property<vertex_source_files_t, std::vector<std::string>,
        boost::property<vertex_output_files_t, std::vector<std::string>,
        IndirectTangleGraphTraits::vertex_property > > > > > TangleVertexProperty;

typedef boost::property<boost::edge_name_t, std::string,
        IndirectTangleGraphTraits::edge_property> TangleEdgeProperty;

typedef boost::adjacency_list<boost::setS, boost::listS, boost::bidirectionalS, TangleVertexProperty, TangleEdgeProperty> TangleGraph;
typedef boost::graph_traits<TangleGraph> TangleTraits;

typedef TangleTraits::edge_descriptor TangleEdgeDescriptor;
typedef TangleTraits::vertex_descriptor TangleVertexDescriptor;
typedef TangleTraits::vertex_iterator TangleVertexIter;

}

ThorBuildGraph::ThorBuildGraph(const boost::filesystem::path& root) : mRootPath(root), mVertexIndex(0L)
{
    mIndex = boost::get(boost::vertex_index, mDependencyGraph);
//  mGroupFlag = boost::get(boost::vertex_color, mDependencyGraph);
//  mName = boost::get(boost::vertex_name, mDependencyGraph);
//  mDiscoverTime = boost::get(boost::vertex_discover_time, mDependencyGraph);
}

ThorBuildGraph::~ThorBuildGraph()
{ }

void ThorBuildGraph::setPrependPackage(const std::wstring& prepend_package)
{
    mPrependPackage = prepend_package;
}

bool ThorBuildGraph::addSource(const boost::filesystem::path& src)
{
    // get the full path of this source
    boost::filesystem::path normalized_src = Filesystem::normalize_path(src);

    // make sure we are adding source
    if(normalized_src.extension() != THOR_EXTENSION_SOURCE)
        return false;

    // make sure there's no duplicated source
    VertexDescriptor vd;
    if(mDependencyGraphMapping.findVertexReference(normalized_src.string(), vd))
        return false;

    // use the path name as vertex name and save it through indirect graph traits
    vd = add_vertex(normalized_src.string(), mDependencyGraph, mDependencyGraphMapping);
    mIndex[vd] = mVertexIndex++;

    // get the package of this source and add it into the package-->set<source/ast> map
    std::wstring package;
    if(!enumerateSourcePackage(normalized_src, package))
        return false;

    std::wstring s;
    if(package.empty())
        s = mPrependPackage;
    else if(mPrependPackage.empty())
        s = package;
    else
        s = mPrependPackage + L"." + package;

    mPackagesToFiles.insert(std::make_pair(s, vertex(normalized_src.string(), mDependencyGraph, mDependencyGraphMapping)));

    return true;
}

bool ThorBuildGraph::addImport(const boost::filesystem::path& ast)
{
    // get the full path of this ast
    boost::filesystem::path normalized_ast = Filesystem::normalize_path(ast);

    // make sure we are adding imported ast
    if(normalized_ast.extension() != THOR_EXTENSION_AST)
        return false;

    // make sure there's no duplicated ast
    VertexDescriptor vd;
    if(mDependencyGraphMapping.findVertexReference(normalized_ast.string(), vd))
        return false;

    // use the path name as vertex name and save it through indirect graph traits
    vd = add_vertex(normalized_ast.string(), mDependencyGraph, mDependencyGraphMapping);
    mIndex[vd] = mVertexIndex++;

    // enumerate all packages within this ast and add it into the package-->set<source/ast> map
    std::vector<std::wstring> packages;
    if(!enumerateImportPackages(normalized_ast, packages))
        return false;

    for(auto& package : packages)
    {
        std::wstring s;
        if(package.empty())
            s = mPrependPackage;
        else if(mPrependPackage.empty())
            s = package;
        else
            s = mPrependPackage + L"." + package;

        mPackagesToFiles.insert(std::make_pair(s, vertex(normalized_ast.string(), mDependencyGraph, mDependencyGraphMapping)));
    }

    return true;
}

bool ThorBuildGraph::build(std::function<int(std::vector<boost::filesystem::path>& /*source*/, std::vector<boost::filesystem::path>& /*imports*/, std::vector<boost::filesystem::path>& /*outputs*/)> builder, int concurrency)
{
    // for all source files, collect dependency (imports) from the source and create dependency edges from the source to the target
    {
        VertexIter vi, vi_end;
        for(boost::tie(vi,vi_end) = vertices(mDependencyGraph); vi != vi_end; ++vi)
        {
            std::string src = vertex_ref(*vi, mDependencyGraph, mDependencyGraphMapping);
            boost::filesystem::path p(src);
            if(p.extension() == THOR_EXTENSION_SOURCE)
            {
                std::vector<std::wstring> packages;
                if(!parseSourceImportedPackages(p, packages))
                    return false;

                for(auto& package : packages)
                {
                    auto range = mPackagesToFiles.equal_range(package);
                    for(auto j = range.first; j != range.second; ++j)
                    {
                        std::string dest = vertex_ref(j->second, mDependencyGraph, mDependencyGraphMapping);

                        static int dependent_edge_increment = 0;
                        ++dependent_edge_increment;
                        add_edge(dependent_edge_increment, src, dest, mDependencyGraph, mDependencyGraphMapping);
                    }
                }
            }
        }
    }

    // perform strong-connected-component analysis on the dependency graph
    std::vector<int> component_indices(boost::num_vertices(mDependencyGraph));
    std::map<VertexDescriptor, int> component_map;
    //boost::default_color_type;
    boost::property_map<Graph, boost::vertex_color_t>::type color = boost::get(boost::vertex_color, mDependencyGraph);
    boost::property_map<Graph, boost::vertex_root_t>::type root = boost::get(boost::vertex_root, mDependencyGraph);
    boost::property_map<Graph, boost::vertex_discover_time_t>::type discoverTime = boost::get(boost::vertex_discover_time, mDependencyGraph);

    // FIXME : can NOT compiled with clang
    //int components = boost::strong_components(mDependencyGraph, boost::make_assoc_property_map(component_map), color_map(color).root_map(root).discover_time_map(discoverTime));

    // create the tangle graph object
    TangleGraph tangle;
    indirect_graph_mapping<IndirectTangleGraphTraits, TangleVertexDescriptor, TangleEdgeDescriptor> tangle_mapping;

    boost::property_map<TangleGraph, boost::vertex_index_t>::type tangle_vertex_index_map = boost::get(boost::vertex_index, tangle);
    boost::property_map<TangleGraph, boost::vertex_name_t>::type tangle_vertex_name_map = boost::get(boost::vertex_name, tangle);
    boost::property_map<TangleGraph, vertex_source_files_t>::type tangle_source_files_map = boost::get(vertex_source_files, tangle);
    boost::property_map<TangleGraph, vertex_output_files_t>::type tangle_output_files_map = boost::get(vertex_output_files, tangle);

    // for each component/tangle, collect all source/ast file names and save it as a vertex in tangle graph
    for(auto i = component_map.begin(), e = component_map.end(); i != e; ++i)
    {
        std::string src = vertex_ref(i->first, mDependencyGraph, mDependencyGraphMapping);

        // get the corresponding tangle vertex descriptor
        TangleVertexDescriptor tangle_vd;
        if(!tangle_mapping.findVertexReference(i->second, tangle_vd))
        {
            // create the tangle vertex if not exist
            tangle_vd = add_vertex(i->second, tangle, tangle_mapping);
        }

        // add it to the tangle source list
        tangle_source_files_map[tangle_vd].push_back(src);

        // TODO save the tangle id to the source vertex as well (we will use it look up from source to tangle later)
        //source_to_tangle_index_map[i->first] = i->second;
    }

    // for each component/tangle and for each source/ast in this tangle, we find out if the depending vertex is in different tangle, we create a dependent edge between the tangles
    {
        TangleVertexIter vi, vi_end;
        for(boost::tie(vi,vi_end) = vertices(tangle); vi != vi_end; ++vi)
        {
            // get the source tangle
            int src_tangle = vertex_ref(*vi, tangle, tangle_mapping);

            // for each source/ast in this tangle
            std::vector<std::string>& sources = tangle_source_files_map[*vi];
            for(auto i = sources.begin(), e = sources.end(); i != e; ++i)
            {
                VertexDescriptor vd;
                if(!mDependencyGraphMapping.findVertexReference(*i, vd))
                {
                    BOOST_ASSERT(false && "invalid graph constructed during build");
                    return false;
                }

                // TODO for each out-edge (dependency) of this source vertex, create an edge if the dependency lies in different tangles
                out_edge_iter it_oe, it_oe_end;
                for(boost::tie(it_oe, it_oe_end) = boost::out_edges(vd, mDependencyGraph); it_oe != it_oe_end; ++it_oe)
                {
                    // TODO we need a way to look up from source to the tangle id
                    TangleVertexDescriptor tangle_vd;
                    if(!tangle_mapping.findVertexReference(component_map[boost::target(*it_oe, mDependencyGraph)], tangle_vd))
                    {
                        BOOST_ASSERT(false && "invalid graph constructed during build");
                        return false;
                    }

                    int dest_tangle = vertex_ref(tangle_vd, tangle, tangle_mapping);

                    if(src_tangle != dest_tangle)
                    {
                        static int tangle_edge_increment = 0;
                        ++tangle_edge_increment;
                        // TODO what will happen when there's duplicate edge?
                        add_edge(tangle_edge_increment, src_tangle, dest_tangle, tangle, tangle_mapping);
                    }
                }
            }
        }
    }

    // TODO need to handle if the tangle graph is not a connected graph
    // perform topological sort on the tangle graph
    std::list<TangleVertexDescriptor> ordering;
    try {
        boost::property_map<TangleGraph, boost::vertex_color_t>::type tangle_color = boost::get(boost::vertex_color, tangle);
        boost::topological_sort(tangle, std::back_inserter(ordering), color_map(tangle_color));
    }
    catch(const boost::not_a_dag &nad)
    {
        std::cerr << "Error: dependency graph is not a DAG, topological sort failed : " << nad.what() << std::endl;
        return false;
    }
    catch(std::invalid_argument& ia)
    {
        std::cerr << "Error: fail to compile module load order : " << ia.what() << std::endl;
        return false;
    }

    // define a filter lamda for later use to prepare source list and ast list from tangle vertex descriptor
    auto filter = [&](TangleVertexDescriptor tangle_vd, std::vector<boost::filesystem::path>& sources, std::vector<boost::filesystem::path>& asts) {
        // add all files of this tangle vertex
        std::vector<std::string>& files = tangle_source_files_map[tangle_vd];
        for(auto i = files.begin(), e = files.end(); i != e; ++i)
        {
            boost::filesystem::path p(*i);
            if(p.extension() == THOR_EXTENSION_SOURCE)
                sources.push_back(p);
            else if(p.extension() == THOR_EXTENSION_AST)
                asts.push_back(p);
            else
                UNREACHABLE_CODE();
        }

        // add all output files of dependent tangles
        TangleTraits::out_edge_iterator it_oe, it_oe_end;
        for(boost::tie(it_oe, it_oe_end) = boost::out_edges(tangle_vd, tangle); it_oe != it_oe_end; ++it_oe)
        {
            TangleVertexDescriptor dependent_tangle_vd = boost::target(*it_oe, tangle);
            std::vector<std::string>& files = tangle_output_files_map[dependent_tangle_vd];
            for(auto i = files.begin(), e = files.end(); i != e; ++i)
            {
                boost::filesystem::path p(*i);
                if(p.extension() == THOR_EXTENSION_SOURCE)
                    sources.push_back(p);
                else if(p.extension() == THOR_EXTENSION_AST)
                    asts.push_back(p);
                else
                    UNREACHABLE_CODE();
            }
        }
    };

    // if the concurrency is 1, which means serial execution, put it into a vector and invoke builder one-by-one
    if(concurrency == 1)
    {
        for(auto i = ordering.begin(), e = ordering.end(); i != e; ++i)
        {
            std::vector<boost::filesystem::path> sources;
            std::vector<boost::filesystem::path> asts;
            std::vector<boost::filesystem::path> outputs;

            filter(*i, sources, asts);

            int ec = builder(sources, asts, outputs);
            if(ec != 0)
            {
                std::cerr << "Error: interrupted due to builder error" << std::endl;
                return false;
            }

            BOOST_ASSERT(outputs.size() > 0);

            // add the output files to the tangle vertex
            for(auto& output : outputs)
                tangle_output_files_map[*i].push_back(output.string());
        }
    }
    // TODO otherwise, create a boost asio io_servcice instance and enqueue builder task to that io_service
    else
    {
        // TODO
        // and we keep a atomic completion count on each tangle so that once a tangle is completed,
        // it atomically increments all tangles which depends on it and once the count equals to the number of all dependent edges, it enqueue the next builder task

    }

    return true;
}

bool ThorBuildGraph::enumerateSourcePackage(const boost::filesystem::path& p, /*OUT*/ std::wstring& package)
{
    std::wstring s;

    if(!boost::filesystem::exists(p))
        return false;

    boost::filesystem::path t = p.parent_path();

    // trace up to the root directory and use the directory path as package for this source
    while (true)
    {
        if (t.empty())
            return false;

        if (t == mRootPath)
            break;
        else
            s = (t.stem().wstring()) + s;

        t = t.parent_path();
    }

    package = s;
    return true;
}

bool ThorBuildGraph::enumerateImportPackages(const boost::filesystem::path& p, /*OUT*/ std::vector<std::wstring>& packages)
{
    using namespace zillians::language::tree;

    if(!boost::filesystem::exists(p))
        return false;

    // de-serialize the AST file
    ASTNode* node = ASTSerializationHelper::deserialize(p.string());
    if(!node)
        return false;

    Tangle* tangle = cast<Tangle>(node);
    if(!tangle)
        return false;

    std::function<void(Package&, const std::wstring&, std::vector<std::wstring>&)> package_path_collector = [&](Package& current, const std::wstring& current_path, std::vector<std::wstring>& packages){
        packages.push_back(current_path);

        for(auto* child : current.children)
        {
            const auto& child_name = child->id->toString();

            package_path_collector(*child, current.isRoot() ? child_name : ( current_path + L'.' + child_name), packages);
        }
    };

    package_path_collector(*tangle->root, std::wstring(), packages);

    return true;
}

bool ThorBuildGraph::parseSourceImportedPackages(const boost::filesystem::path& p, std::vector<std::wstring>& packages)
{
    // TODO this can be improved by
    // TODO 1. cache the result (avoid unnecessary parsing)
    // TODO 2. use memory-mapped file to avoid reading the entire file

    std::ifstream in(p.string(), std::ios_base::in);

    if(!in.good())
    {
        std::cerr << "Error: failed to open file: " << p.string() << std::endl;
        return false;
    }

    // ignore the BOM marking the beginning of a UTF-8 file in Windows
    if(in.peek() == '\xef')
    {
        char s[4];
        in >> s[0] >> s[1] >> s[2];
        s[3] = '\0';
        if (s != std::string("\xef\xbb\xbf"))
        {
            std::cerr << "Error: unexpected characters from input file: " << p.string() << std::endl;
            return false;
        }
    }

    std::string source_code_raw;
    in.unsetf(std::ios::skipws); // disable white space skipping
    std::copy(std::istream_iterator<char>(in), std::istream_iterator<char>(), std::back_inserter(source_code_raw));

    // convert is frem UTF8 into UCS4 as a string by using u8_to_u32_iterator
    std::wstring source_code;
    utf8_to_ucs4(source_code_raw, source_code);

	try
	{
    	// enable correct locale so that we can print UCS4 characters
	    enable_default_locale(std::wcout);
	}
	catch (std::runtime_error& e)
	{
		// the default locale does not accpeted by the system, maybe user had define wrong locale setting
		// in this case, we use the safer one
		enable_c_locale(std::wcout);
	}

    // try to parse
    typedef boost::spirit::classic::position_iterator2<std::wstring::iterator> pos_iterator_type;
    try
    {
        pos_iterator_type begin(source_code.begin(), source_code.end(), p.wstring());
        pos_iterator_type end;

        getImportedPackages(begin, end, packages);
    }
    catch (std::exception& e)
    {
        return false;
    }

    return true;
}


} } }
