/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <iostream>
#include <string>
#include <utility>
#include <vector>

#include <boost/format.hpp>
#include <boost/program_options/errors.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/parsers.hpp>
#include <boost/program_options/value_semantic.hpp>
#include <boost/program_options/variables_map.hpp>
#include <boost/timer/timer.hpp>

#include "core/SharedPtr.h"
#include "core/Logger.h"

#include "language/context/ConfigurationContext.h"
#include "language/logging/LoggerWrapper.h"
#include "language/stage/StageConductor.h"

#define ALLOW_DUPLICATED_OPTIONS   1

namespace zillians { namespace language { namespace stage {

namespace {

struct TimingTable
{
    TimingTable(std::string stage_name,
                boost::timer::cpu_times elapsed
    ) : stage_name(std::move(stage_name))
      , elapsed(elapsed)
      , percent(0.0)
    {}

    std::string stage_name;
    boost::timer::cpu_times elapsed;
    double percent;
} ;

void calculate_timing_percent(std::vector<TimingTable>& timing_table)
{
    boost::timer::nanosecond_type total_wall_time = 0;
    for(auto& tt : timing_table) {
        total_wall_time += tt.elapsed.wall;
    }
    for(auto& tt : timing_table) {
        tt.percent = static_cast<double>(tt.elapsed.wall) / total_wall_time;
    }
}

void print_timing(const std::vector<TimingTable>& timing_table)
{
    std::cerr << boost::format("%-30s  %6s %8s %12s %12s %12s  %6s") % "Stage name" % "%" % "seconds" % "Wall" % "User" % "System" % "User%" << std::endl;
    std::cerr << "-------------------------------------------------------------------------------------" << std::endl;
    for(const auto& tt : timing_table) {
        std::cerr << boost::format("%-30s %6.2f%% %8.4f %12d %12d %12d %6.2f%%")
                     % tt.stage_name
                     % (tt.percent * 100)
                     % (static_cast<double>(tt.elapsed.wall) / (1000 * 1000 * 1000))
                     % tt.elapsed.wall
                     % tt.elapsed.user
                     % tt.elapsed.system
                     % (static_cast<double>(tt.elapsed.user) / static_cast<double>(tt.elapsed.user + tt.elapsed.system) * 100.0)
                  << std::endl;
    }
}

}

namespace detail {

#if ALLOW_DUPLICATED_OPTIONS

bool createOptionUnion(po::options_description& options_out, const po::options_description& options_add)
{
    for(auto option : options_add.options())
    {
        if(!options_out.find_nothrow(option->long_name(), false, false, false))
        {
            options_out.add(option);
        }
    }
    return true;
}

#endif

}

StageConductor::StageConductor(bool require_input) : mOptionDescGlobal()
{
    // make sure logger is initialized;
    LoggerWrapper::instance();

    // initialize basic program options
    if(!require_input)
    {
        mOptionDescGlobal.add_options()
            ("help,h", "show this help")
            ("timing,t", po::bool_switch(), "Timing elapsed time of each stage")
        ;
    }
    else
    {
        mOptionDescGlobal.add_options()
            ("help,h", "show this help")
            ("input,i", po::value<std::vector<std::string>>(), "input files")
            ("timing,t", po::bool_switch(), "Timing elapsed time of each stage")
        ;

        mPositionalOptionDesc.add("input", -1);
    }
}

StageConductor::~StageConductor()
{ }

void StageConductor::appendStage(shared_ptr<Stage> stage)
{
    mStages.push_back(stage);
}

void StageConductor::appendOptionsFromAllStages(po::options_description& options_desc_public, po::options_description& options_desc_private)
{
    for(auto& stage : mStages)
    {
        std::pair<shared_ptr<po::options_description>, shared_ptr<po::options_description>> options = stage->getOptions();
#if ALLOW_DUPLICATED_OPTIONS
        detail::createOptionUnion(options_desc_public, *options.first.get());
        detail::createOptionUnion(options_desc_private, *options.second.get());
#else
        if(options.first->options().size() > 0)
            options_desc_public.add(*options.first);
        if(options.second->options().size() > 0)
            options_desc_private.add(*options.second);
#endif
    }
}

int StageConductor::main(int argc, const char** argv)
{
    if(true)
    {
        po::options_description options_desc_pub("Usage");
        po::options_description options_desc_pri("Usage");

        // construct public and private options
        options_desc_pub.add(mOptionDescGlobal);
        options_desc_pri.add(mOptionDescGlobal);
        appendOptionsFromAllStages(options_desc_pub, options_desc_pri);

        // try to parse the command line
        po::variables_map vm;
        try
        {
            po::store(po::command_line_parser(argc, argv).options(options_desc_pri).positional(mPositionalOptionDesc).run(), vm);
            po::notify(vm);
        }
        catch(const boost::program_options::error& e)
        {
            std::cerr << "failed to parse command line: " << e.what() << std::endl;
            std::cerr << options_desc_pub << std::endl;
            return -1;
        }

        // if help option is specified, print the options and exit
        if(vm.count("help") > 0)
        {
            std::cout << "command line options: " << std::endl << std::endl;
            std::cout << options_desc_pub << std::endl;
            return 0;
        }

        // otherwise, process the given commands through each staged execution

        // let each stage determine what to do
        for(auto stage : mStages)
        {
            if(!stage->parseOptions(vm))
            {
                std::cerr << "failed to process command option for \"" << stage->name() << "\"" << std::endl;
                std::cerr << options_desc_pub << std::endl;
                return -1;
            }
        }

        std::vector<TimingTable> timing_table;

        // perform the execution
        for(auto stage : mStages)
        {
            boost::timer::cpu_timer timer;
            timer.start();
            bool continue_execution = false;
            bool stage_result = stage->execute(continue_execution/*out*/);
            timer.stop();
            timing_table.emplace_back(stage->name(), timer.elapsed());

            if(stage_result)
            {
                continue;
            }
            else
            {
                std::cerr << "execution failed at stage: " << stage->name() << std::endl;
                if(continue_execution)
                    continue;
                else
                    return -1;
            }
        }

        if(vm["timing"].as<bool>())
        {
            calculate_timing_percent(timing_table);
            print_timing(timing_table);
        }
    }

    return 0;
}

} } }
