/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <clocale>

#include <locale>
#include <iostream>

#include "language/toolchain/ThorVM.h"

int main(int argc, const char** argv)
{
    // !! workaround for non-ASCII character output !!
    // Standard output/error streams may not work well on non-ASCII characters!
    // Use system locale by setting them through C/C++ API.
    // Note. "sync_with_stdio" is required to make C++ use its own locale handling on standard output/error streams.
    std::setlocale(LC_ALL, "");
    std::ios::sync_with_stdio(false);
    std::locale::global(std::locale(""));
    std:: cout.imbue(std::locale(""));
    std::wcout.imbue(std::locale(""));
    std:: cerr.imbue(std::locale(""));
    std::wcerr.imbue(std::locale(""));

    zillians::language::ThorVM vm;
    return vm.main(argc, argv);
}
