/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
/**
 * @date Oct 7, 2009 rocet - Initial version created.
 */

#include "network/sys/LocalInterface.h"

#include <log4cxx/logger.h>
#include <log4cxx/basicconfigurator.h>

#define MAX_TYPE	1024

using namespace zillians;
using namespace zillians::network::sys;

log4cxx::LoggerPtr mLogger(log4cxx::Logger::getLogger("LocalInterfaceTest"));

int main (int argc, char** argv)
{
	log4cxx::BasicConfigurator::configure();

	LocalInterface interfaces;
	if(!interfaces.enumerate())
	{
		LOG4CXX_ERROR(mLogger, "failed to enumerate network interface list");
		return -1;
	}

	while(interfaces.next())
	{
		LOG4CXX_INFO(mLogger,
				"name = " << interfaces.name() << \
				", hwaddr = " << interfaces.hw_address() << \
				", broadcast = " << interfaces.broadcast_address() << \
				", netmask = " << interfaces.netmask() << \
				", metric = " << interfaces.metric() << \
				", mtu = " << interfaces.mtu() << \
				", flags = " << interfaces.flags_to_string(interfaces.flags()) );
	}

	return 0;
}
