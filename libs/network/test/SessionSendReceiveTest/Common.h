/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
/**
 * @date Aug 13, 2009 sdk - Initial version created.
 */

#include "core/IntTypes.h"
#include "core/SharedPtr.h"
#include "core/Buffer.h"

#include "network/sys/tcp/TcpSession.h"
#include "network/sys/tcp/TcpSessionEngine.h"

using namespace zillians;
using namespace zillians::network::sys;

struct MySessionContext
{
	uint32 id;
	uint32 type;
	shared_ptr<Buffer> buffer;
};

MySessionContext* createContext(uint32 id)
{
	MySessionContext* ctx = new MySessionContext();
	ctx->id = id;
	ctx->buffer.reset(new Buffer(4096));
	return ctx;
}

void sendTestMessage(TcpSession& session)
{
	shared_ptr<Buffer> a0(new Buffer(1024));
	shared_ptr<Buffer> a1(new Buffer(1024));
	shared_ptr<Buffer> a2(new Buffer(1024));
	shared_ptr<Buffer> a3(new Buffer(1024));

	for(int32 i=0;i<1024/sizeof(int32);++i)
	{
		int32 x = 0;
		a0->write(x);
	}

	try
	{
		session.write(0, a0);
		printf("write a0 completed\n");
	}
	catch(boost::system::system_error e)
	{
		printf("write a0 exception: %s\n", e.what());
	}

	for(int32 i=0;i<1024/sizeof(int32);++i)
	{
		int32 x = 1;
		a1->write(x);
	}

	try
	{
		session.write(0, a1);
		printf("write a1 completed\n");
	}
	catch(boost::system::system_error e)
	{
		printf("write a1 exception: %s\n", e.what());
	}

	for(int32 i=0;i<1024/sizeof(int32);++i)
	{
		int32 x = 2;
		a2->write(x);
	}

	try
	{
		session.write(0, a2);
		printf("write a2 completed\n");
	}
	catch(boost::system::system_error e)
	{
		printf("write a2 exception: %s\n", e.what());
	}


	for(int32 i=0;i<1024/sizeof(int32);++i)
	{
		int32 x = 3;
		a3->write(x);
	}

	try
	{
		printf("write a3 completed\n");
		session.write(0, a3);
	}
	catch(boost::system::system_error e)
	{
		printf("write a3 exception: %s\n", e.what());
	}

//
//	shared_ptr<BufferCollection> collection(new BufferCollection);
//	collection->add(a0);
//	collection->add(a1);
//	collection->add(a2);
//	collection->add(a3);

	//session.write(0, collection);
}

bool verifyTestMessage(TcpSession& session, shared_ptr<Buffer> buffer)
{
	for(int32 i=0;i<4096/sizeof(int32);++i)
	{
		int32 x;
		buffer->read(x);

		if(i < (1024/sizeof(int32)))
		{
			if(x != 0)
			{
				printf("verification failed at %d, expected %d, got %d\n", i, 0, x);
				return false;
			}
		}
		else if(i < (1024/sizeof(int32))*2)
		{
			if(x != 1)
			{
				printf("verification failed at %d, expected %d, got %d\n", i, 1, x);
				return false;
			}
		}
		else if(i < (1024/sizeof(int32))*3)
		{
			if(x != 2)
			{
				printf("verification failed at %d, expected %d, got %d\n", i, 2, x);
				return false;
			}
		}
		else if(i < (1024/sizeof(int32))*4)
		{
			if(x != 3)
			{
				printf("verification failed at %d, expected %d, got %d\n", i, 3, x);
				return false;
			}
		}
	}

	return true;
}
