/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <functional>
#include <mutex>
#include <memory>
#include <string>
#include <vector>

#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/system/error_code.hpp>

#include "network/Session.h"

namespace zillians { namespace network {

session::session(boost::asio::io_service& service)
: base_type(service)
{
    set_no_delay(true);
    set_minimal_send(128);
    set_minimal_recv(128);
}

session::~session()
{
    close();
}

boost::system::error_code session::close()
{
    boost::system::error_code error;

    base_type::shutdown(
        base_type::shutdown_both,
        error
    );

    base_type::close(error);

    return error;
}

bool session::set_minimal_send(std::size_t bytes)
{
    const auto handle = base_type::native_handle();

    if (handle != -1)
    {
        if (!setsockopt(handle, SOL_SOCKET, SO_SNDLOWAT, &bytes, sizeof(bytes)))
        {
            return true;
        }
    }

    return false;
}

bool session::set_minimal_recv(std::size_t bytes)
{
    const auto handle = base_type::native_handle();

    if (handle != -1)
    {
        if (!setsockopt(handle, SOL_SOCKET, SO_RCVLOWAT, &bytes, sizeof(bytes)))
            return true;
    }

    return false;
}

bool session::set_no_delay(bool no_delay)
{
    boost::system::error_code error;

    base_type::set_option(
        protocol_type::no_delay(no_delay),
        error
    );

    return !error;
}

session::buffer_type::buffer_type(std::size_t size)
: base_type(size)
{ }

void session::buffer_type::append(const void* data, std::size_t size)
{
    const auto old_size = this->size();

    this->resize(old_size + size);
    std::memcpy(this->data() + old_size, data, size);
}

void session::async_read(buffer_type* buffer, receive_handler_type handler)
{
    auto*const header = new header_type;

    boost::asio::async_read(
        *this,
        boost::asio::buffer(header, sizeof(*header)),
        boost::bind(
            &session::async_read_header_completed,
            this, header, buffer, std::move(handler),
            boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred
        )
    );
}

void session::async_read_header_completed(
    header_type* header,
    buffer_type* buffer, receive_handler_type handler,
    const boost::system::error_code& error,
    std::size_t bytes_transferred
)
{
    std::unique_ptr<header_type> cleaner(header);

    if (error || bytes_transferred < sizeof(header_type))
    {
        handler(this, error, 0);
        return;
    }

    buffer->resize(header->data_size);

    boost::asio::async_read(
        *this,
        boost::asio::buffer(buffer->data(), buffer->size()),
        boost::bind(
            std::move(handler),
            this, boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred
        )
    );
}

void session::async_connect(transport_type type, const std::string& host, std::uint16_t port, connect_handler_type handler)
{
    auto protocol = (type == TCP_V4 ? protocol_type::v4() : protocol_type::v6());
    base_type::get_io_service().post(
        [=] {
            resolver_type resolver(base_type::get_io_service());
            resolver_type::query query(protocol, host, std::to_string(port));

            boost::system::error_code error;
            auto it = resolver.resolve(query, error);
            if (error)
            {
                handler(this, error);
            }

            base_type::async_connect(*it, std::bind(handler, this, std::placeholders::_1));
        }
    );
}

} } // namespace zillians::network
