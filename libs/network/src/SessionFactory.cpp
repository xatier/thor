/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <vector>
#include <string>
#include <functional>

#include <boost/asio.hpp>
#include <boost/assert.hpp>

#include "network/Session.h"
#include "network/Listener.h"
#include "network/SessionFactory.h"

namespace zillians { namespace network {

session_factory::session_factory(std::size_t workers_count)
: base_type(workers_count)
{
    workers.reserve(workers_count);
    for (auto cnt = 0; cnt != workers_count; ++cnt)
    {
        workers.emplace_back(
            [this] {
                boost::asio::io_service::work dummy(*this);
                while (true)
                {
                    try
                    {
                        base_type::run();
                        break;
                    }
                    catch (std::exception& e)
                    {
                        /// TODO: report error here
                    }
                }
            }
        );
    }
}

session_factory::~session_factory()
{
    base_type::stop();
    for (auto& worker : workers)
    {
        if (worker.joinable())
        {
            worker.join();
        }
    }
}

session* session_factory::create_session()
{
    auto result = sessions.emplace(new session(*this));

    if (!result.second)
    {
        return nullptr;
    }

    return (result.first)->get();
}

listener* session_factory::create_listener()
{
    auto result = listeners.emplace(new listener(*this));

    if (!result.second)
    {
        return nullptr;
    }

    return (result.first)->get();
}

} } // namespace zillians::network
