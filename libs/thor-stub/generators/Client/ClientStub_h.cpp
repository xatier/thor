/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include "language/context/ManglingStageContext.h"
#include "language/stage/stub/ThorStubStage.h"
#include "language/tree/ASTNodeHelper.h"
#include "language/tree/ASTNode.h"
#include "language/tree/visitor/GenericDoubleVisitor.h"
#include "utility/Foreach.h"
#include "utility/UnicodeUtil.h"
#include <sstream>
#include <iostream>
#include <string>
#include <vector>
using namespace zillians::language;
using namespace zillians::language::tree; // needed by CREATE_INVOKER
struct AtFuncGeneratorVisitor : public visitor::GenericDoubleVisitor
{
    CREATE_INVOKER(generateInvoker, generate);
        std::vector<FunctionDecl*>& mFuncList;
    std::wstring mTag;
    AtFuncGeneratorVisitor(std::vector<FunctionDecl*>& FuncList, std::wstring tag) : mFuncList(FuncList), mTag(tag)
    {
        REGISTER_ALL_VISITABLE_ASTNODE(generateInvoker);
    }
    void generate(ASTNode& node)
    {
        revisit(node);
    }
    void generate(FunctionDecl& node)
    {
        if(node.hasAnnotation(mTag))
            mFuncList.push_back(&node);
    }
};
    template<>
std::string get_stub_filename<stage::ThorStubStage::CLIENT_CLIENTSTUB_H>(stage::ThorStubStage::var_map_t& var_map)
{
    return "ClientStub.h";
}
    template<>
void print_stub<stage::ThorStubStage::CLIENT_CLIENTSTUB_H>(Tangle* node, stage::ThorStubStage::var_map_t& var_map)
{
    std::wcout <<
        "\n"
        "\n"
        "#ifndef CLIENT_CLIENTSTUB_H_\n"
        "#define CLIENT_CLIENTSTUB_H_\n"
        "\n"
        "#include \"zillians/Base.h\"\n"
        "#include \"GameObjects.h\"\n"
        "\n"
        "class xx_ClientStub\n"
        "{\n"
        "public:\n"
        "\n"
        "";
    std::vector<FunctionDecl*> AtClientFuncList;
    { AtFuncGeneratorVisitor v(AtClientFuncList, L"client"); v.visit(*node); }
    auto f = [&](FunctionDecl* node){
        std::wstring function_name = node->name->toString();
        std::wstringstream ss;
        ss << L"0x" << std::hex << stage::SymbolIdManglingContext::get(node)->managled_id;
        std::wstring function_id = ss.str();
        std::wcout <<
            "\n"
            "        virtual void ";
        std::wcout << function_name;
        std::wcout <<
            "(\n"
            "            // GENERATE_BEGIN\n"
            "            ";
        foreach(i, node->parameters)
        {
            TypeSpecifier* type_specifier = (*i)->type;
            if(ASTNodeHelper::isUnspecifiedType(type_specifier))
            {
                ASTNode* resolved_type = ResolvedType::get(*i);
                if(resolved_type && isa<TypeSpecifier>(resolved_type))
                    type_specifier = cast<TypeSpecifier>(resolved_type);
            }
            if(ASTNodeHelper::isPrimitiveType(type_specifier))
                std::wcout << L"zillians::" << ASTNodeHelper::getNodeName((*i)->type) << L" " << (*i)->name->toString() << L"," << std::endl;
            else
                std::wcout << ASTNodeHelper::getNodeName(type_specifier) << L"& " << (*i)->name->toString() << L"," << std::endl;
        }
        std::wcout <<
            "\n"
            "            // GENERATE_END\n"
            "            ) = 0;\n"
            "        ";
    };
    foreach(i, AtClientFuncList) f(*i);
    std::wcout <<
        "\n"
        "\n"
        "};\n"
        "\n"
        "#endif\n"
        "";
}

