/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include "language/stage/stub/ThorStubStage.h"
#include "utility/UnicodeUtil.h"
#include <iostream>
using namespace zillians::language;
using namespace zillians::language::tree;
    template<>
std::string get_stub_filename<stage::ThorStubStage::GATEWAY_GAMECOMMAND_GAMEMODULE_MODULE>(stage::ThorStubStage::var_map_t& var_map)
{
    auto f = [&](std::wstring &s, std::wstring search_string, std::wstring replace_string){
        for(std::wstring::size_type p = 0; (p = s.find(search_string, p)) != std::wstring::npos; p++)
            s.replace(p, search_string.size(), replace_string);
    };
    std::wstring s = L"${GAME_NAME}GAMEMODULE.module";
    f(s, L"${GAME_NAME}", var_map[L"game-name"]);
    return zillians::ws_to_s(s);
}
    template<>
void print_stub<stage::ThorStubStage::GATEWAY_GAMECOMMAND_GAMEMODULE_MODULE>(Tangle* node, stage::ThorStubStage::var_map_t& var_map)
{
    std::wcout <<
        "\n"
        "\n"
        "";
    std::wstring game_name       = var_map[L"game-name"];
    std::wstring translator_uuid = var_map[L"translator-uuid"];
    std::wstring module_uuid     = var_map[L"module-uuid"];
    std::wcout <<
        "\n"
        "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
        "\n"
        "<module id=\"com.zillians.module.";
    std::wcout << game_name;
    std::wcout <<
        "GameModule\"\n"
        "    name=\"";
    std::wcout << game_name;
    std::wcout <<
        "GameModule\" \n"
        "    classid=";
    std::wcout << module_uuid;
    std::wcout <<
        "\n"
        "    version=\"0.2\">\n"
        "    <require id=\"com.zillians.module.world-gateway\" version=\"0.1\"/>\n"
        "    <component id=\"";
    std::wcout << game_name;
    std::wcout <<
        "_GameCommandTranslator\" name=\"";
    std::wcout << game_name;
    std::wcout <<
        "_GameCommandTranslator\" classid=\"";
    std::wcout << translator_uuid;
    std::wcout <<
        "\" lazy=\"false\"/>\n"
        "    <extension point=\"com.zillians.module.world-gateway.network-translator\" component=\"";
    std::wcout << game_name;
    std::wcout <<
        "_GameCommandTranslator\"/>\n"
        "</module>\n"
        "";
    std::wcout <<
        "\n"
        "";
}
