/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <boost/fusion/container/vector.hpp>
#include <boost/spirit/include/karma.hpp>
#include <boost/spirit/include/phoenix.hpp>

#include "language/stage/import/Constants.h"
#include "language/stage/import/TSInfo.h"
#include "language/stage/import/grammar/CxxPackageGrammar.h"
#include "language/stage/import/grammar/ExpectedIterator.h"
#include "language/stage/import/grammar/Helpers.h"

namespace zillians { namespace language { namespace stage { namespace import { namespace grammar {

template<typename iterator>
cxx_package_fwd_grammar<iterator>::cxx_package_fwd_grammar()
    : cxx_package_fwd_grammar::base_type(start)
    , guard_symbol(CXX_HEADER_FWD_FILENAME_GUARD)
{
    using boost::spirit::karma::_1;
    using boost::spirit::karma::_val;
    using boost::spirit::karma::eol;
    using boost::spirit::karma::eps;

    start = package.alias();

    package =
           "#ifndef " << guard_symbol[_1 = _val] << eol
        << "#define " << guard_symbol[_1 = _val] << eol
        << eol
        << ns_begin[_1 = _val]
        << eol
        << (
             *(
                  cls
                | function
              )
           )[_1 = helpers::ts_package_declarations(_val)]
        << eol
        << ns_end  [_1 = _val]
        << eol
        << "#endif /* " << guard_symbol[_1 = _val] << " */" << eol
    ;

    function =
        eps
    ;
}

template<typename iterator>
cxx_package_decl_grammar<iterator>::cxx_package_decl_grammar()
    : cxx_package_decl_grammar::base_type(start)
    , guard_symbol(CXX_HEADER_FILENAME_GUARD)
    , include(CXX_HEADER_FWD_FILENAME)
    , refers(CXX_HEADER_FWD_FILENAME)
{
    using boost::spirit::karma::_1;
    using boost::spirit::karma::_val;
    using boost::spirit::karma::eps;
    using boost::spirit::karma::eol;
    using boost::spirit::karma::string;

    using boost::phoenix::construct;

    start = package.alias();

    package =
           "#ifndef " << guard_symbol[_1 = _val] << eol
        << "#define " << guard_symbol[_1 = _val] << eol
        << eol
        << "#include <limits>" << eol
        << "#include <type_traits>" << eol
        << eol
        << "#include <thor/lang/Language.h>" << eol
        << (
               eps(helpers::ts_package_has_used_builtins_c_string(_val)) << "#include <thor/lang/String.h>" << eol
             | eps
           )
        << eol
        << (*wrapped_header)[_1 = helpers::ts_package_get_wrapped_includes(_val)]
        << eol
        << include [_1 = construct<boost::fusion::vector<ts_package*, ts_package*>>(_val, _val)]
        << eol
        << refers  [_1 = _val]
        << eol
        << ns_begin[_1 = _val]
        << eol
        << (
             *(
                  cls
                | function
              )
           )[_1 = helpers::ts_package_declarations(_val)]
        << eol
        << ns_end  [_1 = _val]
        << eol
        << "#endif  /* " << guard_symbol[_1 = _val] << " */" << eol
    ;

    wrapped_header =
        "#include <" << string << '>' << eol
    ;

    cls =
        cls_impl << eol
    ;

    function =
        function_impl[_1 = construct<boost::fusion::vector<ts_function*, bool>>(_val, false)] << ';' << eol << eol
    ;
}

template<typename iterator>
cxx_package_def_grammar<iterator>::cxx_package_def_grammar()
    : cxx_package_def_grammar::base_type(start)
    , include(CXX_HEADER_FILENAME)
    , refers(CXX_HEADER_FILENAME)
{
    using boost::spirit::karma::_1;
    using boost::spirit::karma::_val;
    using boost::spirit::karma::eol;
    using boost::spirit::karma::eps;

    using boost::phoenix::construct;

    start = package.alias();

    package =
           include [_1 = construct<boost::fusion::vector<ts_package*, ts_package*>>(_val, _val)]
        << eol
        << builtins[_1 = _val]
        << eol
        << refers  [_1 = _val]
        << eol
        << ns_begin[_1 = _val]
        << eol
        << (
             *(
                  cls
                | function
              )
           )[_1 = helpers::ts_package_declarations(_val)]
        << eol
        << ns_end  [_1 = _val]
        << eol
    ;

    builtins =
          eps(helpers::ts_package_has_used_builtins_c_string(_val)) << CXX_BUILTIN_FUNCTION_TO_STRING_IMPL
        | eps
    ;
}

TS_IMPORT_GRAMMAR_INSTANTIATE_TEMPLATES(cxx_package_fwd_grammar)
TS_IMPORT_GRAMMAR_INSTANTIATE_TEMPLATES(cxx_package_decl_grammar)
TS_IMPORT_GRAMMAR_INSTANTIATE_TEMPLATES(cxx_package_def_grammar)

} } } } }
