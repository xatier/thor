/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <boost/fusion/container/vector.hpp>
#include <boost/spirit/include/karma.hpp>
#include <boost/spirit/include/phoenix.hpp>

#include "language/stage/import/TSInfo.h"
#include "language/stage/import/grammar/ExpectedIterator.h"
#include "language/stage/import/grammar/Helpers.h"
#include "language/stage/import/grammar/TsFunctionGrammar.h"

namespace zillians { namespace language { namespace stage { namespace import { namespace grammar {

template<typename iterator>
ts_function_grammar<iterator>::ts_function_grammar() : ts_function_grammar::base_type(start)
{
    using boost::spirit::karma::_1;
    using boost::spirit::karma::_val;
    using boost::spirit::karma::eol;
    using boost::spirit::karma::eps;

    using boost::phoenix::empty;
    using boost::phoenix::construct;

    start = function.alias();

    function =
           optional_indent   [_1 = _val]
        << "@native" << eol
        << optional_indent   [_1 = _val]
        << optional_modifier [_1 = _val]
        << "function "
        << function_name     [_1 = _val]
        << '('
        << parameters        [_1 = helpers::ts_function_parameters(_val)]
        << "):"
        << function_return   [_1 = _val]
        << ';'
        << eol
    ;

    function_name =
          eps(helpers::ts_function_is_constructor(_val)) << "new"
        | eps(helpers::ts_function_is_destructor (_val)) << "delete"
        | identifier[_1 = helpers::ts_decl_id(_val)]
    ;

    function_return =
             eps(
                 helpers::ts_function_is_constructor(_val) ||
                 helpers::ts_function_is_destructor (_val)
             )
          << "void"
        |    type[
                     _1 = construct<boost::fusion::vector<ts_type, ts_package*>>
                          (
                              helpers::ts_function_result_type(_val),
                              helpers::ts_decl_get_package(_val)
                          )
                 ]
    ;

    parameters =
          eps(empty(_val))
        | parameter % ", "
    ;

    optional_indent =
          eps(helpers::ts_decl_member_of(_val) == nullptr)
        | "    "
    ;

    optional_modifier =
          eps(helpers::ts_decl_member_of(_val) == nullptr)
        | "public "
    ;
}

TS_IMPORT_GRAMMAR_INSTANTIATE_TEMPLATES(ts_function_grammar)

} } } } }
