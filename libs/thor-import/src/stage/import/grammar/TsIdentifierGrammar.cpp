/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <boost/spirit/include/karma.hpp>
#include <boost/spirit/include/phoenix.hpp>

#include "language/stage/import/TSInfo.h"
#include "language/stage/import/grammar/ExpectedIterator.h"
#include "language/stage/import/grammar/Helpers.h"
#include "language/stage/import/grammar/TsIdentifierGrammar.h"

namespace zillians { namespace language { namespace stage { namespace import { namespace grammar {

template<typename iterator>
ts_qualified_identifier_grammar<iterator>::ts_qualified_identifier_grammar()
    : ts_qualified_identifier_grammar::base_type(start)
{
    using boost::spirit::karma::_1;
    using boost::spirit::karma::_val;
    using boost::spirit::karma::eps;

    using boost::phoenix::at_c;

    start = dispatch.alias();

    dispatch =
             eps(helpers::ts_decl_scope(at_c<0>(_val)) == at_c<1>(_val))
          << identifier[_1 = helpers::ts_decl_id(at_c<0>(_val))]
        |    qualified [_1 = at_c<0>(_val)]
    ;

    qualified =
           qualified_impl[_1 = helpers::ts_decl_scope(_val)]
        << identifier    [_1 = helpers::ts_decl_id(_val)]
    ;

    qualified_impl =
          eps(helpers::ts_package_depth(_val) == 0)
        |    qualified_impl[_1 = helpers::ts_decl_scope(_val)]
          << identifier    [_1 = helpers::ts_decl_id(_val)]
          << '.'
    ;
}

TS_IMPORT_GRAMMAR_INSTANTIATE_TEMPLATES(ts_qualified_identifier_grammar);

} } } } }
