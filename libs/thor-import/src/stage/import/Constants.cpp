/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <iterator>
#include <string>

#include <boost/algorithm/string/replace.hpp>
#include <boost/uuid/random_generator.hpp>
#include <boost/uuid/uuid_io.hpp>

#include "language/stage/import/CodeSnippes.h"
#include "language/stage/import/Constants.h"

namespace zillians { namespace language { namespace stage { namespace import {

namespace {

std::string random_underscored_uuid()
{
    std::string result = boost::uuids::to_string(boost::uuids::random_generator()());

    std::replace(result.begin(), result.end(), '-', '_');

    return result;
}

std::string generate_code_impl_from_snip(const char* code_snip, const std::string& suffix)
{
    std::string code_impl;

    boost::algorithm::replace_all_copy(std::back_inserter(code_impl), code_snip, "$suffix$", suffix);

    return code_impl;
}

}

// C++ type names
const char* CXX_VOID_NAME             = "void";
const char* CXX_BOOL_NAME             = "bool";
const char* CXX_INT8_NAME             = "::thor::lang::int8";
const char* CXX_INT16_NAME            = "::thor::lang::int16";
const char* CXX_INT32_NAME            = "::thor::lang::int32";
const char* CXX_INT64_NAME            = "::thor::lang::int64";
const char* CXX_FLOAT32_NAME          = "::thor::lang::float32";
const char* CXX_FLOAT64_NAME          = "::thor::lang::float64";
const char* CXX_THOR_LANG_OBJECT_NAME = "::thor::lang::Object";
const char* CXX_THOR_LANG_STRING_NAME = "::thor::lang::String";
const char* CXX_INVALID_TYPE_NAME     = "<invalid-type>";

// Thor type names
const char* TS_VOID_NAME             = "void";
const char* TS_BOOL_NAME             = "bool";
const char* TS_INT8_NAME             = "int8";
const char* TS_INT16_NAME            = "int16";
const char* TS_INT32_NAME            = "int32";
const char* TS_INT64_NAME            = "int64";
const char* TS_FLOAT32_NAME          = "float32";
const char* TS_FLOAT64_NAME          = "float64";
const char* TS_THOR_LANG_OBJECT_NAME = "thor.lang.Object";
const char* TS_THOR_LANG_STRING_NAME = "thor.lang.String";
const char* TS_INVALID_TYPE_NAME     = "<invalid-type>";

// filenames
const char* CXX_HEADER_FILENAME           = "ts_import.h";
const char* CXX_HEADER_FILENAME_GUARD     = "TS_IMPORT_H";
const char* CXX_HEADER_FWD_FILENAME       = "ts_import_fwd.h";
const char* CXX_HEADER_FWD_FILENAME_GUARD = "TS_IMPORT_FWD_H";
const char* CXX_SOURCE_FILENAME           = "ts_import.cpp";
const char* TS_SOURCE_FILENAME            = "ts_import.t";

// for Pimpl
const std::string CXX_PIMPL_SUFFIX                 = random_underscored_uuid();
const std::string CXX_VARIABLE_OWN_IMPL_NAME       = "own_impl_" + CXX_PIMPL_SUFFIX;
const std::string CXX_VARIABLE_IMPL_NAME           = "impl_" + CXX_PIMPL_SUFFIX;
const std::string CXX_VARIABLE_LOCAL_OWN_IMPL_NAME = "local_own_impl_" + CXX_PIMPL_SUFFIX;
const std::string CXX_VARIABLE_LOCAL_IMPL_NAME     = "local_impl_" + CXX_PIMPL_SUFFIX;
const std::string CXX_FUNCTION_CREATOR_PIMPL_NAME  = "create_" + CXX_PIMPL_SUFFIX;
const std::string CXX_FUNCTION_GET_PIMPL_NAME      = "get_impl_" + CXX_PIMPL_SUFFIX;
const std::string CXX_FUNCTION_SET_PIMPL_NAME      = "set_impl_" + CXX_PIMPL_SUFFIX;

// for builtin helpers
const std::string CXX_BUILTIN_SUFFIX                   = random_underscored_uuid();
const std::string CXX_BUILTIN_FUNCTION_TO_STRING_NAME  = "to_string_" + CXX_BUILTIN_SUFFIX;
const std::string CXX_BUILTIN_FUNCTION_TO_STRING_IMPL  = generate_code_impl_from_snip(code_snippes::TO_STRING, CXX_BUILTIN_SUFFIX);

// for temporary variables
const std::string CXX_LOCAL_VARIABLE_SUFFIX = random_underscored_uuid();
const std::string CXX_MEMBER_VARIABLE_SUFFIX = random_underscored_uuid();

} } } }
