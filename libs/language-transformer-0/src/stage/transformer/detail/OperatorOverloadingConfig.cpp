/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include "language/tree/ASTNodeHelper.h"
#include "language/logging/LoggerWrapper.h"
#include "language/logging/StringTable.h"
#include "language/stage/transformer/detail/OperatorOverloadingConfig.h"


namespace zillians { namespace language { namespace stage { namespace visitor {

namespace OperatorOverloading {

Binary::ReservedNameMap 
Binary::reserved_names = { 
                            { Binary::Operation::ARITHMETIC_ADD, L"__add__" }, { Binary::Operation::ARITHMETIC_SUB, L"__sub__" },  
                            { Binary::Operation::ARITHMETIC_MUL, L"__mul__" }, { Binary::Operation::ARITHMETIC_DIV, L"__div__" },  
                            { Binary::Operation::ARITHMETIC_MOD, L"__mod__" },
  
                            { Binary::Operation::BINARY_AND, L"__and__" }, { Binary::Operation::BINARY_OR, L"__or__" },  
                            { Binary::Operation::BINARY_XOR, L"__xor__" }, { Binary::Operation::BINARY_LSHIFT, L"__lshift__" },
                            { Binary::Operation::BINARY_RSHIFT, L"__rshift__" },
  
                            { Binary::Operation::LOGICAL_AND, L"__land__" }, { Binary::Operation::LOGICAL_OR, L"__lor__" },
  
                            { Binary::Operation::COMPARE_LT, L"__lt__" }, { Binary::Operation::COMPARE_LE, L"__le__" },
                            { Binary::Operation::COMPARE_GT, L"__gt__" }, { Binary::Operation::COMPARE_GE, L"__ge__" },
  
                            { Binary::Operation::ADD_ASSIGN, L"__iadd__" }, { Binary::Operation::SUB_ASSIGN, L"__isub__" },
                            { Binary::Operation::MUL_ASSIGN, L"__imul__" }, { Binary::Operation::DIV_ASSIGN, L"__idiv__" },
                            { Binary::Operation::MOD_ASSIGN, L"__imod__" },
  
                            { Binary::Operation::AND_ASSIGN, L"__iand__" }, { Binary::Operation::OR_ASSIGN, L"__ior__" },  
                            { Binary::Operation::XOR_ASSIGN, L"__ixor__" }, { Binary::Operation::LSHIFT_ASSIGN, L"__ilshift__" },
                            { Binary::Operation::RSHIFT_ASSIGN, L"__irshift__" }
                         };


Unary::ReservedNameMap 
Unary::reserved_names = {
                           { Unary::Operation::INCREMENT, L"__inc__" },
                           { Unary::Operation::DECREMENT, L"__dec__" },

                           { Unary::Operation::BINARY_NOT, L"__invert__" }, { Unary::Operation::LOGICAL_NOT, L"__not__" },

                           { Unary::Operation::ARITHMETIC_NEGATE, L"__neg__" }
                        };


bool Binary::supports(tree::BinaryExpr& node)
{
    tree::Type* left_type = node.left->getCanonicalType();
    // only support expression which LHS is user defined type
    if(left_type == nullptr)
        return false;

    // search method with special name in the class type if we support the kind of operator
    if(!left_type->isRecordType())
        return false;

    if(!Super::supports(node.opcode))
    {
        switch(node.opcode)
        {
        case Operation::ASSIGN:
        case Operation::COMPARE_EQ:
        case Operation::COMPARE_NE:
            return false;
        default:
            return false;
        }
    }

    tree::ClassDecl& declaration = *left_type->getAsClassDecl();
    for(tree::FunctionDecl* method : declaration.member_functions)
    {
        // reserved names for operator overloading are simple, templated identifiers
        tree::SimpleIdentifier * identifier = method->name->getSimpleId();

        BOOST_ASSERT(identifier != nullptr);

        auto method_name = identifier->name;

        if(method_name == getReservedNameOf(node.opcode))
        {
            return true;
        }
    }

    return false;
}

bool Unary::supports(tree::UnaryExpr& node)
{
    tree::Type* type = node.node->getCanonicalType();
    // only support expression which is user defined type
    if(type == nullptr)
        return false;

    // search method with special name in the class type if we support the kind of operator
    if(!type->isRecordType())
        return false;

    if(!Super::supports(node.opcode))
        return false;

    tree::ClassDecl& declaration = *type->getAsClassDecl();
    for(tree::FunctionDecl* method : declaration.member_functions)
    {
        // skip template methods 
        if(!tree::isa<tree::SimpleIdentifier>(method->name))
        {
            continue;
        }

        auto method_name = method->name->getSimpleId()->name;

        if(method_name == getReservedNameOf(node.opcode))
        {
            return true;
        }
    }

    return false;
}

}

} } } } // namespace 'zillians::language::stage::visitor'

