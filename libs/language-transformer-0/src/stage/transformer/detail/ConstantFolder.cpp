/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <algorithm>
#include <functional>

#include <boost/assert.hpp>
#include <boost/next_prior.hpp>

#include "language/logging/LoggerWrapper.h"
#include "language/logging/StringTable.h"

#include "language/context/ResolverContext.h"

#include "language/tree/ASTNode.h"
#include "language/tree/ASTNodeHelper.h"
#include "language/tree/basic/Block.h"
#include "language/tree/basic/Literal.h"
#include "language/tree/basic/PrimitiveType.h"
#include "language/tree/basic/TypeSpecifier.h"
#include "language/tree/declaration/EnumDecl.h"
#include "language/tree/expression/BinaryExpr.h"
#include "language/tree/expression/BlockExpr.h"
#include "language/tree/expression/UnaryExpr.h"
#include "language/tree/module/Internal.h"
#include "language/tree/module/Tangle.h"
#include "language/tree/statement/ExpressionStmt.h"

#include "language/stage/transformer/detail/ConstantFolder.h"

namespace zillians { namespace language { namespace stage { namespace visitor { namespace detail {

using language::tree::cast;

namespace {

template<typename value_type>
struct binary_and {
    value_type operator()(const value_type& lhs, const value_type& rhs) const
    {
        return lhs & rhs;
    }
};

template<typename value_type>
struct binary_or {
    value_type operator()(const value_type& lhs, const value_type& rhs) const
    {
        return lhs | rhs;
    }
};

template<typename value_type>
struct binary_xor {
    value_type operator()(const value_type& lhs, const value_type& rhs) const
    {
        return lhs ^ rhs;
    }
};

template<typename value_type>
struct binary_left_shift {
    value_type operator()(const value_type& lhs, const value_type& rhs) const
    {
        return lhs << rhs;
    }
};

template<typename value_type>
struct binary_right_shift {
    value_type operator()(const value_type& lhs, const value_type& rhs) const
    {
        return lhs >> rhs;
    }
};

template<template<class> class computer>
tree::NumericLiteral* compute_aritimethic_impl(tree::PrimitiveKind result_type, bool contains_float, const tree::NumericLiteral& lhs, const tree::NumericLiteral& rhs)
{
    tree::NumericLiteral* result_literal = nullptr;

    if (contains_float)
        result_literal = new tree::NumericLiteral(result_type, computer<double>()(lhs.get<double>(), rhs.get<double>()));
    else
        result_literal = new tree::NumericLiteral(result_type, computer<int64 >()(lhs.get<int64 >(), rhs.get<int64 >()));

    return result_literal;
}

template<>
tree::NumericLiteral* compute_aritimethic_impl<std::divides>(tree::PrimitiveKind result_type, bool contains_float, const tree::NumericLiteral& lhs, const tree::NumericLiteral& rhs)
{
    tree::NumericLiteral* result_literal = nullptr;

    if (contains_float)
        result_literal = new tree::NumericLiteral(result_type, std::divides<double>()(lhs.get<double>(), rhs.get<double>()));
    else if (rhs.get<int64 >() != 0)
        result_literal = new tree::NumericLiteral(result_type, std::divides<int64 >()(lhs.get<int64 >(), rhs.get<int64 >()));
    else
        result_literal = nullptr;

    return result_literal;
}

template<>
tree::NumericLiteral* compute_aritimethic_impl<std::modulus>(tree::PrimitiveKind result_type, bool contains_float, const tree::NumericLiteral& lhs, const tree::NumericLiteral& rhs)
{
    tree::NumericLiteral* result_literal = nullptr;

    if (!contains_float && rhs.get<int64 >() != 0)
        result_literal = new tree::NumericLiteral(result_type, std::modulus<int64 >()(lhs.get<int64 >(), rhs.get<int64 >()));

    return result_literal;
}

template<template<class> class computer>
tree::NumericLiteral* compute_aritimethic(const tree::NumericLiteral& lhs, const tree::NumericLiteral& rhs)
{
    const auto result_type    = std::max(std::max(lhs.primitive_kind, rhs.primitive_kind), tree::PrimitiveKind(tree::PrimitiveKind::INT32_TYPE));
    const bool contains_float = result_type.isFloatType();

    return compute_aritimethic_impl<computer>(result_type, contains_float, lhs, rhs);
}

template<template<class> class computer>
tree::NumericLiteral* compute_logical(const tree::NumericLiteral& lhs, const tree::NumericLiteral& rhs)
{
    tree::NumericLiteral* result_literal = new tree::NumericLiteral(computer<bool>()(lhs.get<bool>(), rhs.get<bool>()));

    return result_literal;
}

template<template<class> class computer>
void compute_comparison_post_action(tree::BinaryExpr& binary_expr, const bool contains_float)
{
}

template<>
void compute_comparison_post_action<std::equal_to>(tree::BinaryExpr& binary_expr, const bool contains_float)
{
    if (contains_float)
    {
        LOG_MESSAGE(EQUAL_COMPARE_WITH_FLOAT, &binary_expr);
    }
}

template<>
void compute_comparison_post_action<std::not_equal_to>(tree::BinaryExpr& binary_expr, const bool contains_float)
{
    if (contains_float)
    {
        LOG_MESSAGE(EQUAL_COMPARE_WITH_FLOAT, &binary_expr);
    }
}

template<>
void compute_comparison_post_action<std::greater_equal>(tree::BinaryExpr& binary_expr, const bool contains_float)
{
    if (contains_float)
    {
        LOG_MESSAGE(EQUAL_COMPARE_WITH_FLOAT, &binary_expr);
    }
}

template<>
void compute_comparison_post_action<std::less_equal>(tree::BinaryExpr& binary_expr, const bool contains_float)
{
    if (contains_float)
    {
        LOG_MESSAGE(EQUAL_COMPARE_WITH_FLOAT, &binary_expr);
    }
}

template<template<class> class computer>
tree::NumericLiteral* compute_comparison(tree::BinaryExpr& binary_expr, const tree::NumericLiteral& lhs, const tree::NumericLiteral& rhs)
{
    const bool contains_float = lhs.primitive_kind.isFloatType() ||
                                rhs.primitive_kind.isFloatType();

    tree::NumericLiteral* result_literal = nullptr;

    if (contains_float)
        result_literal = new tree::NumericLiteral(computer<double>()(lhs.get<double>(), rhs.get<double>()));
    else
        result_literal = new tree::NumericLiteral(computer<int64 >()(lhs.get<int64 >(), rhs.get<int64 >()));

    compute_comparison_post_action<computer>(binary_expr, contains_float);

    return result_literal;
}

template<template<class> class computer>
bool compute_binary_pre_condition(tree::BinaryExpr& binary_expr, const tree::NumericLiteral& lhs, const tree::NumericLiteral& rhs)
{
    return true;
}

template<>
bool compute_binary_pre_condition<binary_right_shift>(tree::BinaryExpr& binary_expr, const tree::NumericLiteral& lhs, const tree::NumericLiteral& rhs)
{
    const int64_t shift_bound = lhs.byteSize() * 8;

    int64_t maximum_count_can_shift_right = 0LL;
    for (int64_t shift = 0LL; shift < shift_bound; ++shift)
    {
        const int64_t mask = 1LL << shift;
        if (lhs.get<int64>() & mask)
            break;

        ++maximum_count_can_shift_right;
    }

    if(maximum_count_can_shift_right < rhs.get<int64>())
    {
        LOG_MESSAGE(NUMERIC_LITERAL_UNDERFLOW, &binary_expr);

        return false;
    }

    return true;
}

template<>
bool compute_binary_pre_condition<binary_left_shift>(tree::BinaryExpr& binary_expr, const tree::NumericLiteral& lhs, const tree::NumericLiteral& rhs)
{
    const int64_t shift_bound = lhs.byteSize() * 8;

    for (int64_t shift = 0LL; shift < shift_bound; ++shift)
    {
        const int64_t mask = 1LL << shift;

        if ((lhs.get<int64>() & mask) && (shift_bound-shift-1) < rhs.get<int64>())
        {
            LOG_MESSAGE(NUMERIC_LITERAL_OVERFLOW, &binary_expr);

            return false;
        }
    }

    return true;
}

template<template<class> class computer>
tree::NumericLiteral* compute_binary(tree::BinaryExpr& binary_expr, const tree::NumericLiteral& lhs, const tree::NumericLiteral& rhs)
{
    const bool contains_float = lhs.primitive_kind.isFloatType() ||
                                rhs.primitive_kind.isFloatType();

    tree::NumericLiteral* result_literal = nullptr;

    if (!contains_float && compute_binary_pre_condition<computer>(binary_expr, lhs, rhs))
        result_literal = new tree::NumericLiteral(lhs.primitive_kind, computer<int64>()(lhs.get<int64>(), rhs.get<int64>()));

    return result_literal;
}

tree::NumericLiteral* compute(tree::BinaryExpr& binary_expr, const tree::NumericLiteral& lhs, const tree::NumericLiteral& rhs)
{
    tree::NumericLiteral* result_literal = nullptr;

    switch (binary_expr.opcode)
    {
    case tree::BinaryExpr::OpCode::ARITHMETIC_ADD: result_literal = compute_aritimethic<std::plus      >(lhs, rhs); break;
    case tree::BinaryExpr::OpCode::ARITHMETIC_SUB: result_literal = compute_aritimethic<std::minus     >(lhs, rhs); break;
    case tree::BinaryExpr::OpCode::ARITHMETIC_MUL: result_literal = compute_aritimethic<std::multiplies>(lhs, rhs); break;
    case tree::BinaryExpr::OpCode::ARITHMETIC_DIV: result_literal = compute_aritimethic<std::divides   >(lhs, rhs); break;
    case tree::BinaryExpr::OpCode::ARITHMETIC_MOD: result_literal = compute_aritimethic<std::modulus   >(lhs, rhs); break;

    case tree::BinaryExpr::OpCode::BINARY_AND   : result_literal = compute_binary<binary_and        >(binary_expr, lhs, rhs); break;
    case tree::BinaryExpr::OpCode::BINARY_OR    : result_literal = compute_binary<binary_or         >(binary_expr, lhs, rhs); break;
    case tree::BinaryExpr::OpCode::BINARY_XOR   : result_literal = compute_binary<binary_xor        >(binary_expr, lhs, rhs); break;
    case tree::BinaryExpr::OpCode::BINARY_LSHIFT: result_literal = compute_binary<binary_left_shift >(binary_expr, lhs, rhs); break;
    case tree::BinaryExpr::OpCode::BINARY_RSHIFT: result_literal = compute_binary<binary_right_shift>(binary_expr, lhs, rhs); break;

    case tree::BinaryExpr::OpCode::LOGICAL_AND: result_literal = compute_logical<std::logical_and>(lhs, rhs); break;
    case tree::BinaryExpr::OpCode::LOGICAL_OR : result_literal = compute_logical<std::logical_or >(lhs, rhs); break;

    case tree::BinaryExpr::OpCode::COMPARE_EQ: result_literal = compute_comparison<std::equal_to     >(binary_expr, lhs, rhs); break;
    case tree::BinaryExpr::OpCode::COMPARE_NE: result_literal = compute_comparison<std::not_equal_to >(binary_expr, lhs, rhs); break;
    case tree::BinaryExpr::OpCode::COMPARE_GT: result_literal = compute_comparison<std::greater      >(binary_expr, lhs, rhs); break;
    case tree::BinaryExpr::OpCode::COMPARE_GE: result_literal = compute_comparison<std::greater_equal>(binary_expr, lhs, rhs); break;
    case tree::BinaryExpr::OpCode::COMPARE_LE: result_literal = compute_comparison<std::less_equal   >(binary_expr, lhs, rhs); break;
    case tree::BinaryExpr::OpCode::COMPARE_LT: result_literal = compute_comparison<std::less         >(binary_expr, lhs, rhs); break;
    }

    return result_literal;
}

tree::NumericLiteral* compute(tree::UnaryExpr& node, const tree::NumericLiteral& literal)
{
    typedef tree::UnaryExpr::OpCode OpCode;

    tree::NumericLiteral* result_literal = nullptr;

    switch (node.opcode)
    {
    case OpCode::BINARY_NOT:
        if( ! literal.primitive_kind.isFloatType() ) {
           result_literal = new tree::NumericLiteral(
               std::max(
                   literal.primitive_kind,
                   tree::PrimitiveKind(tree::PrimitiveKind::INT32_TYPE)
               ),
               ~literal.get<int64>()
           );
        }
        break;

    case OpCode::LOGICAL_NOT:
        result_literal = new tree::NumericLiteral(!literal.get<bool>());
        break;

    case OpCode::ARITHMETIC_NEGATE:
        if (literal.primitive_kind.isFloatType())
        {
            result_literal = new tree::NumericLiteral(literal.primitive_kind, -literal.get<double>());
        }
        else
        {
            const int64 origin_value = literal.get<int64>();
            const auto  target_kind  = std::max<tree::PrimitiveKind>(literal.primitive_kind, tree::PrimitiveKind::INT32_TYPE);

            if (origin_value < -tree::PrimitiveKind::maxIntegerValue(target_kind))
            {
                LOG_MESSAGE(NUMERIC_LITERAL_OVERFLOW, &node);
            }

            result_literal = new tree::NumericLiteral(target_kind, -origin_value);
        }

        break;

    default:
        break;
    }

    return result_literal;
}

tree::NumericLiteral* get_numeric_literal(const tree::Expression& expr)
{
    return const_cast<tree::NumericLiteral*>(cast<const tree::NumericLiteral>(&expr));
}

const tree::VariableDecl* get_enumerator(const tree::Expression& expr)
{
    const tree::ASTNode* canonical_symbol = tree::ASTNodeHelper::getCanonicalSymbol(&expr);

    if (canonical_symbol == nullptr)
        return nullptr;

    const tree::VariableDecl* canonical_var_decl = cast<const tree::VariableDecl>(canonical_symbol);

    if (canonical_var_decl == nullptr)
        return nullptr;

    if (!canonical_var_decl->isEnumerator())
        return nullptr;

    return canonical_var_decl;
}

void replace_expression_by_constant(tree::ASTNode& parent, tree::Expression& expr, tree::NumericLiteral* constant)
{
    parent.replaceUseWith(expr, *constant);
    tree::ASTNodeHelper::propogateSourceInfo(*constant, parent);

    tree::Tangle* tangle = parent.getOwner<tree::Tangle>();

    BOOST_ASSERT(tangle && "tangle is not found");
    BOOST_ASSERT(tangle->internal && "no internal node under tangle");

    tree::Type* result_type = tangle->internal->getPrimitiveType(constant->primitive_kind);
    ResolvedType::set(constant, result_type);
}

bool convert_value_to_value(tree::Expression& origin_expr, const tree::NumericLiteral& num_literal, const tree::PrimitiveType& target_primitive_type)
{
    BOOST_ASSERT(
        (
            num_literal.primitive_kind.isBoolType() ||
            num_literal.primitive_kind.isIntegerType() ||
            num_literal.primitive_kind.isFloatType()
        ) && "not handled numeric literal value type"
    );

    const auto target_primitive_kind = target_primitive_type.getKind();
    tree::NumericLiteral* num_result = nullptr;

    if (num_literal.primitive_kind.isFloatType())
        num_result = new tree::NumericLiteral(target_primitive_kind, num_literal.get<double>());
    else
        num_result = new tree::NumericLiteral(target_primitive_kind, num_literal.get<int64>());

    replace_expression_by_constant(*origin_expr.parent, origin_expr, num_result);

    return true;
}

bool convert_enum_to_value(tree::Expression& origin_expr, const tree::VariableDecl& enumerator, const tree::PrimitiveType& target_primitive_type)
{
    BOOST_ASSERT(enumerator.isEnumerator() && "we should handle non-enumerator in other places");

    tree::NumericLiteral* num_result = new tree::NumericLiteral(target_primitive_type.getKind(), enumerator.getEnumeratorValue());

    replace_expression_by_constant(*origin_expr.parent, origin_expr, num_result);

    return true;
}

bool convert_to_value(tree::CastExpr& cast_expr, const tree::PrimitiveType& target_primitive_type)
{
    BOOST_ASSERT(cast_expr.node && "null pointer exception");

    bool is_folded = false;

    if (const tree::NumericLiteral* num_literal = get_numeric_literal(*cast_expr.node))
    {
        is_folded = convert_value_to_value(cast_expr, *num_literal, target_primitive_type);
    }
    else if (const tree::VariableDecl* enumerator = get_enumerator(*cast_expr.node))
    {
        is_folded = convert_enum_to_value(cast_expr, *enumerator, target_primitive_type);
    }

    return is_folded;
}

bool convert_to_enum(tree::CastExpr& cast_expr, const tree::EnumDecl& target_enum)
{
    BOOST_ASSERT(cast_expr.node && "null pointer exception");

    int64 target_value = 0;

    if (const tree::NumericLiteral* num_literal = get_numeric_literal(*cast_expr.node))
        target_value = num_literal->get<int64>();
    else if (const tree::VariableDecl* enumerator = get_enumerator(*cast_expr.node))
        target_value = enumerator->getEnumeratorValue();
    else
        return false;

    tree::VariableDecl* enumerator = target_enum.findEnumerator(target_value);

    if (enumerator == nullptr)
    {
        LOG_MESSAGE(INVALID_VALUE_FOR_ENUM, cast_expr.node);

        return false;
    }

    BOOST_ASSERT(target_enum.name && "null pointer exception");
    BOOST_ASSERT(enumerator->name && "null pointer exception");

    const tree::SimpleIdentifier* target_enum_sid = cast<tree::SimpleIdentifier>(target_enum.name);
    const tree::SimpleIdentifier*  enumerator_sid = cast<tree::SimpleIdentifier>(enumerator->name);

    BOOST_ASSERT(target_enum_sid && "name of enumeration should be a simple identifier!");
    BOOST_ASSERT( enumerator_sid && "name of enumerator should be a simple identifier!");

    tree::IdExpr*      expr_enum_name  = new tree::IdExpr(target_enum_sid->clone());
    tree::MemberExpr*  expr_enum_value = new tree::MemberExpr(expr_enum_name, enumerator_sid->clone());

    ResolvedType  ::set(expr_enum_name  , target_enum.getType());
    ResolvedSymbol::set(expr_enum_name  , const_cast<tree::EnumDecl*>(&target_enum));

    ResolvedType  ::set(expr_enum_value , target_enum.getType());
    ResolvedSymbol::set(expr_enum_value , enumerator);

    cast_expr.parent->replaceUseWith(cast_expr, *expr_enum_value);
    tree::ASTNodeHelper::propogateSourceInfo(*expr_enum_value, cast_expr);

    return true;
}

} // anonymous namespace

bool fold_constant(tree::BinaryExpr& binary_expr)
{
    BOOST_ASSERT(binary_expr.left  && "null pointer exception");
    BOOST_ASSERT(binary_expr.right && "null pointer exception");

    tree::NumericLiteral* num_left  = get_numeric_literal(*binary_expr.left );
    tree::NumericLiteral* num_right = get_numeric_literal(*binary_expr.right);

    if (num_left == nullptr || num_right == nullptr)
        return false;

    tree::NumericLiteral* num_result = compute(binary_expr, *num_left, *num_right);

    if (num_result == nullptr)
        return false;

    BOOST_ASSERT(binary_expr.parent && "null pointer exception");

    replace_expression_by_constant(*binary_expr.parent, binary_expr, num_result);

    return true;
}

bool fold_constant(tree::UnaryExpr& unary_expr)
{
    BOOST_ASSERT(unary_expr.node && "null pointer exception");

    tree::NumericLiteral* num_literal = get_numeric_literal(*unary_expr.node);

    if (num_literal == nullptr)
        return false;

    tree::NumericLiteral* num_result = compute(unary_expr, *num_literal);

    if (num_result == nullptr)
        return false;

    BOOST_ASSERT(unary_expr.parent && "null pointer exception");

    replace_expression_by_constant(*unary_expr.parent, unary_expr, num_result);

    return true;
}

bool fold_constant(tree::CastExpr& cast_expr)
{
    bool is_folded = false;

    BOOST_ASSERT(cast_expr.type && "null pointer exception");

    const tree::Type* target_canonical_type = cast_expr.type->getCanonicalType();

    BOOST_ASSERT(target_canonical_type && "canonical type is not ready");

    if (const tree::PrimitiveType* target_type = target_canonical_type->getAsPrimitiveType())
    {
        is_folded = convert_to_value(cast_expr, *target_type);
    }
    else if (const tree::EnumDecl* target_enum = target_canonical_type->getAsEnumDecl())
    {
        is_folded = convert_to_enum(cast_expr, *target_enum);
    }

    return is_folded;
}

bool fold_constant(tree::BlockExpr& block_expr)
{
    using tree::cast;
    using tree::isa;

    if (block_expr.tag != "isa_restructure")
        return false;

    tree::Block* block = block_expr.block;

    BOOST_ASSERT(block && "null pointer exception");
    BOOST_ASSERT(
        (
            block->objects.size() == 2 &&
            isa<tree::ExpressionStmt>(*            block->objects.begin() ) &&
            isa<tree::ExpressionStmt>(*boost::next(block->objects.begin()))
        ) && "restructed format of IsaExpr is not correct"
    );

    tree::ExpressionStmt* origin_expr_stmt = cast<tree::ExpressionStmt>(*            block->objects.begin() );
    tree::ExpressionStmt* result_expr_stmt = cast<tree::ExpressionStmt>(*boost::next(block->objects.begin()));

    BOOST_ASSERT(origin_expr_stmt && "null pointer exception");
    BOOST_ASSERT(origin_expr_stmt->expr && "null pointer exception");
    BOOST_ASSERT(result_expr_stmt && "null pointer exception");
    BOOST_ASSERT(get_numeric_literal(*result_expr_stmt->expr) != nullptr && "we expect second statement is a constant expression");

    const bool is_origin_expr_constant =
        get_numeric_literal(*origin_expr_stmt->expr) != nullptr ||
        get_enumerator     (*origin_expr_stmt->expr) != nullptr
    ;

    if (!is_origin_expr_constant)
        return false;

    block_expr.parent->replaceUseWith(block_expr, *result_expr_stmt->expr);

    return true;
}

bool fold_constant(tree::TernaryExpr& ternary_expr)
{
    // skip if two operands have different types
    if (ResolvedType::get(&ternary_expr) == nullptr)
        return false;

    BOOST_ASSERT(ternary_expr.cond != nullptr &&
                 ternary_expr.true_node != nullptr &&
                 ternary_expr.false_node != nullptr && "nodes cannot be null");

    auto* condition = get_numeric_literal(*ternary_expr.cond);
    if (condition == nullptr)
        return false;

    auto* replacer = condition->get<bool>() ? ternary_expr.true_node : ternary_expr.false_node;

    ternary_expr.parent->replaceUseWith(ternary_expr, *replacer);

    return true;
}

} } } } }
