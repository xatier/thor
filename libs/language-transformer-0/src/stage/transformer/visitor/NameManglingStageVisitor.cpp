/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include "language/resolver/Specialization.h"
#include "language/tree/ASTNodeHelper.h"
#include "language/tree/UniqueName.h"
#include "language/stage/transformer/visitor/NameManglingStageVisitor.h"

namespace zillians { namespace language { namespace stage { namespace visitor {

using namespace language::tree;

NameManglingStageVisitor::NameManglingStageVisitor()// : next_type_id(1024), next_symbol_id(0)
{
    REGISTER_ALL_VISITABLE_ASTNODE(mangleInvoker);
}

void NameManglingStageVisitor::apply(Type& node)
{
    node.unique_name = create_unique_name(node);

    revisit(node);
}

void NameManglingStageVisitor::apply(ASTNode& node)
{
    revisit(node);
}

void NameManglingStageVisitor::apply(Source& node)
{
    revisit(node);
}

void NameManglingStageVisitor::apply(Package& node)
{
    node.unique_name = create_unique_name(node);

    revisit(node);
}

void NameManglingStageVisitor::apply(Identifier& node)
{
    if(!NameManglingContext::get(&node))
    {
        NameManglingContext::set(&node, new NameManglingContext(mangler.encode(node.toString())));
    }
}

void NameManglingStageVisitor::apply(Declaration& node)
{
    node.unique_name = create_unique_name(node);

    revisit(node);
}

void NameManglingStageVisitor::apply(ClassDecl& node)
{
    node.unique_name = create_unique_name(node);

    // generate unique name for typename types, and return if this is
    // a non-fully-specialized template class
    if (auto* templated_identifier = cast<TemplatedIdentifier>(node.name))
    {
        auto& typenames = templated_identifier->templated_type_list;
        for (auto* typename_decl : typenames)
        {
            typename_decl->unique_name = create_unique_name(*typename_decl);

            auto* typename_type = typename_decl->getType();
            typename_type->unique_name = create_unique_name(*typename_type);
        }

        // non-fully-specialized template class
        if (!templated_identifier->isFullySpecialized())
            return;
    }

    if(!NameManglingContext::get(&node))
    {
        mangler.mangleClassAsScope(true);
        mangler.visit(node);
        NameManglingContext::set(&node, new NameManglingContext(mangler.stream.str()));
        mangler.reset();
    }

    if(node.isCompleted())
    {
        auto*const tangle = node.getOwner<Tangle>();
        BOOST_ASSERT(tangle != nullptr && "tangle not found!");

        tangle->addType(node);
    }

    revisit(node);
}

void NameManglingStageVisitor::apply(EnumDecl& node)
{
    node.unique_name = create_unique_name(node);

    if(!NameManglingContext::get(&node))
    {
        mangler.visit(node);
        NameManglingContext::set(&node, new NameManglingContext(mangler.stream.str()));
        mangler.reset();
    }

    revisit(node);
}

void NameManglingStageVisitor::apply(FunctionDecl& node)
{
    node.unique_name = create_unique_name(node);

    // generate unique name for typename types, and return if this is
    // a non-fully-specialized template function
    if (auto* templated_identifier = cast<TemplatedIdentifier>(node.name))
    {
        auto& typenames = templated_identifier->templated_type_list;
        for (auto* typename_decl : typenames)
        {
            typename_decl->unique_name = create_unique_name(*typename_decl);

            auto* typename_type = typename_decl->getType();
            typename_type->unique_name = create_unique_name(*typename_type);
        }

        // non-fully-specialized template function
        if (!templated_identifier->isFullySpecialized())
            return;
    }

    if(!NameManglingContext::get(&node))
    {
        mangler.visit(node);
        NameManglingContext::set(&node, new NameManglingContext(mangler.stream.str()));
        mangler.reset();
    }

    if(node.isCompleted())
    {
        auto*const tangle = node.getOwner<Tangle>();
        BOOST_ASSERT(tangle != nullptr && "tangle not found!");

        tangle->addFunction(node);
    }

    revisit(node);
}

void NameManglingStageVisitor::apply(VariableDecl& node)
{
    node.unique_name = create_unique_name(node);

    if(!NameManglingContext::get(&node))
    {
    	if (node.isGlobal() && !node.getOwner<Package>()->isRoot())
    	{
    		// Only global variables which is under a certain package need special treating for name mangling
    		mangler.visit(node);
    		NameManglingContext::set(&node, new NameManglingContext(mangler.stream.str()));
    	}
    	else
    	{
    		NameManglingContext::set(&node, new NameManglingContext(mangler.encode(node.name->toString())));
    	}
        mangler.reset();
    }

    // use mangling for static/global variables
    if(node.is_static)
    {
        if(!StaticVariableManglingContext::get(&node))
        {
            mangler.visit(node);

            std::string name(mangler.stream.str());
            std::string guard_name(name);

            StringUtil::substitute(guard_name, "_Z", "_ZGV");
            StaticVariableManglingContext::set(&node, new StaticVariableManglingContext(name, guard_name));
            mangler.reset();
        }
    }
}

} } } } // namespace 'zillians::language::stage::visitor'
