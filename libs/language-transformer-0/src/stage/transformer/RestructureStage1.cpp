/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include "language/context/ParserContext.h"
#include "language/logging/LoggerWrapper.h"
#include "language/tree/ASTNodeHelper.h"
#include "language/tree/visitor/PrettyPrintVisitor.h"
#include "language/stage/transformer/RestructureStage1.h"
#include "language/stage/transformer/visitor/RestructureStageVisitor1.h"
#include "language/stage/transformer/visitor/RestructureStageLambdaVisitor1.h"

namespace zillians { namespace language { namespace stage {

RestructureStage1::RestructureStage1() : dump_graphviz(false)
{ }

RestructureStage1::~RestructureStage1()
{ }

const char* RestructureStage1::name()
{
    return "Restructure Stage 1";
}

std::pair<shared_ptr<po::options_description>, shared_ptr<po::options_description>> RestructureStage1::getOptions()
{
    shared_ptr<po::options_description> option_desc_public(new po::options_description());
    shared_ptr<po::options_description> option_desc_private(new po::options_description());

    option_desc_public->add_options()
            ("no-system", "no system bundle");

    for(auto& option : option_desc_public->options()) option_desc_private->add(option);

    option_desc_private->add_options()
        ("debug-restructure-stage", "debug restructure stage")
        //("dump-graphviz", "dump AST in graphviz format")
        //("dump-graphviz-dir", po::value<std::string>(), "dump AST in graphviz format")
    ;

    return std::make_pair(option_desc_public, option_desc_private);
}

bool RestructureStage1::parseOptions(po::variables_map& vm)
{
    debug = (vm.count("debug-restructure-stage") > 0);
    dump_graphviz = vm["dump-graphviz"].as<bool>();
    no_system_bundle = (vm.count("no-system") > 0);

    if(vm.count("dump-graphviz-dir") > 0)
    {
        dump_graphviz_dir = vm["dump-graphviz-dir"].as<std::string>();
    }

    return true;
}

bool RestructureStage1::execute(bool& continue_execution)
{
    UNUSED_ARGUMENT(continue_execution);

    if(!hasParserContext())
        return false;

    ParserContext& parser_context = getParserContext();

    BOOST_ASSERT(parser_context.tangle && "no tangle!");

    restruct_report.reset();
    if(parser_context.tangle)
    {
        static int phase = 0;
        ++phase;

        if(dump_graphviz)
        {
            boost::filesystem::path p(dump_graphviz_dir);
            std::ostringstream oss;
            oss << "pre-restructure-1-" << (phase-1) << ".dot";
            tree::ASTNodeHelper::visualize(getParserContext().tangle, p / oss.str());
            oss.str("");
            oss << "pre-restructure-1-" << (phase-1) << ".t";

            getParserContext().tangle->toSource(p / oss.str());
        }

        // restructure the entire tree in multiple passes
        while(true)
        {
            unsigned split_reference_count;
            unsigned assign_null_count;
            unsigned system_api_transform_count;
            unsigned typeid_resolved_count;
            unsigned new_restruct_count;
            unsigned isa_restruct_count;
            unsigned generated_function_count;

            visitor::RestructureStageVisitor1 restruct(no_system_bundle);
            restruct.visit(*parser_context.tangle);

            if(restruct.hasTransforms())
            {
                restruct.applyTransforms();
            }
            else
            {
                break;
            }

            restruct.getStatus(split_reference_count, assign_null_count, system_api_transform_count, 
                                           typeid_resolved_count, new_restruct_count,
                                           isa_restruct_count, generated_function_count);

            restruct_report.split_reference_count += split_reference_count;
            restruct_report.assign_null_count += assign_null_count;
            restruct_report.system_api_transform_count += system_api_transform_count;
            restruct_report.typeid_resolved_count += typeid_resolved_count;
            restruct_report.new_restruct_count += new_restruct_count;
            restruct_report.isa_restruct_count += isa_restruct_count;
            restruct_report.generated_function_count += generated_function_count;
        }

        LOG4CXX_DEBUG(LoggerWrapper::TransformerStage, "RestructureStage1: Phase " << phase << " Report");
        LOG4CXX_DEBUG(LoggerWrapper::TransformerStage, "      split reference count   : " << restruct_report.split_reference_count);
        LOG4CXX_DEBUG(LoggerWrapper::TransformerStage, "      assign null count       : " << restruct_report.assign_null_count);
        LOG4CXX_DEBUG(LoggerWrapper::TransformerStage, "      system api trans count  : " << restruct_report.system_api_transform_count);
        LOG4CXX_DEBUG(LoggerWrapper::TransformerStage, "      type id resolved count  : " << restruct_report.typeid_resolved_count);
        LOG4CXX_DEBUG(LoggerWrapper::TransformerStage, "      new restruct count      : " << restruct_report.new_restruct_count);
        LOG4CXX_DEBUG(LoggerWrapper::TransformerStage, "      isa restruct count      : " << restruct_report.isa_restruct_count);
        LOG4CXX_DEBUG(LoggerWrapper::TransformerStage, "      lambda class construct  : " << restruct_report.lambda_class_construct_count);
        LOG4CXX_DEBUG(LoggerWrapper::TransformerStage, "      generated function count: " << restruct_report.generated_function_count);

        if(dump_graphviz)
        {
            std::ostringstream oss;
            oss << "post-restructure-1-" << (phase-1) << ".dot";
            boost::filesystem::path p(dump_graphviz_dir);
            tree::ASTNodeHelper::visualize(getParserContext().tangle, p / oss.str());
            oss.str("");
            oss << "post-restructure-1-" << (phase-1) << ".t";
            getParserContext().tangle->toSource(p / oss.str());
        }

        visitor::RestructureStageLambdaVisitor1 restruct_lambda(no_system_bundle);
        restruct_lambda.visit(*parser_context.tangle);
        if(restruct_lambda.hasTransforms())
        {
            restruct_lambda.applyTransforms();
        }

        restruct_lambda.getStatus(restruct_report.lambda_class_construct_count, restruct_report.async_restruct_count);

        if(dump_graphviz)
        {
            std::ostringstream oss;
            oss << "post-restructure-lambda-1-" << (phase-1) << ".dot";
            boost::filesystem::path p(dump_graphviz_dir);
            tree::ASTNodeHelper::visualize(getParserContext().tangle, p / oss.str());
            oss.str("");
            oss << "post-restructure-lambda-1-" << (phase-1) << ".t";
            getParserContext().tangle->toSource(p / oss.str());
        }

        if(debug)
        {
            tree::visitor::PrettyPrintVisitor printer;
            printer.visit(*parser_context.tangle);
        }

        return true;
    }
    else
    {
        return false;
    }
}

bool RestructureStage1::hasProgress()
{
    return restruct_report.hasProgress();
}

} } }
