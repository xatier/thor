/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <algorithm>
#include <functional>
#include <sstream>
#include <string>
#include <vector>

#include <boost/foreach.hpp>
#include <boost/assert.hpp>
#include <boost/logic/tribool.hpp>
#include <boost/optional/optional.hpp>
#include <boost/range/adaptor/indirected.hpp>
#include <boost/range/adaptor/transformed.hpp>
#include <boost/range/algorithm/find_if.hpp>
#include <boost/range/iterator_range.hpp>

#include "language/resolver/Deduction.h"
#include "language/resolver/Deductor.h"
#include "language/resolver/Helper.h"
#include "language/tree/ASTNodeFactory.h"
#include "language/tree/ASTNodeHelper.h"

namespace zillians { namespace language { namespace resolution {

using namespace language::tree;

namespace
{

// forward declarations
template<typename Range>
std::vector<typename Range::iterator> get_most_specialized(const Range& candidates);
template<typename Range>
std::vector<typename Range::iterator> get_best_matches(const Range& candidates);

Type* get_type_of_typename_decl(const decltype(deduction::SingleInfo::deducted_types)& deducted_types, TypenameDecl& td);
std::vector<relinkable_ptr<Type>> get_key_of_template_decl(const decltype(deduction::SingleInfo::deducted_types)& deducted_types, const Declaration& decl);

boost::optional<FunctionDecl*> find_specialization_or_instantiation(FunctionDecl& generic_decl, const decltype(deduction::SingleInfo::deducted_types)& deducted_types);

// implementations
template<typename Range>
std::vector<typename Range::iterator> get_most_specialized(const Range& candidates)
{
    std::vector<typename Range::iterator> most_specialized_candidates;

    for(auto i = candidates.begin(); i != candidates.end(); ++i)
    {
        bool is_specialized_than_others = true;

        for(auto j = candidates.begin(); j != candidates.end(); ++j)
        {
            if(i != j)
            {
                if(!deduction::is_more_or_equally_specialized(*i, *j))
                {
                    is_specialized_than_others = false;

                    break;
                }
            }
        }

        if(is_specialized_than_others)
        {
            most_specialized_candidates.push_back(i);
        }
    }

    return most_specialized_candidates;
}

template<typename Range>
std::vector<typename Range::iterator> get_best_matches(const Range& candidates)
{
    std::vector<typename Range::iterator> result;

    for(auto i = candidates.begin(); i != candidates.end(); ++i)
    {
        bool is_better_or_equal_match = true;

        for(auto j = candidates.begin(); j != candidates.end(); ++j)
        {
            if(i != j)
            {
                const auto& i_value = *i;
                const auto& j_value = *j;

                if(!deduction::is_better_or_equally_matched(i_value, j_value))
                {
                    is_better_or_equal_match = false;

                    break;
                }
            }
        }

        if(is_better_or_equal_match)
        {
            result.push_back(i);
        }
    }

    return result;
}

Type* get_type_of_typename_decl(const decltype(deduction::SingleInfo::deducted_types)& deducted_types, TypenameDecl& td)
{
    const auto& deducted_type_pos = deducted_types.find(&td);

    BOOST_ASSERT(deducted_type_pos != deducted_types.end() && "template parameter is not yet deducted!");
    BOOST_ASSERT(deducted_type_pos->second.type != nullptr && "null pointer exception");

    Type*const canonical_type = deducted_type_pos->second.type->getCanonicalType();

    BOOST_ASSERT(canonical_type && "cannot get canonical type from deduction information!");

    return canonical_type;
}

std::vector<relinkable_ptr<Type>> get_key_of_template_decl(const decltype(deduction::SingleInfo::deducted_types)& deducted_types, const Declaration& decl)
{
    using boost::adaptors::indirected;
    using boost::adaptors::transformed;

    BOOST_ASSERT(decl.name != nullptr && "null pointer exception");

    const auto*const decl_tid = decl.name->getTemplateId();

    BOOST_ASSERT(decl_tid != nullptr && "template declaration has no template identifier");

    const auto& key_range = decl_tid->templated_type_list
                          | indirected
                          | transformed(std::bind(&get_type_of_typename_decl, std::cref(deducted_types), std::placeholders::_1))
                          ;

    return {key_range.begin(), key_range.end()};
}

boost::optional<FunctionDecl*> find_specialization_or_instantiation(FunctionDecl& generic_decl, const decltype(deduction::SingleInfo::deducted_types)& deducted_types)
{
    BOOST_ASSERT(helper::is_template(generic_decl) && "non-template declarations should not be here");
    BOOST_ASSERT(helper::is_generic_template(generic_decl) && "generic declaration is not generic!");

    const auto*const specialization_map = SpecializedAs::get(&generic_decl);

    if(specialization_map == nullptr)
        return {};

    const auto& key = get_key_of_template_decl(deducted_types, generic_decl);

    {
        const auto& specialization_pos = specialization_map->find(key);

        if(specialization_pos != specialization_map->end())
        {
            BOOST_ASSERT(specialization_pos->second != nullptr && "null pointer exception");
            BOOST_ASSERT(isa<FunctionDecl>(specialization_pos->second) && "specialization of function is not a function");

            return cast<FunctionDecl>(specialization_pos->second);
        }
    }

    const auto& instantiated_as   = InstantiatedAs::init(&generic_decl)->value;
    const auto& instantiation_pos = instantiated_as.find(key);

    if(instantiation_pos != instantiated_as.end())
    {
        BOOST_ASSERT(instantiation_pos->second != nullptr && "null pointer exception");
        BOOST_ASSERT(isa<FunctionDecl>(instantiation_pos->second) && "instantiation of function is not a function");

        return cast<FunctionDecl>(instantiation_pos->second);
    }

    return static_cast<FunctionDecl*>(nullptr);
}

}

Deductor::Deductor(const std::vector<ASTNode*>& candidates)
    : deducted_info()
    , not_selected_begin()
    , in_progress_begin()
    , not_match_begin()
{
    deducted_info.reserve(candidates.size());

    BOOST_FOREACH(ASTNode* candidate, candidates)
    {
        SingleInfo info( candidate, SingleInfo::IN_PROGRESS );

        deducted_info.push_back(info);
    };

    not_selected_begin = deducted_info.begin();
    in_progress_begin = deducted_info.begin();
    not_match_begin = deducted_info.end();
}

std::size_t Deductor::get_in_progress_count() const
{
    return static_cast<std::size_t>(get_in_progress_range().size());
}

std::size_t Deductor::get_not_match_count() const
{
    return static_cast<std::size_t>(get_not_match_range().size());
}

std::size_t Deductor::get_full_match_count() const
{
    return static_cast<std::size_t>(get_full_match_range().size());
}

std::size_t Deductor::get_best_match_count() const
{
    return static_cast<std::size_t>(get_best_match_range().size());
}

boost::iterator_range<Deductor::InfoContainer::iterator> Deductor::get_in_progress_range()
{
    return boost::make_iterator_range(in_progress_begin, not_match_begin);
}

boost::iterator_range<Deductor::InfoContainer::iterator> Deductor::get_not_match_range()
{
    return boost::make_iterator_range(not_match_begin, static_cast<InfoContainer::iterator>(deducted_info.end()));
}

boost::iterator_range<Deductor::InfoContainer::iterator> Deductor::get_full_match_range()
{
    return boost::make_iterator_range(not_selected_begin, in_progress_begin);
}

boost::iterator_range<Deductor::InfoContainer::iterator> Deductor::get_best_match_range()
{
    return boost::make_iterator_range(static_cast<InfoContainer::iterator>(deducted_info.begin()), not_selected_begin);
}

boost::iterator_range<Deductor::InfoContainer::const_iterator> Deductor::get_in_progress_range() const
{
    return boost::make_iterator_range(in_progress_begin, not_match_begin);
}

boost::iterator_range<Deductor::InfoContainer::const_iterator> Deductor::get_not_match_range() const
{
    return boost::make_iterator_range(static_cast<InfoContainer::const_iterator>(not_match_begin), deducted_info.end());
}

boost::iterator_range<Deductor::InfoContainer::const_iterator> Deductor::get_full_match_range() const
{
    return boost::make_iterator_range(not_selected_begin, in_progress_begin);
}

boost::iterator_range<Deductor::InfoContainer::const_iterator> Deductor::get_best_match_range() const
{
    return boost::make_iterator_range(deducted_info.begin(), static_cast<InfoContainer::const_iterator>(not_selected_begin));
}

boost::tribool Deductor::deduct(const TemplatedIdentifier& explicit_specializations)
{
    if(!filter_non_matched(explicit_specializations, false))
    {
        return boost::indeterminate;
    }

    auto is_deducted = deduct_impl(explicit_specializations);

    if(is_deducted)
    {
        if(get_full_match_count() > 0)
        {
            const auto most_specialized = get_most_specialized(get_full_match_range());

            BOOST_ASSERT(most_specialized.size() <= get_full_match_count() && "count of most specialization candidate is larget than original!");
            BOOST_ASSERT(not_selected_begin == deducted_info.begin() && "there must not most specialization before here!");

            BOOST_FOREACH(auto i, most_specialized)
            {
                if(not_selected_begin != i)
                {
                    std::swap(*not_selected_begin, *i);
                }

                ++not_selected_begin;
            }
        }

        if(get_best_match_count() != 1)
        {
            is_deducted = false;
        }
    }

    return is_deducted;
}

boost::tribool Deductor::deduct(const std::vector<Expression*>& implicit_expressions)
{
    boost::tribool is_deducted = deduct_impl(implicit_expressions);

    if(is_deducted)
    {
        is_deducted = fill_missing_arguments_by_default_types();
    }

    if(is_deducted)
    {
        is_deducted = verify_best_match();
    }

    return is_deducted;
}

boost::tribool Deductor::deduct(const TemplatedIdentifier& explicit_specializations, const std::vector<Expression*>& implicit_expressions)
{
    if(!filter_non_matched(explicit_specializations, true))
    {
        return boost::indeterminate;
    }

    auto is_deducted = deduct_impl(explicit_specializations);

    if(is_deducted)
    {
        BOOST_ASSERT(
            (
                get_best_match_count()  != 0 ||
                get_full_match_count()  != 0 ||
                get_in_progress_count() != 0
            ) && "there must be in progress or succeeded item on success"
        );

        std::for_each(deducted_info.begin(), in_progress_begin, [](SingleInfo& info) {
            info.state = SingleInfo::IN_PROGRESS;
        });

        not_selected_begin = deducted_info.begin();
        in_progress_begin = deducted_info.begin();

        is_deducted = deduct_impl(implicit_expressions);

        if(is_deducted)
        {
            is_deducted = fill_missing_arguments_by_default_types();
        }

        if(is_deducted)
        {
            is_deducted = verify_best_match();
        }
    }

    return is_deducted;
}

std::wstring Deductor::get_status() const
{
    std::wstringstream stream;

    stream << L"Candidate  : " << deducted_info.size();

    auto outputer = [&stream](const std::wstring& prefix, const boost::iterator_range<InfoContainer::const_iterator>& range)
    {
        stream
            << std::endl << prefix << L": " << range.size();

        for(decltype(range.size()) i = 0; i != range.size(); ++i)
        {
            const SingleInfo& info = range[i];

            stream
                << std::endl << L"\tCandidate #" << i << L": " << std::hex << info.candidate << L", " << info.candidate->instanceName();

            stream
                << std::endl << L"\t\tMatch State: " << info.state;

            for(std::size_t j = 0; j != info.ranks.size(); ++j)
            {
                const Rank& rank = info.ranks[j];

                stream
                    << std::endl << L"\t\tRank #" << j << L": " << static_cast<int>(rank.conv_rank) << L", " << rank.nested_level;
            }

            if(info.deducted_types.empty())
            {
                stream
                    << std::endl << L"\t\tNo deducted types";
            }
            else
            {
                BOOST_FOREACH(const auto& deducted_type, info.deducted_types)
                {
                    TypenameDecl* typename_decl = deducted_type.first;
                    const SingleInfo::DeductInfo& deduct_info = deducted_type.second;

                    stream
                        << std::endl << L"\t\tDeduct " << typename_decl->name->toString() << L":";

                    stream
                        << std::endl << L"\t\t\tAs  : " << deduct_info.type->toString()
                        << std::endl << L"\t\t\tBy  : " << deduct_info.by
                        << std::endl << L"\t\t\tRank: " << static_cast<int>(deduct_info.rank.conv_rank) << L", " << deduct_info.rank.nested_level;
                }
            }
        }
    };

    outputer(L"Best-Match ", get_best_match_range() );
    outputer(L"Full-Match ", get_full_match_range() );
    outputer(L"In-Progress", get_in_progress_range());
    outputer(L"Not-Match  ", get_not_match_range()  );

    return stream.str();
}

void Deductor::err_status() const
{
    std::wcerr << get_status() << std::endl;
}

boost::tribool Deductor::deduct_impl(const TemplatedIdentifier& explicit_template_id)
{
    filter_non_template();

    if(get_in_progress_count() == 0)
    {
        return false;
    }

    if(!helper::is_all_resolved(explicit_template_id))
    {
        return boost::indeterminate;
    }

    if(!filter_explicit(explicit_template_id))
    {
        return boost::indeterminate;
    }

    return  get_best_match_count()  != 0 ||
            get_full_match_count()  != 0 ||
            get_in_progress_count() != 0;
}

boost::tribool Deductor::deduct_impl(const std::vector<Expression*>& implicit_expressions)
{
    filter_non_function();
    filter_function_specialization();

    if(get_in_progress_count() == 0)
    {
        return false;
    }

    bool               has_multi_value = false;
    std::vector<Type*> implicit_types;

    for (Expression* expr : implicit_expressions)
    {
        BOOST_ASSERT(expr && "null pointer exception");

        Type* type = expr->getCanonicalType();

        if (type == nullptr)
            return boost::indeterminate;

        has_multi_value |= type->isMultiType();
        implicit_types.push_back(type);
    }

    if (has_multi_value)
    {
        // multi-value is not allowed in parameters

        simple_filter_impl(
            [](ASTNode*)
            {
                return true;
            }
        );
    }

    if(!filter_implicit(implicit_expressions, implicit_types))
    {
        return boost::indeterminate;
    }

    return  get_best_match_count()  != 0 ||
            get_full_match_count()  != 0 ||
            get_in_progress_count() != 0;
}

boost::tribool Deductor::verify_best_match()
{
    if(get_full_match_count() > 0)
    {
        {
            const auto best_matches = get_best_matches(get_full_match_range());

            BOOST_ASSERT(best_matches.size() <= get_full_match_count() && "count of most specialization candidate is larget than original!");
            BOOST_ASSERT(not_selected_begin == deducted_info.begin() && "there must not most specialization before here!");

            for(InfoContainer::iterator i: best_matches)
            {
                if(not_selected_begin != i)
                {
                    std::swap(*not_selected_begin, *i);
                }

                ++not_selected_begin;
            }
        }

        if(get_best_match_count() == 1)
        {
            SingleInfo& best_match_info = get_best_match_range().front();

            ASTNode* candidate = best_match_info.candidate;

            BOOST_ASSERT(candidate && "null pointer exception");

            FunctionDecl* func_decl = cast<FunctionDecl>(candidate);

            BOOST_ASSERT(func_decl && "we should use this function when function declarations are the all possibilities");

            if(helper::is_template(*func_decl) && helper::is_generic_template(*func_decl))
            {
                const boost::optional<FunctionDecl*> spec_or_inst_info = find_specialization_or_instantiation(*func_decl, best_match_info.deducted_types);

                if(spec_or_inst_info.is_initialized())
                {
                    if(FunctionDecl* spec_or_inst_decl = *spec_or_inst_info)
                    {
                        // there is matched specialization or instantiation, use it as the result

                        const auto spec_or_inst_info_pos = boost::find_if(deducted_info, [spec_or_inst_decl](const SingleInfo& info) -> bool
                        {
                            BOOST_ASSERT(info.candidate && "null pointer exception");

                            return info.candidate == spec_or_inst_decl;
                        });

                        BOOST_ASSERT(spec_or_inst_info_pos != deducted_info.end() && "specialization of function declaration is not collected!");

                        std::swap(best_match_info, *spec_or_inst_info_pos);
                        std::swap(best_match_info.state, spec_or_inst_info_pos->state);
                    }
                }
                else
                {
                    return boost::indeterminate;
                }
            }
        }
    }

    return get_best_match_count() == 1;
}

void Deductor::filter_non_template()
{
    simple_filter_impl(std::not1(std::ptr_fun(&deduction::is_template_declaration)));
}

void Deductor::filter_non_function()
{
    simple_filter_impl(std::not1(std::ptr_fun<const ASTNode*, bool>(&isa<FunctionDecl>)));
}

void Deductor::filter_function_specialization()
{
    simple_filter_impl([](ASTNode* node) -> bool
    {
        BOOST_ASSERT(node && "null exception");

        if(FunctionDecl* func_decl = cast<FunctionDecl>(node))
        {
            BOOST_ASSERT(func_decl->name && "null pointer exception");

            if(helper::is_template(*func_decl) && !helper::is_generic_template(*func_decl))
            {
                return true;
            }
        }

        return false;
    });
}

bool Deductor::filter_non_matched(const TemplatedIdentifier& use_id, const bool is_call)
{
    for(auto i = in_progress_begin; i != not_match_begin; )
    {
        SingleInfo& info = *i;
        ASTNode* candidate = info.candidate;

        SingleInfo::MatchState match_state = SingleInfo::NOT_MATCH;

        if(Declaration* decl = cast<Declaration>(candidate))
        {
            const auto is_matched = deduction::is_identifier_matched(const_cast<TemplatedIdentifier&>(use_id), *decl->name, is_call);

            if(boost::indeterminate(is_matched))
            {
                return false;
            }
            else if(is_matched)
            {
                match_state = SingleInfo::IN_PROGRESS;
            }
            else
            {
                match_state = SingleInfo::NOT_MATCH;
            }
        }

        mark_in_progress_as(i, match_state);
    }

    return true;
}

bool Deductor::fill_missing_arguments_by_default_types()
{
    // for each candidates, try to fill missing arguments by default values
    for(auto single_info = in_progress_begin; single_info != not_match_begin; )
    {
        auto* candidate = (*single_info).candidate;
        auto* decl_template_id = cast<TemplatedIdentifier>(cast<Declaration>(candidate)->name);
        auto& deducted_types = (*single_info).deducted_types;

        fill_by_default_types(deducted_types, *decl_template_id);

        bool all_types_are_deducted = deducted_types.size() == decl_template_id->templated_type_list.size();
        auto match_state = all_types_are_deducted ? SingleInfo::FULL_MATCH : SingleInfo::IN_PROGRESS;

        mark_in_progress_as(single_info, match_state);
    }

    return  get_best_match_count()  != 0 ||
            get_full_match_count()  != 0 ||
            get_in_progress_count() != 0;
}

bool Deductor::filter_explicit(const TemplatedIdentifier& explicit_template_id)
{
    for(auto i = in_progress_begin; i != not_match_begin; )
    {
        SingleInfo& info = *i;
        ASTNode* candidate = info.candidate;

        BOOST_ASSERT(isa<Declaration>(candidate) && isa<TemplatedIdentifier>(cast<Declaration>(candidate)->name) && "candidate must be template declaration");

        TemplatedIdentifier* decl_template_id = cast<TemplatedIdentifier>(cast<Declaration>(candidate)->name);
        SingleInfo::MatchState match_state = SingleInfo::NOT_MATCH;
        decltype(info.deducted_types) deducted_types;

        if(explicit_template_id.templated_type_list.size() <= decl_template_id->templated_type_list.size())
        {
            if(!helper::is_all_resolved(*decl_template_id))
            {
                return false;
            }

            auto specialize_result = deduction::explicit_specialize(explicit_template_id.templated_type_list, decl_template_id->templated_type_list);

            const bool no_error = boost::get<1>(specialize_result);

            if(no_error)
            {
                deducted_types.swap(boost::get<2>(specialize_result));

                match_state = deducted_types.size() == decl_template_id->templated_type_list.size() ? SingleInfo::FULL_MATCH : SingleInfo::IN_PROGRESS;
            }
        }

        std::swap(info.deducted_types, deducted_types);

        mark_in_progress_as(i, match_state);
    }

    return true;
}

bool Deductor::filter_implicit(const std::vector<Expression*>& implicit_expressions, const std::vector<Type*>& implicit_types)
{
    for(auto i = in_progress_begin; i != not_match_begin; )
    {
        SingleInfo& info = *i;
        ASTNode* candidate = info.candidate;

        BOOST_ASSERT(isa<FunctionDecl>(candidate) && "candidate must be function");

        FunctionDecl* decl = cast<FunctionDecl>(candidate);
        SingleInfo::MatchState match_state = SingleInfo::NOT_MATCH;
        auto& deducted_types = info.deducted_types;

        auto result = deduction::implicit(decl->parameters, implicit_expressions, implicit_types, deducted_types);

        const boost::tribool& is_matched = result.first;

        if(boost::indeterminate(is_matched))
        {
            return false;
        }
        else if(!is_matched)
        {
            match_state = SingleInfo::NOT_MATCH;
        }
        else
        {
            TemplatedIdentifier* decl_tid = cast<TemplatedIdentifier>(cast<FunctionDecl>(candidate)->name);

            if(decl_tid != nullptr && !deduction::is_specialization_matched(*decl_tid, deducted_types))
            {
                match_state = SingleInfo::NOT_MATCH;
            }
            else
            {
                std::swap(info.ranks, result.second);

                if(decl_tid != nullptr)
                {
                    match_state = deducted_types.size() == decl_tid->templated_type_list.size() ? SingleInfo::FULL_MATCH : SingleInfo::IN_PROGRESS;
                }
                else
                {
                    match_state = SingleInfo::FULL_MATCH;
                }
            }
        }

        mark_in_progress_as(i, match_state);
    }

    return true;
}

template<typename Condition>
void Deductor::simple_filter_impl(Condition cond)
{
    const auto valid_end = std::partition(in_progress_begin, not_match_begin, [=](const SingleInfo& info) -> bool
    {
        const bool invalid = cond(info.candidate);

        return !invalid;
    });
    std::for_each(valid_end, not_match_begin, [](SingleInfo& info) -> void
    {
        info.state = SingleInfo::NOT_MATCH;
    });
    BOOST_ASSERT(
        std::find_if(
            valid_end,
            not_match_begin,
            [](const SingleInfo& info) -> bool
            {
                return info.state != SingleInfo::NOT_MATCH;
            }
        ) == not_match_begin && "all invalid candidate should be marked as NOT_MATCH"
    );

    not_match_begin = valid_end;
}

void Deductor::mark_in_progress_as(InfoContainer::iterator& i, SingleInfo::MatchState state)
{
    BOOST_ASSERT(in_progress_begin->state == SingleInfo::IN_PROGRESS && "state of item in in-progress range is not IN_PROGERSS");
    BOOST_ASSERT(
        (
            state == SingleInfo::IN_PROGRESS ||
            state == SingleInfo::NOT_MATCH   ||
            state == SingleInfo::FULL_MATCH
        ) && "not supported state"
    );

    i->state = state;

    switch(state)
    {
    case SingleInfo::FULL_MATCH:
        std::iter_swap(i, in_progress_begin);
        ++i;
        ++in_progress_begin;
        break;
    case SingleInfo::NOT_MATCH:
        --not_match_begin;
        std::iter_swap(i, not_match_begin);
        break;
    default:
        ++i;
        break;
    }
}

} } }
