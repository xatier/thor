/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <algorithm>
#include <memory>
#include <string>
#include <vector>

#include <boost/foreach.hpp>
#include <boost/logic/tribool.hpp>
#include <boost/mpl/end.hpp>
#include <boost/mpl/insert_range.hpp>
#include <boost/mpl/vector.hpp>

#include "language/context/ResolverContext.h"
#include "language/logging/StringTable.h"
#include "language/resolver/Deductor.h"
#include "language/resolver/Resolver.h"
#include "language/tree/ASTNodeFactory.h"
#include "language/tree/ASTNodeHelper.h"
#include "language/tree/visitor/NodeInfoVisitor.h"

namespace zillians { namespace language { namespace resolution {

using tree::cast;
using tree::isa;

namespace {

std::wstring get_function_caller_string(tree::Identifier& id, const std::vector<tree::Expression*>& parameters)
{
    std::wstring result = id.toString();

    result += L'(';

    for(auto i = parameters.begin(); i != parameters.end(); ++i)
    {
        tree::Expression* parameter = *i;

        if(i != parameters.begin())
        {
            result += L", ";
        }

        tree::Type* resolved = parameter->getCanonicalType();
        std::wstring type_string = resolved->toString();

        result += type_string;
    }

    result += L')';

    return result;
}

}

bool Instantiater::hasRequest() const
{
    return !items.empty();
}

bool Instantiater::request(const deduction::SingleInfo& info, tree::ASTNode& attach)
{
    tree::ASTNode* candidate = info.candidate;

    BOOST_ASSERT(isa<tree::Declaration>(candidate) && "only declaration requires instantiation");
    BOOST_ASSERT(
          isa<tree::TemplatedIdentifier>(cast<tree::Declaration>(candidate)->name) &&
        !cast<tree::TemplatedIdentifier>(cast<tree::Declaration>(candidate)->name)->isFullySpecialized() &&
        "only not fully specialized template requires instantiation"
    );

    tree::Declaration* decl = cast<tree::Declaration>(candidate);
    tree::TemplatedIdentifier* decl_tid = cast<tree::TemplatedIdentifier>(decl->name);

    InstantiationKey key;

    auto& template_decl = key.first;
    auto& specialization_list = key.second;

    template_decl = decl;

    BOOST_FOREACH(tree::TypenameDecl* typename_decl, decl_tid->templated_type_list)
    {
        tree::Type* specialized_type = NULL;

        if(typename_decl->specialized_type == NULL)
        {
            const auto deducted_type_pos = info.deducted_types.find(typename_decl);

            BOOST_ASSERT(deducted_type_pos != info.deducted_types.end() && "no deducted type for instantiation request");

            specialized_type = deducted_type_pos->second.type->getCanonicalType();
        }

        specialization_list.push_back(specialized_type);
    }

    auto& requested_nodes = items[key];

    BOOST_ASSERT(std::find(requested_nodes.begin(), requested_nodes.end(), &attach) == requested_nodes.end() && "already requested for the same request");

    requested_nodes.push_back(&attach);

    return true;
}

void Instantiater::instantiate()
{
    BOOST_FOREACH(const auto& item, items)
    {
        tree::Declaration* decl = item.first.first;
        const std::vector<tree::Type*>& specialization_list = item.first.second;
        const std::vector<tree::ASTNode*>& requesters = item.second;

        tree::Declaration* cloned_decl = instantiateOne(*decl, specialization_list);
        BOOST_ASSERT(cloned_decl != NULL && "null pointer exception");

        {
            auto& instantiated_as = InstantiatedAs::init(decl)->value;
            auto  key             = InstantiatedAsTraits::generateKey(specialization_list);

            const auto& is_inserted = instantiated_as.emplace(std::move(key), tree::make_relinkable(cloned_decl)).second;
                                      InstantiatedFrom::set(cloned_decl, decl);

            BOOST_ASSERT(is_inserted && "there is another declaration with same instantiation key!?");
        }

        BOOST_FOREACH(tree::ASTNode* requester, requesters)
        {
            //ResolvedType::set(requester, cloned_decl);
            tree::Type* new_type = nullptr;
            if(tree::ClassDecl* class_decl = cast<tree::ClassDecl>(cloned_decl))
            {
                new_type = class_decl->getType();
                ResolvedType::set(requester, new_type);
                ResolvedSymbol::set(requester, cloned_decl);
            }
            else if(tree::FunctionDecl* func_decl = cast<tree::FunctionDecl>(cloned_decl))
            {
                ResolvedSymbol::set(requester, cloned_decl);
            }
        }
    }

    items.clear();
}

tree::Declaration* Instantiater::instantiateOne(tree::Declaration& decl, const std::vector<tree::Type*>& specialization_list)
{
    BOOST_ASSERT(
          isa<tree::TemplatedIdentifier>(decl.name) &&
        !cast<tree::TemplatedIdentifier>(decl.name)->isFullySpecialized() &&
        "instantiating fully specialized template declaration"
    );

    typedef boost::mpl::insert_range<
                        tree::detail::ContextToCloneTypeList       ,
        boost::mpl::end<tree::detail::ContextToCloneTypeList>::type,
        boost::mpl::vector<
            zillians::language::ResolvedType,
            zillians::language::ResolvedSymbol,
            zillians::language::SplitReferenceContext,
            zillians::language::SplitInverseReferenceContext
        >
    >::type ContextToCloneList;

    tree::Declaration*         cloned_decl     = tree::ASTNodeHelper::clone<tree::Declaration, ContextToCloneList>(&decl);
    tree::TemplatedIdentifier* cloned_decl_tid = cast<tree::TemplatedIdentifier>(cloned_decl->name);

    cloned_decl->may_conflict = true;

    if(stage::SourceInfoContext* source_context = stage::SourceInfoContext::get(&decl))
    {
        stage::SourceInfoContext::set(cloned_decl, new stage::SourceInfoContext(*source_context));
    }

    if(tree::FunctionDecl*const cloned_func_decl = cast<tree::FunctionDecl>(cloned_decl))
    {
        for(tree::VariableDecl*const parameter : cloned_func_decl->parameters)
        {
            parameter->setInitializer(nullptr);
        }
    }

    BOOST_ASSERT(cloned_decl != NULL && cloned_decl_tid != NULL && "null pointer exception");
    BOOST_ASSERT(cloned_decl_tid->templated_type_list.size() == specialization_list.size());

    for(size_t i=0; i != specialization_list.size(); ++i)
    {
        tree::Type* specialized_type = specialization_list[i];
        tree::TypenameDecl* cloned_typename_decl = cloned_decl_tid->templated_type_list[i];

        if(cloned_typename_decl->specialized_type == NULL)
        {
            tree::TypeSpecifier* specialized_type_ts = tree::ASTNodeHelper::createTypeSpecifierFrom(specialized_type);

            // specialize the typename declaration
            cloned_typename_decl->setSpecializdType(specialized_type_ts);
        }
        else
        {
            BOOST_ASSERT(specialized_type == NULL && "specialized type for already specialized TypenameDecl!");
        }
    }

    if(tree::ClassDecl* class_decl = cast<tree::ClassDecl>(decl.parent))
    {
        BOOST_ASSERT(
             isa<tree::FunctionDecl>(&decl) &&
            cast<tree::FunctionDecl>(&decl)->is_member &&
            "only template member function could be instantiated under class declaration"
        );

        tree::FunctionDecl* member_func_decl = cast<tree::FunctionDecl>(cloned_decl);

        class_decl->addFunction(member_func_decl);
    }
    else
    {
        BOOST_ASSERT(isa<tree::Source>(decl.parent) && decl.isGlobal() && "only global declaration could be instantiated, except template member function");

        tree::Source* source = cast<tree::Source>(decl.parent);

        source->addDeclare(cloned_decl);
    }

    tree::ASTNodeHelper::foreachApply<tree::ASTNode>(
        *cloned_decl,
        [](tree::ASTNode& node) -> void
        {
            if(tree::Type* resolved_type = ResolvedType::get(&node))
            {
                if(tree::ClassDecl* cls_decl = resolved_type->getAsClassDecl())
                {
                    if(!cls_decl->isCompleted())
                    {
                        ResolvedType::set(&node, nullptr);
                        ResolvedSymbol::set(&node, nullptr);
                    }
                }
            }
        },
        [](tree::ASTNode& node) -> bool
        {
            if(tree::ClassDecl* cls_decl = cast<tree::ClassDecl>(&node))
            {
                return cls_decl->isCompleted();
            }
            else if(tree::FunctionDecl* func_decl = cast<tree::FunctionDecl>(&node))
            {
                return func_decl->isCompleted();
            }
            else
            {
                return true;
            }
        }
    );

    return cloned_decl;
}

bool PackageResolver::isResolved(tree::ASTNode& node) const
{
    return ResolvedPackage::get(&node) != NULL;
}

boost::tribool PackageResolver::resolve(tree::ASTNode& attach, tree::Identifier& id, const std::vector<tree::ASTNode*>& candidates)
{
    BOOST_ASSERT(!candidates.empty() && "there must be at least one candidate for resolving");

    tree::Package* package = cast<tree::Package>(candidates.front());

    const bool is_valid_package = package != NULL;

    if(is_valid_package)
    {
        BOOST_ASSERT(package != NULL && "null pointer exception");
        BOOST_ASSERT(candidates.size() == 1 && "must be has single candidate for package");

        LOG4CXX_DEBUG(LoggerWrapper::Resolver, L"package \"" << id.toString() << L"\" is resolved to: \"" << tree::ASTNodeHelper::getNodeName(package) << L"\"");

        ResolvedPackage::set(&attach, package);
    }

    return is_valid_package;
}

TypeResolver::TypeResolver(Instantiater& instantiater)
    : instantiater(&instantiater)
{
}

bool TypeResolver::isResolved(tree::ASTNode& node) const
{
    return  ResolvedType::get(&node) != NULL;
}

boost::tribool TypeResolver::resolve(tree::ASTNode& attach, tree::Identifier& id, const std::vector<tree::ASTNode*>& candidates)
{
    BOOST_ASSERT(!candidates.empty() && "there must be at least one candidate for resolving");

    std::pair<boost::tribool, tree::Type*> resolve_info(false, nullptr);

    boost::tribool& is_resolved = resolve_info.first;
    tree::Type*& resolved_type = resolve_info.second;

    BOOST_ASSERT(isAllSameType(candidates) && "resolver cannot handle different types of nodes");

    if(tree::TemplatedIdentifier* tid = cast<tree::TemplatedIdentifier>(&id))
    {
        resolve_info = resolveTemplateId(attach, *tid, candidates);
    }
    else if(tree::SimpleIdentifier* sid = cast<tree::SimpleIdentifier>(&id))
    {
        resolve_info = resolveSimpleId(attach, *sid, candidates);
    }
    else
    {
        UNREACHABLE_CODE();
    }

    if(is_resolved)
    {
        BOOST_ASSERT(resolved_type != NULL && "null pointer exception");

        LOG4CXX_DEBUG(LoggerWrapper::Resolver, L"type \"" << id.toString() << L"\" is resolved to: \"" << tree::ASTNodeHelper::getNodeName(resolved_type) << L"\"");

        ResolvedType::set(&attach, resolved_type);
    }
    else if(!is_resolved)
    {
        if(resolved_type != NULL)
        {
            // resolved but invalid
            if(tree::Declaration* decl = resolved_type->getAsDecl())
            {
                if(isa<tree::TemplatedIdentifier>(decl->name))
                {
                    LOG4CXX_DEBUG(LoggerWrapper::Resolver, L"cannot resolve non-template type \"" << id.toString() << L"\" to \"" << decl->name->toString() << L"\"");
                }
                else
                {
                    LOG4CXX_DEBUG(LoggerWrapper::Resolver, L"resolve type \"" << id.toString() << L"\" to declaration \"" << decl->name->toString() << L"\"");
                }
            }
            else
            {
                LOG4CXX_FATAL(LoggerWrapper::Resolver, L"resolve type \"" << id.toString() << L"\" to unknown type");
            }
        }
        else
        {
            if(candidates.size() > 1)
            {
                LOG4CXX_DEBUG(LoggerWrapper::Resolver, L"ambiguous type \"" << id.toString() << L"\"");

                tree::visitor::NodeInfoVisitor node_info_visitor;

                for (tree::ASTNode* candidate: candidates)
                {
                    LOG4CXX_DEBUG(LoggerWrapper::Resolver, L"type can be resolved to: \"" << tree::ASTNodeHelper::getNodeName(candidate) << L"\"");
                }
            }
            else
            {
                LOG4CXX_DEBUG(LoggerWrapper::Resolver, L"unresolved type \"" << id.toString() << L"\"");
            }

        }
    }

    return is_resolved;
}

tree::Type* get_candidate_type(tree::ASTNode* node)
{
    BOOST_ASSERT_MSG(isa<tree::ClassDecl>(node) ||
                     isa<tree::EnumDecl>(node) ||
                     isa<tree::FunctionDecl>(node) ||
                     isa<tree::TypedefDecl>(node) ||
                     isa<tree::TypenameDecl>(node),
                     "If this assert fail, maybe the assertion is WRONG. May add more if-else");

    tree::Type* resolved_type = nullptr;
    if(tree::ClassDecl* class_decl = cast<tree::ClassDecl>(node))
        resolved_type = class_decl->getType();
    else if(tree::EnumDecl* enum_decl = cast<tree::EnumDecl>(node))
        resolved_type = enum_decl->getType();
    else if(tree::TypenameDecl* typename_decl = cast<tree::TypenameDecl>(node))
        resolved_type = typename_decl->getType();
    else if(tree::TypedefDecl* typedef_decl = cast<tree::TypedefDecl>(node))
        resolved_type = typedef_decl->getType();
    else if(tree::FunctionDecl* func_decl = cast<tree::FunctionDecl>(node))
        resolved_type = tree::Type::getBy(*func_decl);
    else
        UNREACHABLE_CODE();

    return resolved_type;
}

std::pair<boost::tribool, tree::Type*> TypeResolver::resolveTemplateId(tree::ASTNode& attach, tree::TemplatedIdentifier& tid, const std::vector<tree::ASTNode*>& candidates)
{
    BOOST_ASSERT(!candidates.empty() && "there must be at least one candidate for resolving");

    std::pair<boost::tribool, tree::Type*> result(false, NULL);

    boost::tribool& is_resolved = result.first;
    tree::Type*& resolved_type = result.second;

    resolution::Deductor deductor(candidates);

    is_resolved = deductor.deduct(tid);

    if(is_resolved)
    {
        const auto best_match_range = deductor.get_best_match_range();

        BOOST_ASSERT(best_match_range.size() == 1 && "succeeded but best match is not 1");

        const deduction::SingleInfo& best_match = best_match_range.front();

        is_resolved = isValidType(*best_match.candidate);

        if(is_resolved)
        {
            BOOST_ASSERT(best_match.candidate && "null pointer exception");
            resolved_type = get_candidate_type(best_match.candidate);
        }
        else if(boost::indeterminate(is_resolved))
        {
            if(isValidInstantiation(tid))
            {
                instantiater->request(best_match, attach);
            }
            else
            {
                tree::Type* resolved_type = get_candidate_type(best_match.candidate);
                ResolvedType::set(&attach, resolved_type);
            }
        }
    }

    return result;
}

std::pair<boost::tribool, tree::Type*> TypeResolver::resolveSimpleId(tree::ASTNode& attach, tree::SimpleIdentifier& sid, const std::vector<tree::ASTNode*>& candidates)
{
    BOOST_ASSERT(!candidates.empty() && "there must be at least one candidate for resolving");

    std::pair<boost::tribool, tree::Type*> result(false, nullptr);

    boost::tribool& is_resolved = result.first;
    tree::Type*& resolved_type = result.second;

    if(candidates.size() == 1)
    {
        auto* candidate = candidates.front();
        BOOST_ASSERT(candidate != nullptr && "candidate is never null when initializing");

        is_resolved = isValidType(*candidate);

        if(boost::indeterminate(is_resolved))
        {
            is_resolved = false;
        }
        else if(is_resolved) // is valid type
        {
            resolved_type = get_candidate_type(candidate);
        }
    }

    return result;
}

boost::tribool TypeResolver::isValidType(tree::ASTNode& resolved)
{
    boost::tribool is_type =
        isa<tree::ClassDecl>(&resolved) ||
        isa<tree::EnumDecl>(&resolved) ||
        isa<tree::TypenameDecl>(&resolved) ||
        isa<tree::TypedefDecl>(&resolved)
    ;

    if(is_type)
    {
        if(tree::Declaration* decl = cast<tree::Declaration>(&resolved))
        {
            tree::TemplatedIdentifier* decl_tid = cast<tree::TemplatedIdentifier>(decl->name);

            if(decl_tid != NULL && !decl_tid->isFullySpecialized())
            {
                is_type = boost::indeterminate;
            }
        }
    }

    return is_type;
}

bool TypeResolver::isValidInstantiation(tree::TemplatedIdentifier& tid)
{
    BOOST_FOREACH(tree::TypenameDecl* typename_decl, tid.templated_type_list)
    {
        tree::Type* resolved = typename_decl->getCanonicalType();

        BOOST_ASSERT(resolved != NULL && "null pointer exception");

        if(tree::Declaration* decl = resolved->getAsDecl())
        {
            if(resolved->isTypenameType())
            {
                return false;
            }
            else
            {
                tree::TemplatedIdentifier* tid = cast<tree::TemplatedIdentifier>(decl->name);

                if(tid != NULL && !tid->isFullySpecialized())
                {
                    return false;
                }
            }
        }
    }

    return true;
}

SymbolResolver::SymbolResolver(Instantiater& instantiater, const std::vector<tree::Expression*>* parameters)
    : instantiater(&instantiater)
    , parameters(parameters)
{
}

bool SymbolResolver::isResolved(tree::ASTNode& node) const
{
    return ResolvedSymbol::get(&node) != NULL;
}

boost::tribool SymbolResolver::resolve(tree::ASTNode& attach, tree::Identifier& id, const std::vector<tree::ASTNode*>& candidates)
{
    BOOST_ASSERT(!candidates.empty() && "there must be at least one candidate for resolving");

    std::pair<boost::tribool, tree::ASTNode*> resolve_info(false, NULL);

    boost::tribool& is_resolved = resolve_info.first;
    tree::ASTNode*& resolved_node = resolve_info.second;

    BOOST_ASSERT(isAllSameType(candidates) && "resolver cannot handle different types of nodes");

    {
        boost::tribool need_deduction = false;

        if(isa<tree::TemplatedIdentifier>(&id))
        {
            need_deduction = true;
        }
        else
        {
            need_deduction = isResolvingCall(candidates);
        }

        if(boost::indeterminate(need_deduction))
        {
            is_resolved = boost::indeterminate;
        }
        else if(need_deduction)
        {
            resolve_info = resolveWithDeduction(attach, id, candidates);
        }
        else
        {
            resolve_info = resolveWithoutDeduction(attach, id, candidates);
        }
    }

    if(is_resolved)
    {
        BOOST_ASSERT(resolved_node != NULL && "null pointer exception");

        is_resolved = setResolved(id, attach, *resolved_node);
    }

    if(!is_resolved)
    {
        reportError(attach, id, candidates);
    }

    return is_resolved;
}

std::pair<boost::tribool, tree::ASTNode*>  SymbolResolver::resolveWithDeduction(tree::ASTNode& attach, tree::Identifier& id, const std::vector<tree::ASTNode*>& candidates)
{
    BOOST_ASSERT(!candidates.empty() && "there must be at least one candidate for resolving");

    const bool has_explicit_deduction = isa<tree::TemplatedIdentifier>(&id);
    const bool has_implicit_deduction = isResolvingCall(candidates);

    BOOST_ASSERT(
        (
            has_explicit_deduction ||
            has_implicit_deduction
        ) && "must have explicit or implicit deduction"
    );
    BOOST_ASSERT(
        (
            !has_implicit_deduction || parameters != NULL
        ) && "has implicit deduction but no parameters"
    );

    std::pair<boost::tribool, tree::ASTNode*> result(false, NULL);

    boost::tribool& is_resolved = result.first;
    tree::ASTNode*& resolved_node = result.second;

    Deductor deductor(candidates);

    if(has_explicit_deduction && has_implicit_deduction)
    {
        BOOST_ASSERT(parameters != NULL && "has implicit deduction but no parameters");

        tree::TemplatedIdentifier* tid = cast<tree::TemplatedIdentifier>(&id);

        is_resolved = deductor.deduct(*tid, *parameters);
    }
    else if(has_explicit_deduction)
    {
        tree::TemplatedIdentifier* tid = cast<tree::TemplatedIdentifier>(&id);

        is_resolved = deductor.deduct(*tid);
    }
    else if(has_implicit_deduction)
    {
        BOOST_ASSERT(parameters != NULL && "has implicit deduction but no parameters");

        is_resolved = deductor.deduct(*parameters);
    }
    else
    {
        UNREACHABLE_CODE();
    }

    if(is_resolved)
    {
        const auto best_match_range = deductor.get_best_match_range();

        BOOST_ASSERT(best_match_range.size() == 1 && "succeeded but best match is not 1");

        const deduction::SingleInfo& best_match = best_match_range.front();

        is_resolved = isValidSymbol(*best_match.candidate);

        if(is_resolved)
        {
            resolved_node = best_match.candidate;
        }
        else if(boost::indeterminate(is_resolved))
        {
            instantiater->request(best_match, attach);
        }
    }

    return result;
}

std::pair<boost::tribool, tree::ASTNode*>  SymbolResolver::resolveWithoutDeduction(tree::ASTNode& attach, tree::Identifier& id, const std::vector<tree::ASTNode*>& candidates)
{
    BOOST_ASSERT(!candidates.empty() && "there must be at least one candidate for resolving");

    std::pair<boost::tribool, tree::ASTNode*> result(false, NULL);

    boost::tribool& is_resolved = result.first;
    tree::ASTNode*& resolved_node = result.second;

    if(candidates.size() == 1)
    {
        tree::ASTNode* candidate = candidates.front();

        is_resolved = isValidSymbol(*candidate);

        if(boost::indeterminate(is_resolved))
        {
            is_resolved = false;
        }
        else
        {
            resolved_node = candidate;
        }
    }

    return result;
}

boost::tribool SymbolResolver::isValidSymbol(tree::ASTNode& resolved)
{
    boost::tribool is_symbol = isa<
        tree::ClassDecl   ,
        tree::EnumDecl    ,
        tree::FunctionDecl,
        tree::TypedefDecl ,
        tree::VariableDecl,
        tree::TypenameDecl
    >(resolved);

    if(is_symbol)
    {
        if(tree::Declaration* decl = cast<tree::Declaration>(&resolved))
        {
            tree::TemplatedIdentifier* decl_tid = cast<tree::TemplatedIdentifier>(decl->name);

            if(decl_tid != NULL && !decl_tid->isFullySpecialized())
            {
                is_symbol = boost::indeterminate;
            }
        }
    }

    return is_symbol;
}

boost::tribool SymbolResolver::isResolvingCall(const std::vector<tree::ASTNode*>& candidates)
{
    BOOST_ASSERT(!candidates.empty() && "no candidate to verify");

    if(parameters == NULL)
    {
        return false;
    }
    else
    {
        tree::ASTNode* candidate = candidates.front();

        BOOST_ASSERT(candidate != NULL && "null pointer exception");

        tree::Type* resolved_type = tree::ASTNodeHelper::getCanonicalType(candidate);

        if(isa<tree::FunctionDecl>(candidate))
        {
            return true;
        }
        else if(resolved_type == nullptr)
        {
            return boost::indeterminate;
        }
        else if(resolved_type->isRecordType())
        {
            return false;
        }
        else
        {
            return true;
        }
    }
}

boost::tribool SymbolResolver::setResolved(tree::Identifier& id, tree::ASTNode& attach, tree::ASTNode& resolved_node)
{
    boost::tribool is_success = true;

    if(tree::VariableDecl* var_decl = cast<tree::VariableDecl>(&resolved_node))
    {
        is_success = setResolvedImpl(attach, *var_decl);
    }
    else if(tree::Declaration* decl = cast<tree::Declaration>(&resolved_node))
    {
        is_success = setResolvedImpl(attach, *decl);
    }
    else
    {
        is_success = false;
    }

    if(is_success)
    {
        LOG4CXX_DEBUG(LoggerWrapper::Resolver, L"symbol \"" << id.toString() << L"\" is resolved to: \"" << tree::ASTNodeHelper::getNodeName(&resolved_node) << L"\"");
    }

    return is_success;
}

boost::tribool SymbolResolver::setResolvedImpl(tree::ASTNode& attach, tree::Declaration& decl)
{
    ResolvedSymbol::set(&attach, &decl);
    ResolvedType::set(&attach, decl.getType());

    return true;
}

boost::tribool SymbolResolver::setResolvedImpl(tree::ASTNode& attach, tree::VariableDecl& decl)
{
    if(tree::EnumDecl* enum_decl = cast<tree::EnumDecl>(decl.parent))
    {
        ResolvedSymbol::set(&attach, &decl);
        ResolvedType::set(&attach, decl.getType());

        return true;
    }
    else
    {
        ResolvedSymbol::set(&attach, &decl);
        // get canonical type has to follow ResolvedSymbol link?
        //ResolvedType::set(&attach, &decl);

        return true;
    }
}

void SymbolResolver::reportError(tree::ASTNode& attach, tree::Identifier& id, const std::vector<tree::ASTNode*>& candidates)
{
    if(isResolvingCall(candidates))
    {
        BOOST_ASSERT(parameters != NULL && "null pointer exception");

        reportCallError(attach, id, candidates, *parameters);
    }
}

void SymbolResolver::reportCallError(tree::ASTNode& attach, tree::Identifier& id, const std::vector<tree::ASTNode*>& candidates, const std::vector<tree::Expression*>& parameters)
{
    const std::wstring caller_string = get_function_caller_string(id, parameters);

    LOG_MESSAGE(FUNCTION_CANT_RESOLVE, &attach, _id(caller_string));

    BOOST_FOREACH(tree::ASTNode* candidate, candidates)
    {
        if(tree::FunctionDecl* func = cast<tree::FunctionDecl>(candidate))
        {
            const std::wstring candidate_string = L"function " + tree::ASTNodeHelper::getExpressiveDesc(*func);

            LOG_MESSAGE(FUNCTION_CANT_RESOLVE_CANDIDATES, candidate, _candidate(candidate_string));
        }
        else
        {
            std::wstring id;

            if(tree::Declaration* decl = cast<tree::Declaration>(candidate))
            {
                BOOST_ASSERT(!isa<tree::FunctionDecl>(decl) && "FunctionDecl should be handled in other place");

                id = decl->name->toString();
            }
            else
            {
                id = tree::ASTNodeHelper::getNodeName(candidate);
            }

            LOG_MESSAGE(CALL_NONFUNC, candidate, _id(id));
        }
    }
}

} } }
