/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <algorithm>
#include <map>
#include <memory>
#include <iterator>
#include <limits>
#include <vector>

#include <boost/assert.hpp>
#include <boost/logic/tribool.hpp>
#include <boost/next_prior.hpp>
#include <boost/range/algorithm/find.hpp>
#include <boost/range/algorithm/find_if.hpp>
#include <boost/scope_exit.hpp>
#include <boost/tuple/tuple.hpp>

#include "core/Common.h"
#include "language/context/ResolverContext.h"
#include "language/resolver/Helper.h"
#include "language/tree/ASTNode.h"
#include "language/tree/ASTNodeHelper.h"
#include "language/tree/basic/FunctionType.h"
#include "language/tree/basic/Identifier.h"
#include "language/tree/basic/Type.h"
#include "language/tree/basic/TypeSpecifier.h"
#include "language/tree/declaration/ClassDecl.h"
#include "language/tree/declaration/Declaration.h"
#include "language/tree/declaration/EnumDecl.h"
#include "language/tree/declaration/FunctionDecl.h"
#include "language/tree/declaration/TypenameDecl.h"
#include "language/tree/declaration/VariableDecl.h"
#include "language/resolver/Deduction.h"

namespace zillians { namespace language { namespace resolution { namespace deduction {

using namespace language::tree;

Rank::Rank(Type::ConversionRank conv, size_t level/* = 0*/) : conv_rank(conv), nested_level(level) {}

// '<' is better, if a < b, means conversion a is better than conversion b
bool Rank::operator<(const Rank& rhs) const
{
    if(    conv_rank    < rhs.conv_rank   ) return true;
    if(rhs.conv_rank    <     conv_rank   ) return false;
    if(    nested_level > rhs.nested_level) return true;  // deduct with deeper level is more specific, so is better
    if(rhs.nested_level >     nested_level) return false;
    return false;
}

bool Rank::operator==(const Rank& rhs) const
{
    if(conv_rank    != rhs.conv_rank   ) return false;
    if(nested_level != rhs.nested_level) return false;
    return true;
}

SingleInfo::SingleInfo(ASTNode* candidate, MatchState state)
    : candidate( candidate )
    , state( state )
{}

std::pair<bool, Rank> DeductAccepter::on_deducted(const std::size_t nested_level, Type& lhs_type, TypenameDecl& rhs_td)
{
    auto position_hint = deducted_types.lower_bound(&rhs_td);

    const bool already_inserted = position_hint != deducted_types.end() && position_hint->first == &rhs_td;
    bool is_updated = false;

    if(already_inserted)
    {
        Type* previous_deducted = position_hint->second.type->getCanonicalType();
        const auto previous_deducted_by = position_hint->second.by;

        BOOST_ASSERT(
            (
                previous_deducted_by == SingleInfo::EXPLICIT ||
                previous_deducted_by == SingleInfo::IMPLICIT ||
                previous_deducted_by == SingleInfo::LITERAL
            ) && "not allowed deduction source"
        );

        switch(previous_deducted_by)
        {
        case SingleInfo::EXPLICIT:
            switch(const auto new_rank = lhs_type.getCanonicalType()->getConversionRank(*previous_deducted))
            {
            case Type::ConversionRank::UnknownYet:
                UNREACHABLE_CODE();
                break;
            case Type::ConversionRank::NotMatch:
                break;
            default:
                position_hint->second.rank.conv_rank = new_rank;
                is_updated = true;
                break;
            }

            break;
        case SingleInfo::IMPLICIT:
            if(&lhs_type == previous_deducted)
            {
                is_updated = true;
            }

            break;
        case SingleInfo::LITERAL:
            BOOST_ASSERT(previous_deducted->getAsClassDecl() == getParserContext().tangle->findThorLangObject() && "deduction result from LITERAL must be thor.lang.Object");
            BOOST_ASSERT(position_hint->second.rank.conv_rank == Type::ConversionRank::StandardConversion && "conversion rank from \"null\" must be standard conversion");

            if(lhs_type.isRecordType())
            {
                position_hint->second.type = &lhs_type;
                position_hint->second.rank.conv_rank = Type::ConversionRank::ExactMatch;

                is_updated = true;
            }

            break;
        }

        if(is_updated)
        {
            position_hint->second.rank.nested_level = std::max(position_hint->second.rank.nested_level, nested_level);
        }
    }
    else
    {
        const auto insertion_result = deducted_types.insert(std::make_pair(
            &rhs_td,
            SingleInfo::DeductInfo
            {
                &lhs_type,
                SingleInfo::IMPLICIT,
                Rank(
                    Type::ConversionRank::ExactMatch,
                    nested_level
                )
            }
        ));

        BOOST_ASSERT(insertion_result.second && "insertion of result from here should not fail since there is no same key is presented");

        position_hint = insertion_result.first;
    }

    return std::make_pair(!already_inserted || is_updated, position_hint->second.rank);
}

std::pair<boost::tribool, Rank> DeductAccepter::on_complete_types(const std::size_t nested_level, ASTNode& lhs, ASTNode& rhs)
{
    boost::tribool is_matched = false;
    const auto conversion_rank = ASTNodeHelper::getCanonicalType(&rhs)->getConversionRank(*ASTNodeHelper::getCanonicalType(&lhs));

    switch(conversion_rank)
    {
    case Type::ConversionRank::NotMatch  : is_matched = false               ; break;
    case Type::ConversionRank::UnknownYet: is_matched = boost::indeterminate; break;
    default                              : is_matched = true                ; break;
    }

    return std::make_pair(
        is_matched,
        Rank(conversion_rank, nested_level)
    );
}

std::pair<bool, Rank> MatchAccepter::on_deducted(const std::size_t nested_level, Type& lhs_type, TypenameDecl& rhs_td)
{
    BOOST_ASSERT(boost::find(rhs_tds, &rhs_td) != rhs_tds.end() && "deducted TypenameDecl is not a part of declaration!");

    Type* accepted_type = nullptr;

    if(TypenameDecl* lhs_td = lhs_type.getAsTypenameDecl())
    {
        const auto lhs_td_pos = boost::find(lhs_tds,  lhs_td);
        const auto rhs_td_pos = boost::find(rhs_tds, &rhs_td);

        const auto lhs_td_offset = std::distance(lhs_tds.begin(), lhs_td_pos);
        const auto rhs_td_offset = std::distance(rhs_tds.begin(), rhs_td_pos);

        if(lhs_td_offset == rhs_td_offset)
        {
            accepted_type = &lhs_type;
        }
    }
    else
    {
        accepted_type = &lhs_type;
    }

    bool is_accepted = false;

    if(accepted_type != nullptr)
    {
        const auto insertion_result = deducted_types.insert(std::make_pair(
            &rhs_td,
            SingleInfo::DeductInfo
            {
                accepted_type,
                SingleInfo::IMPLICIT,
                Rank(
                    Type::ConversionRank::ExactMatch,
                    nested_level
                )
            }
        ));

        const bool& is_inserted = insertion_result.second;

        is_accepted =
            is_inserted ||
            insertion_result.first->second.type->getCanonicalType() == lhs_type.getCanonicalType();
    }

    return std::make_pair(
        is_accepted,
        Rank(
            is_accepted ? Type::ConversionRank::ExactMatch : Type::ConversionRank::NotMatch,
            nested_level
        )
    );
}

std::pair<boost::tribool, Rank> MatchAccepter::on_complete_types(const std::size_t nested_level, ASTNode& lhs, ASTNode& rhs)
{
    boost::tribool is_matched = false;
    auto conversion_rank = ASTNodeHelper::getCanonicalType(&rhs)->getConversionRank(*ASTNodeHelper::getCanonicalType(&lhs));

    switch(conversion_rank)
    {
    default                              : conversion_rank = Type::ConversionRank::NotMatch; is_matched = false               ; break;
    case Type::ConversionRank::UnknownYet:                                                 ; is_matched = boost::indeterminate; break;
    case Type::ConversionRank::ExactMatch:                                                 ; is_matched = true                ; break;
    }

    return std::make_pair(
        is_matched,
        Rank(conversion_rank, nested_level)
    );
}

TypeFollower TypeFollower::from_type(Type& type)
{
    Type* canonical_type = &type;

    if(canonical_type != nullptr)
    {
        TypeSpecifier* ts = ASTNodeHelper::getTypeSpecifierFrom(canonical_type);

        BOOST_ASSERT(ts && " null pointer exception");

        return TypeFollower(*ts, *canonical_type);
    }
    else
    {
        return TypeFollower();
    }
}

TypeFollower TypeFollower::from_ts(TypeSpecifier& ts)
{
    Type* canonical_type = ts.getCanonicalType();

    if(canonical_type != nullptr)
    {
        return TypeFollower(ts, *canonical_type);
    }
    else
    {
        return TypeFollower();
    }
}

TypeFollower TypeFollower::from_ts_with_custom_lookup_node(TypeSpecifier& ts, ASTNode& node)
{
    Type* canonical_type = nullptr;

    if(const auto*const ts_primitive = cast<PrimitiveSpecifier>(&ts))
    {
        Tangle* tangle = node.getOwner<Tangle>();
        BOOST_ASSERT(tangle && "null pointer exception");

        canonical_type = tangle->internal->getPrimitiveType(ts_primitive->getKind());

        BOOST_ASSERT(canonical_type->getCanonicalType() == canonical_type && "canonical type for primitive types is not itself");
    }
    else
    {
        canonical_type = ts.getCanonicalType();
    }

    if(canonical_type != nullptr)
    {
        return TypeFollower(ts, *canonical_type);
    }
    else
    {
        return TypeFollower();
    }
}

TypeFollower::TypeFollower()
    : ts(nullptr)
    , canonical_type(nullptr)
{}

TypeFollower::TypeFollower(TypeSpecifier& ts, Type& canonical_type)
    : ts(&ts)
    , canonical_type(&canonical_type)
{
    BOOST_ASSERT(canonical_type.getCanonicalType() == &canonical_type && "canonical type of canonical type is not itself");
    BOOST_ASSERT(
        (
            isa<TypeSpecifier>(&canonical_type) ||
            isa<ClassDecl>(&canonical_type) ||
            isa<EnumDecl>(&canonical_type) ||
            isa<Type>(&canonical_type) ||
            isa<TypenameDecl>(&canonical_type)
        ) && "not a valid type during deduction!"
    );
}

bool TypeFollower::is_ready() const
{
    BOOST_ASSERT(
        (
            (ts == nullptr && canonical_type == nullptr) ||
            (ts != nullptr && canonical_type != nullptr)
        ) && "type and type specifier is not both ready or non-ready"
    );

    return ts != nullptr && canonical_type != nullptr;
}

TypeSpecifier& TypeFollower::get_ts() const
{
    return *ts;
}

Type& TypeFollower::get_canonical_type() const
{
    return *canonical_type;
}

TemplatedIdentifier* TypeFollower::get_ts_tid() const
{
    BOOST_ASSERT(ts && "null pointer exception");

    const auto*const ts_named = cast<NamedSpecifier>(ts);

    if (ts_named == nullptr)
        return nullptr;

    auto*const id = ts_named->getName();
    NOT_NULL(id);

    auto*const tid = id->getTemplateId();

    if (tid != nullptr)
        return tid;

    TypenameDecl* td = nullptr;

    for(ASTNode* prev_node = ts; ; )
    {
        Type* resolved = ResolvedType::get(prev_node);

        if(resolved != nullptr && resolved->isTypenameType())
        {
            prev_node = resolved;
        }
        else
        {
            TypenameType* tnType = cast<TypenameType>(prev_node);
            td = tnType->getAsTypenameDecl();
            BOOST_ASSERT(ts != nullptr);
            break;
        }
    }

    if (td == nullptr || td->specialized_type == nullptr)
        return nullptr;

    auto*const specialized_named = cast<NamedSpecifier>(td->specialized_type);

    if (specialized_named == nullptr)
        return nullptr;

    NOT_NULL(specialized_named->getName());

    return specialized_named->getName()->getTemplateId();
}

TemplatedIdentifier* TypeFollower::get_type_tid() const
{
    BOOST_ASSERT(canonical_type && "null pointer exception");

    if(Declaration* decl = canonical_type->getAsClassDecl())
    {
        BOOST_ASSERT(decl->name && "null pointer exception");

        return decl->name->getTemplateId();
    }
    else
    {
        return nullptr;
    }
}

bool is_template_declaration(ASTNode* candidate)
{
    if(Declaration* decl = cast<Declaration>(candidate))
    {
        BOOST_ASSERT(decl->name && "null pointer exception");

        if(decl->name->getTemplateId())
        {
            return true;
        }
    }

    return false;
}

bool is_partial_specialized(Identifier& id)
{
    if(TemplatedIdentifier* tid = id.getTemplateId())
    {
        return tid->isFullySpecialized();
    }
    else
    {
        return false;
    }
}

bool is_more_or_equally_specialized(TypenameDecl& lhs, TypenameDecl& rhs)
{
    // if b is a generalized type (not a specialized type), then a must be equally or more specific than b
    if (rhs.specialized_type == nullptr)
        return true;

    // a must be also a specialized type, otherwise b will be more specific than a
    if (lhs.specialized_type == nullptr)
        return false;

    auto*const lhs_primitive = cast<PrimitiveSpecifier>(lhs.specialized_type);
    auto*const rhs_primitive = cast<PrimitiveSpecifier>(rhs.specialized_type);

    // if both a and b are type specifiers of primitive types, than it must be equal
    if (lhs_primitive != nullptr && rhs_primitive != nullptr)
    {
        BOOST_ASSERT(lhs_primitive->getKind() == rhs_primitive->getKind());
        return true;
    }

    auto*const lhs_named = cast<NamedSpecifier>(lhs.specialized_type);
    auto*const rhs_named = cast<NamedSpecifier>(rhs.specialized_type);

    // if both a and b are type specifiers of unspecified types, than
    if (lhs_named != nullptr && rhs_named != nullptr)
    {
        auto*const lhs_named_id = lhs_named->getName();
        auto*const rhs_named_id = rhs_named->getName();

        const auto& is_lhs_partial_specialized = is_partial_specialized(*lhs_named_id);
        const auto& is_rhs_partial_specialized = is_partial_specialized(*rhs_named_id);

        // if both of them are templated identifier, compare them recursively
        if (is_lhs_partial_specialized && is_rhs_partial_specialized)
        {
            auto*const lhs_named_tid = cast<TemplatedIdentifier>(lhs_named_id);
            auto*const rhs_named_tid = cast<TemplatedIdentifier>(rhs_named_id);

            BOOST_ASSERT(lhs_named_tid != nullptr && "partial specialization will not be applied on non-template identifier!");
            BOOST_ASSERT(rhs_named_tid != nullptr && "partial specialization will not be applied on non-template identifier!");

            return is_more_or_equally_specialized(*lhs_named_tid, *rhs_named_tid);
        }
        // if both of them are not templated identifier, which could be either simple identifier or nested identifier,
        // they should be the same because we have ruled out all incompatible types in ResolutionVisitor
        else if (!is_lhs_partial_specialized && !is_rhs_partial_specialized)
        {
            return true;
        }
        // if a is not a partially specialized templated identifier and b is not, then a is more specific than b
        else if (!is_lhs_partial_specialized && is_rhs_partial_specialized)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    auto*const lhs_func = cast<FunctionSpecifier>(lhs.specialized_type);
    auto*const rhs_func = cast<FunctionSpecifier>(rhs.specialized_type);

    // TODO handle function type specialization
    if (lhs_func != nullptr && rhs_func != nullptr)
        return false;

    UNREACHABLE_CODE();
    return false;
}

bool is_more_or_equally_specialized(TemplatedIdentifier& lhs, TemplatedIdentifier& rhs)
{
    for(size_t i = 0; i < lhs.templated_type_list.size(); ++i)
    {
        auto& lhs_typename_decl = *lhs.templated_type_list[i];
        auto& rhs_typename_decl = *rhs.templated_type_list[i];

        if(!is_more_or_equally_specialized(lhs_typename_decl, rhs_typename_decl))
        {
            return false;
        }
    }

    return true;
}

bool is_more_or_equally_specialized(Declaration& lhs, Declaration& rhs)
{
    TemplatedIdentifier* lhs_tid = cast<TemplatedIdentifier>(lhs.name);
    TemplatedIdentifier* rhs_tid = cast<TemplatedIdentifier>(rhs.name);

    BOOST_ASSERT(lhs_tid != nullptr);
    BOOST_ASSERT(rhs_tid != nullptr);

    BOOST_ASSERT(lhs_tid->templated_type_list.size() == rhs_tid->templated_type_list.size());

    return is_more_or_equally_specialized(*lhs_tid, *rhs_tid);
}

bool is_more_or_equally_specialized(const SingleInfo& lhs, const SingleInfo& rhs)
{
    BOOST_ASSERT(isa<Declaration>(lhs.candidate) && isa<Declaration>(rhs.candidate) && "cannot compare non declaration for more or equally specialization");

    return is_more_or_equally_specialized(*cast<Declaration>(lhs.candidate), *cast<Declaration>(rhs.candidate));
}

bool is_better_or_equally_conversion(const RankList& lhs_ranks, const RankList& rhs_ranks)
{
    // the count of rank may be different due to function default parameter
    const auto min_rank_count = std::min(lhs_ranks.size(), rhs_ranks.size());

    for(RankList::size_type i = 0; i != min_rank_count; ++i)
    {
        const Rank& lhs_rank = lhs_ranks[i];
        const Rank& rhs_rank = rhs_ranks[i];

        if(lhs_rank.conv_rank > rhs_rank.conv_rank)
        {
            return false;
        }
    }

    return true;
}

boost::tribool is_better_or_equally_matched_for_conversion_rank(const SingleInfo& lhs, const SingleInfo& rhs)
{
    BOOST_ASSERT(isa<Declaration>(lhs.candidate) && isa<Declaration>(rhs.candidate) && "only declarations will be compared to get the best match");

    Declaration* lhs_decl = cast<Declaration>(lhs.candidate);
    Declaration* rhs_decl = cast<Declaration>(rhs.candidate);

    if(is_better_or_equally_conversion(lhs.ranks, rhs.ranks))
    {
        if(is_better_or_equally_conversion(rhs.ranks, lhs.ranks))
        {
            const bool is_lhs_template = isa<TemplatedIdentifier>(lhs_decl->name);
            const bool is_rhs_template = isa<TemplatedIdentifier>(rhs_decl->name);

            if(!is_lhs_template && !is_rhs_template) return true;

            return boost::indeterminate;
        }
        else
        {
            return true;
        }
    }
    else
    {
        return false;
    }
}

boost::tribool is_better_or_equally_matched_for_deduction_rank(const SingleInfo& lhs, const SingleInfo& rhs)
{
    BOOST_ASSERT(isa<Declaration>(lhs.candidate) && isa<Declaration>(rhs.candidate) && "only declarations will be compared to get the best match");

    const auto& lhs_ranks = lhs.ranks;
    const auto& rhs_ranks = rhs.ranks;

    const auto min_rank_count = std::min(lhs_ranks.size(), rhs_ranks.size());

    const auto lhs_rank_end = boost::next(lhs_ranks.begin(), min_rank_count);

    const auto mismatch_point = std::mismatch(lhs_ranks.begin(), lhs_rank_end, rhs_ranks.begin());

    const auto& lhs_rank_diff_pos = mismatch_point.first;
    const auto& rhs_rank_diff_pos = mismatch_point.second;

    if(lhs_rank_diff_pos == lhs_rank_end)
    {
        return boost::indeterminate;
    }
    else
    {
        return *lhs_rank_diff_pos < *rhs_rank_diff_pos;
    }
}

boost::tribool is_better_or_equally_matched_for_deducted_types(const SingleInfo& lhs, const SingleInfo& rhs)
{
    using std::begin;

    BOOST_ASSERT(isa<Declaration>(lhs.candidate) && isa<Declaration>(rhs.candidate) && "only declarations will be compared to get the best match");

    auto* lhs_decl = cast<Declaration>(lhs.candidate);
    auto* rhs_decl = cast<Declaration>(rhs.candidate);

    using typenames_type = decltype(TemplatedIdentifier::templated_type_list);
    static auto get_typenames_or_default = [](Declaration* decl, typenames_type& other) -> typenames_type& {
        if(auto templated_id = cast<TemplatedIdentifier>(decl->name))
            return templated_id->templated_type_list;

        return other;
    };

    typenames_type empty;
    auto& lhs_typenames = get_typenames_or_default(lhs_decl, empty);
    auto& rhs_typenames = get_typenames_or_default(rhs_decl, empty);

    if(lhs_typenames.size() < rhs_typenames.size())
    {
        // a candidate is better if there are some template arguments others
        // can't deduct from function arguments or provide by default types
        // ex: foo<X,Y>() is equal to foo<X,Y,Z=int32>()
        const auto first_has_default_type = boost::range::find_if(
           rhs_typenames,
           [](typenames_type::value_type tn) {
               return tn->default_type != nullptr;
           }
        );

        return lhs_typenames.size() < std::distance(
            begin(rhs_typenames), first_has_default_type
        );
    }
    else if(lhs_typenames.size() == rhs_typenames.size())
    {
        for(std::size_t i = 0; i != lhs_typenames.size(); ++i)
        {
            auto* lhs_typename = lhs_typenames[i];
            auto* rhs_typename = rhs_typenames[i];

            const auto lhs_info_pos = lhs.deducted_types.find(lhs_typename);
            const auto rhs_info_pos = rhs.deducted_types.find(rhs_typename);

            BOOST_ASSERT(lhs_info_pos != lhs.deducted_types.end() && "there must be deducted info for each TypenameDecl");
            BOOST_ASSERT(rhs_info_pos != rhs.deducted_types.end() && "there must be deducted info for each TypenameDecl");

            const Rank& lhs_rank = lhs_info_pos->second.rank;
            const Rank& rhs_rank = rhs_info_pos->second.rank;

            if(lhs_rank > rhs_rank)
            {
                return false;
            }
        }

        return boost::indeterminate;
    }
    else
    {
        return false;
    }
}

bool is_better_or_equally_matched(const SingleInfo& lhs, const SingleInfo& rhs)
{
    BOOST_ASSERT(isa<Declaration>(lhs.candidate) && isa<Declaration>(rhs.candidate) && "only declarations will be compared to get the best match");

    boost::tribool is_better_or_equally = boost::indeterminate;

    if(boost::indeterminate(is_better_or_equally))
    {
        is_better_or_equally = is_better_or_equally_matched_for_conversion_rank(lhs, rhs);
    }

    if(boost::indeterminate(is_better_or_equally))
    {
        is_better_or_equally = is_better_or_equally_matched_for_deducted_types(lhs, rhs);
    }

    if(boost::indeterminate(is_better_or_equally))
    {
        is_better_or_equally = is_better_or_equally_matched_for_deduction_rank(lhs, rhs);
    }

    return is_better_or_equally || boost::indeterminate(is_better_or_equally);
}

boost::tribool is_identifier_matched(Identifier& use, Identifier& decl, const bool is_call/* = false*/)
{
    if(ASTNodeHelper::hasAncestor(&decl, &use)) return false;
    if(ASTNodeHelper::hasAncestor(&use, &decl)) return false;

    //////////////////////////////////////////////////////////////////////////////
    // nested id
    //////////////////////////////////////////////////////////////////////////////
    //NestedIdentifier* use_nested_id  = cast<NestedIdentifier>(&use);
    //NestedIdentifier* decl_nested_id = cast<NestedIdentifier>(&decl);
    //if((use_nested_id == nullptr) ^ (decl_nested_id == nullptr))
    //    return false;
    //if(use_nested_id != nullptr && decl_nested_id != nullptr)
    //{
    //    if(use_nested_id->identifier_list.size() != decl_nested_id->identifier_list.size()) return false;
    //    for(size_t i = 0; i != use_nested_id->identifier_list.size(); ++i)
    //    {
    //        const boost::tribool is_matched = is_identifier_matched(*use_nested_id->identifier_list[i], *decl_nested_id->identifier_list[i]);
    //
    //        if(!is_matched || boost::indeterminate(is_matched))
    //            return is_matched;
    //    }
    //    return true;
    //}

    //////////////////////////////////////////////////////////////////////////////
    // 0. both non-template id
    //////////////////////////////////////////////////////////////////////////////
    if(!isa<TemplatedIdentifier>(&use) && !isa<TemplatedIdentifier>(&decl))
    {
        BOOST_ASSERT(isa<SimpleIdentifier>(&use));
        BOOST_ASSERT(isa<SimpleIdentifier>(&decl));

        return (use.toString() == decl.toString());
    }

    //////////////////////////////////////////////////////////////////////////////
    // 1. function call
    // it is possible that current is nullptr when compare() is used in other place
    //////////////////////////////////////////////////////////////////////////////
    if(is_call)
    {
        if(isa<TemplatedIdentifier>(&use) && !isa<TemplatedIdentifier>(&decl))
            return false;
        TemplatedIdentifier* declTemplatedId = cast<TemplatedIdentifier>(&decl);
        BOOST_ASSERT(declTemplatedId != nullptr);
        if(TemplatedIdentifier* useTemplatedId = cast<TemplatedIdentifier>(&use))
        {
            if(useTemplatedId->templated_type_list.size() > declTemplatedId->templated_type_list.size())
                return false;
            if(useTemplatedId->id->toString() != declTemplatedId->id->toString())
                return false;
        }
        else
        {
            if(use.toString() != declTemplatedId->id->toString())
                return false;
        }
    }

    //////////////////////////////////////////////////////////////////////////////
    // 2. decl is template, use, not
    //////////////////////////////////////////////////////////////////////////////
    if(!isa<TemplatedIdentifier>(&use) || !isa<TemplatedIdentifier>(&decl))
    {
        return false;
    }

    //////////////////////////////////////////////////////////////////////////////
    // 3. both template id
    //////////////////////////////////////////////////////////////////////////////
    TemplatedIdentifier* use_template = cast<TemplatedIdentifier>(&use);
    TemplatedIdentifier* decl_template = cast<TemplatedIdentifier>(&decl);
    BOOST_ASSERT(use_template != nullptr);
    BOOST_ASSERT(decl_template != nullptr);

    // if both use and decl are templated identifier, we need to make sure it's compatible
    if(use_template->id->toString() != decl_template->id->toString())
        return false;

    if(is_call)
    {
        if(decl_template->templated_type_list.size() < use_template->templated_type_list.size())
        {
            return false;
        }
    }
    else
    {
        if(decl_template->templated_type_list.size() != use_template->templated_type_list.size())
        {
            return false;
        }
    }

    if(decl_template->isVariadic())
    {
        UNIMPLEMENTED_CODE();
        return false;
    }

    std::vector<TypenameDecl*> use_types  =  use_template->templated_type_list;
    std::vector<TypenameDecl*> decl_types = decl_template->templated_type_list;

    // check if all specialized type and also the default type have ResolvedType associated
    if(!helper::is_all_resolved(*use_template) || !helper::is_all_resolved(*decl_template))
        return boost::indeterminate;

    // as we have reject many illegal cases when we reach here
    // now we have a "use" vector of TypenameDecl and every TypenameDecl has ResolvedType associated
    // and we also have a "decl" vector of TypenameDecl, and every specialized_type of TypenameDecl has ResolvedType associated
    // so we have two comparable vectors of types, then we perform the compatible test for these two vectors
    // rule: foreach pair of elements in the two type vectors. the decl should be more wider than the use
    // rule: to define which is wider, an unspecified TypenameDecl is wider than a specified TypenameDecl
    // we just need to rule out all narrower cases

    // note that every TypenameDecl might be associated with ResolvedType later
    // so we need to clean it up
    BOOST_SCOPE_EXIT((&decl_types))
    {
        for(TypenameDecl* decl_type: decl_types)
        {
            ResolvedType::set(decl_type, nullptr);
        }
    } BOOST_SCOPE_EXIT_END;

    for(std::size_t i=0;i<use_types.size();++i)
    {
        if(!decl_types[i]->specialized_type)
        {
            // bind a temporary resolved type to the TypenameDecl so that we can enforce the template constraint
            ResolvedType::set(decl_types[i], use_types[i]->specialized_type->getCanonicalType());

            continue;
        }
        else
        {
            TypeSpecifier*  use_type =  use_types[i]->specialized_type;
            TypeSpecifier* decl_type = decl_types[i]->specialized_type;
            BOOST_ASSERT( use_type != nullptr);
            BOOST_ASSERT(decl_type != nullptr);

            auto*const  use_type_named = cast<NamedSpecifier>( use_type);
            auto*const decl_type_named = cast<NamedSpecifier>(decl_type);

            if(use_type_named != nullptr && decl_type_named != nullptr)
            {
                auto*const  use_type_named_tid = cast<TemplatedIdentifier>( use_type_named->getName());
                auto*const decl_type_named_tid = cast<TemplatedIdentifier>(decl_type_named->getName());

                if(use_type_named_tid != nullptr && decl_type_named_tid)
                {
                    const boost::tribool is_matched = is_identifier_matched(*use_type_named_tid, *decl_type_named_tid);

                    if(!is_matched || boost::indeterminate(is_matched))
                        return is_matched;
                }
                else if(use_type_named_tid != nullptr && decl_type_named_tid == nullptr)
                {
                    Type* resolved = ResolvedType::get(decl_type);
                    TypenameDecl* typenameDecl = resolved->getAsTypenameDecl();
                    if(typenameDecl != nullptr && typenameDecl->specialized_type != nullptr && isa<NamedSpecifier>(typenameDecl->specialized_type))
                    {
                        auto*const canonicalTypenameDecl = get_canonical_typename_decl(typenameDecl);
                        const boost::tribool is_matched = is_identifier_matched(*use_type_named_tid, *cast<NamedSpecifier>(canonicalTypenameDecl->specialized_type)->getName());

                        if(!is_matched || boost::indeterminate(is_matched))
                            return is_matched;
                    }
                    else
                    {
                        Type* resolved_use  =  use_type->getCanonicalType();
                        Type* resolved_decl = decl_type->getCanonicalType();
                        if(!resolved_use || !resolved_decl || resolved_use != resolved_decl)
                            return false;
                    }
                }
                else
                {
                    Type* resolved_use  =  use_type->getCanonicalType();
                    Type* resolved_decl = decl_type->getCanonicalType();
                    if(!resolved_use || !resolved_decl || resolved_use != resolved_decl)
                        return false;
                }
            }
            else
            {
                Type* resolved_use  =  use_type->getCanonicalType();
                Type* resolved_decl = decl_type->getCanonicalType();
                if(!resolved_use || !resolved_decl || resolved_use != resolved_decl)
                    return false;
            }
        }
    }

    return true;
}

bool is_specialization_matched(const TemplatedIdentifier& decl_tid, const decltype(SingleInfo::deducted_types)& deducted_types)
{
    return is_specialization_matched(decl_tid, decl_tid, deducted_types);
}

bool is_specialization_matched(const TemplatedIdentifier& decl_tid, const TemplatedIdentifier& use_tid, const decltype(SingleInfo::deducted_types)& deducted_types)
{
    BOOST_ASSERT(decl_tid.templated_type_list.size() == use_tid.templated_type_list.size());
    BOOST_ASSERT(deducted_types.size() <= decl_tid.templated_type_list.size());

    for(std::size_t i = 0; i != decl_tid.templated_type_list.size(); ++i)
    {
        TypenameDecl* decl_td = decl_tid.templated_type_list[i];
        TypenameDecl*  use_td =  use_tid.templated_type_list[i];

        if(decl_td->specialized_type != nullptr)
        {
            const auto deducted_ts_pos = deducted_types.find(use_td);

            if(deducted_ts_pos != deducted_types.end())
            {
                Type* deducted_ts = deducted_ts_pos->second.type;

                Type* specialized_type = decl_td->specialized_type->getCanonicalType();
                Type* deducted_type    = deducted_ts->getCanonicalType();

                BOOST_ASSERT(specialized_type != nullptr && deducted_type != nullptr && "null pointer exception");

                if(specialized_type != deducted_type)
                {
                    return false;
                }
            }
        }
    }

    return true;
}

bool add_deducted_type_by_explicit(decltype(SingleInfo::deducted_types)& deducted_types, TypenameDecl* decl, Type* type)
{
    const auto insertion_result = deducted_types.insert(std::make_pair(
        decl,
        SingleInfo::DeductInfo
        {
            type,
            SingleInfo::EXPLICIT,
            Rank(
                Type::ConversionRank::ExactMatch,
                std::numeric_limits<decltype(Rank::nested_level)>::max()
            )
        }
    ));

    BOOST_ASSERT(insertion_result.second && "insertion of result from explicit deduction should not fail");

    return insertion_result.second;
}

bool add_deducted_type_by_implicit(Accepter& accepter, TypenameDecl* decl, Type* type, Rank& rank, decltype(Rank::nested_level) nested_level)
{
    const auto result = accepter.on_deducted(nested_level, *type, *decl);

    const bool& is_accepted = result.first;

    if(is_accepted)
    {
        rank = result.second;
    }
    else
    {
        rank.conv_rank = Type::ConversionRank::NotMatch;
    }

    return is_accepted;
}

bool add_deducted_type_by_literal(decltype(SingleInfo::deducted_types)& deducted_types, TypenameDecl* decl, Rank& rank, decltype(Rank::nested_level) nested_level)
{
    const auto position_hint = deducted_types.lower_bound(decl);

    const bool already_inserted = position_hint != deducted_types.end() && position_hint->first == decl;
    bool is_castable = false;

    if(already_inserted)
    {
        Type* previous_deducted = position_hint->second.type->getCanonicalType();

        BOOST_ASSERT(previous_deducted != nullptr && "null pointer exception");

        if(previous_deducted->isRecordType())
        {
            is_castable = true;
        }
    }
    else
    {
        Tangle* tangle = getParserContext().tangle;
        ClassDecl* thor_lang_object = tangle->findThorLangObject();

        deducted_types.insert(
            position_hint,
            std::make_pair(
                decl,
                SingleInfo::DeductInfo
                {
                    thor_lang_object->getType(),
                    SingleInfo::LITERAL,
                    Rank(
                        Type::ConversionRank::StandardConversion,
                        nested_level
                    )
                }
            )
        );
    }

    const bool is_success = !already_inserted || is_castable;

    if(is_success)
    {
        rank.conv_rank = Type::ConversionRank::StandardConversion;
        rank.nested_level = std::max(rank.nested_level, nested_level);
    }
    else
    {
        rank.conv_rank = Type::ConversionRank::NotMatch;
    }

    return is_success;
}

TypenameDecl* get_canonical_typename_decl(TypenameDecl* v)
{
    if(Type* resolved = ResolvedType::get(v->specialized_type))
    {
        if(TypenameDecl* u = resolved->getAsTypenameDecl())
            return get_canonical_typename_decl(u);
        else
            return v;
    }

    return v;
}

TypenameDecl* get_typename_decl(TypeSpecifier* specifier)
{
    Type* resolved = ResolvedType::get(specifier);
    if(resolved == nullptr)
    {
        BOOST_ASSERT(isa<PrimitiveSpecifier>(specifier));
        return nullptr;
    }
    else
    {
        if(TypenameDecl* typename_decl = resolved->getAsTypenameDecl())
            return typename_decl;
        else
            return nullptr;
    }
}

boost::tribool set_conversion_rank(ASTNode* decl_type, ASTNode* use_type, const decltype(Rank::nested_level) nested_level, Accepter& accepter, Rank& rank)
{
    BOOST_ASSERT(decl_type != nullptr && use_type != nullptr && "null pointer exception");

    const auto result = accepter.on_complete_types(nested_level, *decl_type, *use_type);

    rank.conv_rank    = result.second.conv_rank;
    rank.nested_level = std::max(result.second.nested_level, nested_level);

    return result.first;
}

template<typename NodeType>
std::pair<bool, boost::tribool>
deduct_recursively_impl(const TypeFollower& use, const TypeFollower& decl, Accepter& accepter, Rank& rank, const decltype(Rank::nested_level) nested_level)
{
    BOOST_ASSERT(use.is_ready() && decl.is_ready() && "we should not be here if use or decl is not ready!");

    Type&  use_canonical_type =  use.get_canonical_type();
    Type& decl_canonical_type = decl.get_canonical_type();

    NodeType*  use_canonical_node = cast<NodeType>(&use_canonical_type);
    NodeType* decl_canonical_node = cast<NodeType>(&decl_canonical_type);

    const bool  use_is_node =  use_canonical_node != nullptr;
    const bool decl_is_node = decl_canonical_node != nullptr;

    if(use_is_node && decl_is_node)
    {
        const boost::tribool result = set_conversion_rank(&decl_canonical_type, &use.get_canonical_type(), nested_level, accepter, rank);

        return std::make_pair(true, result);
    }
    else
    {
        return std::make_pair(
            use_is_node || decl_is_node,
            false
        );
    }
}

template<>
std::pair<bool, boost::tribool>
deduct_recursively_impl<TypenameType>(const TypeFollower& use, const TypeFollower& decl, Accepter& accepter, Rank& rank, const decltype(Rank::nested_level) nested_level)
{
    Type& decl_canonical_type = decl.get_canonical_type();

    if(TypenameDecl* decl_canonical_td = decl_canonical_type.getAsTypenameDecl())
    {
        const boost::tribool result = add_deducted_type_by_implicit(accepter, decl_canonical_td, &use.get_canonical_type(), rank, nested_level);

        return std::make_pair(true, result);
    }
    else
    {
        return std::make_pair(false, false);
    }
}

template<>
std::pair<bool, boost::tribool>
deduct_recursively_impl<RecordType>(const TypeFollower& use, const TypeFollower& decl, Accepter& accepter, Rank& rank, const decltype(Rank::nested_level) nested_level)
{
    Type&  use_canonical_type =  use.get_canonical_type();
    Type& decl_canonical_type = decl.get_canonical_type();

    ClassDecl*const  use_canonical_cls = use_canonical_type.getAsClassDecl();
    ClassDecl*const decl_canonical_cls = decl_canonical_type.getAsClassDecl();

    const bool  use_is_cls =  use_canonical_cls != nullptr;
    const bool decl_is_cls = decl_canonical_cls != nullptr;

    if (!use_is_cls || !decl_is_cls)
        return {use_is_cls || decl_is_cls, false};

    if (use_canonical_cls->isCompleted() && decl_canonical_cls->isCompleted())
        return {true, set_conversion_rank(decl_canonical_cls, use_canonical_cls, nested_level, accepter, rank)};

    if (!use_canonical_cls->isTemplated() != !decl_canonical_cls->isTemplated())
        return {true, false};

    const auto*const  use_cls_pkg =  use_canonical_cls->getOwner<Package>();
    const auto*const decl_cls_pkg = decl_canonical_cls->getOwner<Package>();

    BOOST_ASSERT( use_cls_pkg != nullptr && "class declaration is not under any package");
    BOOST_ASSERT(decl_cls_pkg != nullptr && "class declaration is not under any package");

    if (use_cls_pkg != decl_cls_pkg)
        return {true, false};

    BOOST_ASSERT( use_canonical_cls->name && decl_canonical_cls->name && "null pointer exception");
    BOOST_ASSERT( use_canonical_cls->name->getTemplateId() && "only template class declarations could be here!");
    BOOST_ASSERT(decl_canonical_cls->name->getTemplateId() && "only template class declarations could be here!");

    const SimpleIdentifier*const  use_sid =  use_canonical_cls->name->getSimpleId();
    const SimpleIdentifier*const decl_sid = decl_canonical_cls->name->getSimpleId();

    BOOST_ASSERT(use_sid && decl_sid && "null pointer exception");

    if (use_sid->name == decl_sid->name)
        return {true, deduct_template_id(use, decl, accepter, rank, nested_level, Type::ConversionRank::ExactMatch)};

    if (InstantiatedFrom::get(use_canonical_cls) != nullptr && ASTNodeHelper::isInheritedFrom(cast<ClassDecl>(InstantiatedFrom::get(use_canonical_cls)), decl_canonical_cls))
        return {true, deduct_template_id(use, decl, accepter, rank, nested_level, Type::ConversionRank::StandardConversion)};

    return {true, false};
}

boost::tribool deduct_template_id(const TypeFollower& use, const TypeFollower& decl, Accepter& accepter, Rank& rank, const decltype(Rank::nested_level) nested_level, const Type::ConversionRank success_rank)
{
    TemplatedIdentifier* decl_type_tid = decl.get_type_tid();
    TemplatedIdentifier*  use_type_tid =  use.get_type_tid();
    TemplatedIdentifier* decl_ts_tid = decl.get_ts_tid();
    TemplatedIdentifier*  use_ts_tid =  use.get_ts_tid();

    BOOST_ASSERT(decl_type_tid && use_type_tid && use_ts_tid && "null pointer exception");

    if(decl_type_tid->templated_type_list.size() != use_type_tid->templated_type_list.size()) {
        rank.conv_rank = Type::ConversionRank::NotMatch;
        return false;
    }

    for(size_t i = 0; i != decl_type_tid->templated_type_list.size(); ++i) {
        TypeSpecifier* decl_inner_ts = decl_ts_tid->templated_type_list[i]->specialized_type;
        TypeSpecifier*  use_inner_ts =  use_ts_tid->templated_type_list[i]->specialized_type;

        BOOST_ASSERT(decl_inner_ts && use_inner_ts && "null pointer exception");

        Rank inner_rank(Type::ConversionRank::UnknownYet);
        auto decl_follower = TypeFollower::from_ts_with_custom_lookup_node(*decl_inner_ts, *decl_type_tid);
        auto  use_follower = TypeFollower::from_ts_with_custom_lookup_node(* use_inner_ts, * use_type_tid);

        boost::tribool result = deduct_recursively(
                use_follower,
                decl_follower,
                accepter,
                inner_rank,
                nested_level + 1);

        if(boost::indeterminate(result))
        {
            rank.conv_rank = Type::ConversionRank::UnknownYet;
            return boost::indeterminate;
        }
        else if(!result || inner_rank.conv_rank != Type::ConversionRank::ExactMatch)
        {
            rank.conv_rank = Type::ConversionRank::NotMatch;
            return false;
        }
        else
        {
            BOOST_ASSERT((bool)result && (inner_rank.conv_rank == Type::ConversionRank::ExactMatch));

            rank.nested_level = std::max(rank.nested_level, inner_rank.nested_level);
        }
    }

    rank.conv_rank = success_rank;
    return true;
}

boost::tribool deduct_null(TypeSpecifier* decl_param_ts, decltype(SingleInfo::deducted_types)& deducted_types, Rank& rank, const decltype(Rank::nested_level) nested_level)
{
    Type* decl_param_type = decl_param_ts->getCanonicalType();
    if(decl_param_type == nullptr)
    {
        return boost::indeterminate;
    }

    // 1. (T, *)
    if(TypenameDecl* typename_decl = get_typename_decl(decl_param_ts))
    {
        return add_deducted_type_by_literal(deducted_types, typename_decl, rank, nested_level);
    }
    // 2. (else, *)
    else
    {
        if(decl_param_type->isRecordType())
        {
            rank.conv_rank = Type::ConversionRank::StandardConversion;
            return true;
        }
        else
        {
            rank.conv_rank = Type::ConversionRank::NotMatch;
            return false;
        }
    }
}

boost::tribool
deduct_recursively(const TypeFollower& use, const TypeFollower& decl, Accepter& accepter, Rank& rank, const decltype(Rank::nested_level) nested_level)
{
    // pre-condition
    BOOST_ASSERT(!std::numeric_limits<decltype(nested_level)>::is_signed);

    if(!use.is_ready() || !decl.is_ready())
        return boost::indeterminate;

    bool is_decided = false;
    boost::tribool deduct_result = false;

    if(!is_decided)
    {
        // template parameter
        boost::tie(is_decided, deduct_result) = deduct_recursively_impl<TypenameType>(use, decl, accepter, rank, nested_level);
    }

    if(!is_decided)
    {
        // primitive type
        //boost::tie(is_decided, deduct_result) = deduct_recursively_impl<TypeSpecifier>(use, decl, accepter, rank, nested_level);
        boost::tie(is_decided, deduct_result) = deduct_recursively_impl<PrimitiveType>(use, decl, accepter, rank, nested_level);
    }

    if(!is_decided)
    {
        // enumeration
        boost::tie(is_decided, deduct_result) = deduct_recursively_impl<EnumType>(use, decl, accepter, rank, nested_level);
    }

    if(!is_decided)
    {
        // function type
        boost::tie(is_decided, deduct_result) = deduct_recursively_impl<FunctionType>(use, decl, accepter, rank, nested_level);
    }

    if(!is_decided)
    {
        // class
        boost::tie(is_decided, deduct_result) = deduct_recursively_impl<RecordType>(use, decl, accepter, rank, nested_level);
    }

    BOOST_ASSERT(is_decided && "not decided after all possible case!");

    return deduct_result;
}

void fill_by_default_types(decltype(SingleInfo::deducted_types)& deducted_types, const TemplatedIdentifier& decl_template_id)
{
    using std::end;

    auto& decl_tds = decl_template_id.templated_type_list;
    // no need to deduct
    if(decl_tds.size() == deducted_types.size())
        return;

    auto next_has_default_type = boost::range::find_if(
        decl_tds,
        [](TypenameDecl* decl) {
            return decl->default_type != nullptr;
        }
    );

    for( ; next_has_default_type != end(decl_tds); ++next_has_default_type)
    {
        auto* default_type = (*next_has_default_type)->default_type->getCanonicalType();
        BOOST_ASSERT(default_type != nullptr && "null pointer exception");

        auto add_result = add_deducted_type_by_explicit(deducted_types, (*next_has_default_type), default_type);
    }
}

boost::tuple<bool, std::size_t, decltype(SingleInfo::deducted_types)>
explicit_specialize(const std::vector<TypenameDecl*>& use_tds, const std::vector<TypenameDecl*>& decl_tds)
{
    BOOST_ASSERT(use_tds.size() <= decl_tds.size() && "specialization on use side is larger than declaration side");

    boost::tuple<bool, std::size_t, decltype(SingleInfo::deducted_types)> result(true, 0u);

    auto& i = boost::get<1>(result);
    auto& deducted_types = boost::get<2>(result);

    for(i = 0; i != use_tds.size(); ++i)
    {
        TypenameDecl* decl_td = decl_tds[i];
        TypenameDecl* explicit_td = use_tds[i];

        Type* explicit_type = explicit_td->specialized_type->getCanonicalType();

        BOOST_ASSERT(explicit_type != nullptr && "null pointer exception");

        // FIXME remove if resolution is work
        //if(decl_td->specialized_type != nullptr)
        //{
        //    ASTNode* decl_type = decl_td->specialized_type->getCanonicalType();
        //
        //    BOOST_ASSERT(decl_type != nullptr && "null pointer exception");
        //
        //    if(decl_type != explicit_type)
        //    {
        //        is_success = false;
        //
        //        break;
        //    }
        //}

        const auto is_added = add_deducted_type_by_explicit(deducted_types, decl_td, explicit_type);
        BOOST_ASSERT(is_added && "insertion of result from explicit deduction should not fail");
    }

    return result;
}

std::pair<boost::tribool, RankList>
implicit(const std::vector<VariableDecl*>& decl_params,
         const std::vector<Expression*>& use_exprs,
         const std::vector<Type*>& use_types,
         decltype(SingleInfo::deducted_types)& deducted_types)
{
    std::pair<boost::tribool, RankList> result(true, RankList());

    boost::tribool& is_success = result.first;
    RankList& ranks = result.second;

    const std::size_t use_expr_count = use_exprs.size();
    const std::size_t decl_param_count = decl_params.size();

    if(use_expr_count > decl_param_count)
    {
        is_success = false;
    }
    else if(!std::all_of(boost::next(decl_params.begin(), use_expr_count), decl_params.end(),
                         std::mem_fun(&VariableDecl::hasInitializer)))
    {
        is_success = false;
    }
    else
    {
        std::size_t i = 0;

        for(i = 0; i != use_expr_count; ++i)
        {
            // arg
            Expression* use_expr = use_exprs[i];
            Type* use_type = use_types[i];

            BOOST_ASSERT(use_expr != nullptr && use_type != nullptr && "null pointer exception");

            VariableDecl* decl_param = decl_params[i];

            BOOST_ASSERT(decl_param && decl_param->type && "null pointer exception");

            TypeSpecifier* decl_param_ts = decl_param->type;
            Rank rank(Type::ConversionRank::UnknownYet);

            // special handling for null literal
            if(use_expr->isNullLiteral())
            {
                is_success = deduct_null(decl_param_ts, deducted_types, rank, 0);
            }
            else
            {
                auto  use_follower = TypeFollower::from_type(*use_type);
                auto decl_follower = TypeFollower::from_ts(*decl_param_ts);
                DeductAccepter accepter(deducted_types);

                is_success = deduct_recursively(use_follower, decl_follower, accepter, rank, 0);
            }

            if(!is_success) break;
            else if(boost::indeterminate(is_success)) break;

            ranks.push_back(rank);
        }

        if(is_success && i != decl_param_count)
        {
            BOOST_ASSERT(i < decl_param_count && "parameter count must not be more than declaration");
            BOOST_ASSERT(i == ranks.size() && "deducted ranks is not the same of the progress");

            ranks.resize(decl_param_count, Rank(Type::ConversionRank::ExactMatch));
        }
    }

    return result;
}

boost::tribool match_template_recursively(FunctionDecl& generic_decl, FunctionDecl& specialized_decl)
{
    BOOST_ASSERT(generic_decl.name && specialized_decl.name && "null pointer exception");
    BOOST_ASSERT(generic_decl.parameters.size() == specialized_decl.parameters.size() && "call parameter aren't the same!");

    TemplatedIdentifier* generic_tid = generic_decl.name->getTemplateId();
    TemplatedIdentifier* specialized_tid = specialized_decl.name->getTemplateId();

    BOOST_ASSERT(generic_tid && specialized_tid && "non-template declaration found!");
    BOOST_ASSERT(generic_tid->templated_type_list.size() == specialized_tid->templated_type_list.size() && "template parameter count aren't the same!");

    const auto& generic_typename_decls = generic_tid->templated_type_list;
    const auto& specialized_typename_decls = specialized_tid->templated_type_list;

    const auto& generic_parameters = generic_decl.parameters;
    const auto& specialized_parameters = specialized_decl.parameters;

    std::map<TypenameDecl*, SingleInfo::DeductInfo> deducted_types;

    boost::tribool is_matched = true;

    for(std::size_t i = 0; i < generic_parameters.size(); ++i)
    {
        VariableDecl* generic_var = generic_parameters[i];
        VariableDecl* specialized_var = specialized_parameters[i];

        BOOST_ASSERT(generic_var && specialized_var && "null pointer exception");

        TypeSpecifier* generic_ts = generic_var->type;
        TypeSpecifier* specialized_ts = specialized_var->type;

        BOOST_ASSERT(generic_ts && specialized_ts && "null pointer exception");

        Rank rank(Type::ConversionRank::UnknownYet);
        auto  use_follower = TypeFollower::from_ts(*specialized_ts);
        auto decl_follower = TypeFollower::from_ts(*generic_ts);
        MatchAccepter accepter(deducted_types, specialized_typename_decls, generic_typename_decls);

        const boost::tribool single_is_matched = deduct_recursively(use_follower, decl_follower, accepter, rank, 0);

        if(boost::indeterminate(single_is_matched))
        {
            is_matched = boost::indeterminate;
        }
        else if(!single_is_matched)
        {
            is_matched = false;

            break;
        }
    }

    if(is_matched)
    {
        is_matched = is_specialization_matched(*specialized_tid, *generic_tid, deducted_types);
    }

    return is_matched;
}

boost::tribool is_same_specialization(ClassDecl& specialization_a, ClassDecl& specialization_b)
{
    BOOST_ASSERT(specialization_a.name && specialization_b.name && "null pointer exception");

    TemplatedIdentifier* a_tid = specialization_a.name->getTemplateId();
    TemplatedIdentifier* b_tid = specialization_b.name->getTemplateId();

    BOOST_ASSERT(a_tid != nullptr && b_tid != nullptr && "you cannot verify non-template class declaration for template specialization");

    if(a_tid->templated_type_list.size() == b_tid->templated_type_list.size())
    {
        boost::tribool is_same = true;
        std::map<TypenameDecl*, SingleInfo::DeductInfo> deducted_types;

        const auto& a_tds = a_tid->templated_type_list;
        const auto& b_tds = b_tid->templated_type_list;

        for(std::size_t i = 0; i != a_tid->templated_type_list.size(); ++i)
        {
            BOOST_ASSERT(i < a_tds.size() && "index out of range!");
            BOOST_ASSERT(i < b_tds.size() && "index out of range!");

            TypenameDecl* a_td = a_tds[i];
            TypenameDecl* b_td = b_tds[i];

            BOOST_ASSERT(a_td && b_td && "null pointer exception");

            if(a_td->specialized_type != nullptr && b_td->specialized_type != nullptr)
            {
                Rank rank(Type::ConversionRank::UnknownYet);
                auto a_follower = TypeFollower::from_ts(*a_td->specialized_type);
                auto b_follower = TypeFollower::from_ts(*b_td->specialized_type);
                MatchAccepter accepter(deducted_types, a_tds, b_tds);

                const boost::tribool single_is_matched = deduct_recursively(a_follower, b_follower, accepter, rank, 0);

                if(!single_is_matched)
                {
                    return false;
                }
                else if(boost::indeterminate(single_is_matched))
                {
                    is_same = boost::indeterminate;
                }
            }
            else if(a_td->specialized_type != nullptr || b_td->specialized_type != nullptr)
            {
                BOOST_ASSERT(
                    (
                        (a_td->specialized_type == nullptr && b_td->specialized_type != nullptr) ||
                        (a_td->specialized_type != nullptr && b_td->specialized_type == nullptr)
                    ) && "unexpected case!"
                );

                return false;
            }
            else
            {
                BOOST_ASSERT(a_td->specialized_type == nullptr && b_td->specialized_type == nullptr && "only both nullptr case should be here!");
            }
        }

        return is_same;
    }
    else
    {
        return false;
    }
}

} } } }
