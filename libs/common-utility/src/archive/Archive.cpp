/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <cstring>

#include <fstream>

#include <boost/filesystem.hpp>

#include "utility/archive/Archive.h"
#include "utility/Filesystem.h"

namespace zillians {

Archive::Archive(const std::string& archive_name, ArchiveMode mode) :
    mArchive(NULL),
    mArchiveName(archive_name),
    mArchiveMode(mode),
    mCompressLevel(Z_DEFAULT_COMPRESSION)
{
    open();
}

Archive::~Archive()
{
    if (!!mArchive)
    {
        close();
    }
}

bool Archive::open()
{
    if (mArchiveMode == ArchiveMode::ARCHIVE_FILE_COMPRESS)
    {
        mArchive = zipOpen64(mArchiveName.c_str(), 0);
    }
    else
    if (mArchiveMode == ArchiveMode::ARCHIVE_FILE_DECOMPRESS)
    {
        mArchive = unzOpen64(mArchiveName.c_str());
    }

    return mArchive != NULL;
}

bool Archive::close()
{
    if (mArchive == NULL) return false;

    if (mArchiveMode == ArchiveMode::ARCHIVE_FILE_COMPRESS)
    {
        zipClose(mArchive, NULL);
    }
    else
    if (mArchiveMode == ArchiveMode::ARCHIVE_FILE_DECOMPRESS)
    {
        unzClose(mArchive);
    }

    mArchive = NULL;
    return true;
}


bool Archive::add(ArchiveItem& archive_item)
{
    if (mArchive == NULL || mArchiveMode != ArchiveMode::ARCHIVE_FILE_COMPRESS) return false;

    // Open file in the archive
    int result = ZIP_OK;
    result = zipOpenNewFileInZip3_64(mArchive, archive_item.filename.c_str(), &archive_item.zip_info,
                                NULL, 0, NULL, 0, NULL /* comment */,
                                Z_DEFLATED,
                                mCompressLevel, 0,
                                /* -MAX_WBITS, DEF_MEM_LEVEL, Z_DEFAULT_STRATEGY, */
                                -MAX_WBITS, DEF_MEM_LEVEL, Z_DEFAULT_STRATEGY,
                                NULL /* PASSWORD */, 0 /* CRC */, 0 /* large file */);
    if (result != ZIP_OK) return false;

    // Write buffer to the file in the archive
    result = zipWriteInFileInZip(mArchive, &archive_item.buffer[0], archive_item.buffer.size());
    if (result != ZIP_OK) return false;

    // Close the file in the archive
    result = zipCloseFileInZip(mArchive);
    if (result != ZIP_OK) return false;

    return true;
}

bool Archive::add(const std::string& filename)
{
    if (mArchive == NULL || mArchiveMode != ArchiveMode::ARCHIVE_FILE_COMPRESS) return false;

    ArchiveItem archive_item;
    archive_item.filename = filename;

    // read the file into the buffer
    std::ifstream file(filename.c_str(), std::ios::in | std::ios::binary | std::ios::ate);
    archive_item.buffer.resize( file.tellg() );
    file.seekg(0, std::ios::beg);
    file.read( (char*)&archive_item.buffer[0], archive_item.buffer.size() );
    file.close();

    // TODO: fill the archive_item.zip_info (which inculdes file date)
    std::memset(&archive_item.zip_info, 0, sizeof(zip_fileinfo));

    return add(archive_item);
}

bool Archive::extractAll(std::vector<ArchiveItem>& archive_items)
{
    if (mArchive == NULL || mArchiveMode != ArchiveMode::ARCHIVE_FILE_DECOMPRESS) return false;

    unz_global_info64 global_info;

    if ( unzGetGlobalInfo64(mArchive, &global_info) != UNZ_OK) return false;

    // Extract one file at a time and then jump to the next file
    archive_items.resize( global_info.number_entry );
    for (size_t i = 0; i < global_info.number_entry; i++)
    {
        if (!extractCurrentFile(archive_items[i])) return false;

        // We don't need to jump to the next file if the current one is that last
        if (i != global_info.number_entry - 1)
        {
            int result = unzGoToNextFile(mArchive);
            if (result != UNZ_OK) return false;
        }
    }

    return true;
}

static bool isPath(const std::string& path)
{
    return path[path.size()-1] == '/';
}

bool Archive::extractAllToFolder(std::vector<ArchiveItem>& archive_items, const std::string& folder_path)
{
    if (!folder_path.empty())
    {
        boost::filesystem::create_directories(folder_path);
    }

    // extract all items from archive
    if (!extractAll(archive_items)) return false;

    boost::filesystem::path path_to_restore = boost::filesystem::current_path();
    boost::filesystem::current_path(folder_path);

    // Now, write to the disk
    for (size_t i = 0; i < archive_items.size(); i++)
    {
        ArchiveItem& item = archive_items[i];
        if(isPath(item.filename))
        {
            boost::filesystem::create_directories(item.filename);
        }
        else
        {
            // create the parent directories if necessary
            boost::filesystem::path f = item.filename;
            boost::filesystem::path p = f.parent_path();
            if(!p.empty() && !Filesystem::is_directory(p))
            {
                boost::filesystem::create_directories(p);
            }

            std::ofstream file(item.filename, std::ios::out | std::ios::binary);
            file.write((char*)&item.buffer[0], item.buffer.size());
            file.close();
        }
    }

    // change back the target folder
    boost::filesystem::current_path(path_to_restore);
    return true;
}

bool Archive::extractCurrentFile(ArchiveItem& archive_item)
{
    unz_file_info64 file_info;
    int result;

    // TODO: Uh... make the file length more reasonable
    const int max_filename_length = 1024;
    char inzip_filename[max_filename_length] = {0};

    // Retrive current file information
    result = unzGetCurrentFileInfo64(mArchive, &file_info, inzip_filename, max_filename_length, NULL, 0, NULL, 0);
    if (result != UNZ_OK) return false;

    archive_item.filename = inzip_filename;
    archive_item.unzip_info = file_info;

    // Open current file
    result = unzOpenCurrentFile(mArchive);
    if (result != UNZ_OK) return false;

    // Now, retrieve the buffer chunk by chunk
    int read_count = 0;
    char byte;

    while ( (read_count = unzReadCurrentFile(mArchive, &byte, 1)) > 0 )
    {
        archive_item.buffer.push_back(byte);
    }
    // Check unzReadCurrentFile comment, you will know there's an error if the read count is negative.
    if (read_count < 0) return false;

    // Close the current file
    result = unzCloseCurrentFile(mArchive);
    if (result != UNZ_OK) return false;

    return true;
}

void Archive::setCompressLevel(int level)
{
    // the range is 0~9
    mCompressLevel = (level < 0) ? (0) : level;
    mCompressLevel = (mCompressLevel > 9) ? (9) : mCompressLevel;
}

}
