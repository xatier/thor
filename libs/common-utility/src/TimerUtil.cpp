/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include "utility/TimerUtil.h"
#include "core/Platform.h"
#include <stdint.h>

#ifdef __PLATFORM_LINUX__
#include <time.h>
#include <sys/timeb.h>
#include <sys/time.h>
#endif
#ifdef __PLATFORM_MAC__
#include <mach/clock.h>
#include <mach/mach.h>
#endif



namespace zillians {

uint64_t TimerUtil::clock_get_time_ms()
{
    #ifdef __PLATFORM_LINUX__
        timespec ts;
        clock_gettime(CLOCK_REALTIME, &ts);
        uint64_t time_mu_s = static_cast<uint64_t>(ts.tv_sec)*1000000LL+static_cast<uint64_t>(ts.tv_nsec)/1000LL;
        return time_mu_s/1000;
    #endif

    #ifdef __PLATFORM_MAC__
        clock_serv_t cclock;
        mach_timespec_t ts;
        clock_get_time(cclock, &ts);
                uint64_t time_mu_s = static_cast<uint64_t>(ts.tv_sec)*1000000LL+static_cast<uint64_t>(ts.tv_nsec)/1000LL;
                return time_mu_s/1000;
    #endif 



}

}
