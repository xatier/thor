/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include "utility/DemanglingUtil.h"

#if defined(__GLIBCXX__)
    #include <cxxabi.h>
    #include <cstdlib>
#endif

namespace zillians {

std::string demangle(const std::type_info &ti)
{
#if defined(__GLIBCXX__)

    int status = 0;

    struct raii
    {
        raii(char *text) : text_(text) { }
        ~raii() { free(text_); }
        char *text_;
    }
    demangled(abi::__cxa_demangle(ti.name(), 0, 0, &status));

    if (status == 0) return demangled.text_;

#endif
    return ti.name();
}

}
