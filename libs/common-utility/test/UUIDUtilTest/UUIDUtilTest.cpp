/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include "utility/UUIDUtil.h"
#include "utility/StringUtil.h"

#define BOOST_TEST_MODULE UUIDUtilTest
#define BOOST_TEST_MAIN
#include <boost/test/unit_test.hpp>

#include <boost/algorithm/string/case_conv.hpp>

using namespace std;
using namespace zillians;

BOOST_AUTO_TEST_SUITE( UUIDUtilTestSuite )

BOOST_AUTO_TEST_CASE( UUIDCase1 )
{
    std::string value = "1a03c570-379a-11de-9b9a-001d92648305";

    // test copy constructor (from string)
    UUID id0 = UUID::parse(value);
    BOOST_CHECK(value.compare(id0.toString()) == 0);

    // test copy constructor (from another UUID)
    UUID id1 = id0;
    BOOST_CHECK(value.compare(id1.toString()) == 0);
}

BOOST_AUTO_TEST_CASE( UUIDCase2 )
{
    // test random UUID
    for(int i=0;i<1000;++i)
    {
        UUID id0 = UUID::random();
        UUID id1 = UUID::random();

        BOOST_CHECK_MESSAGE(id0 != id1, "fail to check at index " << i << ", id0 = " << id0 << ", id1 = " << id1);
    }
}

BOOST_AUTO_TEST_CASE( UUIDCase3 )
{
    // test nil UUID
    UUID id0 = UUID::nil();
    UUID id1 = UUID::nil();
    BOOST_CHECK(id0 == id1);

    // test nil string
    std::string value = "00000000-0000-0000-0000-000000000000";
    id0 = UUID::parse(value);
    BOOST_CHECK(id0 == id1);

    // test valid/invalid flag
    BOOST_CHECK(id0.is_nil());
}

BOOST_AUTO_TEST_CASE( UUIDCase4 )
{
    // test UUID comparison
    for(int i=0;i<1000;++i)
    {
        UUID id0 = UUID::random();
        UUID id1 = UUID::random();

        BOOST_CHECK(id0 > id1 || id0 < id1);
    }
}

BOOST_AUTO_TEST_CASE( UUIDCase5 )
{
    // test iostream input & output
    std::stringstream ss(std::stringstream::in | std::stringstream::out);
    UUID id0 = UUID::random();
    UUID id1;
    ss << id0;
    ss >> id1;
    BOOST_CHECK(id0 == id1);
}

BOOST_AUTO_TEST_CASE( UUIDCase7 )
{
    // test std::string conversion
    std::string value = "1A03C570-379A-11DE-9B9A-001D92648305";

    // test string comparison (case insensitive comparison)
    UUID id0 = UUID::parse(value);
    BOOST_CHECK(id0.toString() == boost::to_lower_copy(value));
}

BOOST_AUTO_TEST_CASE( UUIDCase8 )
{
    std::string value = "5a1e115437bf11deb7fc001d92648382";

    // test invalid string comparison (case insensitive comparison)
    UUID id0 = UUID::parse(value);
    BOOST_CHECK(id0.toString() != value);
}

BOOST_AUTO_TEST_CASE( UUIDCase9 )
{
    std::string value = "ef5eebfa-37b8-11de-9d60-001d92648305";

    // test special UUID
    UUID id0 = UUID::parse(value);
    UUID id1 = UUID::parse(value);
    BOOST_CHECK(id0 == id1);
}

BOOST_AUTO_TEST_CASE( UUIDCase10 )
{
    std::string value0 = "ef5eebfa-37b8-11de-9d60-001d92648305";
    std::string value1 = "eefe4c0a-37b8-11de-aa06-001d92648305";

    // test the comparison (must be smaller or greater or equal)
    UUID id0 = UUID::parse(value0);
    UUID id1 = UUID::parse(value1);
    BOOST_CHECK( (id0 > id1 && !(id0<id1) && !(id0==id1)) || !(id0 > id1 && (id0<id1) && !(id0==id1)) || !(id0 > id1 && !(id0<id1) && (id0==id1)) );
}

BOOST_AUTO_TEST_CASE( UUIDCase11 )
{
    UUID id0;
    UUID id1;

    // test the comparison
    id0 = UUID::parse("ef5eebfa-37b8-11de-9d60-001d92648305");
    id1 = UUID::parse("ef5eebfa-37b8-11de-9d60-001d92648306");
    BOOST_CHECK( !(id0 > id1 && (id0<id1) && !(id0==id1)) );

    id0 = UUID::parse("ef5eebfa-37b8-11de-9d60-001d92648305");
    id1 = UUID::parse("ef5eebfa-37b8-11de-9d60-001d92648315");
    BOOST_CHECK( !(id0 > id1 && (id0<id1) && !(id0==id1)) );

    id0 = UUID::parse("ef5eebfa-37b8-11de-9d60-001d92648305");
    id1 = UUID::parse("ef5eebfa-37b8-11de-9d60-001d92648405");
    BOOST_CHECK( !(id0 > id1 && (id0<id1) && !(id0==id1)) );

    id0 = UUID::parse("ef5eebfa-37b8-11de-9d60-001d92648305");
    id1 = UUID::parse("ef5eebfa-37b8-11de-9d60-001d92649305");
    BOOST_CHECK( !(id0 > id1 && (id0<id1) && !(id0==id1)) );

    id0 = UUID::parse("ef5eebfa-37b8-11de-9d60-001d92648305");
    id1 = UUID::parse("ef5eebfa-37b8-11de-9d60-001d92658305");
    BOOST_CHECK( !(id0 > id1 && (id0<id1) && !(id0==id1)) );

    id0 = UUID::parse("ef5eebfa-37b8-11de-9d60-001d92648305");
    id1 = UUID::parse("ef5eebfa-37b8-11de-9d60-001d92748305");
    BOOST_CHECK( !(id0 > id1 && (id0<id1) && !(id0==id1)) );

    id0 = UUID::parse("ef5eebfa-37b8-11de-9d60-001d92648305");
    id1 = UUID::parse("ef5eebfa-37b8-11de-9d60-001d93648305");
    BOOST_CHECK( !(id0 > id1 && (id0<id1) && !(id0==id1)) );

    id0 = UUID::parse("ef5eebfa-37b8-11de-9d60-001d92648305");
    id1 = UUID::parse("ef5eebfa-37b8-11de-9d60-001e02648305");
    BOOST_CHECK( !(id0 > id1 && (id0<id1) && !(id0==id1)) );

    id0 = UUID::parse("ef5eebfa-37b8-11de-9d60-001d92648305");
    id1 = UUID::parse("ef5eebfa-37b8-11de-9d60-002d02648305");
    BOOST_CHECK( !(id0 > id1 && (id0<id1) && !(id0==id1)) );

    id0 = UUID::parse("ef5eebfa-37b8-11de-9d60-001d92648305");
    id1 = UUID::parse("ef5eebfa-37b8-11de-9d60-011d02648305");
    BOOST_CHECK( !(id0 > id1 && (id0<id1) && !(id0==id1)) );

    id0 = UUID::parse("ef5eebfa-37b8-11de-9d60-001d92648305");
    id1 = UUID::parse("ef5eebfa-37b8-11de-9d60-101d02648305");
    BOOST_CHECK( !(id0 > id1 && (id0<id1) && !(id0==id1)) );

    id0 = UUID::parse("ef5eebfa-37b8-11de-9d60-001d92648305");
    id1 = UUID::parse("ef5eebfa-37b8-11de-9d61-001d02648305");
    BOOST_CHECK( !(id0 > id1 && (id0<id1) && !(id0==id1)) );

    id0 = UUID::parse("ef5eebfa-37b8-11de-9d60-001d92648305");
    id1 = UUID::parse("ef5eebfa-37b8-11de-9d61-001d02648305");
    BOOST_CHECK( !(id0 > id1 && (id0<id1) && !(id0==id1)) );

    id0 = UUID::parse("ef5eebfa-37b8-11de-9d60-001d92648305");
    id1 = UUID::parse("ef5eebfa-37b8-11de-9d70-001d02648305");
    BOOST_CHECK( !(id0 > id1 && (id0<id1) && !(id0==id1)) );

    id0 = UUID::parse("ef5eebfa-37b8-11de-9d60-001d92648305");
    id1 = UUID::parse("ef5eebfa-37b8-11de-9e60-001d02648305");
    BOOST_CHECK( !(id0 > id1 && (id0<id1) && !(id0==id1)) );

    id0 = UUID::parse("ef5eebfa-37b8-11de-9d60-001d92648305");
    id1 = UUID::parse("ef5eebfa-37b8-11de-ad60-001d02648305");
    BOOST_CHECK( !(id0 > id1 && (id0<id1) && !(id0==id1)) );

    id0 = UUID::parse("ef5eebfa-37b8-11de-9d60-001d92648305");
    id1 = UUID::parse("ef5eebfa-37b8-11df-9d60-001d02648305");
    BOOST_CHECK( !(id0 > id1 && (id0<id1) && !(id0==id1)) );

    id0 = UUID::parse("ef5eebfa-37b8-11de-9d60-001d92648305");
    id1 = UUID::parse("ef5eebfa-37b8-11ee-9d60-001d02648305");
    BOOST_CHECK( !(id0 > id1 && (id0<id1) && !(id0==id1)) );

    id0 = UUID::parse("ef5eebfa-37b8-11de-9d60-001d92648305");
    id1 = UUID::parse("ef5eebfa-37b8-12de-9d60-001d02648305");
    BOOST_CHECK( !(id0 > id1 && (id0<id1) && !(id0==id1)) );

    id0 = UUID::parse("ef5eebfa-37b8-11de-9d60-001d92648305");
    id1 = UUID::parse("ef5eebfa-37b8-21de-9d60-001d02648305");
    BOOST_CHECK( !(id0 > id1 && (id0<id1) && !(id0==id1)) );

    id0 = UUID::parse("ef5eebfa-37b8-11de-9d60-001d92648305");
    id1 = UUID::parse("ef5eebfa-37b9-11de-9d60-001d02648305");
    BOOST_CHECK( !(id0 > id1 && (id0<id1) && !(id0==id1)) );

    id0 = UUID::parse("ef5eebfa-37b8-11de-9d60-001d92648305");
    id1 = UUID::parse("ef5eebfa-37c8-11de-9d60-001d02648305");
    BOOST_CHECK( !(id0 > id1 && (id0<id1) && !(id0==id1)) );

    id0 = UUID::parse("ef5eebfa-37b8-11de-9d60-001d92648305");
    id1 = UUID::parse("ef5eebfa-38b8-11de-9d60-001d02648305");
    BOOST_CHECK( !(id0 > id1 && (id0<id1) && !(id0==id1)) );

    id0 = UUID::parse("ef5eebfa-37b8-11de-9d60-001d92648305");
    id1 = UUID::parse("ef5eebfa-47b8-11de-9d60-001d02648305");
    BOOST_CHECK( !(id0 > id1 && (id0<id1) && !(id0==id1)) );

    id0 = UUID::parse("ef5eebfa-37b8-11de-9d60-001d92648305");
    id1 = UUID::parse("ef5eebfb-37b8-11de-9d60-001d02648305");
    BOOST_CHECK( !(id0 > id1 && (id0<id1) && !(id0==id1)) );

    id0 = UUID::parse("ef5eebfa-37b8-11de-9d60-001d92648305");
    id1 = UUID::parse("ef5eec0a-37b8-11de-9d60-001d02648305");
    BOOST_CHECK( !(id0 > id1 && (id0<id1) && !(id0==id1)) );

    id0 = UUID::parse("ef5eebfa-37b8-11de-9d60-001d92648305");
    id1 = UUID::parse("ef5efbfa-37b8-11de-9d60-001d02648305");
    BOOST_CHECK( !(id0 > id1 && (id0<id1) && !(id0==id1)) );

    id0 = UUID::parse("ef5eebfa-37b8-11de-9d60-001d92648305");
    id1 = UUID::parse("ef5febfa-37b8-11de-9d60-001d02648305");
    BOOST_CHECK( !(id0 > id1 && (id0<id1) && !(id0==id1)) );

    id0 = UUID::parse("ef5eebfa-37b8-11de-9d60-001d92648305");
    id1 = UUID::parse("ef6eebfa-37b8-11de-9d60-001d02648305");
    BOOST_CHECK( !(id0 > id1 && (id0<id1) && !(id0==id1)) );

    id0 = UUID::parse("ef5eebfa-37b8-11de-9d60-001d92648305");
    id1 = UUID::parse("f05eebfa-37b8-11de-9d60-001d02648305");
    BOOST_CHECK( !(id0 > id1 && (id0<id1) && !(id0==id1)) );

    id0 = UUID::parse("ef5eebfa-37b8-11de-9d60-001d92648306");
    id1 = UUID::parse("ff5eebfa-37b8-11de-9d60-001d02648305");
    BOOST_CHECK( !(id0 > id1 && (id0<id1) && !(id0==id1)) );

    id0 = UUID::parse("ef5eebfa-37b8-11de-9d60-001d92648315");
    id1 = UUID::parse("ee5eebfa-37b8-11de-9d60-001d02648305");
    BOOST_CHECK( !(id0 > id1 && (id0<id1) && !(id0==id1)) );

    id0 = UUID::parse("ef5eebfa-37b8-11de-9d60-001d92648405");
    id1 = UUID::parse("ef6eebfa-37b8-11de-9d60-001d02648305");
    BOOST_CHECK( !(id0 > id1 && (id0<id1) && !(id0==id1)) );

    id0 = UUID::parse("ef5eebfa-37b8-11de-9d60-001d92649305");
    id1 = UUID::parse("ef5febfa-37b8-11de-9d60-001d02648305");
    BOOST_CHECK( !(id0 > id1 && (id0<id1) && !(id0==id1)) );

    id0 = UUID::parse("ef5eebfa-37b8-11de-9d60-001d92658305");
    id1 = UUID::parse("ef5efbfa-37b8-11de-9d60-001d02648305");
    BOOST_CHECK( !(id0 > id1 && (id0<id1) && !(id0==id1)) );

    id0 = UUID::parse("ef5eebfa-37b8-11de-9d60-001d92748305");
    id1 = UUID::parse("ef5eecfa-37b8-11de-9d60-001d02648305");
    BOOST_CHECK( !(id0 > id1 && (id0<id1) && !(id0==id1)) );

    id0 = UUID::parse("ef5eebfa-37b8-11de-9d60-001d93648305");
    id1 = UUID::parse("ef5eec0a-37b8-11de-9d60-001d02648305");
    BOOST_CHECK( !(id0 > id1 && (id0<id1) && !(id0==id1)) );

    id0 = UUID::parse("ef5eebfa-37b8-11de-9d60-001da2648305");
    id1 = UUID::parse("ef5eebfb-37b8-11de-9d60-001d02648305");
    BOOST_CHECK( !(id0 > id1 && (id0<id1) && !(id0==id1)) );

    id0 = UUID::parse("ef5eebfa-37b8-11de-9d60-001e92648305");
    id1 = UUID::parse("ef5eebfa-47b8-11de-9d60-001d02648305");
    BOOST_CHECK( !(id0 > id1 && (id0<id1) && !(id0==id1)) );

    id0 = UUID::parse("ef5eebfa-37b8-11de-9d60-002d92648305");
    id1 = UUID::parse("ef5eebfa-38b8-11de-9d60-001d02648305");
    BOOST_CHECK( !(id0 > id1 && (id0<id1) && !(id0==id1)) );

    id0 = UUID::parse("ef5eebfa-37b8-11de-9d60-011d92648305");
    id1 = UUID::parse("ef5eebfa-37c8-11de-9d60-001d02648305");
    BOOST_CHECK( !(id0 > id1 && (id0<id1) && !(id0==id1)) );

    id0 = UUID::parse("ef5eebfa-37b8-11de-9d60-101d92648305");
    id1 = UUID::parse("ef5eebfa-37b9-11de-9d60-001d02648305");
    BOOST_CHECK( !(id0 > id1 && (id0<id1) && !(id0==id1)) );

    id0 = UUID::parse("ef5eebfa-37b8-11de-9d61-001d92648305");
    id1 = UUID::parse("ef5eebfa-37b8-21de-9d60-001d02648305");
    BOOST_CHECK( !(id0 > id1 && (id0<id1) && !(id0==id1)) );

    id0 = UUID::parse("ef5eebfa-37b8-11de-9d70-001d92648305");
    id1 = UUID::parse("ef5eebfa-37b8-12de-9d60-001d02648305");
    BOOST_CHECK( !(id0 > id1 && (id0<id1) && !(id0==id1)) );
}

BOOST_AUTO_TEST_CASE( UUIDCase12 )
{
    BOOST_CHECK(sizeof(UUID) == 16);
}

BOOST_AUTO_TEST_SUITE_END()
