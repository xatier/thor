/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
 
import . = thor.unmanaged;
import . = thor.lang;

/**
 * @brief A class wraps key and associated value together.
 *
 * Usually this class is instantiated by @c HashMapIterator to
 * put key and value as a pair in loop iterations. An entry
 * object should be used only for the duration of the
 * iteration.
 *
 * @remark The behavior is undefined if backing map has been
 *         modified after the entry was returned by the iterator.
 * @see HashMapIterator
 * @see HashMap
 */
@cpu
@native { include = "thor/container/HashMap.h" }
class HashMapEntry<Key, Value> extends Object
{
    /**
     * @brief construct a copy from given map entry.
     *
     * @param e The source entry to copy from.
     */
	@native
    public function new(e : HashMapEntry<Key, Value>): void;

    /**
     * @brief Wrap key and value together.
     *
     * @param k The map key.
     * @param v The associated value.
     */
	@native
    public function new(k : Key, v : Value): void;

    /**
     * @cond THOR_WORKAROUNDS
     *
     * @brief Provides functionality to create entries in C++.
     *
     * @param k The map key.
     * @param v The associated value.
     * @return An entry instance which has given arguments.
     * @remark This function is current workaround to create object through C++.
     */
	public static function create(k: Key, v: Value): HashMapEntry<Key, Value>
	{
		return new HashMapEntry<Key, Value>(k, v);
	}
    /**
     * @endcond THOR_WORKAROUNDS
     */

    /**
     * Wrapped map key
     */
    @cpu public var key : Key;
    /**
     * Wrapped map value
     */
    @cpu public var value : Value;
}

/**
 * @brief Helper class which supports navigating key and values
 *        and put them together by using @c HashMapEntry as view.
 *
 * In foreach loop, Thor will call @c HashMap 's  member function
 * @c iterator() to get a @c HashMapIterator instance(shorten as @b it later).
 * Then Thor calls @b it 's member function @c hasNext() to determine
 * to terminate executing loop or not. In each iteration, Thor gets an
 * key-value pair by calling @b it 's member function @c next().
 *
 * @remark The behavior is undefined if backing map has been
 *         modified after the iterator was returned.
 * @see HashMap
 * @see HashMapEntry
 */
@cpu
@native { include = "thor/container/HashMap.h" }
class HashMapIterator<Key, Value> extends Object implements Cloneable
{
    /**
     * @brief constructor creates an iterator which points to a hashmap.
     *
     * @param c The target @c HashMap this newly created iterator will point to.
     */
    @native
    public function new(c : HashMap<Key, Value>): void;

    /**
     * @brief Destructor which releases all resources hold by instance.
     */
    @native
    public virtual function delete(): void;

    /**
     * @cond THOR_WORKAROUNDS
     *
     * @brief Provides functionality to create iterators in C++.
     *
     * @param c The target hashmap to navigate.
     * @return An iterator instance which can work on @b c.
     * @remark This function is current workaround to create object through C++.
     */
    public static function create(c : HashMap<Key, Value>): HashMapIterator<Key, Value>
    {
        return new HashMapIterator<Key, Value>(c);
    }
    /**
     * @endcond THOR_WORKAROUNDS
     */

    /**
     * @brief Get a copy of @c this.
     *
     * Get an iterator which has same status as @c this, both of them
     * point to the same @c HashMap and same element in it.
     *
     * @return A copy of @c this.
     * @remark This function performs @b shallow copy. That means
     *         the returned iterator will share the same map with
     *         @c this.
     */
    @native
    public virtual function clone(): HashMapIterator<Key, Value>;

    /**
     * @brief Tell if there is any entry not visited yet in
     *        the target @c HashMap @c this points to.
     *
     * @return A boolean value indicates visiting status.
     * @retval true if there is another key not visited yet.
     * @retval false means all key are visited.
     */
    @native
    public function hasNext(): bool;

    /**
     * @brief Get the next unvisited key and its value together as a pair in @c HashMap.
     *
     * @return A @c HashMapEntry instance which wraps unvisited key and
     *         associated value.
     */
    @native
    public function next(): HashMapEntry<Key, Value>;

    /**
     * @cond THOR_PRIVATE_IMPLEMENTATION
     */
    var target : HashMap<Key, Value>;
    var _cur   : ptr_<int8>;
    var _end   : ptr_<int8>;
    /**
     * @endcond THOR_PRIVATE_IMPLEMENTATION
     */
}

/**
 * @brief A Hash Function based data structure that maps keys to values.
 *
 * HashMap is an associative container which stores immutable type
 * as keys. HashMap are indexed by keys and keep elements the keys
 * map to. Keys are hashed in map, to be used as a key, user defined
 * types should provide two public member functions:
 * - @c isEqual()
 * - @c hash()
 * .
 * For example, a type @c Foo can be written as following: @n
 *
 * @code
 * class Foo
 * {
 *     public function isEqual(other: Foo): bool
 *     {
 *         return this.value == other.value;
 *     }
 *
 *     public function hash(): int64
 *     {
 *         return time_stamp;
 *     }
 *
 *     private var time_stamp: int64; // unique
 *     private var value: int32;
 * }
 * @endcode
 *
 * @c Foo should be convertiable to @c isEqual() 's parameter,
 * and the @c isEqual() return type should be convertible to
 * bool.
 *
 * @c hash() takes no aguments and it should return a
 * non-negative integer and avoid to return identical hash
 * codes from different objects.
 */
@cpu
@native { include = "thor/container/HashMap.h" }
class HashMap<Key, Value> extends Object implements GarbageCollectable, Cloneable
{
    /**
     * @brief constructor creates @c HashMap with no entries.
     */
	@native
    public function new(): void;

    /**
     * @cond THOR_WORKAROUNDS
     *
     * @brief Provides functionality to create empty hashmaps in C++.
     *
     * @return An empty hashmap instance.
     * @remark This function is current workaround to create object through C++.
     */
    public static function create(): HashMap<Key, Value>
    {
        return new HashMap<Key, Value>();
    }
    /**
     * @endcond THOR_WORKAROUNDS
     */

    /**
     * @brief Destructor which releases all resources hold by instance.
     */
    @native
    public virtual function delete(): void;

    /**
     * @brief Create a copy of @c this.
     *
     * @return a newly created @c HashMap instance with same
     *         keys and values.
     * @remark This function performs @b shallow copy. That means
     *         only elements' values are copied. For non-primitive
     *         types, elements in the produced @c HashMap share same
     *         objects with @c this.
     */
    @native
    public virtual function clone(): HashMap<Key, Value>;

    /**
     * @brief Get a mapped value by a given key.
     *
     * @return A value-result pair, first value is the mapped
     *         and second boolean value indicates if the key
     *         was found in map or not.
     * @retval (any, true) Found key in map, and first
     *         is its value.
     * @retval (any, false) Can not find key in map,
     *         first value is meaningless.
     */
    public function get(k : Key): (Value, bool)
    {
        if(has(k))
            return getImpl(k), true;
        else
            return getDefaultValue(), false;
    }

    /**
     * @brief Add a new key and its value.
     *
     * Add a new key into map and set its value. If the key is
     * already in map, rewrite the mapped value by passed one.
     *
     * @param k the key we want to add to map.
     * @param v the value we want to map by key @b k.
     */
	@native
    public function set(k : Key, v : Value): void;

    /**
     * @brief Remove a key from map.
     *
     * Remove a key and the value it maps from map. If the key
     * is not find in map, do nothing.
     *
     * @param k the key we want to remove from map.
     */
	@native
    public function remove(k : Key): void;

    /**
     * @brief Returns an iterator.
     *
     * @return An iterator which points to @c this.
     * @see HashMapIterator
     */
	@native
    public function iterator(): HashMapIterator<Key, Value>;

    /**
     * @brief Tell if the given key is in map.
     *
     * @param k The key we want to find.
     * @return Boolean value indicates if key is in map.
     * @retval true If the key is exist in map.
     * @retval false If the key is not in map.
     */
	@native
    public function has(k : Key): bool;

    /**
     * @brief Tell if there is no keys in map.
     *
     * What this function does is equivalent to call: @n
     * @code
     * return this.size() == 0;
     * @endcode
     * @return Boolean value indicates if @c this has no
     *         keys.
     * @retval true At least one key is in map.
     * @retval false No keys stay in @c this.
     * @see size()
     */
	@native
    public function empty(): bool;

    /**
     * @brief Returns the numbers of keys in @c this.
     *
     * @return The number of keys in @c this.
     */
	@native
    public function size(): int64;

    /**
     * @cond THOR_PRIVATE_IMPLEMENTATION
     */
    @native
    private function getImpl(k : Key): Value;

    @native
    private function getDefaultValue(): Value;
    /**
     * @endcond THOR_PRIVATE_IMPLEMENTATION
     */

    /**
     * Member function will be called by garbage collector to
     * keep track objects which are still referenced in native
     * implementations.
     *
     * @param o An object which collects still-referenced objects
     * @see CollectableObject
     */
	@native
	private virtual function getContainedObjects(o: CollectableObject): void;

    // member (just a pointer here, so no matter the instantiation type)
    /**
     * @cond THOR_PRIVATE_IMPLEMENTATION
     */
    var _p : ptr_<Key>;
    /**
     * @endcond THOR_PRIVATE_IMPLEMENTATION
     */
}
