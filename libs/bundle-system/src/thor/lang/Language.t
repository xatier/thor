/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */


//
// thor.lang.__initialize
// 
// The initialize function will be called before entry function. Currently, it should
// be set with annotation "export" so that it could be called by the framework.
//
/**
 * @cond THOR_PRIVATE_IMPLEMENTATION
 */
@cpu
@gpu 
function __initialize() : void
{
    on(x86={
        Domain.local();
        __resetRandomSeed();
        __RSTMSystemInitialize();
    },
    cuda = {
        //var obj = Domain.create();
    });        
}
/**
 * @endcond
 */

/**
 * @brief Base class of all classes in Thor.
 *
 * All classes will derived from this (directly or indirectly).
 * Additionally, Thor uses this as base class if no one is given.
 */
@replicable
@system
class Object
{
    @native public         function new   ():void;
    @native public virtual function delete():void;

    /**
     * @brief Generate hash value based on instance.
     *
     * Default implementation generates hash value based on memory location.
     * So user-defined classes may want to overwrite this behavior, in order
     * to generate value based on logical value. For example, string content.
     *
     * @return Hashed value.
     *
     * @remark O(1) Time complexity
     */
    @native public virtual function hash():int64;

    /**
     * @cond THOR_WORKAROUNDS
     */
    // FIXME object replication is not supported by GPU now, provide dummy serialize/deserialize for CPU only
    @cpu protected static function serialize  (encoder:ReplicationEncoder, instance:Object):bool { return true; }
    @cpu protected static function deserialize(decoder:ReplicationDecoder, instance:Object):bool { return true; }
    /**
     * @endcond
     */
}

/**
 * @brief Indicate classes are able to be cloned
 *
 * Classes which implements this interface need to implement method @c clone,
 * which responses to create copies of instance.
 */
@native
interface Cloneable
{
    /**
     * @brief Function to create copies of instance
     *
     * This method defines the copying of instance. For example,
     * user could create a copy of string since thor.lang.String implements
     * this interface. @n
     *
     * This method creates copied, but do not define the copy is deep or
     * shallow copied. For classes implementing @c Cloneable, user needs to
     * implement this method to provide required functionality of it.
     *
     * @return copy of instance
     * @retval non-null Be successfully copied
     * @retval null     Fail to be copied
     *
     * @remark This method did not define data should be deep or shallow
     *         copied. Please refer to classes who implements this interface.
     * @remark For cases that classes implement Cloneable indirectly (by
     *         inheritance), it's meaningful to override this method to copy
     *         required data in sub-classes.
     */
    function clone():Cloneable;
}

/**
 * @cond THOR_PRIVATE_IMPLEMENTATION
 */
@system
@cpu
@gpu
function __createObject(size:int64, type_id:int64):Object;
/**
 * @endcond
 */
