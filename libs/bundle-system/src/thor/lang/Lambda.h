/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef THOR_LANG_LAMBDA_H_
#define THOR_LANG_LAMBDA_H_

#include <type_traits>

#include "thor/Thor.h"
#include "thor/lang/Language.h"

namespace thor { namespace lang {

class Lambda : public Object
{
protected:
    template<typename T>
    using RealThorType = typename std::conditional<std::is_class<T>::value, typename std::add_pointer<T>::type, T>::type;

    template <typename ToType, typename FromType>
    static ToType __unsafeConvert(FromType p)
    {
        union {
            FromType ptrFrom;
            ToType   ptrTo;
        } type_converter;

        type_converter.ptrFrom = p;
        return type_converter.ptrTo;
    }
};

template <typename R>
class Lambda0 : public Lambda
{
    static_assert(thor::lang::is_thor_type<R>::value,  "invalid template type argument");

    using return_type         = R;
    using member_function_ptr = return_type (*)(void*);

public:
    Lambda0() = delete;

    template <typename T>
    Lambda0(T o)
        : _invoke(__unsafeConvert<member_function_ptr>(&std::remove_pointer<T>::type::invoke))
        , _obj(o)
    {
        static_assert(thor::lang::is_class<T>::value, "only accept lambda class objects");
    }

    ~Lambda0() {}

    return_type invoke()
    {
        return _invoke(_obj);
    }

public:
    member_function_ptr _invoke;
    void*               _obj;
};

template <typename R, typename T0>
class Lambda1 : public Lambda
{
    static_assert(thor::lang::is_thor_type<R>::value,  "invalid template type argument");
    static_assert(thor::lang::is_thor_type<T0>::value, "invalid template type argument");

    using return_type         = R;
    using arg0_type           = T0;
    using member_function_ptr = return_type (*)(void*, arg0_type);

public:
    Lambda1() = delete;

    template <typename T>
    Lambda1(T o)
        : _invoke(__unsafeConvert<member_function_ptr>(&std::remove_pointer<T>::type::invoke))
        , _obj(o)
    {
        static_assert(thor::lang::is_class<T>::value, "only accept lambda class objects");
    }

    ~Lambda1() {}

    return_type invoke(arg0_type arg0)
    {
          return _invoke(_obj, arg0);
    }

public:
    member_function_ptr _invoke;
    void*               _obj;
};

template <typename R, typename T0, typename T1>
class Lambda2 : public Lambda
{
    static_assert(thor::lang::is_thor_type<R>::value,  "invalid template type argument");
    static_assert(thor::lang::is_thor_type<T0>::value, "invalid template type argument");
    static_assert(thor::lang::is_thor_type<T1>::value, "invalid template type argument");

    using return_type         = R;
    using arg0_type           = T0;
    using arg1_type           = T1;
    using member_function_ptr = return_type (*)(void*, arg0_type, arg1_type);

public:
    Lambda2() = delete;

    template <typename T>
    Lambda2(T o)
        : _invoke(__unsafeConvert<member_function_ptr>(&std::remove_pointer<T>::type::invoke))
        , _obj(o)
    {
        static_assert(thor::lang::is_class<T>::value, "only accept lambda class objects");
    }

    ~Lambda2() {}

    return_type invoke(arg0_type arg0, arg1_type arg1)
    {
          return _invoke(_obj, arg0, arg1);
    }

public:
    member_function_ptr _invoke;
    void*               _obj;
};

} }

#endif /* THOR_LANG_LAMBDA_H_ */
