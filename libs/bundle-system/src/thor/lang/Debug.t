/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

//////////////////////////////////////////////////////////////////////////////
// print without new line
//////////////////////////////////////////////////////////////////////////////

/**
 * @brief Write @c true or @c false to standard output.
 *
 * @arg value The value indicates @c true or @c false should be written to.
 *
 * @see print(int8)
 * @see print(int16)
 * @see print(int32)
 * @see print(int64)
 * @see print(float32)
 * @see print(float64)
 * @see print(String)
 * @see println(bool)
 */
@cpu
@gpu
@system
function print(value:bool):void;

/**
 * @brief Write string representation of integer value to standard output.
 *
 * @arg value The value of string representation to be written to.
 *
 * @see print(bool)
 * @see print(int16)
 * @see print(int32)
 * @see print(int64)
 * @see print(float32)
 * @see print(float64)
 * @see print(String)
 * @see println(int8)
 */
@cpu
@gpu
@system
function print(value:int8):void;

/**
 * @brief Write string representation of integer value to standard output.
 *
 * @arg value The value of string representation to be written to.
 *
 * @see print(bool)
 * @see print(int8)
 * @see print(int32)
 * @see print(int64)
 * @see print(float32)
 * @see print(float64)
 * @see print(String)
 * @see println(int16)
 */
@cpu
@gpu
@system
function print(value:int16):void;

/**
 * @brief Write string representation of integer value to standard output.
 *
 * @arg value The value of string representation to be written to.
 *
 * @see print(bool)
 * @see print(int8)
 * @see print(int16)
 * @see print(int64)
 * @see print(float32)
 * @see print(float64)
 * @see print(String)
 * @see println(int32)
 */
@cpu
@gpu
@system
function print(value:int32):void;

/**
 * @brief Write string representation of integer value to standard output.
 *
 * @arg value The value of string representation to be written to.
 *
 * @see print(bool)
 * @see print(int8)
 * @see print(int16)
 * @see print(int32)
 * @see print(float32)
 * @see print(float64)
 * @see print(String)
 * @see println(int64)
 */
@cpu
@gpu
@system
function print(value:int64):void;

/**
 * @brief Write string representation of floating value to standard output.
 *
 * @arg value The value of string representation to be written to.
 *
 * @see print(bool)
 * @see print(int8)
 * @see print(int16)
 * @see print(int32)
 * @see print(int64)
 * @see print(float64)
 * @see print(String)
 * @see println(float32)
 */
@cpu
@gpu
@system
function print(value:float32):void;

/**
 * @brief Write string representation of floating value to standard output.
 *
 * @arg value The value of string representation to be written to.
 *
 * @see print(bool)
 * @see print(int8)
 * @see print(int16)
 * @see print(int32)
 * @see print(int64)
 * @see print(float32)
 * @see print(String)
 * @see println(float64)
 */
@cpu
@gpu
@system
function print(value:float64):void;

/**
 * @brief Write string to standard output.
 *
 * @arg value The string be written to.
 *
 * @see print(bool)
 * @see print(int8)
 * @see print(int16)
 * @see print(int32)
 * @see print(int64)
 * @see print(float32)
 * @see print(float64)
 * @see println(String)
 */
@cpu
@system
function print(value:String):void;

//////////////////////////////////////////////////////////////////////////////
// print with new line
//////////////////////////////////////////////////////////////////////////////

/**
 * @brief Write @c true or @c false to standard output, with new line appended.
 *
 * @arg value The value indicates @c true or @c false should be written to.
 *
 * @see println(int8)
 * @see println(int16)
 * @see println(int32)
 * @see println(int64)
 * @see println(float32)
 * @see println(float64)
 * @see println(String)
 * @see print(bool)
 */
@cpu
@gpu
@system
function println(value:bool):void;

/**
 * @brief Write string representation of integer value to standard output, with new line appended.
 *
 * @arg value The value of string representation to be written to.
 *
 * @see println(bool)
 * @see println(int16)
 * @see println(int32)
 * @see println(int64)
 * @see println(float32)
 * @see println(float64)
 * @see println(String)
 * @see print(int8)
 */
@cpu
@gpu
@system
function println(value:int8):void;

/**
 * @brief Write string representation of integer value to standard output, with new line appended.
 *
 * @arg value The value of string representation to be written to.
 *
 * @see println(bool)
 * @see println(int8)
 * @see println(int32)
 * @see println(int64)
 * @see println(float32)
 * @see println(float64)
 * @see println(String)
 * @see print(int16)
 */
@cpu
@gpu
@system
function println(value:int16):void;

/**
 * @brief Write string representation of integer value to standard output, with new line appended.
 *
 * @arg value The value of string representation to be written to.
 *
 * @see println(bool)
 * @see println(int8)
 * @see println(int16)
 * @see println(int64)
 * @see println(float32)
 * @see println(float64)
 * @see println(String)
 * @see print(int32)
 */
@cpu
@gpu
@system
function println(value:int32):void;

/**
 * @brief Write string representation of integer value to standard output, with new line appended.
 *
 * @arg value The value of string representation to be written to.
 *
 * @see println(bool)
 * @see println(int8)
 * @see println(int16)
 * @see println(int32)
 * @see println(float32)
 * @see println(float64)
 * @see println(String)
 * @see print(int64)
 */
@cpu
@gpu
@system
function println(value:int64):void;

/**
 * @brief Write string representation of floating value to standard output, with new line appended.
 *
 * @arg value The value of string representation to be written to.
 *
 * @see println(bool)
 * @see println(int8)
 * @see println(int16)
 * @see println(int32)
 * @see println(int64)
 * @see println(float64)
 * @see println(String)
 * @see print(float32)
 */
@cpu
@gpu
@system
function println(value:float32):void;

/**
 * @brief Write string representation of floating value to standard output, with new line appended.
 *
 * @arg value The value of string representation to be written to.
 *
 * @see println(bool)
 * @see println(int8)
 * @see println(int16)
 * @see println(int32)
 * @see println(int64)
 * @see println(float32)
 * @see println(String)
 * @see print(float64)
 */
@cpu
@gpu
@system
function println(value:float64):void;

/**
 * @brief Write string to standard output, with new line appended.
 *
 * @arg value The string be written to.
 *
 * @see println(bool)
 * @see println(int8)
 * @see println(int16)
 * @see println(int32)
 * @see println(int64)
 * @see println(float32)
 * @see println(float64)
 * @see print(String)
 */
@cpu
@system
function println(value:String):void;
