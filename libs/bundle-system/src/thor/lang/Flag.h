/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef THOR_LANG_FLAG_H_
#define THOR_LANG_FLAG_H_

#include "thor/Thor.h"
#include "thor/lang/Language.h"
#include "thor/lang/String.h"
#include "thor/container/Vector.h"

#include <memory>

namespace thor { namespace lang {

class Flag : public thor::lang::Object
{
public:
    Flag();
    virtual ~Flag();

public:
    bool createInteger(String* name, String* description, bool required = false);
    bool createFloat(String* name, String* description, bool required = false);
    bool createString(String* name, String* description, bool required = false);

    bool createPositionalInteger(String* name, String* description, int32 count = -1, bool required = false);
    bool createPositionalFloat(String* name, String* description, int32 count = -1, bool required = false);
    bool createPositionalString(String* name, String* description, int32 count = -1, bool required = false);

    bool parse();
    bool has(String* name);

    int64 getInteger(String* name);
    double getFloat(String* name);
    String* getString(String* name);

    thor::container::Vector<int64>* getPositionalInteger(String* name);
    thor::container::Vector<double>* getPositionalFloat(String* name);
    thor::container::Vector<String*>* getPositionalString(String* name);

    thor::container::Vector<String*>* getRaw();
    String* help();

public:
    static Flag* __create();

private:
    class FlagImpl;
    std::unique_ptr<FlagImpl> impl;
    static_assert(sizeof(impl) == sizeof(FlagImpl*),"sizeof impl is not equal to FlagImpl*");
};

} }

#endif /* THOR_LANG_FLAG_H_ */
