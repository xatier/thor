/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

/**
 * @brief Interface for native classes which keep object
 *        references in internal storages.
 *
 * Thor Garbage Collector keeps track of each created objects
 * and will take them back when they are no longer referenced. There
 * is no way to know if an object are finally referenced by only a C++
 * pointer, any usage in native code. To keep objects alive,
 * Thor allows native library developers to tell Garbage
 * Collector which objects are still in use, implement interface
 * GarbageCollectable.
 *
 * At each checkpoints in runtime, Garbage Collector trys to call
 * the member function @c getContainedObjects() to give users a chance
 * claiming any reference to objects.
 *
 * @see CollectableObject
 */
@cpu
@native
interface GarbageCollectable
{
    /**
     * @brief Member function will be called by Garbage Collector.
     *
     * Native library developers should work with the passed in
     * @c CollectableObject instance to give it objects via its
     * member functions.
     *
     * @param @c o Object which collects any in-use objects.
     * @see CollectableObject
     */
	@native
	public function getContainedObjects(o: CollectableObject): void;
}

/**
 * @brief A CollectableObject instance which will be passed as an argument
 *        of @c GarbaeCollectable.getContainedObjects().
 *
 * CollectableObject provides two ways to pass it object references in
 * C++ codes. @n
 *
 * // in C++ implementation
 * @code
 * virtual void getContainedObjects(thor::lang::CollectableObject* collector)
 * {
 *     // by single parameter version 'add()'
 *     collector->add(pointer_to_object);
 *
 *     // by C++ range version 'add()'
 *     collector->add(array, array + size);
 * }
 * @endcode
 *
 * @see GarbageCollectable
 */
@cpu
@native
class CollectableObject; 

