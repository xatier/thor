/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
import . = thor.unmanaged;
import . = thor.lang;

/**
 * @brief A class which can be used as template type argument of @c Random class.
 *
 * This type can specialize an uniform distribution random number generator
 * class from * @c Random template.
 *
 * @see Random
 */
@cpu
@native { include="thor/util/Random.h" }
class Uniform extends Object { }

/**
 * @brief A class which can be used as template type argument of @c Random class.
 *
 * This type can specialize a normal distribution random number generator
 * class from * @c Random template.
 *
 * @see Random
 */
@cpu
@native { include="thor/util/Random.h" }
class Normal extends Object { }

/**
 * @brief A Random Number Generator class template accepts all kind of type arguments.
 *
 * This is generic version of Random. We only support primitive types to be first
 * type argument @b T and second argument @b D is one of @c Uniform and @c Normal. @n
 *
 * Users can specialize this template and write their own generator.
 */
@cpu
@native { include="thor/util/Random.h" }
class Random<T,D> extends Object { }

/**
 * @brief An Uniform Random Number Generator class template designed to produce
 *        primitive type values.
 *
 * This template can be instantiated by passing primitive types as its first
 * argument, for example: @c Random<int32, Uniform> can produces random integer
 * values uniformly distributed on close interval [@a min, @a max]. The @a min,
 * @e max are the distribution parameters users pass to @c Random 's constructors.
 *
 * @ see Uniform
 */
@cpu
@native { include="thor/util/Random.h" }
class Random<T,D:Uniform> extends Object
{
    /**
     * @cond THOR_PRIVATE_IMPLEMENTATION
     */
    private var engine : ptr_<int8>;
    private var distribution : ptr_<int8>;
    /**
     * @endcond
     */

    /**
     * @brief Constructor creates a random number generator which produces
     *        any values which can be represented by type T.
     *
     * All generators created by this constructor have different random
     * number sequence from others.
     */
    @native
    public function new() : void;

    /**
     * @brief From a given random @a seed. creates a random number generator
     *        which produces any values which can be represented by type T.
     *
     * All generators created by same @a seed will produce same random number
     * sequence.
     *
     * @param @c seed The random seed.
     */
    @native
    public function new(seed:int64) : void;

    /**
     * @brief Constructor creates a random number generator which produces
     *        any values in close interval [min, max].
     *
     * All generators created by this constructor have different random
     * number sequence from others.
     *
     * @param @c min Minimum result value.
     * @param @c max Maximum result value.
     */
    @native
    public function new(min:T, max:T) : void;

    /**
     * @brief From a given random @a seed, creates a random number generator
     *        which produces any values in close interval [min, max].
     *
     * All generators created by a same @a seed will produce same random number
     * sequence.
     *
     * @param @c seed The random seed.
     * @param @c min Minimum result value.
     * @param @c max Maximum result value.
     */
    @native
    public function new(seed:int64, min:T, max:T) : void;

    /**
     * @brief Destructor releases internal resources.
     */
    @native
    public virtual function delete() : void;

    /**
     * @brief Reset generator's random seed.
     *
     * Set by another random seed, random number sequence will be renewed.
     * All generators which are reset by a same @a seed will produce
     * same sequence.
     *
     * @param @c seed The other new random seed.
     */
    @native
    public function seed(seed:int64) : void;

    /**
     * @brief Generator another random number.
     *
     * @return A new random number.
     */
    @native
    public function next() : T;
}

/**
 * @brief An Normal Random Number Generator class template designed to produce
 *        primitive type values.
 *
 * This template can be instantiated by passing primitive types as its first
 * argument, for example: @c Random<float32, Normal> can produces random real
 * values by given @a mean and @a standard @a deviation to @c Random 's
 * constructors. When user creates a @c Random<int32, Normal> instance,
 * the generator instance still produce floating points in its implementation,
 * but rounds result into a closest integer while return.
 *
 * @see Normal
 */
@cpu
@native { include="thor/util/Random.h" }
class Random<T,D:Normal> extends Object
{
    /**
     * @cond THOR_PRIVATE_IMPLEMENTATION
     */
    private var engine : ptr_<int8>;
    private var distribution : ptr_<int8>;
    /**
     * @endcond
     */

    /**
     * @brief Constructor creates a random number generator which produces
     *        any values by @a mean = 0.0 and @a standard @a deviation = 1.0.
     *
     * All generators created by this constructor have different random
     * number sequence from others.
     */
    @native
    public function new() : void;

    /**
     * @brief From a given random @a seed. creates a random number generator
     *        which produces any values by @a mean = 0.0 and
     *        @a standard @a deviation = 1.0.
     *
     * All generators created by same @a seed will produce same random number
     * sequence.
     *
     * @param @c seed The random seed.
     */
    @native
    public function new(seed:int64) : void;

    /**
     * @brief Constructor creates a random number generator which produces
     *        any values by specific @a mean and @a standard
     *        @a deviation.
     *
     * All generators created by this constructor have different random
     * number sequence from others.
     *
     * @param @c mean The mean.
     * @param @c stddev The standard deviation.
     */
    @native
    public function new(mean:T, stddev:T) : void;

    /**
     * @brief From a given random @a seed, creates a random number generator
     *        which produces any values by specific @a mean and @a standard
     *        @a deviation.
     *
     * All generators created by a same @a seed will produce same random number
     * sequence.
     *
     * @param @c seed The random seed.
     * @param @c mean The mean.
     * @param @c stddev The standard deviation.
     */
    @native
    public function new(seed:int64, mean:T, stddev:T) : void;

    /**
     * @brief Destructor releases internal resources.
     */
    @native
    public virtual function delete() : void;

    /**
     * @brief Reset generator's random seed.
     *
     * Set by another random seed, random number sequence will be renewed.
     * All generators which are reset by a same @a seed will produce
     * same sequence.
     *
     * @param @c seed The other new random seed.
     */
    @native
    public function seed(seed:int64) : void;    

    /**
     * @brief Generate another random number.
     *
     * @return A new random number.
     */
    @native
    public function next() : T;
}
