/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
import thor.math;
import thor.container;

class Compare
{
    private var epsilon: float64;

    public function new( eps: float64 ): void
    {
        epsilon = thor.math.fabs( eps );
    }

    public function isEqual( lhs: float64, rhs: float64 ): bool
    {
        return thor.math.fabs(
            lhs - rhs
        ) < epsilon;
    }
}

function to_radian( degree: float64 ): float64
{
    return (degree / 180) * pi;
}

function get_vec()
{
    return new thor.container.Vector<float64>;
}

function get_vecs()
{
    return new thor.container.Vector<float64>, new thor.container.Vector<float64>;
}


//// constants
var pi: float64 = 3.1415926535;
var e: float64 = 2.71828182846;

var root2: float64 = 1.4142135623;
var root3: float64 = 1.7320508075;
var root5: float64 = 2.2360679775;
var root6: float64 = root2 * root3;

var clog2: float64 = 0.3010299956;
var clog3: float64 = 0.4771212547;
var clog5: float64 = 0.6989700043;

// comparator
var cmp: Compare = new Compare( 0.0000000001 );

// function inputs/outputs
var result: float64 = 0.0;

var inputs: thor.container.Vector<float64> = null;
var inputs2: thor.container.Vector<float64> = null;

var expects: thor.container.Vector<float64> = null;
var expects2: thor.container.Vector<float64> = null;


function test_trigonometric(): bool
{
    // thor.math.cos()
    inputs, expects = get_vecs();
    inputs.pushBack( to_radian(15) );    inputs.pushBack( to_radian(45) ); inputs.pushBack( to_radian(60) );
    expects.pushBack( (root6+root2)/4 ); expects.pushBack( 1/root2 );      expects.pushBack( 0.5 );

    for( var i: int64 = 0; i < inputs.size(); ++i )
    {
        result = thor.math.cos( inputs[i] );
        if( !cmp.isEqual(result, expects[i]) ) return false;
    }

    // thor.math.sin()
    inputs, expects = get_vecs();
    inputs.pushBack( to_radian(15) );    inputs.pushBack( to_radian(45) ); inputs.pushBack( to_radian(60) );
    expects.pushBack( (root6-root2)/4 ); expects.pushBack( 1/root2 );      expects.pushBack( root3/2 );

    for( var i: int64 = 0; i < inputs.size(); ++i )
    {
        result = thor.math.sin( inputs[i] );
        if( !cmp.isEqual(result, expects[i]) ) return false;
    }

    // thor.math.tan()
    inputs, expects = get_vecs();
    inputs.pushBack( to_radian(15) ); inputs.pushBack( to_radian(45) ); inputs.pushBack( to_radian(60) );
    expects.pushBack( 2-root3 );      expects.pushBack( 1 );            expects.pushBack( root3 );

    for( var i: int64 = 0; i < inputs.size(); ++i )
    {
        result = thor.math.tan( inputs[i] );
        if( !cmp.isEqual(result, expects[i]) ) return false;
    }

    // thor.math.cosh()
    // thor.math.sinh()
    // thor.math.tanh()
    return true;
}

function test_inverse_trigonometric(): bool
{
    // thor.math.acos()
    inputs, expects = get_vecs();
    inputs.pushBack( root3/2 );        inputs.pushBack( 1/root2 );        inputs.pushBack( 0.5 );
    expects.pushBack( to_radian(30) ); expects.pushBack( to_radian(45) ); expects.pushBack( to_radian(60) );

    for( var i: int64 = 0; i < inputs.size(); ++i )
    {
        result = thor.math.acos( inputs[i] );
        if( !cmp.isEqual(result, expects[i]) ) return false;
    }

    // thor.math.asin()
    inputs, expects = get_vecs();
    inputs.pushBack( root3/2 );        inputs.pushBack( 1/root2 );        inputs.pushBack( 0.5 );
    expects.pushBack( to_radian(60) ); expects.pushBack( to_radian(45) ); expects.pushBack( to_radian(30) );

    for( var i: int64 = 0; i < inputs.size(); ++i )
    {
        result = thor.math.asin( inputs[i] );
        if( !cmp.isEqual(result, expects[i]) ) return false;
    }

    // thor.math.atan()
    inputs, expects = get_vecs();
    inputs.pushBack( 1/root3 );        inputs.pushBack( root3 );          inputs.pushBack( 1 );
    expects.pushBack( to_radian(30) ); expects.pushBack( to_radian(60) ); expects.pushBack( to_radian(45) );

    for( var i: int64 = 0; i < inputs.size(); ++i )
    {
        result = thor.math.atan( inputs[i] );
        if( !cmp.isEqual(result, expects[i]) ) return false;
    }

    // thor.math.atan2()
    inputs, inputs2 = get_vecs();
    expects = get_vec();

    inputs.pushBack( 1 );              inputs.pushBack( root3 );          inputs.pushBack( 1 );
    inputs2.pushBack( root3 );         inputs2.pushBack( 1 );             inputs2.pushBack( 1 );
    expects.pushBack( to_radian(30) ); expects.pushBack( to_radian(60) ); expects.pushBack( to_radian(45) );

    for( var i: int64 = 0; i < inputs.size(); ++i )
    {
        result = thor.math.atan2( inputs[i], inputs2[i] );
        if( !cmp.isEqual(result, expects[i]) ) return false;
    }

    // thor.math.acosh()
    // thor.math.asinh()
    // thor.math.atanh()
    return true;
}

function test_numeric(): bool
{
    // thor.math.fabs();
    inputs, expects = get_vecs();
    inputs.pushBack( 0 );  inputs.pushBack( -0.123 ); inputs.pushBack( -10 ); inputs.pushBack( 100 );
    expects.pushBack( 0 ); expects.pushBack( 0.123 ); expects.pushBack( 10 ); expects.pushBack( 100 );

    for( var i: int64 = 0; i < inputs.size(); ++i )
    {
        result = thor.math.fabs( inputs[i] );

        if( result != expects[i] ) return false;
    }

    // thor.math.cbrt()
    inputs, expects = get_vecs();
    inputs.pushBack( 1 );  inputs.pushBack( -2*root2 ); inputs.pushBack( 3*root3 ); inputs.pushBack( -5*root5 );
    expects.pushBack( 1 ); expects.pushBack( -root2 );  expects.pushBack( root3 );  expects.pushBack( -root5 );

    for( var i: int64 = 0; i < inputs.size(); ++i )
    {
        result = thor.math.cbrt( inputs[i] );

        if( !cmp.isEqual(result, expects[i]) ) return false;
    }

    // thor.math.hypot()
    inputs, inputs2 = get_vecs();
    inputs.pushBack( -2 );  inputs.pushBack( 2 );  inputs.pushBack( -1 );  inputs.pushBack( 1 );  inputs.pushBack( 0.5 );
    inputs2.pushBack( -2 ); inputs2.pushBack( 2 ); inputs2.pushBack( -1 ); inputs2.pushBack( 1 ); inputs2.pushBack( 0.5 );

    for( var i: int64 = 0; i < inputs.size(); ++i )
    {
        for( var j: int64 = 0; j < inputs2.size(); ++j )
        {
            result = thor.math.hypot( inputs[i], inputs2[j] );

            var left: float64 = inputs[ i ];
            var right: float64 = inputs2[ j ];

            var expect: float64 = thor.math.pow(
                (left*left) + (right*right),
                0.5
            );

            if( !cmp.isEqual(result, expect) ) return false;
        }
    }

    // thor.math.sqrt()
    inputs, expects = get_vecs();
    inputs.pushBack( 1 );  inputs.pushBack( 2 );      inputs.pushBack( 3 );      inputs.pushBack( 5 );
    expects.pushBack( 1 ); expects.pushBack( root2 ); expects.pushBack( root3 ); expects.pushBack( root5 );

    for( var i: int64 = 0; i < inputs.size(); ++i )
    {
        result = thor.math.sqrt( inputs[i] );

        if( !cmp.isEqual(result, expects[i]) ) return false;
    }

    // thor.math.ceil()
    inputs, expects = get_vecs();
    inputs.pushBack( -1.1 );  inputs.pushBack( -0.5 ); inputs.pushBack( 0.0 );  inputs.pushBack( 0.5 );  inputs.pushBack( 1.1 );
    expects.pushBack( -1.0 ); expects.pushBack( 0.0 ); expects.pushBack( 0.0 ); expects.pushBack( 1.0 ); expects.pushBack( 2.0 );

    for( var i: int64 = 0; i < inputs.size(); ++i )
    {
        result = thor.math.ceil( inputs[i] );

        if( !cmp.isEqual(result, expects[i]) ) return false;
    }

    // thor.math.floor()
    inputs, expects = get_vecs();
    inputs.pushBack( -1.1 );  inputs.pushBack( -0.5 );  inputs.pushBack( 0.0 );  inputs.pushBack( 0.5 );  inputs.pushBack( 1.1 );
    expects.pushBack( -2.0 ); expects.pushBack( -1.0 ); expects.pushBack( 0.0 ); expects.pushBack( 0.0 ); expects.pushBack( 1.0 );

    for( var i: int64 = 0; i < inputs.size(); ++i )
    {
        result = thor.math.floor( inputs[i] );

        if( !cmp.isEqual(result, expects[i]) ) return false;
    }

    // thor.math.round()
    inputs, expects = get_vecs();
    inputs.pushBack( -1.6 );  inputs.pushBack( -1.5 );  inputs.pushBack( -1.4 );  inputs.pushBack( 1.4 );  inputs.pushBack( 1.5 );  inputs.pushBack( 1.6 );
    expects.pushBack( -2.0 ); expects.pushBack( -2.0 ); expects.pushBack( -1.0 ); expects.pushBack( 1.0 ); expects.pushBack( 2.0 ); expects.pushBack( 2.0 );

    for( var i: int64 = 0; i < inputs.size(); ++i )
    {
        result = thor.math.round( inputs[i] );

        if( !cmp.isEqual(result, expects[i]) ) return false;
    }

    // thor.math.trunc()
    inputs, expects = get_vecs();
    inputs.pushBack( -1.6 );  inputs.pushBack( -1.4 );  inputs.pushBack( 1.4 );  inputs.pushBack( 1.6 );  inputs.pushBack( 2.0 );
    expects.pushBack( -1.0 ); expects.pushBack( -1.0 ); expects.pushBack( 1.0 ); expects.pushBack( 1.0 ); expects.pushBack( 2.0 );

    for( var i: int64 = 0; i < inputs.size(); ++i )
    {
        result = thor.math.trunc( inputs[i] );

        if( !cmp.isEqual(result, expects[i]) ) return false;
    }

    // thor.math.mod()
    inputs, inputs2 = get_vecs();
    inputs.pushBack( -123 ); inputs.pushBack( -123 ); inputs.pushBack( -123 ); inputs.pushBack( -123 );
    inputs2.pushBack( -20 ); inputs2.pushBack( -41 ); inputs2.pushBack( 20 );  inputs2.pushBack( 41 );

    inputs.pushBack( 123 );  inputs.pushBack( 123 );  inputs.pushBack( 123 ); inputs.pushBack( 123 );
    inputs2.pushBack( -20 ); inputs2.pushBack( -41 ); inputs2.pushBack( 20 ); inputs2.pushBack( 41 );

    for( var i: int64 = 0; i < inputs.size(); ++i )
    {
        result = thor.math.mod( inputs[i], inputs2[i] );

        var expect: float64 = inputs[i] % inputs2[i];

        if( !cmp.isEqual(result, expect) ) return false;
    }

    // thor.math.remainder()
    inputs, inputs2 = get_vecs();
    inputs.pushBack( -123 ); inputs.pushBack( -123 ); inputs.pushBack( -123 ); inputs.pushBack( -123 );
    inputs2.pushBack( -20 ); inputs2.pushBack( -41 ); inputs2.pushBack( 20 );  inputs2.pushBack( 41 );

    inputs.pushBack( 123 );  inputs.pushBack( 123 );  inputs.pushBack( 123 ); inputs.pushBack( 123 );
    inputs2.pushBack( -20 ); inputs2.pushBack( -41 ); inputs2.pushBack( 20 ); inputs2.pushBack( 41 );

    for( var i: int64 = 0; i < inputs.size(); ++i )
    {
        result = thor.math.remainder( inputs[i], inputs2[i] );

        var expect: float64 = inputs[i] % inputs2[i];

        if( !cmp.isEqual(result, expect) ) return false;
    }

    // thor.math.ma()
    // thor.math.nextafter()
    // thor.math.nexttoward()
    // thor.nearbyint()
    // thor.rint()
    // thor.irint()
    return true;
}

function test_float_property(): bool
{
    // thor.math.copysign()
    inputs, inputs2 = get_vecs(); expects = get_vec();

    inputs.pushBack( 0.5 );   inputs.pushBack( -0.5 );
    inputs2.pushBack( -1 );   inputs2.pushBack( 1 );
    expects.pushBack( -0.5 ); expects.pushBack( 0.5 );

    for( var i: int64 = 0; i < inputs.size(); ++i )
    {
        result = thor.math.copysign( inputs[i], inputs2[i] );

        if( !cmp.isEqual(result, expects[i]) ) return false;
    }

    // thor.math.scalbn()
    var float_radix: float64 = thor.math.radix();
    inputs = get_vec();
    inputs.pushBack( 0.5 );  inputs.pushBack( -2 );  inputs.pushBack( 2 );

    for( var i: int64 = 0; i < inputs.size(); ++i )
    {
        for( var second: int64 = 0; second < 10; ++second ) // for each powers
        {
            result = thor.math.scalbn( inputs[i], second );
            var expect = inputs[i] * thor.math.pow( float_radix, second );

            if( !cmp.isEqual(result, expect) ) return false;
        }
    }

    // thor.math.logb(), thor.math.ilogb()
    //inputs = get_vec();
    //inputs.pushBack( 0.5 );  inputs.pushBack( 2 );  inputs.pushBack( 10 );

    //for( var i: int64 = 0; i < inputs.size(); ++i )
    //{
    //    result = thor.math.logb( inputs[i] );
    //    var result2 = thor.math.ilogb( inputs[i] );

    //    var expect = cast<int32>( thor.math.log2( inputs[i] ) ); // might different on machines

    //    println("expect: \{expect}");
    //    println("result: \{result}");
    //    println("result2: \{result2}");
    //    if( !cmp.isEqual(result, expect) ) return false;
    //    if( !cmp.isEqual(result2, expect) ) return false;
    //}
    if(thor.math.ilogb(0) != thor.math.ilogb0()) return false;
    if(thor.math.ilogb(thor.math.infinity()) != 0x7FFFFFFF) return false;
    if(thor.math.ilogb(thor.math.NaN()) != thor.math.ilogbNaN()) return false;

    // thor.math.ldexp()
    inputs = get_vec();
    inputs.pushBack( 0.5 );  inputs.pushBack( -2 );  inputs.pushBack( 2 );

    for( var i: int64 = 0; i < inputs.size(); ++i )
    {
        for( var second: int64 = 0; second < 10; ++second ) // for each powers
        {
            result = thor.math.ldexp( inputs[i], second );
            var expect = inputs[i] * thor.math.pow( float_radix, second );

            if( !cmp.isEqual(result, expect) ) return false;
        }
    }

    // thor.math.isFinite(), thor.math.infinity()
    // float32 version  
    if( thor.math.isFinite(cast<float32>(thor.math.infinity()))) return false;
    if(!thor.math.isFinite(1.0f                               )) return false;
    // float64 version  
    if( thor.math.isFinite(thor.math.infinity())) return false;
    if(!thor.math.isFinite(1.0                 )) return false;

    // thor.math.isInfinite()
    // float32 version
    if( thor.math.isInfinite(1.0f                ))                return false;
    if(!thor.math.isInfinite(cast<float32>(thor.math.infinity()))) return false;
    // float64 version
    if( thor.math.isInfinite(1.0                 )) return false;
    if(!thor.math.isInfinite(thor.math.infinity())) return false;

    // thor.math.isNaN(), thor.math.NaN()
    // float32 version
    if( thor.math.isNaN(1.0f                         )) return false;
    if(!thor.math.isNaN(cast<float32>(thor.math.NaN()))) return false;
    // float64 version
    if( thor.math.isNaN(1.0            )) return false;
    if(!thor.math.isNaN(thor.math.NaN())) return false;

    // thor.math.hugeValue()
    if(thor.math.hugeValue() != thor.math.infinity()) return false;

    // thor.math.isNormal()()
    // float32 version
    if(!thor.math.isNormal( 1.0f)                              ) return false;
    if(!thor.math.isNormal(-1.0f)                              ) return false;
    if( thor.math.isNormal(cast<float32>(thor.math.infinity()))) return false;
    // float64 version
    if(!thor.math.isNormal( 1.0)                ) return false;
    if(!thor.math.isNormal(-1.0)                ) return false;
    if( thor.math.isNormal(thor.math.infinity())) return false;

    // thor.math.roundMode()
    switch(thor.math.roundMode())
    {
    case -1: // undetermined rounding direction
        /// TODO: need ways to test in this situation.
    case  0: // toward zero
        if(thor.math.round( 0.1) !=  0.0) return false;
        if(thor.math.round(-0.1) !=  0.0) return false;
    case  1: // to nearest
        if(thor.math.round( 0.1) !=  0.0) return false;
        if(thor.math.round(-0.1) !=  0.0) return false;
    case  2: // toward positive infinity
        if(thor.math.round( 0.1) !=  1.0) return false;
        if(thor.math.round(-0.1) !=  0.0) return false;
    case  3: // toward positive infinity
        if(thor.math.round( 0.1) !=  0.0) return false;
        if(thor.math.round(-0.1) != -1.0) return false;
    }

    return true;
}

function test_logarithm(): int32
{
    // thor.math.log()
    expects = get_vec();
    expects.pushBack( 0.5 ); expects.pushBack( 1 ); expects.pushBack( e );
    for( var i: int64 = 0; i < expects.size(); ++i )
    {
        var power = thor.math.log( expects[i] );
        result = thor.math.pow( e, power );

        if( !cmp.isEqual(result, expects[i]) ) return false;
    }

    // thor.math.log2()
    expects = get_vec();
    expects.pushBack( 0.5 ); expects.pushBack( 1 ); expects.pushBack( 2 );
    for( var i: int64 = 0; i < expects.size(); ++i )
    {
        var power = thor.math.log2( expects[i] );
        result = thor.math.pow( 2, power );

        if( !cmp.isEqual(result, expects[i]) ) return false;
    }

    // thor.math.log10()
    inputs, expects = get_vecs();
    inputs.pushBack( 2 );     inputs.pushBack( 3 );     inputs.pushBack( 5 );
    expects.pushBack( clog2 ); expects.pushBack( clog3 ); expects.pushBack( clog5 );
    for( var i: int64 = 0; i < inputs.size(); ++i )
    {
        result = thor.math.log10( inputs[i] );

        if( !cmp.isEqual(result, expects[i]) ) return false;
    }

    // thor.math.log1p()
    inputs = get_vec();
    inputs.pushBack( 2 ); inputs.pushBack( 3 ); inputs.pushBack( 5 );
    for( var i: int64 = 0; i < inputs.size(); ++i )
    {
        result = thor.math.log1p( inputs[i] );
        var expect = thor.math.log( 1 + inputs[i] );

        if( !cmp.isEqual(result, expect) ) return false;
    }

    return true;
}

function test_exp(): int32
{
    var powers = get_vec();
    powers.pushBack( 0.5 ); powers.pushBack( 2 ); powers.pushBack( -3 );
    // thor.math.exp(), thor.math.pow()
    for( var i: int64 = 0; i < powers.size(); ++i )
    {
        var expect = thor.math.pow( e, powers[i] );

        result = thor.math.exp( powers[i] );
        if( !cmp.isEqual(result, expect) ) return false;
    }

    // thor.math.exp2()
    powers = get_vec();
    powers.pushBack( 0.5 ); powers.pushBack( 2 ); powers.pushBack( -3 );
    for( var i: int64 = 0; i < powers.size(); ++i )
    {
        var expect = thor.math.pow( 2, powers[i] );

        result = thor.math.exp2( powers[i] );
        if( !cmp.isEqual(result, expect) ) return false;
    }

    // thor.math.expm1()
    powers = get_vec();
    powers.pushBack( 0.5 ); powers.pushBack( 2 ); powers.pushBack( -3 );
    for( var i: int64 = 0; i < powers.size(); ++i )
    {
        var expect = thor.math.exp( powers[i] ) - 1;

        result = thor.math.expm1( powers[i] );
        if( !cmp.isEqual(result, expect) ) return false;
    }

    // thor.math.erf()
    // thor.math.erfc()
    // thor.math.lgamma()
    // thor.math.tgamma()
    return true;
}

function test_relation(): bool
{
    // thor.math.dim()
    inputs, inputs2 = get_vecs(); expects = get_vec();

    inputs.pushBack( 0.2 );  inputs.pushBack( 0.3 );
    inputs2.pushBack( 0.3 ); inputs2.pushBack( 0.2 );
    expects.pushBack( 0 );   expects.pushBack( 0.1 );

    for( var i: int64 = 0; i < inputs.size(); ++i )
    {
        result = thor.math.dim( inputs[i], inputs2[i] );

        if( !cmp.isEqual(result, expects[i]) ) return false;
    }
    // thor.math.max(), thor.math.min()
    inputs, inputs2 = get_vecs(); expects, expects2 = get_vecs();

    inputs.pushBack( -1 );   inputs.pushBack( -1 );   inputs.pushBack( 1 );    inputs.pushBack( 1 );
    inputs2.pushBack( -2 );  inputs2.pushBack( 2 );   inputs2.pushBack( -2 );  inputs2.pushBack( 2 );

    expects.pushBack( -1 );  expects.pushBack( 2 );   expects.pushBack( 1 );   expects.pushBack( 2 );
    expects2.pushBack( -2 ); expects2.pushBack( -1 ); expects2.pushBack( -2 ); expects2.pushBack( 1 );

    for( var i: int64 = 0; i < inputs.size(); ++i )
    {
        // thor.math.max()
        result = thor.math.max( inputs[i], inputs2[i] );
        if( !cmp.isEqual(result, expects[i]) ) return false;

        // thor.math.min()
        result = thor.math.min( inputs[i], inputs2[i] );
        if( !cmp.isEqual(result, expects2[i]) ) return false;
    }

    return true;
}

@entry
task test() : void
{
    if(!test_trigonometric())         exit(1);
    if(!test_inverse_trigonometric()) exit(2);
    if(!test_numeric())               exit(3);
    if(!test_float_property())        exit(4);
    if(!test_exp())                   exit(5);
    if(!test_logarithm())             exit(6);
    if(!test_relation())              exit(7);

    exit(0);
}
