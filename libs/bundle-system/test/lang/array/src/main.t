/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
class Integer
{
    public function new(init_value: int32): void
    {                                                                                                                                         
        internal_value = init_value;
    }   

    public function getValue(): int32
    {
        return internal_value;
    }

    private var internal_value: int32;
}

function get_int_array_iterator(): ArrayIterator<Integer>
{
    const arr = [ 
        new Integer(0), new Integer(1), new Integer(2), new Integer(3), new Integer(4)
    ];  

    return arr.iterator();
}

function pass_or_print(counter: int32, it: ArrayIterator<Integer>): void
{
    if (counter > 0)
    {
        async -> pass_or_print(counter - 1, it); 
        return;
    }                                                                                                                                         

    while (it.hasNext())
    {   
        const integer       = it.next();
        const integer_value = integer.getValue();

        print("\{integer_value} ");
    }   
    print("\n");
    exit(0);
}

@entry
task main(): void
{
    // normal case
    {
        var a = [2, 3, 5];
        if(a.size() != 3) exit(10);
        if(a.get(0) != 2) exit(11);
        if(a.get(1) != 3) exit(12);
        if(a.get(2) != 5) exit(13);
    }

    // explicit type int32
    {
        var a = <int32>[2.25, 3.3, 5.5];
        if(a.size() != 3) exit(20);
        if(a.get(0) != 2) exit(21);
        if(a.get(1) != 3) exit(22);
        if(a.get(2) != 5) exit(23);
    }

    // implicit type float64
    {
        var a = [2.25, 3.3, 5.5];
        if(a.size() != 3)    exit(30);
        if(a.get(0) != 2.25) exit(31);
    }

    {
        var a = [[2], [3], [5]];
        if(a.size(0) != 3)   exit(40);
        if(a.size(1) != 1)   exit(41);
        if(a.get(0, 0) != 2) exit(42);
        if(a.get(1, 0) != 3) exit(43);
        if(a.get(2, 0) != 5) exit(44);
    }

    {
        var a = [
                    [[ 2,  3,  5], [12, 13, 15]],
                    [[22, 23, 25], [32, 33, 35]]
                ] ;
        if(a.size(0) != 2)       exit(50 );
        if(a.size(1) != 2)       exit(51 );
        if(a.size(2) != 3)       exit(52 );
        if(a.get(0, 0, 0) !=  2) exit(53 );
        if(a.get(0, 0, 1) !=  3) exit(54 );
        if(a.get(0, 0, 2) !=  5) exit(55 );
        if(a.get(0, 1, 0) != 12) exit(56 );
        if(a.get(0, 1, 1) != 13) exit(57 );
        if(a.get(0, 1, 2) != 15) exit(58 );
        if(a.get(1, 0, 0) != 22) exit(59 );
        if(a.get(1, 0, 1) != 23) exit(510);
        if(a.get(1, 0, 2) != 25) exit(511);
        if(a.get(1, 1, 0) != 32) exit(512);
        if(a.get(1, 1, 1) != 33) exit(513);
        if(a.get(1, 1, 2) != 35) exit(514);
    }

    {
        var values = [ 1, 2, 3, 4 ];
        var it = values.iterator();
        for(var expect = 1; it.hasNext(); ++expect) {
            if(expect != it.next()) exit(60); 
        }
    }

    pass_or_print(100, get_int_array_iterator());
}
