/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
import thor.container;

var execute_success : int32 = 0;

class Integer
{
    public function new(init_value: int32): void
    {                                                                                                                                         
        internal_value = init_value;
    }   

    public function getValue(): int32
    {
        return internal_value;
    }

    private var internal_value: int32;
}

function get_int_vector_iterator(): thor.container.VectorIterator<Integer>
{
    var int_vector = new thor.container.Vector<Integer>();
    for(var new_value = 0; new_value < 5; ++new_value)
        int_vector.pushBack( new Integer(new_value) );

    return int_vector.iterator();
}

function pass_or_print(counter: int32, it: thor.container.VectorIterator<Integer>): void
{
    if (counter > 0)
    {
        async -> pass_or_print(counter - 1, it); 
        return;
    }                                                                                                                                         

    while (it.hasNext())
    {   
        const integer       = it.next();
        const integer_value = integer.getValue();

        print("\{integer_value} ");
    }   
    print("\n");

    exit(execute_success);
}

@entry
task main(): void
{
    pass_or_print(100, get_int_vector_iterator());
}
