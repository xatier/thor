/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
import thor.container;
import thor.util;

var execute_success : int32 = 0;

class Integer
{
    public function new(init_value: int32): void
    {                                                                                                                                         
        internal_value = init_value;
    }   

    public function getValue(): int32
    {
        return internal_value;
    }

    private var internal_value: int32;
}

function get_hashmap_iterator(): thor.container.HashMapIterator<String, Integer>
{
    var str_int_map = new thor.container.HashMap<String, Integer>();
    for(var new_value = 0; new_value < 5; ++new_value)
    {
        var key   = thor.util.Convert.toString(new_value);
        var value = new Integer(new_value);

        str_int_map.set(key, value);
    }

    return str_int_map.iterator();
}

function pass_or_print(counter: int32, it: thor.container.HashMapIterator<String, Integer>): void
{
    if (counter > 0)
    {
        async -> pass_or_print(counter - 1, it); 
        return;
    }                                                                                                                                         

    while (it.hasNext())
    {   
        const str_and_int = it.next();
        const key   = str_and_int.key;
        const value = str_and_int.value.getValue();

        print("(\{key}, \{value}) ");
    }   
    print("\n");

    exit(execute_success);
}

class Foo
{
    public function new(value:int32):void 
    {
        x = value;
    }
    
    public function isLessThan(rhs:Foo):bool
    {
        return x < rhs.x;
    }
    public function isEqual(rhs:Foo):bool
    {
        return x == rhs.x;
    }
    public function hash():int64
    {
        return x;
    }
    public var x:int32;
}

function test_access(): void
{
    ////////////////////////////////////////
    // int hashmap
    ////////////////////////////////////////
    var int_hashmap:thor.container.HashMap<int32, int32>;
    int_hashmap = new thor.container.HashMap<int32, int32>();

    int_hashmap.set(1, 2);
    int_hashmap.set(2, 4);
    int_hashmap.set(3, 6);

    if(int_hashmap.size() != 3) exit(1);
    var value;
    var result;
    value, result = int_hashmap.get(3);
    if(!result || value != 6) exit(2);

    ////////////////////////////////////////
    // Foo hashmap
    ////////////////////////////////////////
    var hashmap_foo : thor.container.HashMap<Foo, int32>;
    hashmap_foo = new thor.container.HashMap<Foo, int32>();
    
    var fa:Foo = new Foo(7);
    var fb:Foo = new Foo(8);
    var fc:Foo = new Foo(9);
    
    hashmap_foo.set(fa, 1);
    hashmap_foo.set(fb, 2);
    hashmap_foo.set(fc, 3);
    
    if(hashmap_foo.size()  != 3) exit(3);
    var foo;
    foo, result = hashmap_foo.get(fa);
    if(!result || foo != 1) exit(4);
    foo, result = hashmap_foo.get(fb);
    if(!result || foo != 2) exit(4);
    foo, result = hashmap_foo.get(fc);
    if(!result || foo != 3) exit(5);
}

@entry
task main(): void
{
    test_access();

    pass_or_print(100, get_hashmap_iterator());
}
