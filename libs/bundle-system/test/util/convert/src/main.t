/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
import .= thor.util;

@entry
task main() : void
{
    var exit_success : int32 = 0;
    var exit_failure : int32 = -1;

    var i32 : int32 = 2147483647;
    if( !(Convert.toInt32(Convert.toString(i32)) == i32) ) exit(exit_failure);

    var f64 : float64 = 0.56;
    if( !(Convert.toFloat64(Convert.toString(f64)) == f64) ) exit(exit_failure);

    var s : String = "hello world";
    if( !Convert.toString(s).isEqual(s) ) exit(exit_failure);

    exit(exit_success);
}
