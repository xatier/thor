/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
import .= thor.util;
import .= thor.container;


function test_uniform(): bool
{
    var counters_size = 10;
    var expect_amount = 100000;
    var epsilon = cast<int32>(expect_amount * 0.01);

    var generator = new Random<int32, Uniform>(0, counters_size-1);
    var counters = new Vector<int32>(counters_size);

    // initialize all counters to zeros
    for(var i = 0; i != counters_size; ++i)
        counters[ i ] = 0;

    // generate random number and count on the counters
    for(var repeat = expect_amount * counters_size; 0 < repeat;)
    {
        var next = generator.next();
        if(next < 0 || counters_size <= next)
            continue;

        --repeat;
        counters[ next ] = counters[ next ] + 1;
    }

    var acceptable = lambda(a: int32): bool {
        return (expect_amount-epsilon <= a) && (a <= expect_amount+epsilon);
    };

    // verfiy if the counters are all in acceptable range
    for(var i = 0; i != counters_size; ++i)
    {
        if(!acceptable(counters[i]))
            return false;
    }

    return true;
}

function test_normal(): bool
{
    var stddev = 20;
    var mean = 5 * stddev;
    var counters_size = 2 * mean;
    var total_area = 100000;

    var generator = new Random<int32, Normal>(mean, stddev);
    var counters = new Vector<int32>(counters_size);

    // initialize all counters to zeros
    for(var i = 0; i != counters_size; ++i)
        counters[ i ] = 0;

    // generate random number and count on the counters
    for(var repeat = total_area; 0 < repeat; --repeat)
    {
        var next = generator.next();
        counters[ next ] = counters[ next ] + 1;
    }

    // the expected areas enclosed by stddevs
    var expected_areas = <int32>[ cast<int32>(total_area * 0.999937),
                                  cast<int32>(total_area * 0.997300),
                                  cast<int32>(total_area * 0.954500),
                                  cast<int32>(total_area * 0.682689) ];

    var acceptable = lambda(expect:int32, real:int32): bool {
        var epsilon = cast<int32>(expect*0.005);
        return (expect-epsilon <= real) && (real <= expect+epsilon);
    };

    // check for summed up stddevs
    var left = mean - 4*stddev;
    var stddev_count = 8;
    for(var expected_area in expected_areas)
    {
        var right = left + stddev_count*stddev;

        var real_area = 0;
        for(var scan = left; scan != right; ++scan)
            real_area += counters[ scan ];

        if(acceptable(expected_area, real_area) == false)
            return false;

        left += stddev;
        stddev_count -= 2;
    }

    return true;
}

@entry
task test() : void
{
    if(test_uniform() == false)
        exit(1);

    if(test_normal() == false)
        exit(2);

    exit(0);
}
