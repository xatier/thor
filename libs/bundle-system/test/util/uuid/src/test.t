/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
import ns_uuid = thor.util;

function get_nil_uuid_repr(): String {
    return "00000000-0000-0000-0000-000000000000";
}

function is_bool_equal(a: bool, b: bool): bool {
    if ( a &&  b) return true;
    if (!a && !b) return true;

    return false;
}

function happy_test(uuid: ns_uuid.UUID, is_nil: bool, str: String, error_begin: int32): int32 {
    const uuid_nil_repr     : String       = get_nil_uuid_repr();
    const uuid_from_string_1: ns_uuid.UUID = ns_uuid.UUID.parse(str            );
    const uuid_from_string_2: ns_uuid.UUID = ns_uuid.UUID.parse(uuid.toString());
    const uuid_cloned       : ns_uuid.UUID = uuid.clone();
    const hashed_value_uuid                = uuid              .hash();
    const hashed_value_uuid_from_string_1  = uuid_from_string_1.hash();
    const hashed_value_uuid_from_string_2  = uuid_from_string_2.hash();
    const hashed_value_uuid_cloned         = uuid_cloned       .hash();

    if (!is_bool_equal(is_nil, uuid              .isNil())) return error_begin - 1;
    if (!is_bool_equal(is_nil, uuid_from_string_1.isNil())) return error_begin - 2;
    if (!is_bool_equal(is_nil, uuid_from_string_2.isNil())) return error_begin - 3;
    if (!is_bool_equal(is_nil, uuid_cloned       .isNil())) return error_begin - 4;

    if (!uuid.isEqual(uuid_from_string_1)) return error_begin - 5;
    if (!uuid.isEqual(uuid_from_string_2)) return error_begin - 6;
    if (!uuid.isEqual(uuid_cloned       )) return error_begin - 7;

    if (hashed_value_uuid != hashed_value_uuid_from_string_1) return error_begin - 8;
    if (hashed_value_uuid != hashed_value_uuid_from_string_2) return error_begin - 9;
    if (hashed_value_uuid != hashed_value_uuid_cloned       ) return error_begin -10;

    if (!str.isEqual(uuid              .toString())) return error_begin -11;
    if (!str.isEqual(uuid_from_string_1.toString())) return error_begin -12;
    if (!str.isEqual(uuid_from_string_2.toString())) return error_begin -13;
    if (!str.isEqual(uuid_cloned       .toString())) return error_begin -14;

    uuid.setNil();

    const hashed_value_uuid_new = uuid.hash();

    if (!is_bool_equal(true  , uuid              .isNil())) return error_begin -15;
    if (!is_bool_equal(is_nil, uuid_from_string_1.isNil())) return error_begin -16;
    if (!is_bool_equal(is_nil, uuid_from_string_2.isNil())) return error_begin -17;
    if (!is_bool_equal(is_nil, uuid_cloned       .isNil())) return error_begin -18;

    if (is_nil)
    {
        if (!uuid.isEqual(uuid_from_string_1)) return error_begin -19;
        if (!uuid.isEqual(uuid_from_string_2)) return error_begin -20;
        if (!uuid.isEqual(uuid_cloned       )) return error_begin -21;

        if (hashed_value_uuid_new != hashed_value_uuid              ) return error_begin -22;
        if (hashed_value_uuid_new != hashed_value_uuid_from_string_1) return error_begin -23;
        if (hashed_value_uuid_new != hashed_value_uuid_from_string_2) return error_begin -24;
        if (hashed_value_uuid_new != hashed_value_uuid_cloned       ) return error_begin -25;

        if (!str          .isEqual(uuid.toString())) return error_begin -26;
        if (!uuid_nil_repr.isEqual(uuid.toString())) return error_begin -27;
    }
    else
    {
        if ( uuid.isEqual(uuid_from_string_1)) return error_begin -28;
        if ( uuid.isEqual(uuid_from_string_2)) return error_begin -29;
        if ( uuid.isEqual(uuid_cloned       )) return error_begin -30;

        if (hashed_value_uuid_new == hashed_value_uuid              ) return error_begin -31;
        if (hashed_value_uuid_new == hashed_value_uuid_from_string_1) return error_begin -32;
        if (hashed_value_uuid_new == hashed_value_uuid_from_string_2) return error_begin -33;
        if (hashed_value_uuid_new == hashed_value_uuid_cloned       ) return error_begin -34;

        if ( str          .isEqual(uuid.toString())) return error_begin -35;
        if (!uuid_nil_repr.isEqual(uuid.toString())) return error_begin -36;
    }

    return 0;
}

function failure_test_null_count(str: String): int32 {
    var count: int32 = 0;

    for (var i: int32 = 0; i <= str.length(); ++i)
    {
        const uuid_from_partial_1: ns_uuid.UUID = ns_uuid.UUID.parse(str.substring(i));
        const uuid_from_partial_2: ns_uuid.UUID = ns_uuid.UUID.parse(str.substring(0, i));

        if (uuid_from_partial_1 == null) ++count;
        if (uuid_from_partial_2 == null) ++count;
    }

    return count;
}

function failure_test(error_begin: int32): int32 {
    const sub_string_test_1  : String       = "00000000-0000-0000-0000-000000000000";
    const sub_string_test_2  : String       = "01234567-89ab-cdef-FEDC-BA9876543210";
    const uuid_from_empty    : ns_uuid.UUID = ns_uuid.UUID.parse(new String()                          );
    const uuid_from_illform_1: ns_uuid.UUID = ns_uuid.UUID.parse("@@@@@@@@-@@@@-@@@@-@@@@-@@@@@@@@@@@@");
    const uuid_from_illform_2: ns_uuid.UUID = ns_uuid.UUID.parse("********-****-****-****-************");
    const uuid_from_illform_3: ns_uuid.UUID = ns_uuid.UUID.parse("------------------------------------");
    const uuid_from_illform_4: ns_uuid.UUID = ns_uuid.UUID.parse("^^^^^^^^-^^^^-^^^^-^^^^-^^^^^^^^^^^^");

    if (uuid_from_empty     != null) return error_begin - 1;
    if (uuid_from_illform_1 != null) return error_begin - 2;
    if (uuid_from_illform_2 != null) return error_begin - 3;
    if (uuid_from_illform_3 != null) return error_begin - 4;
    if (uuid_from_illform_4 != null) return error_begin - 5;

    if (failure_test_null_count(sub_string_test_1) != 72) return error_begin - 6;
    if (failure_test_null_count(sub_string_test_2) != 72) return error_begin - 7;

    return 0;
}

@entry
task main(): void {
    var      error       : int32        = 0;
    const uuid_nil_repr  : String       = get_nil_uuid_repr();
    const      nil_uuid_1: ns_uuid.UUID = new ns_uuid.UUID();
    const      nil_uuid_2: ns_uuid.UUID = ns_uuid.UUID.nil();
    const randomed_uuid_1: ns_uuid.UUID = ns_uuid.UUID.random();
    const randomed_uuid_2: ns_uuid.UUID = ns_uuid.UUID.random();

    error =   happy_test(     nil_uuid_1, true , uuid_nil_repr             , -  0); if (error != 0) exit(error);
    error =   happy_test(     nil_uuid_2, true , uuid_nil_repr             , -100); if (error != 0) exit(error);
    error =   happy_test(randomed_uuid_1, false, randomed_uuid_1.toString(), -200); if (error != 0) exit(error);
    error =   happy_test(randomed_uuid_2, false, randomed_uuid_2.toString(), -300); if (error != 0) exit(error);
    error = failure_test(                                                    -400); if (error != 0) exit(error);

    exit(0);
}
