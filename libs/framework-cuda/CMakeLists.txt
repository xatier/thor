#
# Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
#
# This file is part of Thor.
# Thor is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License, version 3,
# as published by the Free Software Foundation.
#
# Thor is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Thor.  If not, see <http://www.gnu.org/licenses/>.
#
# If you want to develop any commercial services or closed-source products with
# Thor, to adapt sources of Thor in your own projects without
# disclosing sources, purchasing a commercial license is mandatory.
#
# For more information, please contact Zillians, Inc.
# <thor@zillians.com>
#

MACRO(FILE_TO_HEX_DIGITS input output)
    FIND_PROGRAM(XXD_EXECUTABLE xxd)
    FIND_PROGRAM(SED_EXECUTABLE sed)

    IF("${XXD_EXECUTABLE}" STREQUAL "XXD_EXECUTABLE-NOTFOUND")
        MESSAGE(SEND_ERROR "xxd is not found! macro FILE_TO_HEX_DIGITS needs it")
    ELSEIF("${SED_EXECUTABLE}" STREQUAL "SED_EXECUTABLE-NOTFOUND")
        MESSAGE(SEND_ERROR "sed is not found! macro FILE_TO_HEX_DIGITS needs it")
    ELSE()
        ADD_CUSTOM_COMMAND(
            OUTPUT   ${output}
            COMMAND  ${XXD_EXECUTABLE} -c 1 -p ${input} ${output}
            COMMAND  sed -i "s/\\([a-zA-Z0-9]\\{2\\}\\)$/0x\\1,/" ${output}
            DEPENDS  ${input}
            VERBATIM
        )
    ENDIF()
ENDMACRO(FILE_TO_HEX_DIGITS)

IF(ENABLE_FEATURE_CUDA)
    SET(GENERATED_HEADER_DIR_ROOT         "${CMAKE_CURRENT_BINARY_DIR}/include")
    SET(GENERATED_HEADER_CUDA_DETAIL_ROOT "${GENERATED_HEADER_DIR_ROOT}/framework/cuda_detail")
    SET(GLOBAL_DISPATCHER_SKELETON_IN     "${CMAKE_CURRENT_SOURCE_DIR}/src/framework/cuda_detail/Kernel.gd.cu")
    SET(GLOBAL_DISPATCHER_SKELETON_OUT    "${GENERATED_HEADER_CUDA_DETAIL_ROOT}/Kernel.gd.cu.inc")

    FILE(MAKE_DIRECTORY ${GENERATED_HEADER_DIR_ROOT}
                        ${GENERATED_HEADER_CUDA_DETAIL_ROOT}
    )

    FILE_TO_HEX_DIGITS(${GLOBAL_DISPATCHER_SKELETON_IN} ${GLOBAL_DISPATCHER_SKELETON_OUT})

    INCLUDE_DIRECTORIES(
        ${GENERATED_HEADER_DIR_ROOT}
        "${CMAKE_CURRENT_SOURCE_DIR}/src"
        ${THOR_SYSTEM_BUNDLE_INCLUDE_DIR}
        ${CUDA_INCLUDE_DIRS}
        "/usr/local/cuda/targets/x86_64-linux/include"
    )

    ADD_LIBRARY(thor-framework-cuda
        src/framework/ExecutorCUDA.cpp
        src/framework/RegisterCUDA.cpp
        src/framework/cuda_detail/GlobalDispatcherGenerator.cpp
        src/framework/cuda_detail/Kernel.cpp
        src/framework/cuda_detail/Launcher.cpp
        ${GLOBAL_DISPATCHER_SKELETON_OUT}
    )

    TARGET_LINK_LIBRARIES(thor-framework-cuda
        thor-framework
        ${CUDA_LIBRARIES}
    )
ELSE()
    MESSAGE(STATUS "framework-cuda is disabled since cuda is not enabled")
ENDIF()
