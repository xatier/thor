/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_FRAMEWORK_EXECUTOR_CUDA_H_
#define ZILLIANS_FRAMEWORK_EXECUTOR_CUDA_H_

#include <cstdint>

#include <future>
#include <memory>
#include <string>
#include <vector>

#include "framework/Executor.h"

namespace zillians { namespace framework {

namespace cuda_detail {

class kernel;
class launcher;

}

class executor_cuda : public executor_rt
{
public:
    explicit  executor_cuda(int new_id);
             ~executor_cuda();

    virtual bool initialize() override;
    virtual bool finalize() override;

    virtual const char* get_binary_file_suffix() const override;

    virtual void set_arguments(const std::vector<std::wstring>& new_args) override;

    virtual bool call(std::int64_t       func_id  ) override;
    virtual bool call(const std::string& func_name) override;
    virtual bool call(std::int64_t session_id, invocation_request&& request) override;

    virtual bool call_and_wait(std::int64_t       func_id  ) override;
    virtual bool call_and_wait(const std::string& func_name) override;

    virtual int  get_exit_code() override;
            void set_exit_code(std::int32_t new_exit_code);

protected:
    virtual void do_work() override;

private:
    int         device;
    std::string arch;

    std::promise<int> exit_code;

    std::unique_ptr<cuda_detail::kernel  > kern    ;
    std::unique_ptr<cuda_detail::launcher> launcher;
};

} }

#endif /* ZILLIANS_FRAMEWORK_EXECUTOR_CUDA_H_ */
