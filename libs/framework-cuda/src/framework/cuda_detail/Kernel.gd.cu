#include <stdint.h>

#include <iostream>
#include <utility>

#include <cuda_runtime.h>

namespace {

struct invocation_exec_state_t
{
    bool    is_exited;
    int32_t exit_code;
};

__device__ invocation_exec_state_t exec_state = {false, -1};

}

namespace thor { namespace lang {

__device__ void __initialize();

} }

/* $forward_declarations$ */

namespace zillians { namespace framework { namespace proxies { namespace cuda {

__device__ void exit(int32_t exit_code)
{
    exec_state.is_exited = true;
    exec_state.exit_code = exit_code;

    asm("exit;");
}

} } } }

namespace {

#define THREADS_PER_BLOCK /* $threads_per_block$ */

#define INVOCATION_SIZE           /* $invocation_size$ */
#define INVOCATION_HEADER_SIZE    /* $invocation_header_size$ */
#define INVOCATION_PARAMETER_SIZE (INVOCATION_SIZE - INVOCATION_HEADER_SIZE)

#define INVOCATION_MEMBER_OFFSET_FUNC_ID    /* $member_offset_func_id$ */

#define INVOCATION_MEMBER_REF_FUNC_ID()         (*reinterpret_cast<int64_t*>(block_invocations[threadIdx.x] + INVOCATION_MEMBER_OFFSET_FUNC_ID))
#define INVOCATION_MEMBER_REF_PARAMETER(offset) (*reinterpret_cast<int64_t*>(block_invocations[threadIdx.x] + INVOCATION_HEADER_SIZE + offset))

#if INVOCATION_PARAMETER_ELEMENTS * DISPATCHER_THREADS_PER_BLOCK > 16000
    #error "required shared memory size too large"
#endif

__shared__ int8_t block_invocations[THREADS_PER_BLOCK][INVOCATION_SIZE];

__device__ void load_invocations_for_block(const int8_t* all_invocations, unsigned invocation_count)
{
    if (threadIdx.x == 0)
    {
        const unsigned begin_idx = blockIdx.x * THREADS_PER_BLOCK;

        if (begin_idx < invocation_count)
        {
            const unsigned count_for_block = invocation_count - begin_idx >= THREADS_PER_BLOCK ? THREADS_PER_BLOCK
                                                                                               : invocation_count - begin_idx;
            const unsigned bytes_for_block = count_for_block * INVOCATION_SIZE;
            const unsigned begin_offset    = begin_idx       * INVOCATION_SIZE;

            memcpy(block_invocations, all_invocations + begin_offset, bytes_for_block);
        }
    }

    __syncthreads();
}

__device__ bool has_invocation(unsigned invocation_count)
{
    const unsigned idx = blockIdx.x * THREADS_PER_BLOCK + threadIdx.x;

    return idx < invocation_count;
}

__device__ void launch_invocation(unsigned invocation_count)
{
    if (!has_invocation(invocation_count))
        return;

    // TODO implement it
    switch (INVOCATION_MEMBER_REF_FUNC_ID())
    {
    case /* $system_init_id$ */:
        /* $global_offset_setters$ */
        thor::lang::__initialize();
        break;

    /* $global_inits$ */

    /* $exported_functions$ */

    default:
        break;
    }
}

__global__ void cuda_global_dispatcher(const int8_t* all_invocations, unsigned invocation_count)
{
    load_invocations_for_block(all_invocations, invocation_count);
    launch_invocation(invocation_count);
}

}

std::pair<bool, int> global_dispatcher(unsigned blocks, const int8_t* all_invocations, unsigned invocation_count)
{
    cuda_global_dispatcher<<<blocks, THREADS_PER_BLOCK>>>(all_invocations, invocation_count);

    ::cudaDeviceSynchronize();

    invocation_exec_state_t local_exec_state  = {false, -1};
    const cudaError_t       exec_trans_result = ::cudaMemcpyFromSymbol(&local_exec_state, &exec_state, sizeof(local_exec_state));
    if (exec_trans_result != ::cudaSuccess)
        std::cerr << "fail to retrieve execution state for cuda" << std::endl;

    return std::make_pair(local_exec_state.is_exited, local_exec_state.exit_code);
}
