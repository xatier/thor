/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_FRAMEWORK_CUDA_DETAIL_GLOBALDISPATCHERGENERATOR_H_
#define ZILLIANS_FRAMEWORK_CUDA_DETAIL_GLOBALDISPATCHERGENERATOR_H_

#include <cstdint>

#include <deque>
#include <iosfwd>
#include <iterator>
#include <map>
#include <string>
#include <utility>
#include <vector>

#include <boost/range/iterator_range.hpp>

#include <log4cxx/logger.h>

#include "language/Architecture.h"

#include "framework/detail/GlobalDispatcherGenerator.h"
#include "framework/detail/GlobalOffsetsComputor.h"

namespace zillians { namespace language { namespace tree {

struct FunctionDecl;
struct PrimitiveType;
struct Type;
struct TypeSpecifier;

} } }

namespace zillians { namespace framework { namespace cuda_detail {

class global_dispatcher_generator : public detail::global_dispatcher_generator
{
private:
    using FunctionDecl = language::tree::FunctionDecl;

public:
    global_dispatcher_generator(
        const log4cxx::LoggerPtr&       new_logger        ,
        language::Architecture          new_arch          ,
        const detail::global_offsets_t& new_global_offsets,
        std::ostream&                   new_output
    );

protected:
    virtual bool load_skeleton() override;

    virtual bool set_global_offset(const std::string& name, std::int64_t offset) override;

    virtual bool add_global_init(const FunctionDecl& func_decl) override;
    virtual bool add_exported_function(boost::iterator_range<detail::func_mapping_t::right_const_iterator> exported_func_with_ids) override;

    virtual bool finish() override;

private:
    // constants exposed from framework
    std::ostream_iterator<char> on_threads_per_block(std::ostream_iterator<char> out);
    std::ostream_iterator<char> on_invocation_size(std::ostream_iterator<char> out);
    std::ostream_iterator<char> on_invocation_header_size(std::ostream_iterator<char> out);
    std::ostream_iterator<char> on_member_offset_func_id(std::ostream_iterator<char> out);

    // forward declarations from bundles
    std::ostream_iterator<char> on_forward_declarations(std::ostream_iterator<char> out);

    // redirect asynchronous calls to real functions
    std::ostream_iterator<char> on_system_init_id(std::ostream_iterator<char> out);
    std::ostream_iterator<char> on_global_offset_setters(std::ostream_iterator<char> out);
    std::ostream_iterator<char> on_global_inits(std::ostream_iterator<char> out);
    std::ostream_iterator<char> on_exported_functions(std::ostream_iterator<char> out);

    // function declaration
    std::ostream_iterator<char> declare_function(std::ostream_iterator<char> out, const FunctionDecl& func_decl);

    // function invocation
    std::ostream_iterator<char> call_function(std::ostream_iterator<char> out, const FunctionDecl& func_decl);

    // type name
    const std::string& get_type_name(const language::tree::TypeSpecifier& fs  );
    const std::string& get_type_name(const language::tree::Type&          type);
    const std::string& get_type_name(const language::tree::PrimitiveType& primitive_type);

private:
    std::deque<std::pair<std::string, std::int64_t>>                      global_offset_setters;
    std::deque<const FunctionDecl*>                                       global_inits;
    std::deque<std::pair<const FunctionDecl*, std::vector<std::int64_t>>> exported_functions;

    std::map<const language::tree::Type*, std::string> type_cache;

    std::ostream* output;
};

} } }

#endif /* ZILLIANS_FRAMEWORK_CUDA_DETAIL_GLOBALDISPATCHERGENERATOR_H_ */
