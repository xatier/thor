/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <cassert>
#include <cstddef>
#include <cstdint>

#include <atomic>
#include <condition_variable>
#include <mutex>
#include <stdexcept>
#include <string>
#include <utility>

#include <boost/range/adaptor/transformed.hpp>

#include <cuda_runtime.h>

#include <thrust/device_vector.h>
#include <thrust/memory.h>

#include <log4cxx/level.h>
#include <log4cxx/logger.h>

#include "framework/ExecutorCUDA.h"
#include "framework/cuda_detail/Kernel.h"
#include "framework/cuda_detail/Launcher.h"

namespace zillians { namespace framework { namespace cuda_detail {

namespace {

class auto_cuda_device
{
public:
    auto_cuda_device(log4cxx::LoggerPtr logger, int device)
    {
        const auto& result = ::cudaSetDevice(device);

        if (result != ::cudaSuccess)
        {
            const auto*const err_msg = ::cudaGetErrorString(result);

            LOG4CXX_ERROR(logger, L"fail to set CUDA device: " << err_msg);
            throw std::runtime_error("fail to set CUDA device" + std::string(err_msg));
        }
    }

    ~auto_cuda_device()
    {
        const auto& result = ::cudaDeviceReset();

        if (result != ::cudaSuccess)
            LOG4CXX_ERROR(logger, L"fail to reset CUDA device");
    }

private:
    log4cxx::LoggerPtr logger;
};

}

launcher::launcher(executor_cuda& new_executor, kernel& new_kern, int new_device)
    : verbose(false)
    , executor(&new_executor)
    , kern(&new_kern)
    , device(new_device)
    , invocations_from_external_guard()
    , invocations_from_external()
    , logger(log4cxx::Logger::getLogger("framework.cuda.launcher"))
{
    set_verbose(false);
}

bool launcher::is_verbose() const noexcept
{
    return verbose;
}

void launcher::set_verbose(bool new_verbose/* = true*/) noexcept
{
    verbose = new_verbose;

    logger->setLevel(verbose ? log4cxx::Level::getInfo() : log4cxx::Level::getError());
}

bool launcher::launch_batch()
{
    static thread_local auto_cuda_device device_holder(logger, device);

    const auto& invocations = prepare_invocations();
    if (invocations.empty())
        return false;

    thrust::device_vector<invocation_t> device_invocations = invocations;

    auto*const gd = kern->get_global_dispatcher();
    assert(gd != nullptr && "null pointer exception");

          auto*const ptr   = thrust::raw_pointer_cast(device_invocations.data());
    const auto&      count = device_invocations.size();

    assert(ptr != nullptr && "null pointer exception");

    // TODO compute suitable block count in runtime?
    const auto& result    = gd(32 /* block count */, reinterpret_cast<const std::int8_t*>(ptr), count);
    const auto& is_exited = std::get<0>(result);
    const auto& exit_code = std::get<1>(result);

    // FIXME current implementation assumes that there is no invocation will be delayed
    for (const auto*const notifier : invocations | boost::adaptors::transformed(std::mem_fn(&invocation_t::done_notifier)))
    {
        if (notifier == nullptr)
            continue;

        try
        {
            notifier->operator()();
        }
        catch (const std::exception& e)
        {
            LOG4CXX_ERROR(logger, L"exception thrown in notifier passed from outside");
        }
    }

    if (is_exited)
        executor->set_exit_code(exit_code);

    return true;
}

bool launcher::add_invocation_from_external(const std::string& func_name)
{
    const auto& func_id = kern->get_function_id(func_name);

    if (func_id == -1)
        return false;

    return add_invocation_from_external(func_id);
}

bool launcher::add_invocation_from_external(std::int64_t func_id)
{
    assert(func_id >= 0 && "invalid function id");

    invocation_t invocation;

    invocation.session_id    = -1;
    invocation.function_id   = func_id;
    invocation.done_notifier = nullptr;

    push_invocation_from_external(std::move(invocation));

    return true;
}

bool launcher::run_invocation_from_external(const std::string& func_name)
{
    const auto& func_id = kern->get_function_id(func_name);

    if (func_id == -1)
        return false;

    return run_invocation_from_external(func_id);
}

bool launcher::run_invocation_from_external(std::int64_t func_id)
{
    assert(func_id >= 0 && "invalid function id");

    std::mutex              guard;
    std::condition_variable cond;
    std::function<void()>   notifier = [&guard, &cond]
                                       {
                                           std::lock_guard<std::mutex> lock(guard);

                                           cond.notify_one();
                                       };

    std::unique_lock<std::mutex> lock(guard);

    {
        invocation_t invocation;

        invocation.session_id    = -1;
        invocation.function_id   = func_id;
        invocation.done_notifier = &notifier;

        push_invocation_from_external(std::move(invocation));
    }

    cond.wait(lock);

    return true;
}

std::size_t launcher::push_invocation_from_external(invocation_t&& invocation)
{
    LOG4CXX_INFO(logger, L"add invocation for function: " << invocation.function_id);

    std::lock_guard<std::mutex> lock(invocations_from_external_guard);

    invocations_from_external.push_back(std::move(invocation));

    return invocations_from_external.size() - 1;
}

thrust::host_vector<invocation_t> launcher::prepare_invocations()
{
    thrust::host_vector<invocation_t> invocations;

    {
        std::lock_guard<std::mutex> lock(invocations_from_external_guard);

        invocations = invocations_from_external;
        invocations_from_external.clear();
    }

    return invocations;
}

} } }
