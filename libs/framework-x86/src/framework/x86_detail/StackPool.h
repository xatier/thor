/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_FRAMEWORK_X86_DETAIL_STACKPOOL_H_
#define ZILLIANS_FRAMEWORK_X86_DETAIL_STACKPOOL_H_

#include <cstddef>

#include <vector>

#include <boost/coroutine/stack_allocator.hpp>
#include <boost/noncopyable.hpp>
#include <boost/version.hpp>

// fr3@K
// stack_allocator interface changed in 1.55, requiring stack_context
#if(BOOST_VERSION >= 105500)
#include <boost/coroutine/stack_context.hpp>
#endif

namespace zillians { namespace framework { namespace x86_detail {

class stack_pool : boost::noncopyable
{
public:
              stack_pool();
    explicit  stack_pool(std::size_t concurrency);
             ~stack_pool();

    std::size_t get_concurrency() const noexcept;

    std::pair<std::size_t, void*> get_stack(std::size_t index) const;

private:
    std::size_t                        stack_size;
    boost::coroutines::stack_allocator allocator;
#if(BOOST_VERSION >= 105500)
    std::vector<boost::coroutines::stack_context> contexts;
#else
    std::vector<void*>                            stack_ptrs;
#endif
};

} } }

#endif /* ZILLIANS_FRAMEWORK_X86_DETAIL_STACKPOOL_H_ */
