/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_FRAMEWORK_X86_DETAIL_KERNEL_H_
#define ZILLIANS_FRAMEWORK_X86_DETAIL_KERNEL_H_

#include <cstdint>

#include <memory>
#include <string>
#include <utility>
#include <vector>

#include <boost/filesystem/path.hpp>
#include <boost/noncopyable.hpp>

#include <llvm/ExecutionEngine/ExecutionEngine.h>

#include <log4cxx/logger.h>

#include "utility/DllPtr.h"

#include "language/stage/generator/context/SynthesizedObjectLayoutContext.h"

#include "framework/proxies/X86.h"

#include "framework/detail/GlobalOffsetsComputor.h"

namespace zillians { namespace language { namespace tree {

struct ClassDecl;
struct FunctionDecl;
struct Tangle;

} } }

namespace zillians { namespace framework { namespace x86_detail {

class kernel : boost::noncopyable
{
private:
    using path_t = boost::filesystem::path;

public:
    using global_dispatcher_t = void (*)(
        long long   /* function id   */,
        const char* /* parameter ptr */,
        char*       /* store_to      */
    );

    using object_layout_t = language::stage::SynthesizedObjectLayoutContext;

     kernel(const path_t& ast_path, bool enable_server, bool enable_client);
    ~kernel();

    bool is_initialized() const noexcept;
    bool initialize(const path_t& ast_path, const path_t& dll_path, const std::vector<path_t>& dep_pathes);
    bool finalize() noexcept;

    bool is_verbose() const noexcept;
    void set_verbose(bool new_verbose = true) noexcept;

    global_dispatcher_t get_global_dispatcher() const noexcept;

    const language::tree::Tangle*          get_tangle() const noexcept;
    const language::tree::ClassDecl*       get_class_decl(std::int64_t type_id) const;
    const object_layout_t&                 get_object_layout(const language::tree::ClassDecl& decl) const;
    std::int64_t                           get_function_id(const std::string& name) const;
    const language::tree::FunctionDecl*    get_function_decl(std::int64_t func_id) const;
    std::pair<const wchar_t*, std::size_t> get_string(std::int64_t str_id) const;

    proxies::x86::remote_adaptor_t     * get_remote_adaptor     (std::int64_t func_id) const;
    proxies::x86::object_creator_t     * get_object_creator     (std::int64_t type_id) const;
    proxies::x86::object_serializer_t  * get_object_serializer  (std::int64_t type_id) const;
    proxies::x86::object_deserializer_t* get_object_deserializer(std::int64_t type_id) const;

private:
    template<typename traits>
    typename traits::signature_t* get_normal_function(std::int64_t func_id) const;

    template<typename traits>
    typename traits::signature_t* get_object_special_function(std::int64_t type_id) const;

private:
    bool                          verbose;
    const language::tree::Tangle* tangle;
    detail::global_offsets_t      global_offsets;
    detail::  func_mapping_t        func_mapping;
    detail::  type_mapping_t        type_mapping;
    detail::string_mapping_t      string_mapping;

    std::vector<dll_ptr>                   dlls;
    std::unique_ptr<llvm::ExecutionEngine> exec_engine;
    global_dispatcher_t                    global_dispatcher;

    log4cxx::LoggerPtr logger;
};

} } }

#endif /* ZILLIANS_FRAMEWORK_X86_DETAIL_KERNEL_H_ */
