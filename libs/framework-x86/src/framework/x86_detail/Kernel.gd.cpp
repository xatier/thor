/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

namespace thor { namespace lang {

void __domainOnConnect();
void __domainOnDisconnect();
void __domainOnError();

void __remoteInvoker();

void __initialize();

} }

namespace zillians { namespace framework { namespace proxies { namespace x86 {

void destroy_objects(void* ptr);

} } } }

void global_dispatch(long long function_id, const char* parameter_ptr, char* store_to)
{
    switch (function_id)
    {
    case 0 /* destroy objects */:
        ::zillians::framework::proxies::x86::destroy_objects(const_cast<char*>(parameter_ptr));
        break;

    case 1 /* remote invocation */:
        ::thor::lang::__remoteInvoker();
        break;

    case 2 /* system initialization */:
        ::thor::lang::__initialize();
        break;

    case 3 /* global initialization */:
        break;

    case 4 /* handle thor.lang.Domain connect events */:
        ::thor::lang::__domainOnConnect();
        break;

    case 5 /* handle thor.lang.Domain disconnect events */:
        ::thor::lang::__domainOnDisconnect();
        break;

    case 6 /* handle thor.lang.Domain error events */:
        ::thor::lang::__domainOnError();
        break;
    }
}
