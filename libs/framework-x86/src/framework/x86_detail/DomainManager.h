/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_FRAMEWORK_X86_DETAIL_DOMAIN_MANAGER_H
#define ZILLIANS_FRAMEWORK_X86_DETAIL_DOMAIN_MANAGER_H

#include <cstddef>
#include <cstdint>

#include <string>
#include <utility>
#include <mutex>
#include <deque>

#include <boost/variant.hpp>
#include <boost/noncopyable.hpp>
#include <boost/system/error_code.hpp>

#include <tbb/concurrent_hash_map.h>

#include "utility/UUIDUtilTBB.h"
#include "network/SessionFactory.h"

#include "framework/proxies/x86/RootSetPtr.h"
#include "framework/x86_detail/DomainEventHandler.h"

#include "thor/lang/Domain.h"

namespace zillians { namespace network {

class session;
class listener;

} }

namespace zillians { namespace framework {

class executor_remote;

} }

namespace zillians { namespace framework { namespace x86_detail {


class domain_manager : public boost::noncopyable
{
public:
    // sub types
    using domain_type               = ::thor::lang::Domain;
    using domain_error_type         = ::thor::lang::DomainError;
    using domain_conn_callback_type = domain_type::ConnCallback;
    using domain_err_callback_type  = domain_type::ErrCallback;

private:
    struct request_type
    {
        enum // defaule underlying type is 'int'
        {
            LISTEN, CONNECT, NOT_EXIST
        };

        int type;

        proxies::x86::root_set_ptr<domain_conn_callback_type> on_connect;
        proxies::x86::root_set_ptr<domain_conn_callback_type> on_disconnect;
        proxies::x86::root_set_ptr<domain_err_callback_type>  on_error;
    };

public:
    domain_manager(
        executor_remote& remote,
        object_manager& object_manager,
        domain_event_handler& domain_event_handler
    );
    ~domain_manager();

    // interfaces for thor.lang
    UUID listen(const std::wstring&        wide_endpoint,
                domain_conn_callback_type* on_connect,
                domain_conn_callback_type* on_disconnect,
                domain_err_callback_type*  on_error);
    UUID connect(const std::wstring&        wide_endpoint,
                 domain_conn_callback_type* on_connect,
                 domain_conn_callback_type* on_disconnect,
                 domain_err_callback_type*  on_error);
    bool cancel(const UUID& connection_id);

    domain_type* find(std::int64_t session_id) const;
    domain_type* insert(std::int64_t session_id, domain_type* domain);

private:
    // interfacess for thor.lang implementations
    UUID listen_impl(network::listener::transport_type type, const UUID& connection_id, std::uint16_t port);
    UUID connect_impl(network::session::transport_type type, const UUID& connection_id, const std::string& address, std::uint16_t port);
    bool close(const UUID& connection_id);

    // handlers
    UUID notify_connect(const UUID& connection_id, network::session* session);
    UUID notify_disconnect(const UUID& connection_id, network::session* session);
    UUID notify_error(const UUID& connection_id, domain_error_type error);

    void handle_accepted(const UUID& connection_id, network::listener* listener, const boost::system::error_code& error);
    void handle_connected(const UUID& connection_id, network::session* session, const boost::system::error_code& error);

    // data transmission handler
    void handle_read_data(const UUID& connection_id,
                          network::session* session,
                          const boost::system::error_code& error,
                          std::size_t bytes_transferred);

    // manipulators of internal data structures
    std::pair<UUID, bool> add_request(
        int                           type,
        domain_conn_callback_type*    on_connect,
        domain_conn_callback_type*    on_disconnect,
        domain_err_callback_type*     on_error
    );

    bool                drop_request(const UUID& connection_id);
    const request_type* get_request(const UUID& connection_id);
    int                 get_request_type(const UUID& connection_id);

    network::listener* create_listener_for(const UUID& connstion_id);
    network::session*  create_session_for(const UUID& connection_id);

    network::session*  remove_session(const UUID& connection_id);
    network::listener* remove_listener(const UUID& connection_id);

    network::listener* get_listener(const UUID& connection_id) const;
    network::session*  get_session(const UUID& connection_id) const;

private:
    executor_remote&         remote;
    object_manager&          obj_manager;
    domain_event_handler&    event_handler;
    network::session_factory session_factory;

    // session_id -> thor.lang.Domain mapping
    tbb::concurrent_hash_map<std::int64_t, proxies::x86::root_set_ptr<domain_type>> session_id_to_domain;

    // request objects created from listen() & connect()
    tbb::concurrent_hash_map<UUID, request_type> requests;

    // uuid -> listener, store connections created by listen()
    tbb::concurrent_hash_map<UUID, network::listener*> uuid_to_listener;

    // uuid -> session, store connections created by connect()
    tbb::concurrent_hash_map<UUID, network::session*> uuid_to_session;
};

} } }

#endif
