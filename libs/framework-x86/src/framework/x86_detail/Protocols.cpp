/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <utility>

#include "framework/x86_detail/Protocols.h"

namespace zillians { namespace framework { namespace x86_detail {

invocation_header_t::invocation_header_t() noexcept
    : function_id(-1)
    , session_id(-2)
    , valid(true)
    , depend_on_counter(nullptr)
    , be_depended_counter(nullptr)
    , store_to(nullptr)
    , done_notifier(nullptr)
{
}

invocation_header_t::invocation_header_t(invocation_header_t&& ref) noexcept
    : invocation_header_t()
{
    *this = std::move(ref);
}

invocation_header_t& invocation_header_t::operator=(invocation_header_t&& ref) noexcept
{
    function_id         = std::move(ref.function_id        );
    session_id          = std::move(ref.session_id         );
    valid               = std::move(ref.valid              );
    depend_on_counter   = std::move(ref.depend_on_counter  );
    be_depended_counter = std::move(ref.be_depended_counter);
    store_to            = std::move(ref.store_to           );
    done_notifier       = std::move(ref.done_notifier      );

    ref.valid               = false;
    ref.be_depended_counter = nullptr;
    ref.store_to            = nullptr;
    ref.done_notifier       = nullptr;

    return *this;
}

} } }
