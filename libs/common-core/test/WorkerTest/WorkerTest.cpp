/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include "core/Worker.h"
#include <iostream>
#include <string>
#include <limits>
#include <tbb/tick_count.h>

#define BOOST_TEST_MODULE WorkerTest
#define BOOST_TEST_MAIN
#include <boost/test/unit_test.hpp>

using namespace zillians;
using namespace std;

BOOST_AUTO_TEST_SUITE( WorkerTest )

void increment(int* value)
{
    ++(*value);
}

BOOST_AUTO_TEST_CASE( WorkerTestCase1 )
{
    Worker worker;

    int counter = 0;
    for(int i=0;i<5000;++i)
    {
        worker.post(boost::bind(increment, &counter));
    }
    worker.dispatch(boost::bind(increment, &counter), true);
    BOOST_CHECK(counter == 5001);
}

BOOST_AUTO_TEST_CASE( WorkerTestCase2 )
{
    Worker worker;

    int counter = 0;
    for(int i=0;i<5000;++i)
    {
        worker.dispatch(boost::bind(increment, &counter), false);
    }
    worker.dispatch(boost::bind(increment, &counter), true);
    BOOST_CHECK(counter == 5001);
}

BOOST_AUTO_TEST_CASE( WorkerTestCase3 )
{
    Worker worker;

    int counter = 0;
    for(int i=0;i<5000;++i)
    {
        worker.dispatch(boost::bind(increment, &counter), true);
    }
    BOOST_CHECK(counter == 5000);
}
//
//BOOST_AUTO_TEST_CASE( WorkerTestCase6 )
//{
//  if(!GlobalWorker::instance())
//      new GlobalWorker();
//
//  int counter = 0;
//  for(int i=0;i<5000;++i)
//  {
//      int key = GlobalWorker::instance()->async(boost::bind(increment, &counter));
//      GlobalWorker::instance()->wait(key);
//      BOOST_CHECK(counter == i+1);
//  }
//}
//
//BOOST_AUTO_TEST_CASE( WorkerTestCase7 )
//{
//  WorkerGroup group(2, 2, WorkerGroup::load_balancing_t::round_robin, 10);
//
//  int counter = 0;
//  for(int i=0;i<5000;++i)
//  {
//      int key = group.async(boost::bind(increment, &counter));
//      group.wait(key);
//      BOOST_CHECK(counter == i+1);
//  }
//}

BOOST_AUTO_TEST_SUITE_END()
