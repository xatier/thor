/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
/**
 * @date Oct 7, 2009 rocet - Initial version created.
 */

#include <iostream>

#include <log4cxx/logger.h>
#include <log4cxx/basicconfigurator.h>

#include <boost/thread/thread.hpp>

#include "threading/Dispatcher.h"
#include "threading/DispatcherThreadContext.h"
#include "threading/DispatcherDestination.h"

#define ITERATIONS  1024
#define READER_WRITER_THREAD_COUNT  32

using namespace zillians;
using namespace zillians::threading;
using std::cout;
using std::endl;

log4cxx::LoggerPtr mLogger(log4cxx::Logger::getLogger("DispatcherThreadTest"));

namespace {

struct Message
{
    int count;
};

}

void writer_thread(shared_ptr<DispatcherThreadContext<Message> > dt, uint32 destination)
{
    shared_ptr<DispatcherDestination<Message> > dest = dt->createDestination(destination);

    Message m;

    for(int i=0;i<ITERATIONS;++i)
    {
//      cout << "writer iteration: " << i << endl;
        m.count = i;
        dest->write(m);
    }
}

void reader_thread(shared_ptr<DispatcherThreadContext<Message> > dt, uint32 source, bool blocking)
{
    Message m[ITERATIONS];
    uint32  s[ITERATIONS];
    uint32 k = 0;
    uint32 n = ITERATIONS;
    uint32 totalMsg = 0;
    uint32 i = 0;
    while(totalMsg < ITERATIONS)
    {
//      cout << "reader iteration: " << i << endl;

        n = ITERATIONS;
        if(dt->read(s, m, n, blocking)) 
        {
            totalMsg += n;
            for(int j = 0; j < n; ++j)
            {
                BOOST_ASSERT(m[j].count == k);
                BOOST_ASSERT(s[j] == source);
                ++k;
            }
        }
        ++i;
    }
}

void writer_reader_thread(shared_ptr<DispatcherThreadContext<Message> > dt, std::vector<uint32> destinations)
{
    Message m; m.count = 1;
    for(std::vector<uint32>::iterator it = destinations.begin(); it != destinations.end(); ++it)
    {
        shared_ptr<DispatcherDestination<Message> > dest = dt->createDestination(*it);
        for(int i=0;i<ITERATIONS;++i)
        {
            dest->write(m);
        }
    }

    for(int i=0;i<destinations.size() * ITERATIONS;++i)
    {
        for(uint32 i=0;i<ITERATIONS;++i)
        {
            uint32 source;
            while(dt->read(source, m));

            BOOST_ASSERT(m.count == 1);
        }
    }
}

int main (int argc, char** argv)
{
    log4cxx::BasicConfigurator::configure();

    Dispatcher<Message> dispatcher(
        DispatcherOptions()
            .setMaxDispatcherThreads(63)
            .setPipeBoundSize(ITERATIONS));

    for(int i=0;i<10;++i)
    {
        shared_ptr<DispatcherThreadContext<Message> > reader_dt = dispatcher.createThreadContext();
        shared_ptr<DispatcherThreadContext<Message> > writer_dt = dispatcher.createThreadContext();

        boost::thread t0(boost::bind(reader_thread, reader_dt, writer_dt->getIdentity(), false));
        boost::thread t1(boost::bind(writer_thread, writer_dt, reader_dt->getIdentity()));

        t0.join();
        t1.join();

        cout << "iteration: " << i+1 << " of 10, non-blocking reading ok" << endl;
    }
    
    for(int i=0;i<10;++i)
    {
        shared_ptr<DispatcherThreadContext<Message> > reader_dt = dispatcher.createThreadContext();
        shared_ptr<DispatcherThreadContext<Message> > writer_dt = dispatcher.createThreadContext();

        boost::thread t0(boost::bind(reader_thread, reader_dt, writer_dt->getIdentity(), true));
        boost::thread t1(boost::bind(writer_thread, writer_dt, reader_dt->getIdentity()));

        t0.join();
        t1.join();

        cout << "iteration: " << i+1 << " of 10, blocking reading ok" << endl;
    }

    for(int i=0;i<10;++i)
    {
        cout << "iteration: " << i+1 << " of 10" << endl;
        std::vector<uint32> destinations;
        std::vector<shared_ptr<DispatcherThreadContext<Message> > > dts;
        for(int i=0;i<READER_WRITER_THREAD_COUNT;++i)
        {
            shared_ptr<DispatcherThreadContext<Message> > dt = dispatcher.createThreadContext();
            destinations.push_back(dt->getIdentity());
            dts.push_back(dt);
        }

        boost::thread** wr_threads = new boost::thread*[READER_WRITER_THREAD_COUNT];

        for(int i=0;i<READER_WRITER_THREAD_COUNT;++i)
        {
            wr_threads[i] = new boost::thread(boost::bind(writer_reader_thread, dts[i], destinations));
        }

        for(int i=0;i<READER_WRITER_THREAD_COUNT;++i)
        {
            wr_threads[i]->join();
        }

        delete[] wr_threads;
    }

    return 0;
}
