/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include "language/stage/generator/GeneratorDriverStage.h"

namespace zillians { namespace language { namespace stage {

GeneratorDriverStage::GeneratorDriverStage() : current_target(-1)
{
}

GeneratorDriverStage::~GeneratorDriverStage()
{ }

const char* GeneratorDriverStage::name()
{
    return "GeneratorDriverStage";
}

std::pair<shared_ptr<po::options_description>, shared_ptr<po::options_description>> GeneratorDriverStage::getOptions()
{
    shared_ptr<po::options_description> option_desc_public(new po::options_description());
    shared_ptr<po::options_description> option_desc_private(new po::options_description());

    option_desc_public->add_options();

    for(auto& option : option_desc_public->options()) option_desc_private->add(option);

    option_desc_private->add_options();

    return std::make_pair(option_desc_public, option_desc_private);
}

bool GeneratorDriverStage::parseOptions(po::variables_map& vm)
{
    return true;
}

bool GeneratorDriverStage::execute(bool& continue_execution)
{
    // advance to next generator configuration
	GeneratorContext& generator_context = getGeneratorContext();
	++generator_context.active_config_index;

	if(generator_context.active_config_index >= generator_context.target_config.size())
	{
		generator_context.active_config = NULL;
	}
	else
	{
		generator_context.active_config = generator_context.target_config[generator_context.active_config_index];
	}

    return true;
}

bool GeneratorDriverStage::hasProgress()
{
	return getGeneratorContext().active_config != NULL;
}


} } }
