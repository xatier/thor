/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <boost/range/algorithm_ext/push_back.hpp>
#include <boost/range/adaptors.hpp>

#include "utility/Foreach.h"
#include "utility/RangeUtil.h"
#include "utility/StringUtil.h"

#include "language/tree/ASTNodeHelper.h"
#include "language/tree/visitor/GenericDoubleVisitor.h"
#include "language/stage/generator/detail/LLVMHelper.h"
#include "language/stage/generator/visitor/LLVMGeneratorStagePreambleVisitor.h"

using namespace zillians::language::tree;
using zillians::language::tree::visitor::GenericDoubleVisitor;

namespace zillians { namespace language { namespace stage { namespace visitor {

LLVMGeneratorStagePreambleVisitor::LLVMGeneratorStagePreambleVisitor(llvm::LLVMContext& context, llvm::Module& module) :
    mContext(context), mModule(module), mBuilder(context), mHelper(context, module, mBuilder), mGlobalAddrSpace(0), mConstAddrSpace(0)
{
    REGISTER_ALL_VISITABLE_ASTNODE(generateInvoker);
}

void LLVMGeneratorStagePreambleVisitor::generate(ASTNode& node)
{
    revisit(node);
}

void LLVMGeneratorStagePreambleVisitor::generate(VariableDecl& node)
{
    if(!mHelper.isCurrentCodeGenerationTarget(&node))
        return;

    // handle only global variable
    revisit(node);
    if(node.isGlobal())
    {
        // global variable
        generateGlobalVariable(node, NameManglingContext::get(&node)->mangled_name);
    }
    else if(node.is_static)
    {
        if(isa<ClassDecl>(node.parent))
        {
            // static member variable
            generateGlobalVariable(node, StaticVariableManglingContext::get(&node)->mangled_name);
        }
        else
        {
            // static local variable
            generateStaticVariable(node);
        }
    }
}

void LLVMGeneratorStagePreambleVisitor::generate(Tangle& node)
{
    if(getGeneratorContext().active_config->arch.is_cuda())
    {
        mFunctionAddrSpace = NVPTX_AS_GENERIC;
        mGlobalAddrSpace = NVPTX_AS_GLOBAL;
        mConstAddrSpace = NVPTX_AS_CONST; //NVPTX_AS_CONST_NOT_GEN;
//    		mFunctionAddrSpace = 0;
//    		mGlobalAddrSpace = 0;
//    		mConstAddrSpace = 0;
    }
    else
    {
        mFunctionAddrSpace = 0;
        mGlobalAddrSpace = 0;
        mConstAddrSpace = 0;
    }

    const unsigned&      address_space      = mGlobalAddrSpace;
              auto*const llvm_type          = mHelper.getType(PrimitiveKind::INT64_TYPE);
              auto*const llvm_constant_init = mHelper.getTypeInitializer(PrimitiveKind::INT64_TYPE);
    constexpr auto       linkage_type       = llvm::GlobalValue::WeakAnyLinkage; // FIXME these variables should be defined exactly once, do not use weak any linkage

    new llvm::GlobalVariable(
        mModule,
        llvm_type,
        false,
        linkage_type,
        llvm_constant_init,
        mHelper.getGlobalOffsetNameOfType(node.getId()),
        NULL /*insert_before*/, llvm::GlobalVariable::NotThreadLocal,
        address_space
    );

    new llvm::GlobalVariable(
        mModule,
        llvm_type,
        false,
        linkage_type,
        llvm_constant_init,
        mHelper.getGlobalOffsetNameOfFunction(node.getId()),
        NULL /*insert_before*/, llvm::GlobalVariable::NotThreadLocal,
        address_space
    );

    new llvm::GlobalVariable(
        mModule,
        llvm_type,
        false,
        linkage_type,
        llvm_constant_init,
        mHelper.getGlobalOffsetNameOfSymbol(node.getId()),
        NULL /*insert_before*/, llvm::GlobalVariable::NotThreadLocal,
        address_space
    );

    revisit(node);
}

void LLVMGeneratorStagePreambleVisitor::generate(ClassDecl& node)
{
    // we don't generate code for non-fully-specialized classes
    if(isa<TemplatedIdentifier>(node.name) && !cast<TemplatedIdentifier>(node.name)->isFullySpecialized())
        return;

    if(!LLVMHelper::isCurrentCodeGenerationTarget(&node))
        return;

    // TODO do we need to generate llvm struct type even for interface?
    visit(*node.name);

    llvm::StructType* llvm_struct_type = mHelper.getStructType(node);
    if(llvm_struct_type == nullptr)
    {
        BOOST_ASSERT(false && "failed to generate LLVM struct type");
        terminateRevisit();
        return;
    }

    revisit(node);

    generateVTable(node);
}

void LLVMGeneratorStagePreambleVisitor::generateGlobalVariable(VariableDecl& var, const std::string& mangled_name)
{
    // generate global value for all global variables
    // note that as we collect all user-defined global variables into a global struct (using the "global" literal to access them)
    // the only variable declarations that are still sitting directly under package scope are those implicit global variables such as function id or symbol id
    // so we need to generate a symbol for it
    if (llvm::Value* value = GET_SYNTHESIZED_LLVM_VALUE(&var)) return;

    llvm::Constant* llvm_constant_init = NULL;
    llvm::GlobalValue::LinkageTypes linkage_type = llvm::GlobalValue::ExternalLinkage;
    if ( !var.hasAnnotation(L"native") )
    {
        linkage_type       = llvm::GlobalValue::CommonLinkage;
        llvm_constant_init = mHelper.getTypeInitializer(*var.type);
    }

    llvm::Type* llvm_type = mHelper.getType(*var.type);

#if LLVM_VERSION_MAJOR >= 3 && LLVM_VERSION_MINOR >= 2
    llvm::GlobalVariable* global_variable = new llvm::GlobalVariable(
            mModule, llvm_type, false,
            linkage_type, llvm_constant_init,
            mangled_name,
            NULL /*insert_before*/, llvm::GlobalVariable::NotThreadLocal /*no_tls*/,
            mGlobalAddrSpace);
#else
    llvm::GlobalVariable* global_variable = new llvm::GlobalVariable(
            mModule, llvm_type, false,
            linkage_type, llvm_constant_init,
            mangled_name,
            NULL /*insert_before*/, false /*no_tls*/,
            mGlobalAddrSpace);
#endif
    SET_SYNTHESIZED_LLVM_VALUE(&var, global_variable);
}

void LLVMGeneratorStagePreambleVisitor::generateStaticVariable(VariableDecl& var)
{
    if(GET_SYNTHESIZED_LLVM_VALUE(&var)) return;

    BOOST_ASSERT(StaticVariableManglingContext::get(&var) && "No static variable mangling context!");

    if(!LLVMHelper::isCurrentCodeGenerationTarget(&var))
        return;

    llvm::GlobalValue::LinkageTypes linkage_type = llvm::GlobalValue::InternalLinkage;

    // static variable
    auto*const llvm_type          = mHelper.getType(*var.type);
    auto*const llvm_constant_init = mHelper.getTypeInitializer(*var.type);

#if LLVM_VERSION_MAJOR >= 3 && LLVM_VERSION_MINOR >= 2
    llvm::GlobalVariable* static_variable = new llvm::GlobalVariable(
            mModule,
            llvm_type,
            false,
            linkage_type,
            llvm_constant_init,
            StaticVariableManglingContext::get(&var)->mangled_name,
            NULL /*insert_before*/, llvm::GlobalVariable::NotThreadLocal /*no_tls*/,
            mGlobalAddrSpace);
#else
    llvm::GlobalVariable* static_variable = new llvm::GlobalVariable(
            mModule,
            llvm_type,
            false,
            linkage_type,
            llvm_constant_init,
            StaticVariableManglingContext::get(&var)->mangled_name,
            NULL /*insert_before*/, false /*no_tls*/,
            mGlobalAddrSpace);
#endif
        SET_SYNTHESIZED_LLVM_VALUE(&var, static_variable);

    // guard variable
#if LLVM_VERSION_MAJOR >= 3 && LLVM_VERSION_MINOR >= 2
    llvm::GlobalVariable* guard_variable = new llvm::GlobalVariable(
            mModule,
            llvm::Type::getInt64Ty(mContext),
            false,
            linkage_type,
            llvm::ConstantInt::get(llvm::IntegerType::get(mContext, 64), 0, true),
            StaticVariableManglingContext::get(&var)->mangled_guard_name,
            NULL /*insert_before*/, llvm::GlobalVariable::NotThreadLocal /*no_tls*/,
            mGlobalAddrSpace);
#else
    llvm::GlobalVariable* guard_variable = new llvm::GlobalVariable(
            mModule,
            llvm::Type::getInt64Ty(mContext),
            false,
            linkage_type,
            llvm::ConstantInt::get(llvm::IntegerType::get(mContext, 64), 0, true),
            StaticVariableManglingContext::get(&var)->mangled_guard_name,
            NULL /*insert_before*/, false /*no_tls*/,
            mGlobalAddrSpace);
#endif
        SET_SYNTHESIZED_LLVM_GUARD_VALUE(&var, guard_variable);
}

std::string LLVMGeneratorStagePreambleVisitor::getThunkName(const FunctionDecl& derived_func, std::int64_t class_offset, std::int64_t return_offset)
{
    // Mangling rule: Note that we don't consider virtual inheritance
    // 1. Covariant return type: _ZTch + (n) + offset_in_bytes + _ + h + return_offset_in_bytes + _ + function mangling name
    // 2. Non-covariant return type:Following Itanium C++ ABI. The name is _ZTh + (n) + offset_in_bytes + _ + function mangling name
    //
    // ps. If the return_offset_in_bytes is zero, it is considered as case (2).

    auto function_mangled_name = NameManglingContext::get(&derived_func)->mangled_name;
    function_mangled_name.erase(1, 1); // "_Z" -> "_"

    const auto& is_covariance = return_offset != 0;

    std::string thunk_name;

    if (is_covariance)
        thunk_name = "_ZTchn" + std::to_string(class_offset) + "_h" + std::to_string(return_offset) + std::move(function_mangled_name);
    else
        thunk_name = "_ZThn"  + std::to_string(class_offset) +                                        std::move(function_mangled_name);

    return thunk_name;
}

void LLVMGeneratorStagePreambleVisitor::generateVTable(ClassDecl& node)
{
    using boost::adaptors::transformed;

    if (!node.hasVirtual())
        return;

    BOOST_ASSERT(GET_SYNTHESIZED_VTABLE(&node) == nullptr && "generate virtual table more than once");

    const auto& segment_infos = node.generateVirtualTableInfo();

    BOOST_ASSERT(!segment_infos.empty() && "generating virtual table for class contains no virtuals");

    auto*const llvm_i8     = llvm::Type::getInt8Ty(mContext);
    auto*const llvm_i64    = llvm::Type::getInt64Ty(mContext);
    auto*const llvm_ptr_i8 = llvm_i8->getPointerTo();

    std::vector<llvm::Constant*> entries;
    std::vector<uint32>          subtable_starts;

    for (const auto& segment_info : segment_infos)
        generateVTableSegment(node, segment_info, entries, subtable_starts);

    // By following the C++ ABI, we need to replace the class mangled name with _ZTV which is for virtual table
    std::string vt_name = NameManglingContext::get(&node)->mangled_name;
    vt_name.insert(2, "TV"); // "_Z" -> "_ZTV"

    auto*const array_type = llvm::ArrayType::get(llvm_ptr_i8, entries.size());
    auto*const table      = new llvm::GlobalVariable(
                                mModule,
                                array_type,
                                true,
                                llvm::GlobalValue::WeakAnyLinkage,
                                llvm::ConstantArray::get(array_type, entries),
                                vt_name,
                                nullptr /* insert_before */,
                                llvm::GlobalVariable::NotThreadLocal /* not TLS */,
                                mGlobalAddrSpace
                            );

    SET_SYNTHESIZED_VTABLE(&node, table, std::move(subtable_starts));
}

void LLVMGeneratorStagePreambleVisitor::generateVTableSegment(const ClassDecl& node, const virtual_table::segment_info_type& segment_info, std::vector<llvm::Constant*>& entries, std::vector<uint32>& subtable_starts)
{
    using boost::adaptors::transformed;

    NOT_NULL(GET_SYNTHESIZED_OBJECTLAYOUT(&node));

    const auto*const object_layout = GET_SYNTHESIZED_OBJECTLAYOUT(&node);

    NOT_NULL(object_layout);

    const auto& class_offsets = object_layout->class_offset;

    BOOST_ASSERT(class_offsets.count(segment_info.source_class) > 0 && "base class layout did not present in object layout!?");

    const auto& class_offset = class_offsets.find(segment_info.source_class)->second.second;

    auto*const  llvm_i8      = llvm::Type::getInt8Ty(mContext);
    auto*const  llvm_i64     = llvm::Type::getInt64Ty(mContext);
    auto*const  llvm_ptr_i8  = llvm_i8->getPointerTo();

    // TODO implement type info (RTTI) ?
    auto*const value_offset_to_top = llvm::ConstantExpr::getIntToPtr(llvm::ConstantInt::getSigned(llvm_i64, -1 * class_offset), llvm_ptr_i8);
    auto*const rtti_ptr            = llvm::ConstantPointerNull::get(llvm_ptr_i8); // RTTI

    entries.insert(entries.end(), {value_offset_to_top, rtti_ptr});
    subtable_starts.emplace_back(static_cast<uint32>(entries.size()));

    boost::push_back(
        entries,
          segment_info.entry_infos
        | transformed(std::bind(&LLVMGeneratorStagePreambleVisitor::generateVTableEntry, this, class_offset, std::placeholders::_1))
        | transformed(std::bind(&llvm::ConstantExpr::getBitCast, std::placeholders::_1, llvm_ptr_i8))
    );
}

llvm::Constant* LLVMGeneratorStagePreambleVisitor::generateVTableEntry(std::int64_t class_offset, const virtual_table::entry_info_type& entry_info)
{
    auto*const origin_decl = const_cast<tree::FunctionDecl*>(entry_info.origin_decl);
    auto*const   impl_decl = const_cast<tree::FunctionDecl*>(entry_info.  impl_decl);

    if (impl_decl == nullptr)
        return getOrInsertPureVirtual();

    if (origin_decl == nullptr)
    {
        // 1. origin_decl == nullptr => in main segment => offset is zero

        BOOST_ASSERT(class_offset == 0 && "we should generate thunk for ");

        return getOrInsertFunction(*impl_decl);
    }

    NOT_NULL(origin_decl->type);
    NOT_NULL(  impl_decl->type);

    const auto*const origin_return_type = origin_decl->type->getCanonicalType();
    const auto*const   impl_return_type =   impl_decl->type->getCanonicalType();

    BOOST_ASSERT(origin_return_type != nullptr && "canonical type is not ready!");
    BOOST_ASSERT(  impl_return_type != nullptr && "canonical type is not ready!");

    std::int64_t     return_offset            = 0;
    const auto*const origin_return_class_decl = origin_return_type->getAsClassDecl();
    const auto*const   impl_return_class_decl =   impl_return_type->getAsClassDecl();

    BOOST_ASSERT(
        (
            (origin_return_class_decl == nullptr && impl_return_class_decl == nullptr) ||
            (origin_return_class_decl != nullptr && impl_return_class_decl != nullptr)
        ) && "invalid override of return type for virtual functions"
    );

    if (origin_return_class_decl != nullptr && origin_return_class_decl != impl_return_class_decl)
    {
        NOT_NULL(GET_SYNTHESIZED_OBJECTLAYOUT(impl_return_class_decl));

        const auto& return_class_offsets = GET_SYNTHESIZED_OBJECTLAYOUT(impl_return_class_decl)->class_offset;

        BOOST_ASSERT(return_class_offsets.count(origin_return_class_decl) > 0 && "invalid covariance");

        return_offset = return_class_offsets.find(origin_return_class_decl)->second.second;
    }

    if (class_offset == 0 && return_offset == 0)
        return getOrInsertFunction(*impl_decl);

    return getOrInsertThunk(class_offset, return_offset, *origin_decl, *impl_decl);
}

llvm::Constant* LLVMGeneratorStagePreambleVisitor::getOrInsertPureVirtual()
{
    if(getGeneratorContext().active_config->arch.is_cuda())
    {
        auto*const llvm_ptr_i8 = llvm::Type::getInt8PtrTy(mContext);
        return llvm::cast<llvm::Constant>(llvm::ConstantPointerNull::get(llvm_ptr_i8));
    }
    else
    {
        auto*const llvm_void          = llvm::Type::getVoidTy(mContext);
        auto*const llvm_function_type = llvm::FunctionType::get(llvm_void, false /* not variadic */);
        return llvm::cast<llvm::Constant>(mModule.getOrInsertFunction("__cxa_pure_virtual", llvm_function_type));
    }
}

llvm::Function* LLVMGeneratorStagePreambleVisitor::getOrInsertFunction(const tree::FunctionDecl& decl)
{
    auto*const llvm_function = GET_SYNTHESIZED_LLVM_FUNCTION(&decl);

    BOOST_ASSERT(
        llvm_function &&
        "acquiring virtual function (non-pure) but no one, "
        "please check 'decl' is not pure-virtual or function preamble visitor is working correctly for it"
    );

    return llvm_function;
}

llvm::Function* LLVMGeneratorStagePreambleVisitor::getOrInsertThunk(std::int64_t class_offset, std::int64_t return_offset, tree::FunctionDecl& origin_decl, tree::FunctionDecl& impl_decl)
{
    using boost::adaptors::transformed;

    BOOST_ASSERT((class_offset != 0 || return_offset != 0) && "generating thunk for virtual function not requires one");

    auto*const origin_cls = origin_decl.getOwner<tree::ClassDecl>();
    auto*const   impl_cls =   impl_decl.getOwner<tree::ClassDecl>();

    BOOST_ASSERT(origin_cls != nullptr && "generate thunk function for global function!?");
    BOOST_ASSERT(  impl_cls != nullptr && "generate thunk function for global function!?");

    if (impl_decl.name->toString() == L"get")
    {
        int i = 0;
    }

    const auto& is_covariance = return_offset != 0;
    const auto& thunk_name    = getThunkName(impl_decl, class_offset, return_offset);

    auto*const llvm_struct_type   = mHelper.getStructType(*impl_cls);
    auto*const llvm_function_type = mHelper.getFunctionType(origin_decl);

    auto*const llvm_thunk_function = llvm::cast<llvm::Function>(mModule.getOrInsertFunction(thunk_name, llvm_function_type));

    if (!llvm_thunk_function->empty())
        return llvm_thunk_function;

    llvm_thunk_function->setLinkage(llvm::GlobalValue::WeakAnyLinkage);

   // Implement function body
    auto*const block = llvm::BasicBlock::Create(mContext, "entry", llvm_thunk_function);
    mBuilder.SetInsertPoint(block);

    BOOST_ASSERT(!llvm_thunk_function->arg_empty() && "'this' pointer should be there");

    const auto& origin_args = boost::make_iterator_range(llvm_thunk_function->arg_begin(), llvm_thunk_function->arg_end())
                            | transformed(&boost::addressof<llvm::Value>)
                            ;

    std::vector<llvm::Value*> arguments(origin_args.begin(), origin_args.end());

    {
        // Adjust this pointer offset
        auto*const adjusted_pointer = mHelper.adjustPointer(arguments.front(), origin_cls, impl_cls);
        auto*const casted_pointer   = mBuilder.CreateBitCast(adjusted_pointer, llvm_struct_type->getPointerTo());

        arguments.front() = casted_pointer;
    }

    auto*const   llvm_derived_function = GET_SYNTHESIZED_LLVM_FUNCTION(&impl_decl);
    llvm::Value* result                = mBuilder.CreateCall(llvm_derived_function, llvm::ArrayRef<llvm::Value*>(arguments));

    {
        const auto&  impl_arg_range = boost::make_iterator_range(llvm_derived_function->arg_begin(), llvm_derived_function->arg_end());
        const auto& thunk_arg_range = boost::make_iterator_range(llvm_thunk_function->arg_begin(), llvm_thunk_function->arg_end());

        for (const auto& tuple : make_zip_range(impl_arg_range, thunk_arg_range))
        {
            auto&  impl_arg = tuple.get<0>();
            auto& thunk_arg = tuple.get<1>();

            thunk_arg.setName(impl_arg.getName());
        }
    }

    // Generate return type
    if (llvm_function_type->getReturnType()->isVoidTy())
    {
        mBuilder.CreateRetVoid();
        return llvm_thunk_function;
    }

    if (!is_covariance)
    {
        mBuilder.CreateRet(result);
        return llvm_thunk_function;
    }

    // Special handle covariant return type
    auto eval_if_null_block =
        llvm::BasicBlock::Create(mContext,
                                 "null-thunking.eval_if_null_block",
                                 llvm_thunk_function);
    auto null_block =
        llvm::BasicBlock::Create(mContext,
                                 "null-thunking.null_block",
                                 llvm_thunk_function);
    auto nonnull_block =
        llvm::BasicBlock::Create(mContext,
                                 "null-thunking.nonnull_block",
                                 llvm_thunk_function);
    auto final_block =
        llvm::BasicBlock::Create(mContext,
                                 "null-thunking.final_block",
                                 llvm_thunk_function);

    mBuilder.CreateBr(eval_if_null_block);
    mBuilder.SetInsertPoint(eval_if_null_block);
    mBuilder.CreateCondBr(mBuilder.CreateIsNull(result),
                          null_block,
                          nonnull_block);

    // Get the return type classes
    auto*const origin_return_type = origin_decl.type->getCanonicalType()->getAsClassDecl();
    auto*const   impl_return_type =   impl_decl.type->getCanonicalType()->getAsClassDecl();

    llvm::Value* null_value = nullptr;
    mBuilder.SetInsertPoint(null_block);
    {
        auto origin_struct_type =
            mHelper.getStructType(*origin_return_type);
        null_value = llvm::ConstantPointerNull::get(origin_struct_type->getPointerTo());
        mBuilder.CreateBr(final_block);
    }

    llvm::Value* nonnull_value = nullptr;
    mBuilder.SetInsertPoint(nonnull_block);
    {
        // So, we need to adjust the pointer before return
        nonnull_value = mHelper.adjustPointer(result, impl_return_type, origin_return_type);

        mBuilder.CreateBr(final_block);
    }

    mBuilder.SetInsertPoint(final_block);
    {
        llvm::PHINode* phi = mBuilder.CreatePHI(null_value->getType(), 2);
        phi->addIncoming(null_value, null_block);
        phi->addIncoming(nonnull_value, nonnull_block);
        mBuilder.CreateRet(phi);
    }

    return llvm_thunk_function;
}

} } } }

