<?xml version="1.0"?>

<!--
This file is part of Thor.
Thor is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License, version 3,
as published by the Free Software Foundation.

Thor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Thor.  If not, see <http://www.gnu.org/licenses/>.

If you want to develop any commercial services or closed-source products with
Thor, to adapt sources of Thor in your own projects without
disclosing sources, purchasing a commercial license is mandatory.

For more information, please contact Zillians, Inc.
<thor@zillians.com>
-->

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="xml" indent="yes"/>

<xsl:template match="node()|@*">
  <xsl:copy>
    <xsl:apply-templates select="node()|@*"/>
  </xsl:copy>
</xsl:template>

<xsl:template match="parameter">
  <xsl:text>$</xsl:text>
  <xsl:value-of select="@name"/>
  <xsl:text>$</xsl:text>
</xsl:template>

<xsl:template match="table">
  <table>
    <xsl:copy-of select="@*"/>

    <parameters>
      <xsl:for-each select="message/translation/parameter">
        <xsl:sort select="@name"/>

        <xsl:copy-of select="."/>
      </xsl:for-each>
    </parameters>

    <messages>
      <xsl:for-each select="message">
        <xsl:sort select="@name"/>

        <message>
          <xsl:copy-of select="@*"/>

          <parameters>
            <xsl:for-each select="translation/parameter">
              <xsl:sort select="@name"/>

              <xsl:copy-of select="."/>
            </xsl:for-each>
          </parameters>

          <translations>
            <xsl:apply-templates/>
          </translations>
        </message>
      </xsl:for-each>
    </messages>

  </table>
</xsl:template>

</xsl:stylesheet>
