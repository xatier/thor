/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include "language/context/ManglingStageContext.h"
#include "language/tree/ASTNode.h"
#include "language/tree/ASTNodeFactory.h"
#include "language/tree/ASTNodeHelper.h"
#include "../ASTNodeSamples.h"

#include "language/stage/parser/context/SourceInfoContext.h"

#include <iostream>
#include <string>
#include <limits>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>

#define BOOST_TEST_MODULE ThorTreeTest_TreeCloneTest
#define BOOST_TEST_MAIN
#include <boost/test/unit_test.hpp>

using namespace zillians;
using namespace zillians::language::tree;
using namespace zillians::language::tree::visitor;
using namespace zillians::language::stage;

BOOST_AUTO_TEST_SUITE( ThorTreeTest_TreeCloneTestSuite )

BOOST_AUTO_TEST_CASE( ThorTreeTest_TreeCloneTestCase1 )
{
    SimpleIdentifier* original_id = new SimpleIdentifier(L"hello");

    SourceInfoContext* original_src_ctx = new SourceInfoContext(123, 456);
    original_id->set<SourceInfoContext>(original_src_ctx);

    NameManglingContext* original_name_ctx = new NameManglingContext("hello");
    original_id->set<NameManglingContext>(original_name_ctx);

    SimpleIdentifier* cloned_id = cast<SimpleIdentifier>(ASTNodeHelper::clone(original_id));

    BOOST_CHECK(cloned_id != NULL);

    SourceInfoContext* cloned_src_ctx = cloned_id->get<SourceInfoContext>();
    NameManglingContext* cloned_name_ctx = cloned_id->get<NameManglingContext>();

    BOOST_CHECK(cloned_src_ctx != NULL);

    if(cloned_src_ctx)
    {
        BOOST_CHECK(cloned_src_ctx->line == original_src_ctx->line);
        BOOST_CHECK(cloned_src_ctx->column == original_src_ctx->column);
    }

    BOOST_CHECK(cloned_name_ctx != NULL);

    if(cloned_name_ctx)
    {
        BOOST_CHECK(cloned_name_ctx->mangled_name == original_name_ctx->mangled_name);
    }

}

BOOST_AUTO_TEST_CASE( ThorTreeTest_TreeCloneTestCaseRecursive )
{
    // simple id 1
    SimpleIdentifier* original_sid1 = new SimpleIdentifier(L"a");
    SourceInfoContext* original_src_ctx_s1 = new SourceInfoContext(123, 456);
    original_sid1->set<SourceInfoContext>(original_src_ctx_s1);

    // simple id 2
    SimpleIdentifier* original_sid2 = new SimpleIdentifier(L"b");
    SourceInfoContext* original_src_ctx_s2 = new SourceInfoContext(123, 458);
    original_sid2->set<SourceInfoContext>(original_src_ctx_s2);

    // nested id
    NestedIdentifier* original_nid = new NestedIdentifier();
    SourceInfoContext* original_src_ctx_n  = new SourceInfoContext(123, 454);
    original_nid->set<SourceInfoContext>(original_src_ctx_n);
    original_nid->appendIdentifier(original_sid1);
    original_nid->appendIdentifier(original_sid2);
    NameManglingContext* original_name_ctx_n = new NameManglingContext("_ZN1a1bE");
    NameManglingContext::set(original_nid, original_name_ctx_n);

    // clone it!
    NestedIdentifier* cloned_nid = cast<NestedIdentifier>(ASTNodeHelper::clone(original_nid));
    BOOST_CHECK(cloned_nid != NULL);

    // check nested id context
    SourceInfoContext* cloned_src_ctx_n = cloned_nid->get<SourceInfoContext>();
    NameManglingContext* cloned_name_ctx = cloned_nid->get<NameManglingContext>();
    BOOST_CHECK_EQUAL(cloned_src_ctx_n->line        , original_src_ctx_n->line);
    BOOST_CHECK_EQUAL(cloned_src_ctx_n->column      , original_src_ctx_n->column);
    BOOST_CHECK_EQUAL(cloned_name_ctx->mangled_name , original_name_ctx_n->mangled_name);

    // check simple id context
    BOOST_CHECK_EQUAL(SourceInfoContext::get(cloned_nid->identifier_list[0])->line  , SourceInfoContext::get(original_sid1)->line);
    BOOST_CHECK_EQUAL(SourceInfoContext::get(cloned_nid->identifier_list[0])->column, SourceInfoContext::get(original_sid1)->column);
    BOOST_CHECK_EQUAL(SourceInfoContext::get(cloned_nid->identifier_list[1])->line  , SourceInfoContext::get(original_sid2)->line);
    BOOST_CHECK_EQUAL(SourceInfoContext::get(cloned_nid->identifier_list[1])->column, SourceInfoContext::get(original_sid2)->column);
}

BOOST_AUTO_TEST_SUITE_END()
