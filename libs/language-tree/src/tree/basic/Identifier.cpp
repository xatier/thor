/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <boost/algorithm/string/join.hpp>
#include <boost/range/adaptor/transformed.hpp>

#include <initializer_list>
#include <functional>
#include <string>

#include "utility/UUIDUtil.h"
#include "utility/UnicodeUtil.h"

#include "language/context/ResolverContext.h"
#include "language/tree/basic/Identifier.h"
#include "language/tree/basic/TypeSpecifier.h"
#include "language/tree/declaration/TypenameDecl.h"
#include "language/tree/ASTNodeHelper.h"

namespace zillians { namespace language { namespace tree {

Identifier::Identifier()
    : resolved_type(nullptr)
{
}

Type* Identifier::getCanonicalType() const
{
    if(Type* resolved = ResolvedType::get(this))
    {
        return resolved->getCanonicalType();
    }
    else
    {
        return nullptr;
    }
}

SimpleIdentifier::SimpleIdentifier()
{ }

SimpleIdentifier::SimpleIdentifier(const std::wstring& s) : name(s)
{ }

std::wstring SimpleIdentifier::toString() const
{
    return name;
}

bool SimpleIdentifier::isEmpty() const
{
    return (name.length() == 0);
}

bool SimpleIdentifier::isEqualImpl(const ASTNode& rhs) const
{
    AST_NODE_IS_EQUAL(
        (name)
    );
}

bool SimpleIdentifier::replaceUseWith(const ASTNode& from, const ASTNode& to, bool update_parent)
{
    AST_NODE_REPLACE(
        result,
        (name)
    );
}

SimpleIdentifier* SimpleIdentifier::clone() const
{
    return new SimpleIdentifier(name);
}

std::wostream& SimpleIdentifier::toSource(std::wostream& output, unsigned indent/* = 0*/) const
{
    return output << name;
}

NestedIdentifier::NestedIdentifier()
{ }

NestedIdentifier::NestedIdentifier(const std::initializer_list<Identifier*>& id_list)
{
    for(auto e : id_list)
    {
        appendIdentifier(e);
    }
}

std::wstring NestedIdentifier::toString() const
{
    return boost::algorithm::join(
        identifier_list | boost::adaptors::transformed(std::mem_fn(&Identifier::toString)),
        std::wstring(L".")
    );
}

bool NestedIdentifier::isEmpty() const
{
    for(auto* identifier : identifier_list)
    {
        if(!identifier->isEmpty())
            return false;
    }

    return true;
}

void NestedIdentifier::appendIdentifier(Identifier* id)
{
    id->parent = this;
    identifier_list.push_back(id);
}

bool NestedIdentifier::isEqualImpl(const ASTNode& rhs) const
{
    AST_NODE_IS_EQUAL(
        (identifier_list)
    );
}

bool NestedIdentifier::replaceUseWith(const ASTNode& from, const ASTNode& to, bool update_parent)
{
    AST_NODE_REPLACE(
        result,
        (identifier_list)
    );
}

NestedIdentifier* NestedIdentifier::clone() const
{
    NestedIdentifier* cloned = new NestedIdentifier();
    for(auto* identifier : identifier_list) cloned->appendIdentifier(cast<Identifier>(identifier->clone()));
    return cloned;
}

std::wostream& NestedIdentifier::toSource(std::wostream& output, unsigned indent/* = 0*/) const
{
    BOOST_ASSERT(!identifier_list.empty() && "empty nested identifer");

          auto i    = identifier_list.cbegin();
    const auto iend = identifier_list.cend();

    output << out_source(**i, indent);

    while (++i != iend)
        output << L'.' << out_source(**i, indent);

    return output;
}

NestedIdentifier* NestedIdentifier::create(std::initializer_list<Identifier*> new_identifier_list)
{
    auto* new_nid = new NestedIdentifier();

    for(Identifier* identifier: new_identifier_list)
    {
        BOOST_ASSERT(identifier && "null pointer exception");

        new_nid->appendIdentifier(identifier);
    }

    return new_nid;
}

TemplatedIdentifier* Identifier::getTemplateId()
{
    if(TemplatedIdentifier* tid = cast<TemplatedIdentifier>(this))
        return tid;

    if(NestedIdentifier* nestId = cast<NestedIdentifier>(this))
    {
        Identifier* lastId = nestId->identifier_list[nestId->identifier_list.size()-1];
        if(TemplatedIdentifier* tid = cast<TemplatedIdentifier>(lastId))
            return tid;
    }
    return NULL;
}

SimpleIdentifier* Identifier::getSimpleId()
{
    if(NestedIdentifier* nested_id = cast<NestedIdentifier>(this))
    {
        BOOST_ASSERT(!nested_id->identifier_list.empty() && "nested identifier with empty identifier list!");

        return nested_id->identifier_list.back()->getSimpleId();
    }
    else if(TemplatedIdentifier* template_id = cast<TemplatedIdentifier>(this))
    {
        return template_id->id->getSimpleId();
    }
    else if(SimpleIdentifier* simple_id = cast<SimpleIdentifier>(this))
    {
        return simple_id;
    }
    else
    {
        UNREACHABLE_CODE();

        return NULL;
    }
}

SimpleIdentifier* Identifier::randomId(const std::wstring& prefix/* = std::wstring()*/)
{
    const std::wstring& name = prefix + s_to_ws(UUID::random().toString('_'));

    return new SimpleIdentifier(name);
}

TemplatedIdentifier::TemplatedIdentifier()
{ }

TemplatedIdentifier::TemplatedIdentifier(Usage::type type, SimpleIdentifier* id) : type(type), id(id)
{
    id->parent = this;
}

TemplatedIdentifier::TemplatedIdentifier(SimpleIdentifier* id, const std::initializer_list<TypeSpecifier*>& templated_types) : type(Usage::ACTUAL_ARGUMENT), id(id)
{
    id->parent = this;
    for(auto e : templated_types)
    {
        append(new TypenameDecl(new SimpleIdentifier(L"_"), e));
    }
}

std::wstring TemplatedIdentifier::toString() const
{
    std::wstring t;

    t += id->toString();

    t += L"<";
    zillians::for_each_and_pitch(
        templated_type_list,
        [&t](decltype(templated_type_list)::const_reference type) {
            t += type->name->toString();

            if(type->specialized_type)
                t += L":" + type->specialized_type->toString();
        },
        [&t](){ t += L","; }
    );
    t += L">";

    return t;
}

bool TemplatedIdentifier::isVariadic() const
{
    auto last = *templated_type_list.rbegin();
    return last->name->toString() == L"...";
}

bool TemplatedIdentifier::isFullySpecialized() const
{
    for(auto* templated : templated_type_list)
    {
        if(!templated->specialized_type)
        {
            return false;
        }
        else
        {
            if(const auto*const name_specifier = cast<NamedSpecifier>(templated->specialized_type))
            {
                if(const auto*const name_specified_templated = name_specifier->getName()->getTemplateId())
                {
                    if(!name_specified_templated->isFullySpecialized())
                    {
                        return false;
                    }
                }
            }
        }
    }
    return true;
}

void TemplatedIdentifier::specialize()
{
    if(type == Usage::FORMAL_PARAMETER)
    {
        for(auto* templated : templated_type_list)
        {
            if(!templated->specialized_type)
            {
                TypeSpecifier* specialized_type = NULL;
                if(isa<TemplatedIdentifier>(templated->name))
                {
                    TemplatedIdentifier* duplicated_id = cast<TemplatedIdentifier>(templated->name->clone());
                    duplicated_id->specialize();
                    specialized_type = new NamedSpecifier(duplicated_id);
                    specialized_type->parent = templated;
                }
                else
                {
                    specialized_type = new NamedSpecifier(ASTNodeHelper::clone(templated->name));
                    specialized_type->parent = templated;
                }

                SimpleIdentifier* dummy_identifier = new SimpleIdentifier(L"_");
                templated->replaceUseWith(*(templated->name), *dummy_identifier);
                templated->specialized_type = specialized_type;
            }
        }
    }
}

bool TemplatedIdentifier::isEmpty() const
{
    if(id->isEmpty() && templated_type_list.size() == 0)
        return true;
    else
        return false;
}

void TemplatedIdentifier::setIdentifier(SimpleIdentifier* identifier)
{
    if(id) id->parent = NULL;
    id = identifier;
    id->parent = this;
}

void TemplatedIdentifier::append(TypenameDecl* type)
{
    type->parent = this;
    templated_type_list.push_back(type);
}

bool TemplatedIdentifier::isEqualImpl(const ASTNode& rhs) const
{
    AST_NODE_IS_EQUAL(
        (type               )
        (id                 )
        (templated_type_list)
    );
}

bool TemplatedIdentifier::replaceUseWith(const ASTNode& from, const ASTNode& to, bool update_parent)
{
    AST_NODE_REPLACE(
        result,
        (type               )
        (id                 )
        (templated_type_list)
    );
}

TemplatedIdentifier* TemplatedIdentifier::clone() const
{
    TemplatedIdentifier* cloned = new TemplatedIdentifier(type, clone_or_null(id));

    for(auto* templated : templated_type_list)
        cloned->append(templated->clone());

    return cloned;
}

std::wostream& TemplatedIdentifier::toSource(std::wostream& output, unsigned indent/* = 0*/) const
{
    BOOST_ASSERT(id && "null pointer exception");

    output << out_source(*id, indent) << L'<';

    if (!templated_type_list.empty())
    {
              auto i    = templated_type_list.cbegin();
        const auto iend = templated_type_list.cend();

        output << out_source(**i, indent);

        while (++i != iend)
            output << L", " << out_source(**i, indent);
    }

    output << L'>';

    return output;
}

} } }
