/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <algorithm>
#include <functional>
#include <ostream>
#include <string>
#include <vector>

#include <boost/algorithm/string/join.hpp>
#include <boost/range/adaptor/transformed.hpp>

#include "language/tree/ASTNode.h"
#include "language/tree/basic/Type.h"
#include "language/tree/basic/FunctionType.h"
#include "language/tree/module/Internal.h"

namespace zillians { namespace language { namespace tree {

FunctionType::FunctionType()
    : Type()
    , return_type(nullptr)
    , class_type(nullptr)
{}

size_t FunctionType::getTypeClassSerialNumber() const
{
    return ASTNodeType::FunctionType;
}

Type::ConversionRank FunctionType::getConversionRank(const Type& target, const Type::ConversionPolicy policy) const
{
    if (this == &target)
        return ConversionRank::ExactMatch;
    else
        return ConversionRank::NotMatch;
}

bool FunctionType::isSame(const Type& rhs) const
{
    if(!rhs.isFunctionType())
        return false;

    if(isLessThan(rhs))
        return false;

    if(rhs.isLessThan(*this))
        return false;

    return true;
}

bool FunctionType::isLessThan(const Type& rhs) const
{
    if(getTypeClassSerialNumber() < rhs.getTypeClassSerialNumber())
        return true;
    if(rhs.getTypeClassSerialNumber() < getTypeClassSerialNumber())
        return false;
    BOOST_ASSERT(isa<FunctionType>(&rhs));

    const FunctionType* l = this;
    const FunctionType* r = cast<FunctionType>(&rhs);

    // class type
    if(l->class_type != NULL && r->class_type != NULL)
    {
        const auto*const l_class_canonical_type = l->class_type;
        const auto*const r_class_canonical_type = r->class_type;

        if(l_class_canonical_type < r_class_canonical_type) return true;
        if(r_class_canonical_type < l_class_canonical_type) return false;
    }
    else if(l->class_type == NULL && r->class_type != NULL)
    {
        return true;
    }
    else if(l->class_type != NULL && r->class_type == NULL)
    {
        return false;
    }

    // return type
    if(l->return_type != NULL && r->return_type != NULL)
    {
        const auto*const l_return_canonical_type = l->return_type;
        const auto*const r_return_canonical_type = r->return_type;

        if(l_return_canonical_type < r_return_canonical_type) return true;
        if(r_return_canonical_type < l_return_canonical_type) return false;
    }
    else if(l->return_type == NULL && r->return_type != NULL)
    {
        return true;
    }
    else if(l->return_type != NULL && r->return_type == NULL)
    {
        return false;
    }

    // parameters
    size_t min_len = std::min(l->parameter_types.size(), r->parameter_types.size());
    for(size_t i = 0; i != min_len; ++i)
    {
        const auto*const l_parameter_canonical_type = l->parameter_types[i];
        const auto*const r_parameter_canonical_type = r->parameter_types[i];

        if(l_parameter_canonical_type < r_parameter_canonical_type) return true;
        if(r_parameter_canonical_type < l_parameter_canonical_type) return false;
    }
    if(l->parameter_types.size() < r->parameter_types.size()) return true;
    if(r->parameter_types.size() < l->parameter_types.size()) return false;

    return false;
}


std::wstring FunctionType::toString() const
{
    std::wstring result = L"function";

    if (class_type != nullptr)
    {
        result += L' ';
        result += class_type->toString();
        result += L'.';
    }

    result += L'(';

    result += boost::algorithm::join(
        parameter_types | boost::adaptors::transformed(std::mem_fn(&Type::toString)),
        L", "
    );

    result += L"):";
    result += return_type == NULL ? L"<not-ready>" : return_type->toString();
    return result;
}

bool FunctionType::isEqualImpl(const ASTNode& rhs) const
{
    AST_NODE_IS_EQUAL(
        (parameter_types)
        (return_type    )
    );
}

bool FunctionType::replaceUseWith(const ASTNode& from, const ASTNode& to, bool update_parent/* = true*/)
{
    AST_NODE_REPLACE(
        result,
        (parameter_types)
        (return_type    )
    );
}

FunctionType* FunctionType::clone() const
{
    BOOST_ASSERT(false && "function type should not be cloned");

    return nullptr;
}

FunctionType* FunctionType::getCanonicalType() const
{
    BOOST_ASSERT(isa<Internal>(parent) && "function type should under internal node only");

    return const_cast<FunctionType*>(this);
}

std::wostream& FunctionType::toSource(std::wostream& output, unsigned indent/* = 0*/) const
{
    output << L"function (";

    if (!parameter_types.empty())
    {
              auto i    = parameter_types.cbegin();
        const auto iend = parameter_types.cend();

        output << out_source(**i, indent);

        while (++i != iend)
            output << L',' << out_source(**i, indent);
    }

    output << L"):" << out_source(*return_type, indent);

    return output;
}

} } }
