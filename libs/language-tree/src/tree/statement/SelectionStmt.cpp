/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include "language/tree/ASTNode.h"
#include "language/tree/basic/Annotations.h"
#include "language/tree/basic/Block.h"
#include "language/tree/expression/Expression.h"
#include "language/tree/statement/Statement.h"
#include "language/tree/statement/SelectionStmt.h"

namespace zillians { namespace language { namespace tree {

Selection::Selection()
    : Selection(nullptr, nullptr)
{
}

Selection::Selection(Expression* cond, Block* block)
    : cond(cond)
    , block(block)
{
    if (cond  != nullptr) cond ->parent = this;
    if (block != nullptr) block->parent = this;
}

bool Selection::isEqualImpl(const ASTNode& rhs) const
{
    AST_NODE_IS_EQUAL(
        (cond )
        (block)
    );
}

bool Selection::replaceUseWith(const ASTNode& from, const ASTNode& to, bool update_parent/* = true*/)
{
    AST_NODE_REPLACE(
        result,
        (cond )
        (block)
    );
}

std::wostream& Selection::toSource(std::wostream& output, unsigned indent/* = 0*/) const
{
    UNREACHABLE_CODE();

    return output;
}

Selection* Selection::clone() const
{
    return new Selection(
        clone_or_null(cond ),
        clone_or_null(block)
    );
}

IfElseStmt::IfElseStmt()
    : if_branch(nullptr)
    , else_block(nullptr)
{
}

IfElseStmt::IfElseStmt(Selection* if_branch)
    : if_branch(if_branch)
    , else_block(nullptr)
{
    BOOST_ASSERT(if_branch != nullptr);
    BOOST_ASSERT(if_branch->cond != nullptr);
    BOOST_ASSERT(if_branch->block != nullptr);

    if_branch->parent = this;
}

IfElseStmt::IfElseStmt(Selection* if_branch, Block* else_block)
    : if_branch(if_branch)
    , else_block(else_block)
{
    BOOST_ASSERT(if_branch        != nullptr && "null pointer exception");
    BOOST_ASSERT(if_branch->cond  != nullptr && "null pointer exception");
    BOOST_ASSERT(if_branch->block != nullptr && "null pointer exception");
    BOOST_ASSERT(else_block       != nullptr && "null pointer exception");

    if_branch ->parent = this;
    else_block->parent = this;
}

void IfElseStmt::addElseIfBranch(Selection* branch)
{
    BOOST_ASSERT(branch->cond != nullptr);
    BOOST_ASSERT(branch->block != nullptr);

    branch->parent = this;

    elseif_branches.push_back(branch);
}

void IfElseStmt::setElseBranch(Block* block)
{
    BOOST_ASSERT(block != nullptr);

    block->parent = this;
    else_block = block;
}

bool IfElseStmt::isEqualImpl(const ASTNode& rhs) const
{
    AST_NODE_IS_EQUAL(
        (if_branch      )
        (elseif_branches)
        (else_block     )
    );
}

bool IfElseStmt::replaceUseWith(const ASTNode& from, const ASTNode& to, bool update_parent/* = true*/)
{
    AST_NODE_REPLACE(
        result,
        (if_branch      )
        (elseif_branches)
        (else_block     )
    );
}

IfElseStmt* IfElseStmt::clone() const
{
    BOOST_ASSERT(if_branch && "no if branch!");

    IfElseStmt* cloned = new IfElseStmt(if_branch->clone());

    for (Selection* elseif_branch: elseif_branches)
    {
        BOOST_ASSERT(elseif_branch && "null pointer exception");

        cloned->addElseIfBranch(elseif_branch->clone());
    }

    if (else_block)
        cloned->setElseBranch(else_block->clone());

    if(annotations)
        cloned->setAnnotations(annotations->clone());

    return cloned;
}

std::wostream& IfElseStmt::toSource(std::wostream& output, unsigned indent/* = 0*/) const
{
    BOOST_ASSERT(if_branch && "null pointer exception");

    output << out_indent(indent) << L"if (" << out_source(*if_branch->cond, indent) << L')' << std::endl
           << out_source(*if_branch->block, indent);

    for (Selection* elseif_branch : elseif_branches)
    {
        output << std::endl
               << out_indent(indent) << L"else if (" << out_source(*elseif_branch->cond, indent) << L')' << std::endl
               << out_source(*elseif_branch->block, indent);
    }

    if (else_block != nullptr)
    {
        output << std::endl
               << out_indent(indent) << L"else" << std::endl
               << out_source(*else_block, indent);
    }

    return output;
}

SwitchStmt::SwitchStmt()
    : node(nullptr)
    , default_block(nullptr)
{
}

SwitchStmt::SwitchStmt(Expression* node)
    : node(node)
    , default_block(nullptr)
{
    BOOST_ASSERT(node && "null node for switch statement is not allowed");

    node->parent = this;
}

void SwitchStmt::addCase(Selection* branch)
{
    BOOST_ASSERT(branch && "null pointer exception");
    BOOST_ASSERT(branch->cond && "null condition for selection statement is not allowed");

    branch->parent = this;
    cases.push_back(branch);
}

void SwitchStmt::setDefaultCase(Block* block)
{
    if(default_block) default_block->parent = nullptr;
    default_block = block;
    if(default_block) default_block->parent = this;
}

bool SwitchStmt::isEqualImpl(const ASTNode& rhs) const
{
    AST_NODE_IS_EQUAL(
        (node         )
        (cases        )
        (default_block)
    );
}

bool SwitchStmt::replaceUseWith(const ASTNode& from, const ASTNode& to, bool update_parent/* = true*/)
{
    AST_NODE_REPLACE(
        result,
        (node         )
        (cases        )
        (default_block)
    );
}

SwitchStmt* SwitchStmt::clone() const
{
    BOOST_ASSERT(node && "null node for switch statement is not allowed");

    SwitchStmt* cloned = new SwitchStmt(node->clone());

    for (Selection* case_: cases)
    {
        BOOST_ASSERT(case_ && "null pointer exception");

        cloned->addCase(case_->clone());
    }

    cloned->setDefaultCase(clone_or_null(default_block));

    if(annotations) cloned->setAnnotations(annotations->clone());

    return cloned;
}

std::wostream& SwitchStmt::toSource(std::wostream& output, unsigned indent/* = 0*/) const
{
    BOOST_ASSERT(node && "null pointer exceptoin");

    output << out_indent(indent) << L"switch (" << out_source(*node, indent) << L')' << std::endl
           << L'{' << std::endl;

    for (Selection* case_ : cases)
    {
        BOOST_ASSERT(case_->cond && "null pointer exceptoin");

        output << out_indent(indent) << out_source(*case_->cond, indent) << L':' << std::endl;

        if (case_->block != nullptr)
            output << out_source(*case_->block, indent + 1) << std::endl;
    }

    if (default_block != nullptr)
    {
        output << out_indent(indent) << L"default:" << std::endl
               << out_source(*default_block, indent + 1) << std::endl;
    }

    output << L'}';

    return output;
}

} } }
