/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <iterator>
#include <type_traits>
#include <utility>
#include <vector>

#include <boost/assert.hpp>
#include <boost/foreach.hpp>
#include <boost/logic/tribool.hpp>
#include <boost/mpl/push_back.hpp>
#include <boost/phoenix/core.hpp>
#include <boost/phoenix/operator.hpp>
#include <boost/range/adaptor/indirected.hpp>
#include <boost/range/adaptor/transformed.hpp>
#include <boost/range/algorithm_ext/push_back.hpp>
#include <boost/range/begin.hpp>
#include <boost/range/end.hpp>
#include <boost/range/numeric.hpp>
#include <boost/variant.hpp>

#include "language/tree/ASTNodeHelper.h"
#include "language/tree/visitor/NodeInfoVisitor.h"

namespace zillians { namespace language { namespace tree {

namespace {

template<typename NodeType>
Annotations* insertOrGetAnnotations(NodeType& node, ASTNode& node_ref)
{
    if (node.annotations == nullptr)
    {
        Annotations*const newed_annos = new Annotations;

        ASTNodeHelper::propogateSourceInfo(*newed_annos, node_ref);

        node.setAnnotations(newed_annos);
    }

    return node.annotations;
}

struct TypeSpecifierCreator
{
    using result_type = TypeSpecifier*;

    template<typename... ArgTypes>
    result_type operator()(ArgTypes&&... args) const
    {
        return ASTNodeHelper::createTypeSpecifierFrom(std::forward<ArgTypes>(args)...);
    }
};

}

ASTNodeHelper::ASTNodeHelper()
{ }

ASTNodeHelper::~ASTNodeHelper()
{ }

void ASTNodeHelper::tryPropogateSourceInfo(ASTNode& to, const ASTNode& from)
{
    const auto*const from_src_info = from.get<stage::SourceInfoContext>();

    if (from_src_info != nullptr)
        to.set<stage::SourceInfoContext>(new stage::SourceInfoContext(*from_src_info));
}

void ASTNodeHelper::propogateSourceInfo(ASTNode& to, const ASTNode& from)
{
    stage::SourceInfoContext* from_src_info = from.get<stage::SourceInfoContext>();
    BOOST_ASSERT( from_src_info != nullptr && "source node has no source info context to copy" );

    to.set<stage::SourceInfoContext>(new stage::SourceInfoContext(*from_src_info));
}

void ASTNodeHelper::propogateSourceInfoRecursively(ASTNode& to, const ASTNode& from)
{
    tree::ASTNodeHelper::foreachApply<ASTNode>(
        to,
        [&from](tree::ASTNode& n)
        {
            tree::ASTNodeHelper::propogateSourceInfo(n, from);
        }
    );
}

void ASTNodeHelper::propogateArchitecture(Declaration& node)
{
    if (node.arch.is_not_zero())
        return;

    if (!isa<ClassDecl, EnumDecl, FunctionDecl, VariableDecl, TypedefDecl>(node))
        return;

    if (const VariableDecl* var = cast<VariableDecl>(&node))
    {
        if (!var->isGlobal() && !var->is_member)
            return;
    }

    Architecture arch;

    if (const auto*const annotations = node.getAnnotations())
    {
        arch = boost::accumulate(
            annotations->annotation_list,
            Architecture::zero(),
            [](const Architecture& arch, Annotation* ann) -> Architecture
            {
                const bool is_valid =
                    ann->name->name == L"cpu"    ||
                    ann->name->name == L"gpu"    ||
                    ann->name->name == L"cuda"   ||
                    ann->name->name == L"opencl"
                ;

                if (is_valid && Architecture::is_valid(ann->name->name))
                    return arch | Architecture(ann->name->name);
                else
                    return arch;
            }
        );
    }

    if (arch.is_zero())
    {
        for (const Declaration* parent_decl = node.getOwner<Declaration>(); parent_decl != nullptr; parent_decl = parent_decl->getOwner<Declaration>())
        {
            if (parent_decl->arch.is_not_zero())
            {
                arch = parent_decl->arch;

                break;
            }
        }
    }

    if (arch.is_not_zero())
    {
        node.arch = std::move(arch);
    }
    else
    {
        node.arch = Architecture::default_;
    }
}

Type* ASTNodeHelper::getCanonicalType(const ASTNode* node)
{
    if     (const TypeSpecifier* type_specifier = cast<const TypeSpecifier>(node)) { return type_specifier->getCanonicalType(); }
    else if(const Declaration*   declaration    = cast<const Declaration  >(node)) { return declaration   ->getCanonicalType(); }
    else if(const Expression*    expression     = cast<const Expression   >(node)) { return expression    ->getCanonicalType(); }
    else if(const Literal*       literal        = cast<const Literal      >(node)) { return literal       ->getCanonicalType(); }
    else if(const Type*          type           = cast<const Type         >(node)) { return type          ->getCanonicalType(); }
    else if(const Identifier*    identifier     = cast<const Identifier   >(node)) { return identifier    ->getCanonicalType(); }
    else                                                                           { return nullptr; }
}

ASTNode* ASTNodeHelper::getCanonicalSymbol(const ASTNode* node)
{
    ASTNode* resolved = ResolvedSymbol::get(node);
    if(!resolved)
        return nullptr;

    BOOST_ASSERT(ResolvedSymbolTraits::isValid(resolved) && "invalid symbol resolution");

    return resolved;
}

bool ASTNodeHelper::isInheritedFrom(const ClassDecl* node_derived, const ClassDecl* node_base)
{
    BOOST_ASSERT(node_derived && node_base && "null pointer exception");

    auto is_inherited_from = isInheritedFromEx(node_derived, node_base);

    static_assert(std::is_same<decltype(is_inherited_from), boost::tribool>::value, "we expect the return value of ASTNodeHelper::isInheritedFromEx is boost::tribool!");
    BOOST_ASSERT(!boost::indeterminate(is_inherited_from) && "we should use this function when type information is completed!");

    return is_inherited_from;
}

boost::tribool ASTNodeHelper::isInheritedFromEx(const ClassDecl* node_derived, const ClassDecl* node_base)
{
    BOOST_ASSERT(node_derived && node_base && "null pointer exception");

    if(!node_derived->is_interface && node_derived->base)
    {
        if(Type* resolved_base_type = node_derived->base->getCanonicalType())
        {
            ClassDecl* resolved_base_class_type = resolved_base_type->getAsClassDecl();

            if(resolved_base_class_type)
            {
                if(resolved_base_class_type->isEqual(*node_base))
                {
                    return true;
                }
                else
                {
                    const auto& is_inherited_from = isInheritedFromEx(resolved_base_class_type, node_base);

                    static_assert(
                        std::is_same<
                            boost::tribool,
                            std::remove_cv<std::remove_reference<decltype(is_inherited_from)>::type>::type
                        >::value, "implementation is changed, please modify code"
                    );

                    if (is_inherited_from)
                        return true;
                    else if (boost::indeterminate(is_inherited_from))
                        return boost::indeterminate;
                }
            }
        }
        else
        {
            return boost::indeterminate;
        }
    }

    if(node_base->is_interface)
    {
        BOOST_FOREACH(TypeSpecifier* implement, node_derived->implements)
        {
            if(Type* resolved_base_type = implement->getCanonicalType())
            {
                ClassDecl* resolved_base_class_type = resolved_base_type->getAsClassDecl();

                if(resolved_base_class_type->isEqual(*node_base))
                {
                    return true;
                }
                else
                {
                    const auto& is_inherited_from = isInheritedFromEx(resolved_base_class_type, node_base);

                    static_assert(
                        std::is_same<
                            boost::tribool,
                            std::remove_cv<std::remove_reference<decltype(is_inherited_from)>::type>::type
                        >::value, "implementation is changed, please modify code"
                    );

                    if (is_inherited_from)
                        return true;
                    else if (boost::indeterminate(is_inherited_from))
                        return boost::indeterminate;
                }
            }
            else
            {
                return boost::indeterminate;
            }
        }
    }

    return false;
}

bool ASTNodeHelper::hasAncestor(ASTNode* child, ASTNode* ancestor)
{
    BOOST_ASSERT(child    != NULL);
    BOOST_ASSERT(ancestor != NULL);

    child = child->parent;
    while(child)
    {
        if(child == ancestor)
            return true;
        child = child->parent;
    }
    return false;
}

bool ASTNodeHelper::hasOwnerNamedScope(const ASTNode* node)
{
    return getOwnerNamedScope(node) != nullptr;
}

ASTNode* ASTNodeHelper::getOwnerNamedScope(const ASTNode* node)
{
    BOOST_ASSERT(node && "null pointer exception");

    for(ASTNode* p = node->parent; p; p = p->parent)
        if(isNamedScope(p))
            return p;

    return NULL;
}

ASTNode* ASTNodeHelper::insertIntoOwnerNameScope(const ASTNode& ref_node, FunctionDecl& func_decl)
{
    auto*const owner = ref_node.getAnyOwner<Source, ClassDecl>();

    BOOST_ASSERT(owner != nullptr && "no source/class is found");

    if (auto*const source = cast<Source>(owner))
        source->addDeclare(&func_decl);
    else if (auto*const cls_decl = cast<ClassDecl>(owner))
        cls_decl->addFunction(&func_decl);
    else
        BOOST_ASSERT((isa<ClassDecl, Source>(owner)) && "unexpected owner for function declaration");

    return owner;
}

bool ASTNodeHelper::hasSystemLinkage(const Annotatable* node)
{
    if(node->findAnnotation(L"system"))
        return true;

    if(ClassDecl* classDecl = node->getOwner<ClassDecl>())
        if(classDecl->findAnnotation(L"system"))
            return true;

    return false;
}

bool ASTNodeHelper::hasNativeLinkage(const Declaration* node)
{
    if(node->findAnnotation(L"native"))
        return true;

    if(node->findAnnotation(L"system"))
        return true;

    // function has implementation is not native
    if(const FunctionDecl* function_decl = cast<FunctionDecl>(node))
    {
        if( function_decl->block != nullptr )
            return false;
    }

    if(ClassDecl* classDecl = node->getOwner<ClassDecl>())
    {
        if(classDecl->findAnnotation(L"native"))
            return true;
        if(classDecl->findAnnotation(L"system"))
            return true;
    }

    return false;
}

bool ASTNodeHelper::isExported(const Annotatable* node)
{
    if(node->findAnnotation(L"server"))
        return true;
    if(node->findAnnotation(L"client"))
        return true;
    if(node->findAnnotation(L"entry"))
        return true;
    if(node->findAnnotation(L"export"))
        return true;

    return false;
}

ASTNodeHelper::ExportKind ASTNodeHelper::getExportKind(const Annotatable* node)
{
    bool server = node->findAnnotation(L"server");
    bool client = node->findAnnotation(L"client");
    bool entry  = node->findAnnotation(L"entry");
    bool exp    = node->findAnnotation(L"export");

    if(!server && !client && !entry && !exp)
        return ASTNodeHelper::ExportKind::NoExport;
    else
    {
        if(!server && !client)
            return ASTNodeHelper::ExportKind::GeneralExport;
        else
        {
            // either server export or client export
            if(client)
                return ASTNodeHelper::ExportKind::ClientExport;
            else
                return ASTNodeHelper::ExportKind::ServerExport;
        }
    }
}

ASTNodeHelper::ExportKind ASTNodeHelper::mergeExportKind(ExportKind lhs, ExportKind rhs)
{
    if (lhs == rhs)
        return lhs;

    if (lhs == ASTNodeHelper::ExportKind::GeneralExport || rhs == ASTNodeHelper::ExportKind::GeneralExport)
        return ASTNodeHelper::ExportKind::GeneralExport;
    else if (lhs == ASTNodeHelper::ExportKind::ClientExport && rhs == ASTNodeHelper::ExportKind::ServerExport)
        return ASTNodeHelper::ExportKind::GeneralExport;
    else if (lhs == ASTNodeHelper::ExportKind::ServerExport && rhs == ASTNodeHelper::ExportKind::ClientExport)
        return ASTNodeHelper::ExportKind::GeneralExport;
    else if (lhs == ASTNodeHelper::ExportKind::NoExport)
        return rhs;
    else
        return lhs;
}

bool ASTNodeHelper::parseCudaAnnotation(Annotation* cuda_annotation, std::vector<std::wstring>& isa)
{
    // TODO parse the isa version from @cuda annotation
//    foreach(i, cuda_annotation->attribute_list)
//    {
//        if(i->first->toString() == L"isa")
//        {
//            PrimaryExpr* isa_expr = cast<PrimaryExpr>(i->second);
//            if(isa_expr->catagory != PrimaryExpr::Catagory::LITERAL)
//            {
//
//            }
//        }
//    }
    UNIMPLEMENTED_CODE();
    return true;
}

const ASTNode* ASTNodeHelper::getDebugAnnotationAttachPoint(const ASTNode* node)
{
    BOOST_ASSERT(node && "null pointer exception");

    const ASTNode* current = nullptr;
    const ASTNode* next    = node;
    do
    {
        current = next;
        if(!(next = getSplitReferenceAttachPoint(current))) // NOTE: check split-reference first
            break;
        next = getOwnerDebugAnnotationAttachPoint(next);
    } while(next && next != current);
    return current;
}

bool ASTNodeHelper::isFuncParam(VariableDecl* node)
{
    BOOST_ASSERT(node && "null pointer exception");
    return isa<FunctionDecl>(node->parent);
}

bool ASTNodeHelper::isDeclStmtVarDecl(const VariableDecl* node)
{
    BOOST_ASSERT(node && "null pointer exception");
    return isa<DeclarativeStmt>(node->parent);
}

bool ASTNodeHelper::isInvalidDebugAnnotationAttachPoint(const ASTNode* node)
{
    BOOST_ASSERT(node && "null pointer exception");

    if(const auto*const var_decl = cast<VariableDecl>(node))
    {
        return isDeclStmtVarDecl(var_decl);
    }

    return false;
}

bool ASTNodeHelper::isCallIdentifier(Identifier* node)
{
    // system String & Object
    if(node->toString() == L"String" || node->toString() == L"Object")
        return false;

    // primary form
    // f();
    if(isa<IdExpr>(node->parent) &&
       isa<CallExpr>(node->parent->parent) &&
       node->parent == cast<CallExpr>(node->parent->parent)->node)
    {
        return true;
    }

    // member form
    // a.b.c.f();
    if(isa<MemberExpr>(node->parent) && isa<CallExpr>(node->parent->parent))
        return true;

    return false;
}

bool ASTNodeHelper::isSameFunctionSignature(FunctionDecl& func1, FunctionDecl& func2)
{
    BOOST_ASSERT(func1.name && func2.name && "null pointer exception");

    bool is_signature_same =
            func1.name->isEqual(*func2.name) &&
            func1.parameters.size() == func2.parameters.size();

    for(auto i = func1.parameters.size(); is_signature_same && i-- != 0; )
    {
        if(func1.parameters[i]->getCanonicalType() != func2.parameters[i]->getCanonicalType())
        {
            is_signature_same = false;
        }
    }

    return is_signature_same;
}

std::wstring ASTNodeHelper::getNodeName(ASTNode* node, bool FQN)
{
    BOOST_ASSERT(node && "null pointer exception");
    const int32 max_depth = 20;
    tree::visitor::NodeInfoVisitor v(FQN ? max_depth : 1);
    v.visit(*node);
    return v.stream.str();
}

std::wstring ASTNodeHelper::getTypeString(ASTNode* node, bool FQN)
{
    if(TypeSpecifier* spec = cast<TypeSpecifier>(node))
        return spec->toString();
    else if(Declaration* decl = cast<Declaration>(node))
        return decl->name->toString();
    else if(Type* type = cast<Type>(node))
        return type->toString();

    UNREACHABLE_CODE();
    return L"";
}

void ASTNodeHelper::toSource(char* path)
{
    getParserContext().tangle->toSource(path);
}

void ASTNodeHelper::visualize(char* path)
{
    visualize(getParserContext().tangle, path);
}

void ASTNodeHelper::visualize(ASTNode* node, const boost::filesystem::path& path)
{
    if(!path.parent_path().empty())
        boost::filesystem::create_directories(path.parent_path());

    std::wofstream fout(path.string());
    if(!fout.is_open())
    {
        std::cerr << "Error: cannot open file" << path.string() << std::endl;
        return;
    }

    fout << L"digraph G {" << std::endl;
    fout << L"    rankdir=LR;" << std::endl;

    fout << L"    // nodes" << std::endl;
    zillians::language::stage::visitor::ASTGraphvizNodeGenerator nodeGen(fout);
    nodeGen.visit(*node);

    fout << std::endl;
    fout << L"    // children to parent edges" << std::endl;
    zillians::language::stage::visitor::ASTGraphvizParentEdgeGenerator parentEdgeGen(fout);
    parentEdgeGen.visit(*node);

    fout << std::endl;
    fout << L"    // parent to children edge" << std::endl;
    zillians::language::stage::visitor::ASTGraphvizChildEdgeGenerator childEdgeGen(fout);
    childEdgeGen.visit(*node);
    fout << "}" << std::endl;
}

TypeSpecifier* ASTNodeHelper::getTypeSpecifierFrom(ASTNode* node)
{
    typedef boost::mpl::push_back<
        detail::ContextToCloneTypeList,
        zillians::language::ResolvedType
    >::type CloneTypeList;

    BOOST_ASSERT(isa<TypeSpecifier>(node) ||
                 isa<Declaration  >(node) ||
                 isa<FunctionType >(node) ||
                 isa<PrimitiveType>(node) ||
                 isa<DeclType     >(node) ||
                 isa<MultiType    >(node)
                 );

    if(DeclType* decl_type = cast<DeclType>(node))
    {
        Identifier* id = decl_type->getDecl()->name;
        TypeSpecifier* result = new NamedSpecifier(clone<Identifier, CloneTypeList>(id));
        ResolvedType::set(result, decl_type);
        return result;
    }
    else if(PrimitiveType* pt = cast<PrimitiveType>(node))
    {
        Internal* internal = getParserContext().tangle->internal;
        return internal->getPrimitiveTypeSpecifier(pt->getKind());
    }
    else if(isa<TypeSpecifier>(node))
    {
        return cast<TypeSpecifier>(node);
    }
    else if(Declaration* decl = cast<Declaration>(node))
    {
        Internal* internal = getParserContext().tangle->internal;
        TypeSpecifier* result = new NamedSpecifier(clone<Identifier, CloneTypeList>(decl->name));

        if(ClassDecl   * class_decl    = cast<ClassDecl   >(decl)) ResolvedType::set(result, class_decl   ->getType());
        if(EnumDecl    * enum_decl     = cast<EnumDecl    >(decl)) ResolvedType::set(result, enum_decl    ->getType());
        if(TypenameDecl* typename_decl = cast<TypenameDecl>(decl)) ResolvedType::set(result, typename_decl->getType());
        if(TypedefDecl * typedef_decl  = cast<TypedefDecl >(decl)) ResolvedType::set(result, typedef_decl ->getType());
        if(FunctionDecl* function_decl = cast<FunctionDecl>(decl)) ResolvedType::set(result, internal->addFunctionType(*function_decl));
        return result;
    }
    else if(isa<FunctionType>(node) || isa<MultiType>(node))
    {
        auto*const type = cast<Type>(node);

        return createTypeSpecifierFrom(type);
    }

    UNREACHABLE_CODE();
    return NULL;
}

TypeSpecifier* ASTNodeHelper::createTypeSpecifierFrom(const Type* type)
{
    //BOOST_ASSERT(isa<TypeSpecifier>(node) || isa<Declaration>(node) || isa<FunctionType>(node));

    if (const auto*const type_primitive = type->getAsPrimitiveType())
    {
        return createTypeSpecifierFrom(type_primitive);
    }
    else if (const auto*const type_record = type->getAsRecordType())
    {
        return createTypeSpecifierFrom(type_record);
    }
    else if (const auto*const type_enum = type->getAsEnumType())
    {
        return createTypeSpecifierFrom(type_enum);
    }
    else if (const auto*const type_func = type->getAsFunctionType())
    {
        return createTypeSpecifierFrom(type_func);
    }
    else if (const auto*const type_multi = type->getAsMultiType())
    {
        return createTypeSpecifierFrom(type_multi);
    }

    UNREACHABLE_CODE();
    return nullptr;
}

FunctionSpecifier* ASTNodeHelper::createTypeSpecifierFrom(const FunctionType* type_func)
{
    using boost::adaptors::transformed;

    const auto&       param_type_specifiers = type_func->parameter_types | transformed(TypeSpecifierCreator());
          auto*const return_type_specifier  = type_func->return_type == nullptr ? nullptr : createTypeSpecifierFrom(type_func->return_type);

    auto*const result = new FunctionSpecifier({boost::begin(param_type_specifiers), boost::end(param_type_specifiers)}, return_type_specifier, false);

    ResolvedType::set(result, type_func->getCanonicalType());

    return result;
}

MultiSpecifier* ASTNodeHelper::createTypeSpecifierFrom(const MultiType* type_multi)
{
    using boost::adaptors::transformed;

    const auto&      multi_tss = type_multi->types | transformed(TypeSpecifierCreator());
          auto*const result    = new MultiSpecifier({boost::begin(multi_tss), boost::end(multi_tss)});

    ResolvedType::set(result, type_multi->getCanonicalType());

    return result;
}

NamedSpecifier* ASTNodeHelper::createTypeSpecifierFrom(const RecordType* type_record)
{
    typedef boost::mpl::push_back<
        detail::ContextToCloneTypeList,
        zillians::language::ResolvedType
    >::type CloneTypeList;

    const auto*const class_decl = type_record->getAsClassDecl();
    BOOST_ASSERT(class_decl != nullptr && "record type does not refer to class declaration!?");

    auto*const name   = clone<Identifier, CloneTypeList>(class_decl->name);
    auto*const result = new NamedSpecifier(name);

    ResolvedType::set(result, type_record->getCanonicalType());

    return result;
}

NamedSpecifier* ASTNodeHelper::createTypeSpecifierFrom(const EnumType* type_enum)
{
    typedef boost::mpl::push_back<
        detail::ContextToCloneTypeList,
        zillians::language::ResolvedType
    >::type CloneTypeList;

    const auto*const enum_decl = type_enum->getAsEnumDecl();
    BOOST_ASSERT(enum_decl != nullptr && "record type does not refer to class declaration!?");

    auto*const name   = clone<Identifier, CloneTypeList>(enum_decl->name);
    auto*const result = new NamedSpecifier(name);

    ResolvedType::set(result, type_enum->getCanonicalType());

    return result;
}

PrimitiveSpecifier* ASTNodeHelper::createTypeSpecifierFrom(const PrimitiveType* type_primitive)
{
    BOOST_ASSERT(type_primitive->isCanonical() && "primitive types are always canonical!");

    const auto&      kind   = type_primitive->getKind();
          auto*const result = new PrimitiveSpecifier(kind);

    ResolvedType::set(result, type_primitive->getCanonicalType());

    return result;
}

Expression* ASTNodeHelper::getDummyValue(const Type& type)
{
    using boost::adaptors::indirected;
    using boost::adaptors::transformed;

    if (const auto*const type_primitive = type.getAsPrimitiveType())
    {
        const auto& kind = type_primitive->getKind();

        if (kind == PrimitiveKind::VOID_TYPE)
            return nullptr;
        else
            return new tree::NumericLiteral(kind, 0);
    }
    else if (type.isEnumType())
    {
        auto*const enum_value = new tree::NumericLiteral(0);
        auto*const enum_ts    = tree::ASTNodeHelper::createTypeSpecifierFrom(&type);

        return new tree::CastExpr(enum_value, enum_ts, tree::CastExpr::CastMethod::STATIC);
    }
    else if (type.isRecordType())
    {
        return new tree::ObjectLiteral(tree::ObjectLiteral::LiteralType::NULL_OBJECT);
    }
    else if (const auto*const type_multi = type.getAsMultiType())
    {
              auto*const tie_expr  = new tree::TieExpr;
        const auto&      sub_exprs = type_multi->types
                                   | indirected
                                   | transformed(&ASTNodeHelper::getDummyValue)
                                   ;

        for (auto*const sub_expr : sub_exprs)
        {
            if (sub_expr == nullptr)
                return nullptr;

            tie_expr->tieExpression(sub_expr);
        }

        return tie_expr;
    }
    else
    {
        UNREACHABLE_CODE();

        return nullptr;
    }
}

bool ASTNodeHelper::isDynamicClass(ClassDecl& node)
{
    /* return true if it is dynamic class, which needs vptr. Otherwise, return false. */

    return node.hasVirtual();
}

std::list<ClassDecl*> ASTNodeHelper::getDirectBases(ClassDecl& node)
{
    /* return direct bases in declaration order */
    std::list<ClassDecl*> bases;

    if (node.base) {
        BOOST_ASSERT(!node.is_interface && "interface must not have base class!");

        Type* resolvedNode = node.base->getCanonicalType();
        BOOST_ASSERT(resolvedNode != NULL);

        ClassDecl* resolvedClass = resolvedNode->getAsClassDecl();
        BOOST_ASSERT(resolvedClass != NULL);

        bases.push_back(resolvedClass);
    }

    for (TypeSpecifier* impl_ts : node.implements) {
        BOOST_ASSERT(impl_ts && "null pointer exception");

        Type* resolvedNode = impl_ts->getCanonicalType();
        BOOST_ASSERT(resolvedNode != NULL);

        ClassDecl* resolvedClass = resolvedNode->getAsClassDecl();
        BOOST_ASSERT(resolvedClass != NULL);

        bases.push_back(resolvedClass);
    }

    return bases;
}

std::vector<Package*> ASTNodeHelper::getParentScopePackages(const ASTNode* node)
{
    std::vector<Package*> result;

    if(node == NULL)
        return result;

    for (ASTNode* parent = node->parent; parent != nullptr; parent = parent->parent)
    {
        if (Package* package =  cast<Package>(parent))
            result.emplace_back(package);
    }

    // if the node is primitive TypeSpecifier, no package
    if(result.empty())
        return result;

    // the last package is the "root package", ignore it.
    result.pop_back();
    std::reverse(result.begin(), result.end());
    return result;
}


std::wstring& ASTNodeHelper::getExpressiveDescSuffix(std::wstring& desc, const std::vector<TypenameDecl*>& typenames)
{
    if(!typenames.empty())
    {
        bool has_param = false;

        desc += L" [with ";

        for(auto* type : typenames)
        {
            if(has_param)
            {
                desc += L',';
            }
            else
            {
                has_param = true;
            }

            desc += type->name->toString();
            desc += L'=';

            if(type->specialized_type)
            {
                desc += type->specialized_type->toString();
            }
            else
            {
                desc += L"<not-specialized>";
            }
        }

        desc += L']';
    }

    return desc;
}

std::wstring ASTNodeHelper::getExpressiveDesc(FunctionDecl& func)
{
    std::vector<TypenameDecl*> typenames;

    std::wstring desc = getExpressiveDescImpl(&func, typenames);

    getExpressiveDescSuffix(desc, typenames);

    return desc;
}

bool ASTNodeHelper::isOnThorLang(const ASTNode* node)
{
    BOOST_ASSERT(node != NULL && "null pointer exception!");
    BOOST_ASSERT(node->getOwner<Tangle>() && "node is not under Tangle!");
    BOOST_ASSERT(node->getOwner<Package>() && "node is not under Package!");

    Package* package = node->getOwner<Package>();

    return package == node->getOwner<Tangle>()->findPackage(L"thor.lang");
}

Expression* ASTNodeHelper::useThorLangId(const ASTNode* node, Identifier* id_in_thor_lang)
{
    BOOST_ASSERT(node != NULL && "null pointer exception!");
    BOOST_ASSERT(id_in_thor_lang != NULL && "null pointer exception!");

    if(isOnThorLang(node))
    {
        return new IdExpr(id_in_thor_lang);
    }
    else
    {
        return
            new MemberExpr(
                new MemberExpr(
                    new IdExpr(
                        new SimpleIdentifier(L"thor")
                    ),
                    new SimpleIdentifier(L"lang")
                ),
                id_in_thor_lang
            );
    }
}

Identifier* ASTNodeHelper::createThorLangId(const ASTNode* node, Identifier* id_in_thor_lang)
{
    BOOST_ASSERT(node != NULL && "null pointer exception!");

    if (!isOnThorLang(node))
    {
        return NestedIdentifier::create(
            {
                new SimpleIdentifier(L"thor"),
                new SimpleIdentifier(L"lang"),
                id_in_thor_lang
            }
        );
    }
    else
    {
        return id_in_thor_lang;
    }
}

NamedSpecifier* ASTNodeHelper::createThorLangTs(const ASTNode* node, Identifier* id_in_thor_lang)
{
    return new NamedSpecifier(createThorLangId(node, id_in_thor_lang));
}

ASTNode* ASTNodeHelper::getDeclarationOwner(Declaration& decl)
{
    if(isa<Source>(decl.parent))
    {
        BOOST_ASSERT(isa<Package>(decl.parent->parent) && "source is not under package!");

        return decl.parent->parent;
    }
    else
    {
        return decl.getOwner<Declaration>();
    }
}

Type::ConversionRank ASTNodeHelper::getConversionRank(Expression& from, TypeSpecifier& to, const Type::ConversionPolicy policy)
{
    Type* toType = to.getCanonicalType();

    if(toType == nullptr)
        return Type::ConversionRank::UnknownYet;
    else
        return getConversionRank(from, *toType, policy);
}

Type::ConversionRank ASTNodeHelper::getConversionRank(Expression& from, Type& toType, const Type::ConversionPolicy policy)
{
    // Special handling for 'null', it should be castable to any class/interface
    if(toType.isRecordType() && from.isNullLiteral())
    {
        return Type::ConversionRank::StandardConversion;
    }

    Type* fromType = from.getCanonicalType();
    if(fromType == nullptr)
        return Type::ConversionRank::UnknownYet;

    return fromType->getConversionRank(toType, policy);
}

bool ASTNodeHelper::isNamedScope(ASTNode* node)
{
    BOOST_ASSERT(node && "null pointer exception");
    return isa<FunctionDecl>(node) ||
           isa<ClassDecl>(node) ||
           (isa<Package>(node) && !cast<Package>(node)->id->toString().empty());
}

bool ASTNodeHelper::isDebugAnnotationAttachPoint(const ASTNode* node)
{
    BOOST_ASSERT(node && "null pointer exception");
    return (isa<Statement>(node) ||
            isa<Declaration>(node) ||
            isa<Package>(node)) &&
            !isInvalidDebugAnnotationAttachPoint(node);
}

const ASTNode* ASTNodeHelper::getSplitReferenceAttachPoint(const ASTNode* node)
{
    BOOST_ASSERT(node && "null pointer exception");

    const ASTNode* current = nullptr;
    const ASTNode* next    = node;
    do
    {
        current = next;
        next = SplitReferenceContext::get(current);
    } while(next && next != current);
    return current;
}

const ASTNode* ASTNodeHelper::getOwnerDebugAnnotationAttachPoint(const ASTNode* node)
{
    BOOST_ASSERT(node && "null pointer exception");
    for(const ASTNode* p = node; p && !isa<Package>(p); p = p->parent)
        if(isDebugAnnotationAttachPoint(p))
            return p;
    return nullptr;
}

} } }
