/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <vector>
#include <string>
#include <unordered_map>
#include <utility>
#include <functional>

#include <boost/assert.hpp>
#include <boost/range.hpp>
#include <boost/range/adaptors.hpp>
#include <boost/range/iterator_range.hpp>
#include <boost/range/algorithm/find.hpp>
#include <boost/algorithm/string/join.hpp>
#include <boost/algorithm/string/predicate.hpp>

#include "utility/UnicodeUtil.h"
#include "language/tree/declaration/Declaration.h"
#include "language/tree/declaration/ClassDecl.h"
#include "language/tree/declaration/FunctionDecl.h"
#include "language/tree/declaration/VariableDecl.h"
#include "language/tree/declaration/EnumDecl.h"
#include "language/tree/declaration/TypenameDecl.h"
#include "language/tree/declaration/TypedefDecl.h"

#include "language/tree/basic/Identifier.h"
#include "language/tree/basic/TypeSpecifier.h"
#include "language/tree/module/Package.h"

#include "language/tree/basic/Type.h"
#include "language/tree/basic/PrimitiveKind.h"
#include "language/tree/basic/PrimitiveType.h"
#include "language/tree/basic/DeclType.h"

#include "language/tree/UniqueName.h"

namespace zillians { namespace language { namespace tree {

namespace {

    std::string wstr_to_str(const std::wstring& ucs4) 
    {
        auto toAsciiNumber = [](char c) -> std::string
            {   
                static char buffer[4];
                snprintf(buffer, 3, "%d", c); 
                return buffer;
            };  

        std::string ucs4_to_utf8_temp;
        std::string utf8_to_llvm_temp;

        // first we should covert UCS-4 to UTF-8
        ucs4_to_utf8(ucs4, ucs4_to_utf8_temp);

        // because LLVM only accept identifier of the form: '[%@][a-zA-Z$._][a-zA-Z$._0-9]*'
        // we have to convert illegal identifier into legal one
        for(auto i = ucs4_to_utf8_temp.cbegin(), e = ucs4_to_utf8_temp.cend(); i != e; ++i)
        {   
            char c = *i; 
            if( ((i == ucs4_to_utf8_temp.begin()) ? false : isdigit(c)) || isalpha(c) || (c == '_') || (c == '.') )
                utf8_to_llvm_temp.push_back(c);
            else
            {   
                utf8_to_llvm_temp.push_back('$');
                utf8_to_llvm_temp.append(toAsciiNumber(c));
                utf8_to_llvm_temp.push_back('$');
            }   
        }   

        return utf8_to_llvm_temp;                                                                                                                 
    }

    // storage type for keeping replacement name for template typenames 
    using replacements_type = std::unordered_map<const TypenameType*, unique_name_t>;
    
    // forward declarations of non-interface functions
    unique_name_t make_replacement(size_t);
    const unique_name_t& get_replacement(replacements_type::key_type, replacements_type&);
    
    unique_name_t    create_unique_name(const Identifier&      );
    unique_name_t    create_unique_name(const TypeSpecifier&   );
    unique_name_t    create_unique_name(const SimpleIdentifier&);

    unique_name_t create_and_substitute(const Identifier&         , replacements_type&);
    unique_name_t create_and_substitute(const TemplatedIdentifier&, replacements_type&);
    unique_name_t create_and_substitute(const Type&               , replacements_type&);
    unique_name_t create_and_substitute(const TypeSpecifier&      , replacements_type&);
    unique_name_t create_and_substitute(const TypenameType&       , replacements_type&);
    unique_name_t create_and_substitute(const MultiType&          , replacements_type&);

    class unique_name_converter
    {
        replacements_type* replacements;  
    public:
        using result_type = unique_name_t;
    
        unique_name_converter(): replacements(nullptr) { }
        unique_name_converter(replacements_type& replacements)
        : replacements(&replacements) { }
    
        result_type operator()(const TypeSpecifier* type) const
        {
            if(replacements)
                return create_and_substitute(*type, *replacements);
            
            return create_unique_name(*type);
        }
    
        result_type operator()(const Type* type) const
        {
            if(replacements)
                return create_and_substitute(*type, *replacements);
    
            return create_unique_name(*type);
        }
    
        result_type operator()(const TypenameDecl* typename_) const
        {
            if(typename_->specialized_type)
            {
                return create_and_substitute(*typename_->specialized_type, *replacements);
            }
            else
            {
                return get_replacement(typename_->getType(), *replacements);
            }
        }
    };

    // definitions of non-interface functions
    unique_name_t create_and_substitute(const Identifier& identifier, replacements_type& replacements)
    {
        if(const auto* simple_identifier = cast<SimpleIdentifier>(&identifier))
        {
            return create_unique_name(*simple_identifier);
        }
        else if(const auto* templated_identifier = cast<TemplatedIdentifier>(&identifier))
        {
            return create_and_substitute(*templated_identifier, replacements);
        }
    
        BOOST_ASSERT(false && "invalid identifier type");
        return unique_name_t();
    }
    
    unique_name_t create_unique_name(const SimpleIdentifier& identifier)
    {
        return wstr_to_str(identifier.name);
    }
    
    unique_name_t create_unique_name(const TypeSpecifier& type)
    {
        return create_unique_name(*type.getCanonicalType());
    }
    
    unique_name_t create_unique_name(const Identifier& identifier)
    {
        if(const auto* simple_identifier = cast<SimpleIdentifier>(&identifier))
        {
            return create_unique_name(*simple_identifier);
        }
    
        BOOST_ASSERT(false && "only handle simple identifiers here");
        return unique_name_t();
    }
    
    unique_name_t make_replacement(size_t serial_number)
    {
        return "@" + std::to_string(serial_number);
    }
    
    unique_name_t create_and_substitute(const TypeSpecifier& type, replacements_type& replacements)
    {
        if(const auto*const type_named = cast<NamedSpecifier>(&type))
        {
            const auto* identifier = type_named->getName();
            if(const auto* templated = cast<TemplatedIdentifier>(identifier))
            {
                return create_and_substitute(*identifier, replacements);
            }
        }
    
        return create_and_substitute(*type.getCanonicalType(), replacements);
    }
    
    
    unique_name_t create_and_substitute(const TypenameType& type, replacements_type& replacements)
    {
        return get_replacement(&type, replacements);
    }
    
    unique_name_t create_and_substitute(const Type& type, replacements_type& replacements)
    {
        if(const auto* primitive_type = cast<PrimitiveType>(&type))
        {
            return create_unique_name(*primitive_type);
        }
        else if(const auto* function_type = cast<FunctionType>(&type))
        {
            return create_unique_name(*function_type);
        }
        else if(const auto* multi_type = cast<MultiType>(&type))
        {
            return create_and_substitute(*multi_type, replacements);
        }
        else if(const auto* pointer_type = cast<PointerType>(&type))
        {
            ///  TODO: decide the return value here
            BOOST_ASSERT(false && "not implemented yet");
        }
        else if(const auto* reference_type = cast<ReferenceType>(&type))
        {
            ///  TODO: decide the return value here
            BOOST_ASSERT(false && "not implemented yet");
        }
        else if(const auto* typename_type = cast<TypenameType>(&type))
        {
            return create_and_substitute(*typename_type, replacements);
        }
        else if(const auto* record_type = cast<RecordType>(&type))
        {
            return create_unique_name(*record_type);
        }
        else if(const auto* enum_type = cast<EnumType>(&type))
        {
            return create_unique_name(*enum_type);
        }
        else if(const auto* typedef_type = cast<TypedefType>(&type))
        {
            return create_unique_name(*typedef_type);
        }
    
        BOOST_ASSERT(false && "meet unsupported type");
        return unique_name_t();
    }
    
    unique_name_t create_and_substitute(const MultiType& type, replacements_type& replacements)
    {
        using namespace boost::adaptors;
    
        unique_name_t result;
    
        result.append("(");
    
        // join sub types
        const auto& sub_types = type.types;
        BOOST_ASSERT(!sub_types.empty() && "invalid multitype with zero subtypes");
    
        result.append(
            boost::algorithm::join(
                sub_types | transformed(unique_name_converter(replacements)),
                ","
            )
        );
    
        result.append(")");
    
        return std::move(result);
    }
    
    const unique_name_t& get_replacement(replacements_type::key_type type, replacements_type& replacements)
    {
        auto insert_result = replacements.emplace(
            type, make_replacement(replacements.size() + 1)
        );
    
        return insert_result.first->second;
    }
    
    unique_name_t create_and_substitute(const TemplatedIdentifier& identifier, replacements_type& replacements)
    {
        using namespace boost::adaptors;
    
        unique_name_t result;
        result.append(create_unique_name(*identifier.id))
              .append("<");
    
        // join typenames
        const auto& typenames = identifier.templated_type_list;
        result.append(
            boost::algorithm::join(
                typenames | transformed(unique_name_converter(replacements)),
                ","
            )
        );
        result.append(">");
    
        return std::move(result);
    }

} // anonymous namespace

// definitions of interfaces
unique_name_t create_unique_name(const Package& package, bool with_header)
{
    unique_name_t result = (with_header ? "package " : "");

    const auto* parent_package = package.getOwner<Package>();
    if(parent_package) 
    {
        result.append(create_unique_name(*parent_package, false))
              .append(".p:")
              .append(create_unique_name(*package.id));
    }
    else
    {
        result.append("p:_");
    }

    return std::move(result);
}

unique_name_t create_unique_name(const EnumDecl& enum_, bool with_header)
{
    unique_name_t result = (with_header ? "enum " : "");

    if(const auto* parent_package = enum_.getOwner<Package>())
    {
        result.append(create_unique_name(*parent_package, false));
    }
    else
    {
        BOOST_ASSERT(false && "meet enum under invalid position");
    }

    result.append("$e:")
          .append(create_unique_name(*enum_.name));

    return std::move(result);
}

unique_name_t create_unique_name(const Declaration& decl, bool with_header)
{
    if(const auto* class_ = cast<ClassDecl>(&decl))
    {
        return create_unique_name(*class_, with_header);
    }
    else if(const auto* function = cast<FunctionDecl>(&decl))
    {
        return create_unique_name(*function, with_header);
    }
    else if(const auto* enum_ = cast<EnumDecl>(&decl))
    {
        return create_unique_name(*enum_, with_header);
    }
    else if(const auto* alias = cast<TypedefDecl>(&decl))
    {
        return create_unique_name(*alias, with_header);
    }
    else if(const auto* typename_ = cast<TypenameDecl>(&decl))
    {
        return create_unique_name(*typename_, with_header);
    }

    BOOST_ASSERT(false && "meet unsupported declaration type");
    return unique_name_t();        
}


unique_name_t create_unique_name(const ClassDecl& class_, bool with_header)
{
    unique_name_t result = (with_header ? "class ": "");

    if(const auto* parent_package = class_.getOwner<Package>())
    {
        result.append(create_unique_name(*parent_package, false));
    }
    else
    {
        BOOST_ASSERT(false && "meet class under invalid position");
    }

    replacements_type replacements;
    result.append("$c:")
          .append(create_and_substitute(*class_.name, replacements));

    return std::move(result);
}

unique_name_t create_unique_name(const FunctionDecl& function, bool with_header)
{
    using namespace boost::adaptors;

    unique_name_t result;

    // member functions
    if(const auto* parent_class = cast<ClassDecl>(function.parent))
    {
        result.append(with_header ? "method " : "")
              .append(create_unique_name(*parent_class, false));
    }
    // non member functions
    else if(const auto* parent_package = function.getOwner<Package>())
    {
        result.append(with_header ? "function " : "")
              .append(create_unique_name(*parent_package, false));
    }
    else
    {
        BOOST_ASSERT(false && "meet function under invalid position");
    }

    replacements_type replacements;
    result.append("$f:")
          .append(create_and_substitute(*function.name, replacements))
          .append("(");

    static const std::function<const TypeSpecifier*(const VariableDecl*)> get_type = [](const VariableDecl* variable) {
        return variable->type;
    };

    // join parameter types
    const auto parameters = function.parameters;
    result.append(
        boost::algorithm::join(
            parameters | transformed(get_type) | transformed(unique_name_converter(replacements)),
            ","
        )
    );
    result.append(")");

    return std::move(result);
}

unique_name_t create_unique_name(const VariableDecl& variable, bool with_header)
{
    unique_name_t result;

    // data members
    if(const auto* parent_class = cast<ClassDecl>(variable.parent))
    {
        result.append(with_header ? "attribute " : "")
              .append(create_unique_name(*parent_class, false));
    }
    // enumerators
    else if(const auto* parent_enum = cast<EnumDecl>(variable.parent))
    {
        result.append(with_header ? "enumerator " : "")
              .append(create_unique_name(*parent_enum, false));
    }
    else if(!variable.isGlobal())
    {
        // parameters
        if(const auto* parent_function = cast<FunctionDecl>(variable.parent))
        {
            result.append(with_header ? "parameter " : "")
                  .append(create_unique_name(*parent_function, false));
        }
        else // local variables
        {
            return unique_name_t();
        }
    }
    // variables under package(global variables))
    else if(const auto* parent_package = variable.getOwner<Package>())
    {
        result.append(with_header ? "variable " : "")
              .append(create_unique_name(*parent_package, false));
    }
    else
    {
        BOOST_ASSERT(false && "meet variable under invalid position");
        return unique_name_t();
    }
   
    result.append("$v:")
          .append(create_unique_name(*variable.name));

    return std::move(result);
}

unique_name_t create_unique_name(const TypenameDecl& typename_, bool with_header)
{
    unique_name_t result;

    result.append(with_header ? "typename " : "");

    const auto* templated_identifier = cast<TemplatedIdentifier>(typename_.parent);
    BOOST_ASSERT(templated_identifier && "typename decls most be under templated identifier");
    if (!templated_identifier)
        return unique_name_t();

    // typename of template class
    if (const auto* parent_class = cast<ClassDecl>(templated_identifier->parent))
    {
        result.append(create_unique_name(*parent_class, false));
    }
    // typename of template class
    else if(const auto* parent_function = cast<FunctionDecl>(templated_identifier->parent))
    {
        result.append(create_unique_name(*parent_function, false));
    }
    else
    {
        // typenames  witch is not in declarations
        return unique_name_t();
    }

    // make placement at rest part
    const auto& typenames = templated_identifier->templated_type_list;

    const auto location_in_identifier = boost::range::find(typenames, &typename_);
    BOOST_ASSERT(location_in_identifier != boost::end(typenames) && "cannot find typename in identifier");
    if (location_in_identifier == boost::end(typenames))
        return unique_name_t();

    const auto replacement = make_replacement(
        std::distance(boost::begin(typenames), location_in_identifier) + 1
    );

    result.append("$tn:")
          .append(replacement);

    return std::move(result);
}

unique_name_t create_unique_name(const FunctionType& function_type)
{
    using namespace boost::adaptors;

    const bool is_member_function_type = (function_type.class_type != nullptr);
    unique_name_t result = (is_member_function_type ? "type-method " : "type-function ");

    if(is_member_function_type)
    {
        auto* parent_type = cast<RecordType>(function_type.class_type);
        BOOST_ASSERT(parent_type && "parent should have record type");

        result.append(create_unique_name(*parent_type))
              .append(".");
    }

    result.append("(");

    // join paramter types
    const auto& param_types = function_type.parameter_types;
    result.append(
        boost::algorithm::join(
            param_types | transformed(unique_name_converter()),
            ","
        )
    );

    result.append(")")
          .append(":")
          .append(create_unique_name(*function_type.return_type));

    return std::move(result);
}

unique_name_t create_unique_name(const MultiType& type)
{
    using namespace boost::adaptors;

    unique_name_t result;

    result.append("type-multi ")
          .append("(");

    // join sub types
    const auto& sub_types = type.types;
    BOOST_ASSERT(!sub_types.empty() && "invalid multitype with zero subtypes");

    result.append(
        boost::algorithm::join(
            sub_types | transformed(unique_name_converter()),
            ","
        )
    );

    result.append(")");

    return std::move(result);
}

unique_name_t create_unique_name(const Type& type)
{
    if(const auto* primitive_type = cast<PrimitiveType>(&type))
    {
        return create_unique_name(*primitive_type);
    }
    else if(const auto* function_type = cast<FunctionType>(&type))
    {
        return create_unique_name(*function_type);
    }
    else if(const auto* multi_type = cast<MultiType>(&type))
    {
        return create_unique_name(*multi_type);
    }
    else if(const auto* pointer_type = cast<PointerType>(&type))
    {
        ///  TODO: decide the return value here
        BOOST_ASSERT(false && "not implemented yet");
    }
    else if(const auto* reference_type = cast<ReferenceType>(&type))
    {
        ///  TODO: decide the return value here
        BOOST_ASSERT(false && "not implemented yet");
    }
    else if(const auto* record_type = cast<RecordType>(&type))
    {
        return create_unique_name(*record_type);
    }
    else if(const auto* enum_type = cast<EnumType>(&type))
    {
        return create_unique_name(*enum_type);
    }
    else if(const auto* typedef_type = cast<TypedefType>(&type))
    {
        return create_unique_name(*typedef_type);
    }
    else if(const auto* typename_type = cast<TypenameType>(&type))
    {
        return create_unique_name(*typename_type);
    }

    BOOST_ASSERT(false && "meet unsupported type");
    return unique_name_t();
}

unique_name_t create_unique_name(const RecordType& record_type)
{
    const auto* origin_decl  = record_type.getDecl();
    const auto* origin_class = cast<ClassDecl>(origin_decl);

    return "type-record " + create_unique_name(*origin_class, false);
}

unique_name_t create_unique_name(const EnumType& enum_type)
{
    const auto* origin_decl = enum_type.getDecl();
    const auto* origin_enum = cast<EnumDecl>(origin_decl);

    return "type-enum " + create_unique_name(*origin_enum, false);
}

unique_name_t create_unique_name(const TypedefType& typedef_type)
{
    const auto* origin_decl    = typedef_type.getDecl();
    const auto* origin_typedef = cast<TypedefDecl>(origin_decl);

    return "type-typedef " + create_unique_name(*origin_typedef, false);
}

unique_name_t create_unique_name(const TypenameType& typename_type)
{
    const auto* origin_decl     = typename_type.getDecl();
    const auto* origin_typename = cast<TypenameDecl>(origin_decl);

    return "type-typename " + create_unique_name(*origin_typename, false);
}
  
unique_name_t create_unique_name(const PrimitiveKind& kind)
{
    return wstr_to_str(kind.toString());
}

unique_name_t create_unique_name(const PrimitiveType& primitive_type)
{
    return "type-primitive " + create_unique_name(primitive_type.getKind());
}

unique_name_t create_unique_name(const TypedefDecl& alias, bool with_header)
{
    unique_name_t result = (with_header ? "typedef " : "");
    if(const auto* parent_package = alias.getOwner<Package>())
    {
        result.append(create_unique_name(*parent_package, false))
              .append("$td:")
              .append(create_unique_name(*alias.name));
        return std::move(result);
    }
   
    BOOST_ASSERT(false && "meet typedef under invalid position");
    return unique_name_t();
}

namespace { 

    class ParenthesesMatcher
    {
    public:
        ParenthesesMatcher()
        : parentheses_counter(0),
          bracket_counter(0)
        { }
    
        ParenthesesMatcher& eat(unique_name_t::value_type ch)
        {
            switch(ch)
            {
            case '(': ++parentheses_counter; break;
            case ')': --parentheses_counter; break;
            case '<': ++bracket_counter;     break;
            case '>': --bracket_counter;     break;
            default:                         break;
            }

            return *this;
        }

        bool is_finished() const
        {
            return parentheses_counter == 0;
        }

        bool in_parsing_name() const
        {
            return bracket_counter != 0;
        }

        void reset()
        {
            parentheses_counter = bracket_counter = 0;
        }
    
    private:
        int parentheses_counter;
        int bracket_counter;
    };

} // anonymous namespace 

bool is_multitype_unique_name(const unique_name_t& name)
{
    return boost::algorithm::starts_with(name, "type-multi ");
}

bool is_function_type_unique_name(const unique_name_t& name)
{
    return boost::algorithm::starts_with(name, "type-method ") || boost::algorithm::starts_with(name, "type-function ");
}

bool is_type_unique_name(const unique_name_t& name)
{
    return boost::algorithm::starts_with(name, "type-");
}

std::pair<
    unique_name_t::size_type,
    unique_name_t::size_type
> find_function_type_splitters(const unique_name_t& function_type_unique_name)
{
    const auto& name = function_type_unique_name;

    const auto first_parenthesis = boost::find(name, '(');
    const auto end_name          = boost::end(name);
    if(first_parenthesis == end_name)
        return std::make_pair(unique_name_t::npos, unique_name_t::npos);

    // keep finding until meet non-enclosed ':' symbol which is used to split
    // parameter types and return type
    ParenthesesMatcher matcher;
    auto next_char = first_parenthesis;
    for( ; next_char != end_name && !matcher.eat(*next_char).is_finished(); ++next_char)
        ;

    if(!matcher.is_finished() || next_char == end_name || ++next_char == end_name)
        return std::make_pair(unique_name_t::npos, unique_name_t::npos);

    if(*next_char != ':')
        return std::make_pair(unique_name_t::npos, unique_name_t::npos);
       
    const auto begin_name = std::begin(name);
    return std::make_pair(
        std::distance(begin_name, first_parenthesis),
        std::distance(begin_name, next_char)
    );
}

std::vector<unique_name_t> extract_param_type_names(const unique_name_t& function_type_unique_name)
{
    BOOST_ASSERT(is_function_type_unique_name(function_type_unique_name));

    const auto splitters = find_function_type_splitters(function_type_unique_name);
    return extract_subtype_names(
        function_type_unique_name.substr(splitters.first, splitters.second - splitters.first)
    );
}

unique_name_t extract_return_type_name(const unique_name_t& function_type_unique_name)
{
    const auto splitters = find_function_type_splitters(function_type_unique_name);

    const auto return_type_begin = splitters.second + 1;
    return function_type_unique_name.substr(return_type_begin);
}

std::vector<unique_name_t> extract_subtype_names(const unique_name_t& multitype_unique_name)
{
    const auto& name = multitype_unique_name;

    std::vector<unique_name_t> results;

    // collect sutypes splitted by commas
    ParenthesesMatcher matcher;

    // find sub-types from next char of first '('
    auto next_char = boost::next(boost::find(name, '('));
    auto subtype_begin = std::distance(boost::begin(name), next_char);
    for( ; next_char != boost::end(name); ++next_char)
    {
        // find end of a subtype
        if((*next_char == ',' || *next_char == ')') && !matcher.in_parsing_name() && matcher.is_finished())
        {
            const auto subtype_end    = std::distance(boost::begin(name), next_char);
            const auto subtype_length = subtype_end - subtype_begin;

            if(subtype_length != 0)
            {
                results.emplace_back(
                    name.substr(subtype_begin, subtype_length)
                );
            }

            subtype_begin = subtype_end + 1;
        }

        // no more subtypes, terminate loop
        if(*next_char == ')' && matcher.is_finished())
            break;

        matcher.eat(*next_char);
    }

    return std::move(results);
}

namespace {

    unique_name_t get_header(unique_name_category_t category)
    {
        switch(category)
        {
        case unique_name_category_t::PACKAGE:
            return unique_name_t("package");
        case unique_name_category_t::CLASS:
            return unique_name_t("class");
        case unique_name_category_t::ENUM:
            return unique_name_t("enum");
        case unique_name_category_t::FUNCTION:
            return unique_name_t("function");
        default:
            break;
        }

        return unique_name_t();
    }

    std::pair<
        unique_name_t, // the header, empty indicates failure
        unique_name_t  // rest name
    >
    split_header(const unique_name_t& unique_name)
    {
        const auto first_blank = unique_name.find_first_of(' ');
        if(first_blank == unique_name_t::npos)
            return std::make_pair(unique_name_t(), unique_name);

        return std::make_pair(
            unique_name.substr(0, first_blank),
            unique_name.substr(first_blank + 1)
        );
    }

    unique_name_t add_header(const unique_name_t& name, unique_name_category_t category)
    {
        return get_header(category) + " " + name;
    }

} // anonymous namespace

unique_name_t extract_class_type_name(const unique_name_t& unique_name)
{
    if(!boost::algorithm::starts_with(unique_name, "type-method"))
        return unique_name_t();

    unique_name_t header, rest_name;
    std::tie(header, rest_name) = split_header(unique_name);
    if(header.empty())
        return unique_name_t();

    const unique_name_t class_type_end_indicator = ".(";
    const auto class_type_end = rest_name.find(class_type_end_indicator);
    return rest_name.substr(0, class_type_end);
}

unique_name_t extract_owner_unique_name(const unique_name_t& decl_unique_name)
{
    struct indicator_and_owner_category_t
    {
        unique_name_t          indicator;
        unique_name_category_t owner_category;
    };

    unique_name_t header, rest_name;
    std::tie(header, rest_name) = split_header(decl_unique_name);
    if(header.empty())
        return unique_name_t();

    static const std::unordered_map<
        unique_name_t,
        indicator_and_owner_category_t
    > indicator_and_owner_categories{
        { get_header(unique_name_category_t::CLASS)  , { "$c" , unique_name_category_t::PACKAGE  } },
        { get_header(unique_name_category_t::ENUM)   , { "$e" , unique_name_category_t::PACKAGE  } },
        { "function"                                 , { "$f" , unique_name_category_t::PACKAGE  } },
        { "method"                                   , { "$f" , unique_name_category_t::CLASS    } },
        { "attribute"                                , { "$v" , unique_name_category_t::CLASS    } },
        { "enumerator"                               , { "$v" , unique_name_category_t::ENUM     } },
        { "parameter"                                , { "$v" , unique_name_category_t::FUNCTION } },
        { "variable"                                 , { "$v" , unique_name_category_t::PACKAGE  } },
        { get_header(unique_name_category_t::TYPEDEF), { "$td", unique_name_category_t::PACKAGE  } }
    };

    // get sub unique name(owner's unique name) and add header to it
    const auto entry = indicator_and_owner_categories.find(header);
    if(entry == indicator_and_owner_categories.end())
        return unique_name_t();

    const auto& info = entry->second;

    const auto indicator_location = rest_name.find(info.indicator);
    return add_header(rest_name.substr(0, indicator_location), info.owner_category);
}

unique_name_category_t get_unique_name_category(const unique_name_t& unique_name)
{
    const auto first_blank = unique_name.find_first_of(' ');
    BOOST_ASSERT(first_blank != unique_name_t::npos && "can not get header from unique name");

    if(first_blank == unique_name_t::npos)
        return unique_name_category_t::NONE;

    const auto header = unique_name.substr(0, first_blank);

    static const std::unordered_map<unique_name_t, unique_name_category_t> categories{
        { "package"   , unique_name_category_t::PACKAGE  },
        { "class"     , unique_name_category_t::CLASS    },
        { "enum"      , unique_name_category_t::ENUM     },
        { "function"  , unique_name_category_t::FUNCTION },
        { "method"    , unique_name_category_t::FUNCTION },
        { "attribute" , unique_name_category_t::VARIABLE },
        { "enumerator", unique_name_category_t::VARIABLE },
        { "parameter" , unique_name_category_t::VARIABLE },
        { "variable"  , unique_name_category_t::VARIABLE },
        { "typedef"   , unique_name_category_t::TYPEDEF  },
    };

    const auto associated_category = categories.find(header);
    if(associated_category == categories.end())
        return unique_name_category_t::NONE;

    return associated_category->second;
}

} } } // namespace 'zillians::language::tree'
