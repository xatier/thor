/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <set>
#include <string>

#include <boost/algorithm/string/classification.hpp>
#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/cxx11/any_of.hpp>

#include "utility/UnicodeUtil.h"

#include "language/tree/ASTNode.h"
#include "language/tree/ASTNodeHelper.h"
#include "language/tree/basic/Annotations.h"
#include "language/tree/basic/Identifier.h"
#include "language/tree/module/Internal.h"
#include "language/tree/module/Package.h"
#include "language/tree/module/Source.h"

namespace zillians { namespace language { namespace tree {

namespace {

std::set<std::string> getDumppingSources()
{
    std::set<std::string> files_to_dump;

    if (const char* files_env = getenv("TS_DUMP_SOURCE_FILES"))
    {
        std::string files_env_str(files_env);

        boost::split(files_to_dump, files_env_str, boost::is_any_of(" "), boost::token_compress_on);
    }

    return files_to_dump;
}

}

Package::Package()
    : id(nullptr)
    , symbol_table(SymbolTable::ImportStatus::NOT_IMPORTED)
{
}

Package::Package(SimpleIdentifier* _id)
    : id(_id)
    , symbol_table(SymbolTable::ImportStatus::NOT_IMPORTED)
{
    BOOST_ASSERT(_id && "null identifier for package node is not allowed");

    id->parent = this;
}

bool Package::hasChild() const
{
    return !children.empty();
}

bool Package::hasSource() const
{
    return !sources.empty();
}

const unique_name_t& Package::getUniqueName() const noexcept
{
    return unique_name;
}

Package* Package::findPackage(const std::wstring& name)
{
    for(auto* child : children)
    {
        if(child->id->toString().compare(name) == 0)
            return child;
    }
    return NULL;
}

void Package::addPackage(Package* package)
{
    BOOST_ASSERT(package && "null child package for package node is not allowed");

    package->parent = this;
    children.push_back(package);
}

void Package::addSource(Source* source)
{
    BOOST_ASSERT(source && "null source object for package node is not allowed");

    source->parent = this;
    sources.push_back(source);
}

bool Package::isRoot() const
{
    return !isa<Package>(parent);
}

Package* Package::getRoot()
{
    if(isRoot())
    {
        return this;
    }
    else
    {
        BOOST_ASSERT(isa<Package>(parent) && "parent of package must be package if it's not root package");

        return cast<Package>(parent)->getRoot();
    }
}

void Package::markImported(bool is_imported)
{
    for(Package* child: children)
    {
        BOOST_ASSERT(child && "null pointer exception");

        child->markImported(is_imported);
    }

    for(Source* source: sources)
    {
        BOOST_ASSERT(source && "null pointer exception");

        source->is_imported = is_imported;
    }
}

bool Package::isEqualImpl(const ASTNode& rhs) const
{
    AST_NODE_IS_EQUAL(
        (id         )
        (children   )
        (sources    )
    );
}

bool Package::replaceUseWith(const ASTNode& from, const ASTNode& to, bool update_parent/* = true*/)
{
    AST_NODE_REPLACE(
        result,
        (id         )
        (children   )
        (sources    )
    );
}

Package* Package::clone() const
{
    Package* cloned = new Package((id) ? id->clone() : NULL);

    for(auto* child : children)
        cloned->addPackage(child->clone());

    for(auto* source : sources)
        cloned->addSource(source->clone());

    if (annotations)
        cloned->setAnnotations(annotations->clone());

    return cloned;
}

std::wostream& Package::toSource(std::wostream& output, unsigned indent/* = 0*/) const
{
    output << out_indent(indent) << L"// Package : ";

    for (const Package* package : ASTNodeHelper::getParentScopePackages(this))
    {
        BOOST_ASSERT(package && "null pointer exception");
        BOOST_ASSERT(package->id && "null pointer exception");

        output << out_source(*package->id, indent) << L'.';
    }

    BOOST_ASSERT(id && "null pointer exception");

    output << out_source(*id, indent) << std::endl;

    for (const Package* child : children)
        output << out_source(*child, indent) << std::endl;

    {
        const std::set<std::string>& dumpping_sources = getDumppingSources();

        for (const Source* source : sources)
        {
            if (dumpping_sources.empty() || dumpping_sources.count(source->filename))
                output << out_source(*source, indent) << std::endl;
            else
                output << out_indent(indent) << L"// Source : " << s_to_ws(source->filename) << L" : not dump" << std::endl;
        }
    }

    return output;
}

bool Package::merge(Package& rhs)
{
    if (!id->isEqual(*rhs.id))
        return false;

    if(!sources.empty() && !rhs.sources.empty())
        return false;

    std::vector<Package*> not_merged_packages;

    for(auto* right_child : rhs.children)
    {
        bool merged = false;
        for(auto* left_child : children)
        {
            if(left_child->id->isEqual(*right_child->id))
            {
                if(!left_child->merge(*right_child))
                {
                    return false;
                }
                else
                {
                    merged = true;
                    break;
                }
            }
        }

        if(!merged)
        {
            not_merged_packages.push_back(right_child);
        }
    }

    for(auto& not_merged : not_merged_packages)
    {
        addPackage(not_merged);
    }

    for(auto* right_source : rhs.sources)
    {
        const auto& has_conflict_source = boost::algorithm::any_of(
            sources,
            [right_source](const Source*const other)
            {
                return right_source->filename == other->filename;
            }
        );

        if (has_conflict_source)
            return false;

        addSource(right_source);
    }

    {
        auto*const     annos =     getAnnotations();
        auto*const rhs_annos = rhs.getAnnotations();

        if(annos != nullptr && rhs_annos != nullptr)
        {
            annos->merge(*rhs_annos);
        }
        else if(annos == nullptr && rhs_annos != nullptr)
        {
            setAnnotations(rhs_annos);
        }
    }

    return true;
}

void Package::setImportStatus(const SymbolTable::ImportStatus::value tag)
{
    symbol_table.set_import_status(tag);
}

SymbolTable::ImportStatus::value Package::getImportStatus() const
{
    return symbol_table.get_import_status();
}

} } }
