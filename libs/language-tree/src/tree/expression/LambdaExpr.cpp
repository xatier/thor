/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <ostream>

#include <boost/assert.hpp>

#include "language/tree/ASTNode.h"
#include "language/tree/declaration/FunctionDecl.h"
#include "language/tree/expression/LambdaExpr.h"

namespace zillians { namespace language { namespace tree {

LambdaExpr::LambdaExpr()
    : lambda(nullptr)
{
}

LambdaExpr::LambdaExpr(FunctionDecl* lambda)
    : lambda(lambda)
{
    BOOST_ASSERT(lambda != nullptr && "null pointer exception");

    lambda->parent = this;
}

FunctionDecl* LambdaExpr::getLambda() const noexcept
{
    return lambda;
}

bool LambdaExpr::hasValue() const
{
    return true;
}

bool LambdaExpr::isRValue() const
{
    return true;
}

bool LambdaExpr::isEqualImpl(const ASTNode& rhs) const
{
    AST_NODE_IS_EQUAL(
        (lambda)
    );
}

bool LambdaExpr::replaceUseWith(const ASTNode& from, const ASTNode& to, bool update_parent/* = true*/)
{
    AST_NODE_REPLACE(
        result,
        (lambda)
    );
}

LambdaExpr* LambdaExpr::clone() const
{
    BOOST_ASSERT(lambda != nullptr && "null pointer exception");

    return new LambdaExpr(lambda->clone());
}

std::wostream& LambdaExpr::toSource(std::wostream& output, unsigned indent/* = 0*/) const
{
    return output << out_source(*lambda, indent);
}

} } }
