/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <ostream>

#include <boost/assert.hpp>
#include <boost/preprocessor/seq/elem.hpp>
#include <boost/preprocessor/seq/for_each.hpp>

#include "language/tree/ASTNode.h"
#include "language/tree/expression/Expression.h"
#include "language/tree/expression/BinaryExpr.h"

namespace zillians { namespace language { namespace tree {

const wchar_t* BinaryExpr::OpCode::toString(type op)
{
    switch(op)
    {
    case ASSIGN: return L"=";
    case LSHIFT_ASSIGN: return L"<<=";
    case RSHIFT_ASSIGN: return L">>=";
    case ADD_ASSIGN: return L"+=";
    case SUB_ASSIGN: return L"-=";
    case MUL_ASSIGN: return L"*=";
    case DIV_ASSIGN: return L"/=";
    case MOD_ASSIGN: return L"%=";
    case AND_ASSIGN: return L"&=";
    case OR_ASSIGN: return L"|=";
    case XOR_ASSIGN: return L"^=";
    case ARITHMETIC_ADD: return L"+";
    case ARITHMETIC_SUB: return L"-";
    case ARITHMETIC_MUL: return L"*";
    case ARITHMETIC_DIV: return L"/";
    case ARITHMETIC_MOD: return L"%";
    case BINARY_AND: return L"&";
    case BINARY_OR: return L"|";
    case BINARY_XOR: return L"^";
    case BINARY_LSHIFT: return L"<<";
    case BINARY_RSHIFT: return L">>";
    case LOGICAL_AND: return L"&&";
    case LOGICAL_OR: return L"||";
    case COMPARE_EQ: return L"==";
    case COMPARE_NE: return L"!=";
    case COMPARE_GT: return L">";
    case COMPARE_LT: return L"<";
    case COMPARE_GE: return L">=";
    case COMPARE_LE: return L"<=";
    case RANGE_ELLIPSIS: return L"...";
    case INVALID: return L"<invalid>";
    default: UNREACHABLE_CODE(); return L"<non_op>";
    }
}

auto BinaryExpr::OpCode::decomposeAssignment(type t) -> type
{
    switch(t)
    {
    case ADD_ASSIGN: return ARITHMETIC_ADD;
    case SUB_ASSIGN: return ARITHMETIC_SUB;
    case MUL_ASSIGN: return ARITHMETIC_MUL;
    case DIV_ASSIGN: return ARITHMETIC_DIV;
    case MOD_ASSIGN: return ARITHMETIC_MOD;
    case AND_ASSIGN: return BINARY_AND;
    case OR_ASSIGN:  return BINARY_OR;
    case XOR_ASSIGN: return BINARY_XOR;
    case RSHIFT_ASSIGN: return BINARY_RSHIFT;
    case LSHIFT_ASSIGN: return BINARY_LSHIFT;
    default: return INVALID;
    }
}

BinaryExpr::BinaryExpr() { }

BinaryExpr::BinaryExpr(OpCode::type opcode, Expression* left, Expression* right) : opcode(opcode), left(left), right(right)
{
    BOOST_ASSERT(left && "null left child for binary expression is not allowed");
    BOOST_ASSERT(right && "null right child for binary expression is not allowed");

    left->parent = this;
    right->parent = this;
}

#define BINARY_EXPR_OP_MAPPING     \
    ((       ASSIGN      )(  '=')) \
    ((LSHIFT_ASSIGN      )('<<=')) \
    ((RSHIFT_ASSIGN      )('>>=')) \
    ((   ADD_ASSIGN      )( '+=')) \
    ((   SUB_ASSIGN      )( '-=')) \
    ((   MUL_ASSIGN      )( '*=')) \
    ((   DIV_ASSIGN      )( '/=')) \
    ((   MOD_ASSIGN      )( '%=')) \
    ((   AND_ASSIGN      )( '&=')) \
    ((    OR_ASSIGN      )( '|=')) \
    ((   XOR_ASSIGN      )( '^=')) \
    ((ARITHMETIC_ADD     )( '+' )) \
    ((ARITHMETIC_SUB     )( '-' )) \
    ((ARITHMETIC_MUL     )( '*' )) \
    ((ARITHMETIC_DIV     )( '/' )) \
    ((ARITHMETIC_MOD     )( '%' )) \
    ((    BINARY_AND     )( '&' )) \
    ((    BINARY_OR      )( '|' )) \
    ((    BINARY_XOR     )( '^' )) \
    ((    BINARY_LSHIFT  )('<<' )) \
    ((    BINARY_RSHIFT  )('>>' )) \
    ((   LOGICAL_AND     )('&&' )) \
    ((   LOGICAL_OR      )('||' )) \
    ((   COMPARE_EQ      )( '==')) \
    ((   COMPARE_NE      )( '!=')) \
    ((   COMPARE_GT      )( '>' )) \
    ((   COMPARE_GE      )( '>=')) \
    ((   COMPARE_LE      )( '<=')) \
    ((   COMPARE_LT      )( '<' )) \
    ((     RANGE_ELLIPSIS)('...'))

#define BINARY_EXPR_OP_MAPPING_DEF(r, _, map)                          \
    template<>                                                         \
    BinaryExpr* BinaryExpr::create<BOOST_PP_SEQ_ELEM(1, map)>(         \
        Expression* lhs,                                               \
        Expression* rhs)                                               \
    {                                                                  \
        return new BinaryExpr(                                         \
            OpCode::BOOST_PP_SEQ_ELEM(0, map),                         \
            lhs, rhs                                                   \
        );                                                             \
    }

BOOST_PP_SEQ_FOR_EACH(BINARY_EXPR_OP_MAPPING_DEF, _, BINARY_EXPR_OP_MAPPING)

#undef BINARY_EXPR_OP_MAPPING_DEF
#undef BINARY_EXPR_OP_MAPPING

bool BinaryExpr::isArithmetic() const
{
    if( opcode == OpCode::ARITHMETIC_ADD ||
        opcode == OpCode::ARITHMETIC_SUB ||
        opcode == OpCode::ARITHMETIC_MUL ||
        opcode == OpCode::ARITHMETIC_DIV ||
        opcode == OpCode::ARITHMETIC_MOD ||
        opcode == OpCode::RSHIFT_ASSIGN ||
        opcode == OpCode::LSHIFT_ASSIGN ||
        opcode == OpCode::ADD_ASSIGN ||
        opcode == OpCode::SUB_ASSIGN ||
        opcode == OpCode::MUL_ASSIGN ||
        opcode == OpCode::DIV_ASSIGN ||
        opcode == OpCode::MOD_ASSIGN ||
        opcode == OpCode::AND_ASSIGN ||
        opcode == OpCode::OR_ASSIGN  ||
        opcode == OpCode::XOR_ASSIGN)
        return true;
    else
        return false;
}

bool BinaryExpr::isAssignment() const
{
    if( opcode == OpCode::ASSIGN ||
        opcode == OpCode::RSHIFT_ASSIGN ||
        opcode == OpCode::LSHIFT_ASSIGN ||
        opcode == OpCode::ADD_ASSIGN ||
        opcode == OpCode::SUB_ASSIGN ||
        opcode == OpCode::MUL_ASSIGN ||
        opcode == OpCode::DIV_ASSIGN ||
        opcode == OpCode::MOD_ASSIGN ||
        opcode == OpCode::AND_ASSIGN ||
        opcode == OpCode::OR_ASSIGN  ||
        opcode == OpCode::XOR_ASSIGN )
        return true;
    else
        return false;
}

bool BinaryExpr::isShift() const
{
    if( opcode == OpCode::BINARY_LSHIFT ||
        opcode == OpCode::BINARY_RSHIFT )
        return true;
    else
        return false;
}

bool BinaryExpr::isBinary() const
{
    if( opcode == OpCode::BINARY_AND ||
        opcode == OpCode::BINARY_OR ||
        opcode == OpCode::BINARY_XOR ||
        opcode == OpCode::BINARY_LSHIFT ||
        opcode == OpCode::BINARY_RSHIFT )
        return true;
    else
        return false;
}

bool BinaryExpr::isLogical() const
{
    if( opcode == OpCode::LOGICAL_AND ||
        opcode == OpCode::LOGICAL_OR )
        return true;
    else
        return false;
}

bool BinaryExpr::isComparison() const
{
    if( opcode == OpCode::COMPARE_EQ ||
        opcode == OpCode::COMPARE_NE ||
        opcode == OpCode::COMPARE_GT ||
        opcode == OpCode::COMPARE_LT ||
        opcode == OpCode::COMPARE_GE ||
        opcode == OpCode::COMPARE_LE )
        return true;
    else
        return false;
}

bool BinaryExpr::isEqualComparison() const
{
    if( opcode == OpCode::COMPARE_EQ ||
        opcode == OpCode::COMPARE_NE ||
        opcode == OpCode::COMPARE_GE ||
        opcode == OpCode::COMPARE_LE )
        return true;
    else
        return false;
}

bool BinaryExpr::isRighAssociative() const
{
    return isAssignment();
}

bool BinaryExpr::isLeftAssociative() const
{
    return !isRighAssociative();
}

bool BinaryExpr::hasValue() const
{
    switch(opcode)
    {
    case OpCode::RANGE_ELLIPSIS:
    case OpCode::INVALID       :
        return false;
    default:
        return true;
    }
}

bool BinaryExpr::isRValue() const
{
    switch(opcode)
    {
    case OpCode::ASSIGN:
    case OpCode::RSHIFT_ASSIGN:
    case OpCode::LSHIFT_ASSIGN:
    case OpCode::ADD_ASSIGN:
    case OpCode::SUB_ASSIGN:
    case OpCode::MUL_ASSIGN:
    case OpCode::DIV_ASSIGN:
    case OpCode::MOD_ASSIGN:
    case OpCode::AND_ASSIGN:
    case OpCode::OR_ASSIGN:
    case OpCode::XOR_ASSIGN:
        return false;
    case OpCode::ARITHMETIC_ADD:
    case OpCode::ARITHMETIC_SUB:
    case OpCode::ARITHMETIC_MUL:
    case OpCode::ARITHMETIC_DIV:
    case OpCode::ARITHMETIC_MOD:
    case OpCode::BINARY_AND:
    case OpCode::BINARY_OR:
    case OpCode::BINARY_XOR:
    case OpCode::BINARY_LSHIFT:
    case OpCode::BINARY_RSHIFT:
    case OpCode::LOGICAL_AND:
    case OpCode::LOGICAL_OR:
    case OpCode::COMPARE_EQ:
    case OpCode::COMPARE_NE:
    case OpCode::COMPARE_GT:
    case OpCode::COMPARE_LT:
    case OpCode::COMPARE_GE:
    case OpCode::COMPARE_LE:
    case OpCode::RANGE_ELLIPSIS:
    case OpCode::INVALID:
        return true;
    default: UNREACHABLE_CODE(); return false;
    }
}

bool BinaryExpr::isEqualImpl(const ASTNode& rhs) const
{
    AST_NODE_IS_EQUAL(
        (opcode)
        (left  )
        (right )
    );
}

bool BinaryExpr::replaceUseWith(const ASTNode& from, const ASTNode& to, bool update_parent/* = true*/)
{
    AST_NODE_REPLACE(
        result,
        (left )
        (right)
    );
}

BinaryExpr* BinaryExpr::clone() const
{
    return new BinaryExpr(
        opcode,
        clone_or_null(left ),
        clone_or_null(right)
    );
}

std::wostream& BinaryExpr::toSource(std::wostream& output, unsigned indent/* = 0*/) const
{
    BOOST_ASSERT(left && "null pointer exception");
    BOOST_ASSERT(right && "null pointer exception");

    output << L'(' << out_source(*left, indent) << L") " << OpCode::toString(opcode) << L" (" << out_source(*right, indent) << L')';

    return output;
}

} } }
