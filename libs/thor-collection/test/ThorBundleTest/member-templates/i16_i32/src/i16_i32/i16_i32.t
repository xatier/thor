/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
import top;

function instantiate_i16_i32_members<T>(t: T): void
{
    t.template_member<int16>();
    t.template_member<int32>();
}

function get_non_template_class(): Object
{
    const cls = new top.non_template_class();

    instantiate_i16_i32_members(cls);

    return cls;
}

function get_template_i16_class(): Object
{
    const cls = new top.template_class<int16>();

    instantiate_i16_i32_members(cls);

    return cls;
}

function get_template_i32_class(): Object
{
    const cls = new top.template_class<int32>();

    instantiate_i16_i32_members(cls);

    return cls;
}
