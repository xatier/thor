/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
import top;
import i16_i32;
import i32_i64;

function test_members(obj: Object): bool
{
    const non_template = cast<top.non_template_class    >(obj);
    const template_i16 = cast<top.template_class<int16> >(obj);
    const template_i32 = cast<top.template_class<int32> >(obj);
    const template_i64 = cast<top.template_class<int64> >(obj);

    if (non_template != null)
        return test_members_impl(non_template);
    else if (template_i16 != null)
        return test_members_impl(template_i16);
    else if (template_i32 != null)
        return test_members_impl(template_i32);
    else if (template_i64 != null)
        return test_members_impl(template_i64);
    else
        return false;
}

function test_members_impl<T>(t: T): bool
{
    t.non_template_member   ();
    t.template_member<int8 >();
    t.template_member<int16>();
    t.template_member<int32>();
    t.template_member<int64>();

    return true;
}

@entry
task do_test()
{
    // force dynamic casting by avoiding type inference
    const top_non_template: Object = new top.non_template_class();
    const top_template_i16: Object = new top.template_class<int16>();
    const top_template_i32: Object = new top.template_class<int32>();
    const top_template_i64: Object = new top.template_class<int64>();

    const i16_i32_non_template: Object = i16_i32.get_non_template_class();
    const i16_i32_template_i16: Object = i16_i32.get_template_i16_class();
    const i16_i32_template_i32: Object = i16_i32.get_template_i32_class();

    const i32_i64_non_template: Object = i32_i64.get_non_template_class();
    const i32_i64_template_i32: Object = i32_i64.get_template_i32_class();
    const i32_i64_template_i64: Object = i32_i64.get_template_i64_class();

    if (cast<Object                    >(top_non_template    ) == null) exit( 1);
    if (cast<Object                    >(top_template_i16    ) == null) exit( 2);
    if (cast<Object                    >(top_template_i32    ) == null) exit( 3);
    if (cast<Object                    >(top_template_i64    ) == null) exit( 4);

    if (cast<Object                    >(i16_i32_non_template) == null) exit( 5);
    if (cast<Object                    >(i16_i32_template_i16) == null) exit( 6);
    if (cast<Object                    >(i16_i32_template_i32) == null) exit( 7);

    if (cast<Object                    >(i32_i64_non_template) == null) exit( 8);
    if (cast<Object                    >(i32_i64_template_i32) == null) exit( 9);
    if (cast<Object                    >(i32_i64_template_i64) == null) exit(10);

    if (cast<top.non_template_class    >(top_non_template    ) == null) exit(11);
    if (cast<top.non_template_class    >(top_template_i16    ) != null) exit(12);
    if (cast<top.non_template_class    >(top_template_i32    ) != null) exit(13);
    if (cast<top.non_template_class    >(top_template_i64    ) != null) exit(14);

    if (cast<top.non_template_class    >(i16_i32_non_template) == null) exit(15);
    if (cast<top.non_template_class    >(i16_i32_template_i16) != null) exit(16);
    if (cast<top.non_template_class    >(i16_i32_template_i32) != null) exit(17);

    if (cast<top.non_template_class    >(i32_i64_non_template) == null) exit(18);
    if (cast<top.non_template_class    >(i32_i64_template_i32) != null) exit(19);
    if (cast<top.non_template_class    >(i32_i64_template_i64) != null) exit(20);

    if (cast<top.template_class<int16> >(top_non_template    ) != null) exit(31);
    if (cast<top.template_class<int16> >(top_template_i16    ) == null) exit(32);
    if (cast<top.template_class<int16> >(top_template_i32    ) != null) exit(33);
    if (cast<top.template_class<int16> >(top_template_i64    ) != null) exit(34);

    if (cast<top.template_class<int16> >(i16_i32_non_template) != null) exit(35);
    if (cast<top.template_class<int16> >(i16_i32_template_i16) == null) exit(36);
    if (cast<top.template_class<int16> >(i16_i32_template_i32) != null) exit(37);

    if (cast<top.template_class<int16> >(i32_i64_non_template) != null) exit(38);
    if (cast<top.template_class<int16> >(i32_i64_template_i32) != null) exit(39);
    if (cast<top.template_class<int16> >(i32_i64_template_i64) != null) exit(40);

    if (cast<top.template_class<int32> >(top_non_template    ) != null) exit(41);
    if (cast<top.template_class<int32> >(top_template_i16    ) != null) exit(42);
    if (cast<top.template_class<int32> >(top_template_i32    ) == null) exit(43);
    if (cast<top.template_class<int32> >(top_template_i64    ) != null) exit(44);

    if (cast<top.template_class<int32> >(i16_i32_non_template) != null) exit(45);
    if (cast<top.template_class<int32> >(i16_i32_template_i16) != null) exit(46);
    if (cast<top.template_class<int32> >(i16_i32_template_i32) == null) exit(47);

    if (cast<top.template_class<int32> >(i32_i64_non_template) != null) exit(48);
    if (cast<top.template_class<int32> >(i32_i64_template_i32) == null) exit(49);
    if (cast<top.template_class<int32> >(i32_i64_template_i64) != null) exit(50);

    if (cast<top.template_class<int64> >(top_non_template    ) != null) exit(51);
    if (cast<top.template_class<int64> >(top_template_i16    ) != null) exit(52);
    if (cast<top.template_class<int64> >(top_template_i32    ) != null) exit(53);
    if (cast<top.template_class<int64> >(top_template_i64    ) == null) exit(54);

    if (cast<top.template_class<int64> >(i16_i32_non_template) != null) exit(55);
    if (cast<top.template_class<int64> >(i16_i32_template_i16) != null) exit(56);
    if (cast<top.template_class<int64> >(i16_i32_template_i32) != null) exit(57);

    if (cast<top.template_class<int64> >(i32_i64_non_template) != null) exit(58);
    if (cast<top.template_class<int64> >(i32_i64_template_i32) != null) exit(59);
    if (cast<top.template_class<int64> >(i32_i64_template_i64) == null) exit(60);

    if (!test_members(top_non_template    )) exit(61);
    if (!test_members(top_template_i16    )) exit(62);
    if (!test_members(top_template_i32    )) exit(63);
    if (!test_members(top_template_i64    )) exit(64);

    if (!test_members(i16_i32_non_template)) exit(65);
    if (!test_members(i16_i32_template_i16)) exit(66);
    if (!test_members(i16_i32_template_i32)) exit(67);

    if (!test_members(i32_i64_non_template)) exit(68);
    if (!test_members(i32_i64_template_i32)) exit(69);
    if (!test_members(i32_i64_template_i64)) exit(70);

    exit(0);
}
