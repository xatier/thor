/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
enum  decl_enum              { dummy_value }
class decl_class_simpl       {             }
class decl_class_template<T> {             }

@entry
task test_main(): void {
    callee(cast<  int8   >(0));
    callee(cast<  int16  >(0));
    callee(cast<  int32  >(0));
    callee(cast<  int64  >(0));
    callee(cast<float32  >(0));
    callee(cast<float64  >(0));
    callee(cast<decl_enum>(0));

    callee(cast<decl_class_simpl                      >(null));
    callee(cast<decl_class_template<  void          > >(null));
    callee(cast<decl_class_template<  int8          > >(null));
    callee(cast<decl_class_template<  int16         > >(null));
    callee(cast<decl_class_template<  int32         > >(null));
    callee(cast<decl_class_template<  int64         > >(null));
    callee(cast<decl_class_template<float32         > >(null));
    callee(cast<decl_class_template<float64         > >(null));
    callee(cast<decl_class_template<decl_enum       > >(null));
    callee(cast<decl_class_template<decl_class_simpl> >(null));

    callee(cast<decl_class_template<decl_enum                             > >(null));
    callee(cast<decl_class_template<decl_class_simpl                      > >(null));
    callee(cast<decl_class_template<decl_class_template<  void          > > >(null));
    callee(cast<decl_class_template<decl_class_template<  int8          > > >(null));
    callee(cast<decl_class_template<decl_class_template<  int16         > > >(null));
    callee(cast<decl_class_template<decl_class_template<  int32         > > >(null));
    callee(cast<decl_class_template<decl_class_template<  int64         > > >(null));
    callee(cast<decl_class_template<decl_class_template<float32         > > >(null));
    callee(cast<decl_class_template<decl_class_template<float64         > > >(null));
    callee(cast<decl_class_template<decl_class_template<decl_enum       > > >(null));
    callee(cast<decl_class_template<decl_class_template<decl_class_simpl> > >(null));

    exit(0);
}
