/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
#ifndef NATIVE_H
#define NATIVE_H

struct used_inner {
    bool   b  ;
    char   i8 ;
    short  i16;
    int    i32;
    long   i64;
    float  f32;
    double f64;
};

namespace ns {

    struct           decl_inner {}; // non-used definition to ensure the name coliision is prevented
    struct ori_class_decl_inner {}; // non-used definition to ensure the name coliision is prevented

    class ori_class {
    public:
        struct decl_inner {
            bool   b  ;
            char   i8 ;
            short  i16;
            int    i32;
            long   i64;
            float  f32;
            double f64;
        };

        used_inner inner_1;
        decl_inner inner_2;
        struct {
            bool   b  ;
            char   i8 ;
            short  i16;
            int    i32;
            long   i64;
            float  f32;
            double f64;
        }          inner_3;
    };

}

struct ori_struct {
        used_inner inner_1;
    ns::decl_inner inner_2;
    struct {
        bool   b  ;
        char   i8 ;
        short  i16;
        int    i32;
        long   i64;
        float  f32;
        double f64;
    }              inner_3;
};

#endif /* NATIVE_H */

