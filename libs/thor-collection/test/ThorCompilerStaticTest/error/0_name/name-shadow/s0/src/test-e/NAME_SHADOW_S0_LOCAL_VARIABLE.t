/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
// test shadowing on local variables

function test_value_1_a(): void
{
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE_PREV"} }
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE_PREV"} }
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE_PREV"} }
    var value_1_a_a: int32 = 0;
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE_PREV"} }
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE_PREV"} }
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE_PREV"} }
    var value_1_a_b: int32 = 0;

    {
        @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE_PREV"} }
        @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE_PREV"} }
        @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE", parameters = { id = "value_1_a_a"} } }
        var value_1_a_a: int32 = 0;

        for(
            @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE_PREV"} }
            @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE", parameters = { id = "value_1_a_b"} } }
            var value_1_a_b: int32 = 0; ;
        )
        {
            @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE", parameters = { id = "value_1_a_a"} } }
            var value_1_a_a: int32 = 0;

            for(
                @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE", parameters = { id = "value_1_a_b"} } }
                var value_1_a_b: int32 = 0; ;
            )
            {
            }
        }

        {
            @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE", parameters = { id = "value_1_a_a"} } }
            var value_1_a_a: int32 = 0;
            @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE", parameters = { id = "value_1_a_b"} } }
            var value_1_a_b: int32 = 0;
        }
    }
}

function test_value_1_b<T>(): void
{
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE_PREV"} }
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE_PREV"} }
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE_PREV"} }
    var value_1_b_a: int32 = 0;
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE_PREV"} }
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE_PREV"} }
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE_PREV"} }
    var value_1_b_b: int32 = 0;

    {
        @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE_PREV"} }
        @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE_PREV"} }
        @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE", parameters = { id = "value_1_b_a"} } }
        var value_1_b_a: int32 = 0;

        for(
            @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE_PREV"} }
            @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE", parameters = { id = "value_1_b_b"} } }
            var value_1_b_b: int32 = 0; ;
        )
        {
            @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE", parameters = { id = "value_1_b_a"} } }
            var value_1_b_a: int32 = 0;

            for(
                @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE", parameters = { id = "value_1_b_b"} } }
                var value_1_b_b: int32 = 0; ;
            )
            {
            }
        }

        {
            @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE", parameters = { id = "value_1_b_a"} } }
            var value_1_b_a: int32 = 0;
            @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE", parameters = { id = "value_1_b_b"} } }
            var value_1_b_b: int32 = 0;
        }
    }
}
