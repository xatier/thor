/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
// test shadowing on parent member variable

// 1.parent member variable + function template parameter
class test_value_1_parent
{
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE_PREV"} }
    var value_1: int32 = 0;
}

class test_value_1 extends test_value_1_parent
{
    function test_value_1<
        @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE", parameters = { id = "value_1"} } }
        value_1
    >(): void {}
}

// 2.parent member variable + function parameter
class test_value_2_parent
{
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE_PREV"} }
    var value_2_a: int32 = 0;
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE_PREV"} }
    var value_2_b: int32 = 0;
}

class test_value_2 extends test_value_2_parent
{
    function test_value_2_a<T>(
        @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE", parameters = { id = "value_2_a"} } }
        value_2_a: int32
    ): void {}

    function test_value_2_b(
        @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE", parameters = { id = "value_2_b"} } }
        value_2_b: int32
    ): void {}
}

// 2.parent member variable + local variables
class test_value_3_parent
{
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE_PREV"} }
    var value_3_a_a: int32 = 0;
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE_PREV"} }
    var value_3_a_b: int32 = 0;
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE_PREV"} }
    var value_3_a_c: int32 = 0;

    @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE_PREV"} }
    var value_3_b_a: int32 = 0;
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE_PREV"} }
    var value_3_b_b: int32 = 0;
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE_PREV"} }
    var value_3_b_c: int32 = 0;

}

class test_value_3 extends test_value_3_parent
{
    function test_value_3_a<T>(): void
    {
        @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE", parameters = { id = "value_3_a_a"} } }
        var value_3_a_a: int32;

        {
            @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE", parameters = { id = "value_3_a_b"} } }
            var value_3_a_b: int32;
        }

        for(
            @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE", parameters = { id = "value_3_a_c"} } }
            var value_3_a_c: int32; ;
        );
    }

    function test_value_3_b(): void
    {
        @static_test { expect_message = { level = "LEVEL_WARNING", id = "VAR_NEVER_USED", parameters = { var_id = "value_3_b_a"} } }
        @static_test { expect_message = { level = "LEVEL_ERROR"  , id = "SHADOW_DECLARE", parameters = {     id = "value_3_b_a"} } }
        var value_3_b_a: int32;

        {
            @static_test { expect_message = { level = "LEVEL_WARNING", id = "VAR_NEVER_USED", parameters = { var_id = "value_3_b_b"} } }
            @static_test { expect_message = { level = "LEVEL_ERROR"  , id = "SHADOW_DECLARE", parameters = {     id = "value_3_b_b"} } }
            var value_3_b_b: int32;
        }

        for(
            @static_test { expect_message = { level = "LEVEL_WARNING", id = "VAR_NEVER_USED", parameters = { var_id = "value_3_b_c"} } }
            @static_test { expect_message = { level = "LEVEL_ERROR"  , id = "SHADOW_DECLARE", parameters = {     id = "value_3_b_c"} } }
            var value_3_b_c: int32; ;
        );
    }
}
