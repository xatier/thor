/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
@static_test { expect_message={ level="LEVEL_ERROR", id="INVALID_TEMPLATE_DEFAULT_ARGUMENT_ORDER" } }
class Foo1<X = bool, Y>{}

// #1 template class
@static_test { expect_message={ level="LEVEL_ERROR", id="INVALID_TEMPLATE_DEFAULT_ARGUMENT_ORDER" } }
class Foo2<X, Y = bool, Z, W = int32>{}

// #2 template class 
@static_test { expect_message={ level="LEVEL_ERROR", id="INVALID_TEMPLATE_DEFAULT_ARGUMENT_ORDER" } }
class Foo3<X = bool, Y, Z = int32, W = int32>{}

// #3 template class
class Foo4<X, Y, Z, W>{}

@static_test { expect_message={ level="LEVEL_ERROR", id="INVALID_TEMPLATE_DEFAULT_ARGUMENT_IN_SPECIALIZATION" } }
@static_test { expect_message={ level="LEVEL_ERROR", id="INVALID_TEMPLATE_DEFAULT_ARGUMENT_ORDER" } }
class Foo4<X, Y = bool, Z, W: int32>{}

@static_test { expect_message={ level="LEVEL_ERROR", id="INVALID_TEMPLATE_DEFAULT_ARGUMENT_IN_SPECIALIZATION" } }
class Foo4<X, Y, Z = int32, W: int32>{}

@static_test { expect_message={ level="LEVEL_ERROR", id="INVALID_TEMPLATE_DEFAULT_ARGUMENT_IN_SPECIALIZATION" } }
class Foo4<X = bool, Y: int32, Z = bool, W: int32>{}

// #4 template function
function Foo5<V, W, X, Y, Z >(): int32 { }

@static_test { expect_message={ level="LEVEL_ERROR", id="INVALID_TEMPLATE_DEFAULT_ARGUMENT_ORDER" } }
function Foo5<V = bool, W, X, Y, Z >(): int32 { }

@static_test { expect_message={ level="LEVEL_ERROR", id="INVALID_TEMPLATE_DEFAULT_ARGUMENT_ORDER" } }
function Foo5<V, W = bool, X, Y, Z >(): int32 { }

function Foo5<V, W, X, Y, Z = bool>(): int32 { }

function Foo6<V, W = int8, X = int16, Y = int32, Z = int64>(): int32 { }

//test INVALID_TEMPLATE_DEFAULT_ARGUMENT_IN_SPECIALIZATION
class Boo1<X, Y=int32>{} 
class Boo1<X:int32, Y>{}

class Boo2<X, Y>{}
@static_test { expect_message={ level="LEVEL_ERROR", id="INVALID_TEMPLATE_DEFAULT_ARGUMENT_IN_SPECIALIZATION" } }
class Boo2<X:int32, Y=int32>{}

function Boo3<X, Y=int32>(): void {}
function Boo3<X:int32, Y>(): void {}

function Boo4<X, Y>(): void {}
@static_test { expect_message={ level="LEVEL_ERROR", id="INVALID_TEMPLATE_DEFAULT_ARGUMENT_IN_SPECIALIZATION" } }
function Boo4<X:int32, Y=int32>(): void {}

