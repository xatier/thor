/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
function main_test(): int32
{
    var local_1_1: function(           ): void;
    var local_1_2: function(int8       ): void;
    var local_1_3: function(int8, int32): void;

    var local_2_1: function(           ): int8;
    var local_2_2: function(int8       ): int8;
    var local_2_3: function(int8, int32): int8;

    @static_test { expect_message = { level = "LEVEL_ERROR", id = "FUNCTION_TYPE_WITHOUT_RETURN_TYPE" } } var local_3_1: function(           )      ;
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "FUNCTION_TYPE_WITHOUT_RETURN_TYPE" } } var local_3_2: function(int8       )      ;
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "FUNCTION_TYPE_WITHOUT_RETURN_TYPE" } } var local_3_3: function(int8, int32)      ;

    return 0;
}

