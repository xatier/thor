/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
function f1() : void
{
}

function f2() : int32
{
    return 0;
}

function fff1() : int32
{
    @static_test {
        expect_message = {
            level      = "LEVEL_ERROR",
            id         = "UNEXPECTED_RETURN_VALUE",
            parameters = {
                function_name = "fff1",
                return_type   = "int32"
            }
        }
    }
    return;
}

function fff2() : int8
{
     @static_test {
        expect_message = {
            level      = "LEVEL_ERROR",
            id         = "UNEXPECTED_RETURN_VALUE",
            parameters = {
                function_name = "fff2",
                return_type   = "int8"
            }
        }
    }
    return f1();
}

function fff3( cond: int32) : int32
{
    if (cond == 0)
    {
        return f2();
    }
    else
    {
        @static_test {
            expect_message = {
                level      = "LEVEL_ERROR",
                id         = "UNEXPECTED_RETURN_VALUE",
                parameters = {
                    function_name = "fff3",
                    return_type   = "int32"
                }
            }
        }
        return f1();
    }
}

function fff4():void
{
    @static_test {
        expect_message = {
            level = "LEVEL_ERROR",
            id    = "UNEXPECTED_RETURN_VALUE",
            parameters = {
                function_name = "fff4",
                return_type   = "void"
            }
        }
    }
    return 13;
}

