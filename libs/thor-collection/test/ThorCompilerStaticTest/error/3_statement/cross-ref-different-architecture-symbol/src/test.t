/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
import other;

// CPU side symbols
@cpu
var global_var_on_cpu: int32 = 0;

@cpu
function func_on_cpu(): void
{ }

// GPU side symbols
@gpu
var global_var_on_gpu: int32 = 0;

@gpu
function func_on_gpu(): void
{ }

// standalone functin: CPU access GPU
@cpu
function cpu_access_gpu(): void
{
    // should compile
    other.global_var_on_cpu = 1;
    func_on_cpu();

    // should be failure 
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "CROSS_REF_DIFFERENT_ARCH_SYMBOL" } }
    global_var_on_gpu = 1;
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "CROSS_REF_DIFFERENT_ARCH_SYMBOL" } }
    other.func_on_gpu();
}

// member function: GPU access CPU
@gpu
class class_on_gpu
{
    function gpu_access_cpu(): void
    {
        // should compile
        global_var_on_gpu = 1;
        other.func_on_gpu();

        // should be failure 
        @static_test { expect_message = { level = "LEVEL_ERROR", id = "CROSS_REF_DIFFERENT_ARCH_SYMBOL" } }
        other.global_var_on_cpu = 1;
        @static_test { expect_message = { level = "LEVEL_ERROR", id = "CROSS_REF_DIFFERENT_ARCH_SYMBOL" } }
        func_on_cpu();
    }
}

@cpu
class class_on_cpu
{
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "CROSS_REF_DIFFERENT_ARCH_SYMBOL" } }
    var data_member_on_cpu1: int32 = global_var_on_gpu;
    var data_member_on_cpu2: int32 = global_var_on_cpu;
}

@static_test { expect_message = { level = "LEVEL_ERROR", id = "CROSS_REF_DIFFERENT_ARCH_SYMBOL" } }
@cpu
typedef class_on_gpu ccog;

@cpu
typedef class_on_cpu ccoc;

@gpu
typedef class_on_gpu gcog;

@static_test { expect_message = { level = "LEVEL_ERROR", id = "CROSS_REF_DIFFERENT_ARCH_SYMBOL" } }
@gpu
typedef class_on_cpu gcoc;

@cpu
interface interface_on_cpu { }

@gpu
interface interface_on_gpu { }

// all good
@cpu
class class_on_cpu2 extends class_on_cpu implements interface_on_cpu
{ }

// invalid base class
@static_test { expect_message = { level = "LEVEL_ERROR", id = "CROSS_REF_DIFFERENT_ARCH_SYMBOL" } }
@cpu
class class_on_cpu3 extends class_on_cpu implements interface_on_gpu
{ }

// invalid base class & interface 
@static_test { expect_message = { level = "LEVEL_ERROR", id = "CROSS_REF_DIFFERENT_ARCH_SYMBOL" } }
@static_test { expect_message = { level = "LEVEL_ERROR", id = "CROSS_REF_DIFFERENT_ARCH_SYMBOL" } }
@gpu
class class_on_gpu2 extends class_on_cpu implements interface_on_cpu
{ }

function test(): int32
{
    on(
        x86 = {
            // should be failure
            @static_test { expect_message = { level = "LEVEL_ERROR", id = "CROSS_REF_DIFFERENT_ARCH_SYMBOL" } }
            func_on_gpu();

            on(
                gpu = {
                    // should be failure
                    @static_test { expect_message = { level = "LEVEL_ERROR", id = "CROSS_REF_DIFFERENT_ARCH_SYMBOL" } }
                    func_on_cpu();
                }
            );

            // should compile
            func_on_cpu();
        }
    );

    return 0;
}
