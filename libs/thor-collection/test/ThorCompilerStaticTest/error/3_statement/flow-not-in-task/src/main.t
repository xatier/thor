function dummy(): void {}

function has_flow_func(): void
{
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "FLOW_SHOULD_BE_USED_IN_TASK" } }
    flow -> {
        dummy();
    }
}

task has_flow_task(): void
{
    flow -> {
        dummy();
    }
}

function has_flow_func_template<T>(): void
{
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "FLOW_SHOULD_BE_USED_IN_TASK" } }
    flow -> {
        dummy();
    }
}

task has_flow_task_template<T>(): void
{
    flow -> {
        dummy();
    }
}

function has_flow_in_lambda_func(): void
{
    lambda(): void
    {
        @static_test { expect_message = { level = "LEVEL_ERROR", id = "FLOW_SHOULD_BE_USED_IN_TASK" } }
        flow -> {
            dummy();
        }
    };
}

task has_flow_in_lambda_task(): void
{
    lambda(): void
    {
        @static_test { expect_message = { level = "LEVEL_ERROR", id = "FLOW_SHOULD_BE_USED_IN_TASK" } }
        flow -> {
            dummy();
        }
    };
}

function has_flow_in_lambda_func_template<T>(): void
{
    lambda(): void
    {
        @static_test { expect_message = { level = "LEVEL_ERROR", id = "FLOW_SHOULD_BE_USED_IN_TASK" } }
        flow -> {
            dummy();
        }
    };
}

task has_flow_in_lambda_task_template<T>(): void
{
    lambda(): void
    {
        @static_test { expect_message = { level = "LEVEL_ERROR", id = "FLOW_SHOULD_BE_USED_IN_TASK" } }
        flow -> {
            dummy();
        }
    };
}
