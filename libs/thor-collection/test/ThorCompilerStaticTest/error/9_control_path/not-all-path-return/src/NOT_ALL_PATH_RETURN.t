/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
@static_test { expect_message={ level="LEVEL_ERROR", id="NOT_ALL_PATH_RETURN" } }
function not_void() : int32
{
}

@static_test { expect_message={ level="LEVEL_ERROR", id="NOT_ALL_PATH_RETURN" } }
function f():int32
{
    var x:int32 = 17;

    {
        if(x)
        {
            { return 13; }
        }
        else
        {
        }
    }
}

function f2():int32
{
    var x:int32 = 17;

    {
        if(x)
        {
            { return 13; }
        }
        else
        {
            return 19;
        }
    }
}

function f3():int32
{
    var x:int32 = 17;

    {
        if(x)
        {
            { return 13; }
        }
        else
        {
        }
        return 19;
    }
}

@static_test { expect_message={ level="LEVEL_ERROR", id="NOT_ALL_PATH_RETURN" } }
function g():int32
{
    var x:int32 = 17;

    {
        switch(x)
        {
        case 13: { return 19; }
        case 17:
        }
    }
}

@static_test { expect_message={ level="LEVEL_ERROR", id="NOT_ALL_PATH_RETURN" } }
function g2():int32
{
    var x:int32 = 17;

    {
        switch(x)
        {
        case 13: { return 19; }
        case 17: return 23;
        }
    }
}

function g3():int32
{
    var x:int32 = 17;

    {
        switch(x)
        {
        case 13: { return 19; }
        case 17:
        }
        return 23;
    }
}

function g4():int32
{
    var x:int32 = 17;

    {
        switch(x)
        {
        case 13: { return 19; }
        default: return 23;
        }
    }
}

@static_test { expect_message={ level="LEVEL_ERROR", id="NOT_ALL_PATH_RETURN" } }
function g( i : int32 ) : bool 
{
    if( i % 2 == 0 ) return true;
}

@static_test { expect_message={ level="LEVEL_ERROR", id="NOT_ALL_PATH_RETURN" } }
function g2( i : int32 ) : bool 
{
    if( i % 2 == 0 ) return true;
    elif( i == 2) return false;
}

@static_test { expect_message={ level="LEVEL_ERROR", id="NOT_ALL_PATH_RETURN" } }
function g3( i : int32 ) : bool 
{
    if( i % 2 == 0 ) return true;
    elif( i == 2) {}
    else return false;
}

