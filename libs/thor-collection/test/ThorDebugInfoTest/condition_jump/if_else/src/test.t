/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
@entry task test1():int32
{
    var x:int64 = 32;
    var y:int64 = 42;
    var a:int64 = 10;
    var b:int64 = 9;
    var c:int64 = 8;

    if (x > y)
    {
        a = y;
        a = a / 2;
    }
    else
    {
        // should enter here
        a = x;  // a = 32
        a = a - 2; // a = 30
    }

    if 
    (x < y)
    { a = y; }  // a = 42
    else
    {
        b = 3; c = 2;
    }

    if (x == y)
    {
        b = 1;
        c = 0;
    }
    elif(x < y)
    {
        y = 3;      // y =3
    }
    else
    {
        x = a + b + c;
    }

    x = x; exit(0); return -1;
}

@entry task test2(): int32
{
    // test empty block

    var x : int32 = 10;


    if (x > 10)
    {
    }
    else
    {
    }

    if (x == 10)
    {
        if (x == 10)
        {
           if (x == 10);
        }
    }
    else
    {
    }


    exit(0);
    return -1;
}

@entry task test3() : int32
{
    var x : int32 = 10;
    var y : int32 = 10;

    if (x > 3)
    {
        x = 5;
        if ( y < 100)
        {
            y = x + y;
        }
    }

    var z : int32 = x + y;
    x = x; exit(0); return -1;
}
