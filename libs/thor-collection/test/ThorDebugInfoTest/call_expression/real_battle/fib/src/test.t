/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
function fib_for_loop(index: int32): int32
{
    if (index <= 1) return index;

    var i : int32;
    var ans : int32 = 0;
    var pre1 : int32 = 1;
    var pre2 : int32 = 0;
    for (i = 2; i <= index; ++i)
    {
        ans = pre1 + pre2;
        pre2 = pre1;
        pre1 = ans;
    }
    return ans;
}

function fib_recursive(index: int32) : int32
{
    if (index <= 1) return index;
    
    return fib_recursive(index-1) + fib_recursive(index-2);
}

/**
 * Test function entry points
 */
@entry task test1() : int32
{
    var x : int32 = fib_for_loop(8);
    var y : int32 = fib_recursive(8);
    var result : int32 = 0;

    if (x == y)
    {
        result = 1;
    }
    else
    {
        result = 0;
    }
    result = result;
    exit(0);
	return -1;
}
