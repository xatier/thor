#!/usr/bin/python
#
# Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
#
# This file is part of Thor.
# Thor is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License, version 3,
# as published by the Free Software Foundation.
#
# Thor is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Thor.  If not, see <http://www.gnu.org/licenses/>.
#
# If you want to develop any commercial services or closed-source products with
# Thor, to adapt sources of Thor in your own projects without
# disclosing sources, purchasing a commercial license is mandatory.
#
# For more information, please contact Zillians, Inc.
# <thor@zillians.com>
#

import sys
sys.path.insert(0, "../../")
sys.path.insert(0, "./script/")

import lib
from lib.baselib import *

#
# Run and check
#
gdb.Breakpoint("test.t:test1")

set_global_config(white_list = ['test.t'])
#lazy_generate_mode('v', 'w', 'input', 'output', threshold=[0, 40000], max_step=30)

try:
        execute_check("run", 3)
        execute_check_ex("s", {"EXPECT_FILE":"test.t"}, 4)
        execute_check_ex("s", {"EXPECT_FILE":"test.t"}, 5)
        execute_check_ex("s", {"EXPECT_FILE":"test.t"}, 7)
        execute_check_ex("s", {"EXPECT_FILE":"test.t"}, 13)
        execute_check_ex("s", {"v":1982, "EXPECT_FILE":"test.t"}, 15)
        execute_check_ex("s", {"v":1982, "input":1982, "EXPECT_FILE":"test.t"}, 18)
        execute_check_ex("s", {"v":1982, "input":1982, "output":1982, "EXPECT_FILE":"test.t"}, 20)
        execute_check_ex("s", {"v":1982, "EXPECT_FILE":"test.t"}, 21)
        execute_check_ex("s", {"v":1982, "w":0, "EXPECT_FILE":"test.t"}, 8)
        execute_check_ex("s", {"v":1.53049818e-41, "EXPECT_FILE":"test.t"}, 13)
        execute_check_ex("s", {"v":39997.1133, "EXPECT_FILE":"test.t"}, 15)
        execute_check_ex("s", {"v":39997.1133, "input":39997.1133, "EXPECT_FILE":"test.t"}, 18)
        execute_check_ex("s", {"v":39997.1133, "input":39997.1133, "output":39997.11328125, "EXPECT_FILE":"test.t"}, 20)
        execute_check_ex("s", {"v":39997.1133, "EXPECT_FILE":"test.t"}, 21)
        execute_check_ex("s", {"v":1982, "w":39997, "EXPECT_FILE":"test.t"}, 10)
        execute_check_ex("s", {"EXPECT_FILE":"test.t"}, 11)
except MismatchException, e:
        gdb.execute('quit 1') # quit with error code
