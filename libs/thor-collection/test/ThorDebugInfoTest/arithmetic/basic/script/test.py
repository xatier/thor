#!/usr/bin/python
#
# Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
#
# This file is part of Thor.
# Thor is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License, version 3,
# as published by the Free Software Foundation.
#
# Thor is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Thor.  If not, see <http://www.gnu.org/licenses/>.
#
# If you want to develop any commercial services or closed-source products with
# Thor, to adapt sources of Thor in your own projects without
# disclosing sources, purchasing a commercial license is mandatory.
#
# For more information, please contact Zillians, Inc.
# <thor@zillians.com>
#

import sys
sys.path.insert(0, "../../")
sys.path.insert(0, "./script/")

import lib
from lib.baselib import *

#
# Run and check
#
gdb.Breakpoint("test.t:test_function")
set_global_config(white_list = ['test.t'])

try:
	execute_check("run", 3)
	execute_check("n", 4, x = 32)
	execute_check("n", 7, x = 32, y = 42)
	execute_check("n", 8, x = 10, y = 42)
	execute_check("n", 11, x = 420, y = 42)
	execute_check("n", 13, x = 420, y = 42, z = 420)
	execute_check("n", 14)
	gdb.execute('quit')
except MismatchException, e:
	gdb.execute('quit 1') # quit with error code

