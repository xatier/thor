#!/usr/bin/python
#
# Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
#
# This file is part of Thor.
# Thor is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License, version 3,
# as published by the Free Software Foundation.
#
# Thor is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Thor.  If not, see <http://www.gnu.org/licenses/>.
#
# If you want to develop any commercial services or closed-source products with
# Thor, to adapt sources of Thor in your own projects without
# disclosing sources, purchasing a commercial license is mandatory.
#
# For more information, please contact Zillians, Inc.
# <thor@zillians.com>
#

import sys
sys.path.insert(0, "../../")
sys.path.insert(0, "./script/")

import lib
from lib.baselib import *

#
# Run and check
#
gdb.Breakpoint("test.t:test3")
set_global_config(white_list = ['test.t', 'print.cpp'])
#lazy_generate_mode('x', threshold=[0, 10], max_step=30)

try:
        execute_check("run", 21)
        execute_check_ex("s", {"EXPECT_FILE":"print.cpp"}, 17)
        execute_check_ex("s", {"x":0, "EXPECT_FILE":"test.t"}, 28)
        execute_check_ex("s", {"x":3, "EXPECT_FILE":"test.t"}, 29)
        execute_check_ex("s", {"x":3, "EXPECT_FILE":"test.t"}, 31)
        execute_check_ex("s", {"EXPECT_FILE":"test.t"}, 32)
        execute_check_ex("s", {"EXPECT_FILE":"print.cpp"}, 18)
        execute_check_ex("s", {"EXPECT_FILE":"test.t"}, 23)
        execute_check_ex("s", {"EXPECT_FILE":"test.t"}, 24)
except MismatchException, e:
        gdb.execute('quit 1') # quit with error code
