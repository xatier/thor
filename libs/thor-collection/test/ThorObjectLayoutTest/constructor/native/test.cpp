/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
#include <cinttypes>
#include <iostream>
#include "thor/lang/Language.h"

using namespace thor::lang;

////////////////////////
// Utility
void assertion(std::int32_t error)
{
    std::cerr << "Error code: " << error << std::endl;
    throw error;
}

////////////////////////
// Class Declaration
class C
{
public:
    C();
    virtual void dummy();
};

class B : public Object
{
public:
    B();
    std::int32_t vb;
};

class A : public B, public C
{
public:
    A();
    std::int32_t va;
};

////////////////////////
// Testing
std::int32_t test1()
{
    A* a = new A();

    if (a->va != 32) assertion(2);
    if (a->vb != 20001) assertion(3);
    delete a;

	exit(0);
	return -1;
}

std::int32_t test2()
{
    // Test replacement new
// TOFIX: disable this test, since tbb will try to delete the memory allocated by replacement new
//    char memory[1024];
//    A* a = new(memory) A();
//
//    if (a->va != 32) assertion(2);
//    if (a->vb != 20001) assertion(3);
	exit(0);
	return -1;
}
