/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
@native
function assertion(e : int32) : void;

class Empty
{
}

class B
{
    public virtual function foo() : int32 { return 1; }
    public var vb : int32;
}

interface C
{
    public function hello() : int32;
    public function set(x: int64) : void;
    public function get() : int64;
}

class A extends B implements C
{
    public virtual function foo() : int32 { return 2; }
    public virtual function hello() : int32 { return 3; }

    public virtual function set(x: int64) : void { va = x; }
    public virtual function get() : int64 { return va; }
    public var va : int64;
}

class Special extends Empty implements C
{
    public virtual function foo() : int32 { return 1; }
    public virtual function hello() : int32 { return 2; }
}


///////////////////////////////////////
// Test for covariant return type
interface CI
{
    public function get() : C;
}

class CB
{
    public virtual function dummy() : void {}
    public var dummy_var : int32;
}

class CD extends CB implements CI
{
    // Well, thor cannot create object, so need a function for C++ to pass pointer
    public function set_source(a : A) : void
    {
        class_AA = a;
    }

    public virtual function get() : A { return class_AA; }
    public var class_AA : A;
}

/////////////////////////////////////
// Test to get correct virtual table -- the test case come from the bug which will get Dog's virtual table, and 
// then llvm would report error in CallInst. We should get the virtual table from Animal
class Animal
{
	public function new() : void { v = 0; }
	public virtual function yelp() : void
	{
		v = 100;
	}
	public virtual function run() : void 
	{
		v = 10;
	}
	public var v : int32;
}

class Dog extends Animal
{
}

/////////////////////////////////////
function thor_test1(b : B) : void
{
    if (b.foo() != 2) 
        assertion(1);
}

function thor_test2(a : Special) : void
{
    // This test the correctness of virtual table layout. It should be in the following order:
    // Special::hello() | Special::foo()  (not in the declartion order in Special)
    if (a.foo() != 1) assertion(1);
    if (a.hello() != 2) assertion(2);
}

function thor_test3(c : C) : void
{
    c.set(199999999999);
    if (c.get() != 199999999999) assertion(1);
}

function thor_test4(i : CI) : void
{
    var c : C = i.get();
    if (c.get() != 1234567890) assertion(1);
}

function thor_test5(d : Dog) : void
{
	d.yelp();
	if (d.v != 100) assertion(1);
	d.run();
	if (d.v != 10) assertion(2);
}

@native
@entry
task test1() : int32;

@native
@entry
task test2() : int32;

@native
@entry
task test3() : int32;

@native
@entry
task test4() : int32;

@native
@entry
task test5() : int32;

