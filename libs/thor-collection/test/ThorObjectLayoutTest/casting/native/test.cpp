/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
#include <cinttypes>
#include <iostream>
#include "thor/lang/Language.h"

using namespace thor::lang;

#include "thor/lang/Language.h"

////////////////////////
// Utility
void assertion(std::int32_t error)
{
    std::cerr << "Error code: " << error << std::endl;
    throw error;
}

////////////////////////
// Class Declaration
class B : public Object
{
public:
    B();
    std::int32_t vb;
};

class A : public B
{
public:
    A();
    virtual void dummy();
};


////////////////////////
// Testing
extern void thor_test1(A* a);
std::int32_t test1()
{
    A* a = new A();
    thor_test1(a);

    if (a->vb != 54321) assertion(2);
    delete a;
	exit(0);
	return -1;
}

extern void thor_test2(B* b);
std::int32_t test2()
{
//    A* a = new A();
//    thor_test2(static_cast<B*>(a));
//
//    if (a->vb != 112233) assertion(2);
//    delete a;
	exit(0);
	return -1;
}
