/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
@native
function assertion(e : int32) : void;

class B
{
    public function new() : void 
    {
        vb1 = 0;
        vb2 = 0;
    }
    public var vb1 : int32;
    public var vb2 : int64;
}

class A extends B
{
    public function new() : void
    {
        fact = 0;
        sum = 0;
    }
    
    public function add(v : int32) : int32 
    {
        sum = sum + v;
        return sum;
    }

    public function factorial(v : int64) : int64
    {
        if (v == 1) return v;
        fact = factorial(v-1) * v;
        return fact;
    }

    public var fact : int64;
    public var sum : int32;
}

class VA extends B
{
    // This class has virtual function, so there exists offset when casting from VA to B
    public virtual function dummy() : void {}
    public var v : int32;
}

class C
{
    public function new() : void
    {
        v = 1992;
    }

    public var v : int32;
}

class D extends C
{
    public function new() : void
    {
        vd = 2002;
    }

    public var vd : int32;
}

class F
{
	public function new() : void
	{
		value =	2305843009213693951L;
	}

	public var value : int64;
}

class E
{
	public function set(f : F) : void
	{
		member_f = f;
	}
	
	public function new() : void
	{
		value = 2147483647;
	}

	public function get() : F
	{
		return member_f;
	}

	public var value : int32;
	public var member_f : F;
}

class WithStatic
{
	public function new() : void {
		access_me = 1234;
	}
	public static var value: int32 = 1234;
	public var access_me : int32;
}

function thor_test1(a : A) : void
{
    // To see if we would override the next variable
    a.vb1 = 2147483647;
    if (a.vb1 != 2147483647) assertion(1);
    a.vb2 = 2305843009213693951L;
    if (a.vb2 != 2305843009213693951L) assertion(2);
    
    a.add(2147483600);
    a.add(1);
    a.factorial(20);
    if (a.fact != 2432902008176640000L) assertion(3);
    if (a.sum != 2147483601) assertion(4);
}

function thor_test2(a : VA) : void
{
    a.vb1 = 2147483647;
    if (a.vb1 != 2147483647) assertion(1);
    a.vb2 = 2305843009213693951L;
    if (a.vb2 != 2305843009213693951L) assertion(2);

    a.v = 1234576;
    if (a.v != 1234576) assertion(3);
}

function thor_test3(d: D) : void
{
    if (d.v != 1992) assertion(1);
    if (d.vd != 2002) assertion(2);
}

function thor_test4(e: E) : void
{
	if (e.value != 2147483647) assertion(1);
	if (e.member_f.value != 2305843009213693951L) assertion(2);
	if (e.get().value != 2305843009213693951L) assertion(3);
}

function thor_test4(b: WithStatic) : void
{
	if(b.access_me != 1234) assertion(1);
}

@native
@entry
task test1() : int32;

@native
@entry
task test2() : int32;

@native
@entry
task test3() : int32;

@native
@entry
task test4() : int32;

@native
@entry
task test5() : int32;
