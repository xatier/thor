/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
import . = thor.container;
import . = thor.lang;
import . = thor.gc;

class Result
{
    public function print(): void
    {
        thor.lang.print("GC result ---------------------\n");
        thor.lang.print("int64_map_exist_count: \{int64_map_exist_count}\n");
        thor.lang.print("int32_map_exist_count: \{int32_map_exist_count}\n");
        thor.lang.print("--------------------- GC result\n");
    }

    public var int64_map_exist_count : int32 = 0;
    public var int32_map_exist_count : int32 = 0;
}

function check_result(): Result
{
    return check_result(__getActiveObjects());
}

function check_result(objs: Vector<Object>): Result
{
    var result = new Result();

    for (var i = 0; i < objs.size(); ++i)
    {
        if (isa<Map<int32, int32> >(objs.get(i)))
        {
            var o = cast<Map<int32, int32> >(objs.get(i));
            if (o.size() == 10)
            {
                var match = true;
                var iter = o.iterator();
                do
                {
                    var entry = iter.next();
                    if (entry.key != entry.value) match = false;

                } while (iter.hasNext());

                if (match) ++result.int32_map_exist_count;
            }
        }
        else if (isa<Map<int64, int64> >(objs.get(i)) )
        {
            var o = cast<Map<int64, int64> >(objs.get(i));
            if (o.size() == 100)
            {
                var match = true;
                var iter = o.iterator();
                do
                {
                    var entry = iter.next();
                    if (entry.key != entry.value) match = false;

                } while (iter.hasNext());

                if (match) ++result.int64_map_exist_count;
            }
        }
    }

    return result;
}

function create_objects(): bool
{
	var collect_me = new Map<int64, int64>();
    for (var i = 0; i < 100; ++i)
        collect_me.set(i, i);
    
    var r = check_result();
    return (r.int32_map_exist_count == 1 && r.int64_map_exist_count == 1);
}

var v: Map<int32, int32> = null;

@entry
task test1() : void
{
	v = new Map<int32, int32>();

	var i : int32 = 0;
	for (i = 0; i < 10; ++i)
	{
		v.set(i, i);
	}

    if (!create_objects()) exit(1);

    __waitGC(
        lambda(objs: Vector<Object>): void
        {
            var r = check_result();
            if (r.int32_map_exist_count == 1 && r.int64_map_exist_count == 0)
                exit(0);
            else
                exit(2);
        }
    );
}
