/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
import . = thor.container;
import . = thor.lang;
import . = thor.gc;

class A
{
    public function new(): void
    {
        id = A.base + A.count;
        ++A.count;
    }

    public static var base: int32 = 11;
    public static var count: int32 = 0;
    public var id: int32;
}

class B
{
    public function new(): void
    {
        id = B.base + B.count;
        ++B.count;
    }

    public static var base: int32 = 11001;
    public static var count: int32 = 0;
    public var id: int32;
}

class Result
{
    public var a_array_exist: bool = false;
    public var b_array_exist: bool = false;
    public var elem_in_a_array_ok: bool = false;
    public var elem_in_b_array_ok: bool = false;
}

function check_result() : Result
{
    performAndWaitGC();
    var result = new Result();

    var v = getActiveObject();
    var a_count = 0;
    var b_count = 0;
    for (var i = 0; i < v.size(); ++i)
    {
        if (isa<Array<A> >(v[i]))
        {
            var vv = cast<Array<A> >(v[i]);
            if (vv.size() == 10)
            {
                var match = true;
                for (var j = 0; j < vv.size(); ++j)
                {
                    var o = vv[i];
                    if (o.id != A.base + i) match = false;
                }
                if (match) result.a_array_exist = true;
            }
        }
        else if (isa<Array<B> >(v[i]))
        {
            var vv = cast<Array<B> >(v[i]);
            if (vv.size() == 20)
            {
                var match = true;
                for (var j = 0; j < vv.size(); ++j)
                {
                    var o = vv[i];
                    if (o.id != B.base + i) match = false;
                }
                if (match) result.b_array_exist = true;
            }
        }
        else if (isa<A>(v[i]))
        {
            ++a_count;
        }
        else if (isa<B>(v[i]))
        {
            ++b_count;
        }
    }

    if (a_count == 10) result.elem_in_a_array_ok = true;
    if (b_count == 20) result.elem_in_b_array_ok = true;

    print("A array exist = "); print(result.a_array_exist); print("\n");
    print("B array exist = "); print(result.b_array_exist); print("\n");
    print("element a exist = "); print(result.elem_in_a_array_ok); print("\n");
    print("element b exist = "); print(result.elem_in_b_array_ok); print("\n");

    return result;
}

function create_objects(): bool
{
    var v = new Array<B>(20);
    for (var i = 0; i < 20; ++i)
    {
        v[i] = new B();
    }

    var r = check_result();
    return (r.a_array_exist && r.b_array_exist && r.elem_in_a_array_ok && r.elem_in_b_array_ok);
}

@entry
task test1() : void
{
	var v = new Array<A>(10);
	for (var i = 0; i < 10; ++i)
	{
		v[i] = new A();
	}
    
    if (!create_objects()) exit(1);

    var r = check_result();
    if (r.a_array_exist && !r.b_array_exist && r.elem_in_a_array_ok && !r.elem_in_b_array_ok) exit(0);
    else
    	exit(2);
}
