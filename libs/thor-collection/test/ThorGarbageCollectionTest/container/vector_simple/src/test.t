/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
import . = thor.container;
import . = thor.lang;
import . = thor.gc;

class Result
{
    public function print(): void
    {
        thor.lang.print("GC result ---------------------\n");
        thor.lang.print("int64_vector_exist_count: \{int64_vector_exist_count}\n");
        thor.lang.print("int32_vector_exist_count: \{int32_vector_exist_count}\n");
        thor.lang.print("--------------------- GC result\n");
    }

    public var int64_vector_exist_count : int32 = 0;
    public var int32_vector_exist_count : int32 = 0;
}

function check_result(): Result
{
    return check_result(__getActiveObjects());
}

function check_result(objs: Vector<Object>): Result
{
    var result = new Result();

    for (var i = 0; i < objs.size(); ++i)
    {
        if (isa<Vector<int32> >(objs.get(i)))
        {
            var o = cast<Vector<int32> >(objs.get(i));
            if (o.size() == 100)
            {
                var match = true;
                for (var j = 0; j < o.size(); ++j)
                {
                    if (o.get(j) != j) match = false;
                }
                if (match) ++result.int32_vector_exist_count;
            }
        }
        else if (isa<Vector<int64> >(objs.get(i)) )
        {
            var o = cast<Vector<int64> >(objs.get(i));
            if (o.size() == 10)
            {
                var match = true;
                for (var j = 0; j < o.size(); ++j)
                {
                    if (o.get(j) != j) match = false;
                }
                if (match) ++result.int64_vector_exist_count;
            }
        }
    }

    result.print();

    return result;
}

function create_objects(): bool
{
	var collect_me = new Vector<int32>();
    for (var i = 0; i < 100; ++i)
        collect_me.pushBack(i);
    
    var r = check_result();
    return (r.int32_vector_exist_count == 1 && r.int64_vector_exist_count == 1);
}

var v: Vector<int64> = null;

@entry
task test1() : void
{
	v = new Vector<int64>();
	for (var i = 0; i < 10; ++i)
	{
		v.pushBack(i);
	}

    if (!create_objects()) exit(1);

    __waitGC(
        lambda(objs: Vector<Object>): void
        {
            var g = check_result(objs);

            if (g.int32_vector_exist_count == 0 && g.int64_vector_exist_count == 1)
                exit(0);
            else
                exit(2);
        }
    );
}
