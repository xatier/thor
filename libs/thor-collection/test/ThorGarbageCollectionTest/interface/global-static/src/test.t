/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
import . = thor.lang;
import . = thor.container;
import . = thor.gc;

interface Iface
{
    function dummy(): void;
}

class Dummy<T> implements Iface
{
    public var magic : int32; 

    public function new(id: int32) : void
    {
        magic = id;
    }

    public function dummy(): void {}
}

// static class
class StaticClass
{
    public static var dummy: Iface = new Dummy<float32>(222);
    public static var dummies: Vector<Iface> = new Vector<Iface>();
    public static var primitive: int32 = 3333;
}

// global variables
var gDummy : Iface = new Dummy<int16>(124);
var gDummies : Vector<Iface> = new Vector<Iface>();
var gPrimitive : int32 = 333;     // should not be trace

function check_vectortype<T>(objs: Vector<Object>, num: int32): bool
{
    var match = true;
    for (var i = 0; i < objs.size(); ++i)
    {
        var o = cast<T>(objs.get(i));
        if (o == null) continue;

        if (o.size() == num)
        {
            for (var j = 0; j < o.size(); ++j)
            {
                var oo = o.get(j);
                if (oo.magic != j) match = false;
            }
        }
    }

    return match;
}

function check_dummytype<T>(objs: Vector<Object>, magic: int32) : bool
{
    for (var i = 0; i < objs.size(); ++i)
    {
        // check gDummy
        var o = cast<T>(objs.get(i));
        if (o != null)
        {
            if (o.magic != magic)
                return false;
            else
                break;
        }
    }

    return true;
}

function check_global(objs: Vector<Object>) : bool
{
    if (!check_dummytype<Dummy<int16> >(objs, 124)) return false;
    if (!check_vectortype<Vector<Dummy<int32> > >(objs, 100)) return false;

    return true;
}

function check_class_static(objs: Vector<Object>) : bool
{
    if (!check_dummytype<Dummy<float32> >(objs, 222)) return false;
    if (!check_vectortype<Vector<Dummy<float64> > >(objs, 200)) return false;

    return true;
}

function check_local_static(objs: Vector<Object>) : bool
{
    if (!check_dummytype<Dummy<bool> >(objs, 999)) return false;
    if (!check_vectortype<Vector<Dummy<int64> > >(objs, 300)) return false;
    return true;
}

function initialize(): void
{
    // global 
    for (var i = 0; i < 100; ++i)
    {
        gDummies.pushBack(cast<Iface>(new Dummy<int32>(i)));
    }

    // class static
    for (var i = 0; i < 200; ++i)
    {
        StaticClass.dummies.pushBack(cast<Iface>(new Dummy<float64>(i)));
    }

    // static local
    static var dummy     : Iface = new Dummy<bool>(999);
    static var dummies           = new Vector<Iface>();
    static var primitive         = 333333;

    for (var i = 0; i < 300; ++i)
    {
        dummies.pushBack(cast<Iface>(new Dummy<int64>(i)));
    }
}

@entry
task test1() : void
{
    initialize();

    __waitGC(
        lambda(objs: Vector<Object>): void
        {
            if (!check_global      (objs)) exit(1);
            if (!check_class_static(objs)) exit(2);
            if (!check_local_static(objs)) exit(3);

            exit(0);
        }
    );
}
