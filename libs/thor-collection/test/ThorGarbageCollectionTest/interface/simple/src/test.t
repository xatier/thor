/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
import . = thor.lang;
import . = thor.gc;
import . = thor.container;

interface iface
{
    function dummy(): void;
}

class B implements iface
{
    public function dummy(): void {}

    public var id: int32;
}

class A implements iface
{
	public function new() : void
	{
		b = new B();
	}

    public function dummy(): void {}

	public var b : iface;
    public var id: int32;
}

class Result
{
    public function print(): void
    {
        thor.lang.print("GC result ---------------------\n");
        thor.lang.print("collect_count: \{collect_count}\n");
        thor.lang.print("   stay_count: \{   stay_count}\n");
        thor.lang.print("--------------------- GC result\n");
    }

    public var collect_count: int32 = 0;
    public var    stay_count: int32 = 0;
}

function check_result(): Result
{
    return check_result(__getActiveObjects());
}

function check_result(objs: Vector<Object>): Result
{
    var result = new Result();
    
    // verify collected objects
    for (var i = 0; i < objs.size(); ++i)
    {
        var o = cast<A>(objs.get(i));
        if (o == null) continue;

        if (o.id == 2002 && cast<B>(o.b).id == 2003)
        {
            ++result.stay_count;
        }

        if (o.id == 4002 && cast<B>(o.b).id == 4003)
        {
            ++result.collect_count;
        }
    }

    result.print();

    return result;
}

function create_objects_impl(): A
{
	var collect_me : A = new A();

    collect_me.id = 4002;
    cast<B>(collect_me.b).id = 4003;

    return collect_me;
}

function create_objects(): bool
{
	var collect_me : iface = create_objects_impl();
    
    // should not collect anything
    var r = check_result();
    return (r.collect_count == 1 && r.stay_count == 1);
}

var a : A = null;

@entry
task test1() : void
{
	a = new A();
    a.id = 2002;
    cast<B>(a.b).id = 2003;

    if (!create_objects()) exit(1);

    __waitGC(
        lambda(objs: Vector<Object>): void
        {
            // well, check gc, need to collect some objects in create_objects() funciton
            var r = check_result(objs);

            if (r.stay_count == 1 && r.collect_count == 0)
                exit(0);
            else
                exit(2);
        }
    );
}
