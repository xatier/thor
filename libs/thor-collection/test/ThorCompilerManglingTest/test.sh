#!/bin/sh
#
# Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
#
# This file is part of Thor.
# Thor is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License, version 3,
# as published by the Free Software Foundation.
#
# Thor is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Thor.  If not, see <http://www.gnu.org/licenses/>.
#
# If you want to develop any commercial services or closed-source products with
# Thor, to adapt sources of Thor in your own projects without
# disclosing sources, purchasing a commercial license is mandatory.
#
# For more information, please contact Zillians, Inc.
# <thor@zillians.com>
#

TEMP_FILE_A=`mktemp`
TEMP_FILE_B=`mktemp`

TS_DRIVER=$1
BUILD_PATH=$2
INPUT_FILE=$3
INTERMEDIATE_PATH=$4
PROJECT_NAME=$5
KEYWORD=$6
GOLD_OUTPUT=$7
INPUT_FILE2=$8

cd $BUILD_PATH
rm -rf $PROJECT_NAME
$TS_DRIVER project create $PROJECT_NAME
cd $PROJECT_NAME
mkdir -p src/$INTERMEDIATE_PATH
cp -f $INPUT_FILE src/$INTERMEDIATE_PATH
cp -f $INPUT_FILE2 ./ 
$TS_DRIVER b r

nm bin/lib$PROJECT_NAME.so | xargs -I{} sh -c "echo {} | cut -d\" \" -f3 | grep \"${KEYWORD}\"" > $TEMP_FILE_A
cat $GOLD_OUTPUT > $TEMP_FILE_B
diff $TEMP_FILE_A $TEMP_FILE_B
ERROR_CODE="$?"
if [ $ERROR_CODE -ne 0 ];
then
    #echo `pwd` # for debugging
    #nm $PROJECT_NAME.so | xargs -I{} sh -c "echo {} | cut -d\" \" -f3 | c++filt -t" # for debugging
    echo "fail! (test.sh)"
    exit 1
fi
echo "success! (test.sh)"
exit 0

