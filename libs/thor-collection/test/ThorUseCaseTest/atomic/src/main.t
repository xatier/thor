/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
/* This test case test "atomic" keywowd.
 * hello1 and hello2 will increase global variable "gi32" and "x.v",
 * if the operation is transactional, the reslut will be "6000000".
 */

import thor.lang;

class X {
    public var v : int32 = 0;
}

var gb  : bool = 0;
var gi8  : int8 = 0;
var gi32 : int32 = 0;
var gi64 : int64 = 0;
var gf32 : float32 = 0;
var gf64 : float64 = 0;
var x : X = new X();
var hello1_finished : bool = false;
var hello2_finished : bool = false;

function hello1() : void
{
    for(var i = 0; i < 100000; ++i)
    {
        atomic -> {
            gb   = !gb;
            gi8  = gi8  + 1; gi8  += 1; ++gi8;
            gi32 = gi32 + 1; gi32 += 1; ++gi32;
            gi64 = gi64 + 1; gi64 += 1; ++gi64;
            gf32 = gf32 + 1; gf32 += 1; ++gf32;
            gf64 = gf64 + 1; gf64 += 1; ++gf64;
            x.v = x.v + 1; x.v += 1; ++x.v;
        }
    }
    hello1_finished = true;
    return;
}

function hello2() : void
{
    for(var i = 0; i < 100000; ++i)
    {
        atomic -> {
            gb   = !gb;
            gi8  = gi8  + 1; gi8  += 1; ++gi8;
            gi32 = gi32 + 1; gi32 += 1; ++gi32;
            gi64 = gi64 + 1; gi64 += 1; ++gi64;
            gf32 = gf32 + 1; gf32 += 1; ++gf32;
            gf64 = gf64 + 1; gf64 += 1; ++gf64;
            x.v = x.v + 1; x.v += 1; ++x.v;
        }
    }
    hello2_finished = true;
    return;
}

function checker() : void
{
    while(!hello1_finished || !hello2_finished) ;
    if(
       gb   ==  false &&
       gi8  ==    -64 &&
       gi32 == 600000 &&
       gi64 == 600000 &&
       gf32 == 600000 &&
       gf64 == 600000 &&
       x.v  == 600000
    )
    {
        exit(0);
    }
    else
    {
        print(gb);    print("\n");
        print(gi8);   print("\n");
        print(gi32);  print("\n");
        print(gi64);  print("\n");
        print(gf32);  print("\n");
        print(gf64);  print("\n");
        exit(1);
    }
}

@entry
task main() : void {
    async -> checker();
    async -> hello1();
    async -> hello2();
}
