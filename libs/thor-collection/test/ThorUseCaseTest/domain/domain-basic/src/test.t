/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
@server
function hello(x:int32):void
{
	print(x);
	print("Hello\n");
}

@client
function world(x:int32):void
{ 
	print(x);
	print("World\n");
}

@entry
@server
task server_main():void
{

	Domain.watch(0, lambda(target:Domain):void { 
		@remote { domain = target } 
		world(1);
//		__invokeFunction(target, 0);
	});
}

@entry
@client
task client_main():void
{
	Domain.watch(0, lambda(target:Domain):void { 
		@remote { domain = target } 
		hello(2);
//		__invokeFunction(target, 2);
	});
}
