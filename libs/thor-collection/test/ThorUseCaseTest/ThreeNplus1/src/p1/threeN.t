/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
function threeNplus1( a:int32, b:int32):int32
{
        var i:int32;
        var tmp:int32;
        var result:int32;
        result = 0 ; 
        for(i = a; i <=b ; ++i)
        {
                tmp = threeNplus1length(i);
                if(tmp >result)
                        result = tmp;
        }
        return result;
}

function threeNplus1length(a:int32):int32
{
        var len:int32;
        len = 1;

        while(a !=1)
        {
                if( a%2 == 1 )
                        a = a*3 +1;
                else
                        a = a/2;
//                print(a);
                ++len;
        }

        return len;
}
