/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
/****
 * These test cases test debug info generation for those in which there exists some
 * instructions placed after the branch statement. For those instructions, they will
 * not generate llvm value 
 */

class Dummy{}

@entry
task test1(): void
{
    var i = 0;
    while (i == 0)
    {
        ++i;
        continue;
        var zzz = 4;
    }

    for (;;)
    {
        if (true) break;

        var ggg = 3;
    }

    for (;;)
    {
        break;
        lambda() : void {};
    }

    exit(0);
}

