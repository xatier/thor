/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
import p1;

@entry
task main():void
{
        var q:p1.QueueInt32;
        q = new p1.QueueInt32();
        var i:int32;
        var size:int32;
        var tmp:int32;
        size = 50000;
        for(i=0; i < size; ++i)
        {
                q.enQueue(i);
        }


        for(i=0; i < size - 50; ++i)
        {
                tmp = q.deQueue();
        }
        
        print(q.nextValue());

        for( i = i; i < size ; ++i)
        {
                tmp = q.deQueue();
        }

        print(tmp);        
        exit(0); 
}

