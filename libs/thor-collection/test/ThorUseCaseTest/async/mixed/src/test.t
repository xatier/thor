/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
function do_exit(ec: int32, msg: String)
{
    print("done with: \{msg}\n");

    exit(ec);
}

function do_test_impl<T>(count: int32): void
{
    if (T.end)
    {
        if (T.ok)
            do_exit( 0, "ok");
        else
            do_exit(-1, "not_ok");
    }
    else if (count != 0)
    {
        async -> do_test_impl<T>(count - 1); // re-try
    }
    else
    {
        do_exit(-2, "hang");
    }
}

function do_test<T>(): void
{
    if (T.end)
        do_exit(-3, "initialization failure");

    T.trigger();

    async -> do_test_impl<T>(10);
}

class lambda_in_async
{
    public static function trigger(): void
    {
        var func_local: int8 = 1;
        async -> {
            var async_local:           int32 = func_local + 1;
            var l          : lambda(): int32 = lambda(): int32 { return async_local; };

            ++async_local;

            ok  = l() + 1 == async_local;
            end = true;
        }
    }

    public static var end: bool = false;
    public static var ok : bool = false;
}

class async_in_lambda
{
    public static function trigger(): void
    {
        var func_local:           int8 = 1;
        var l         : lambda(): void = lambda(): void {
            var lambda_local: int32 = func_local + 1;
    
            async -> {
                ok  = func_local + 1 == lambda_local;
                end = true;
            }
        };
    
        ++func_local;
    
        l();
    }

    public static var end: bool = false;
    public static var ok : bool = false;
}

class lambda_in_remote
{
    public static function trigger(): void
    {
        var func_local_1: int8 = 1;
        var func_local_2: int8 = 1;
        async[Domain.local()] -> {
            var remote_local:           int32 = func_local_1 + 1;
            var l1          : lambda(): int32 = lambda(): int32 { return   func_local_1 + 1; };
            var l2          : lambda(): int32 = lambda(): int32 { return   func_local_2 + 1; };
            var l3          : lambda(): int32 = lambda(): int32 { return remote_local      ; };

            ++  func_local_1;
            ++  func_local_2;
            ++remote_local  ;

            ok  = l1() + 1 == remote_local &&
                  l2() + 1 == remote_local &&
                  l3() + 1 == remote_local;
            end = true;
        }
    }

    public static var end: bool = false;
    public static var ok : bool = false;
}

@entry
task test_lambda_in_async(): void {
    return do_test<lambda_in_async>();
}

@entry
task test_async_in_lambda(): void {
    return do_test<async_in_lambda>();
}

@entry
task test_lambda_in_remote(): void {
    return do_test<lambda_in_remote>();
}
