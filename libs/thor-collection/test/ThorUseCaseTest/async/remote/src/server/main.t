/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
import thor.util;
import common;

typedef thor.util.UUID UUID;

function test_impl<T, U>(endpoint: String): void
{
    Domain.listen(
        endpoint,
        lambda(id: UUID, target: Domain): void
        {
            return client_connected<T, U>(target);
        },
        lambda(id: UUID, target: Domain): void
        {
            return client_disconnected(target);
        },
        lambda(id: UUID, error: DomainError): void
        {
            if (error != DomainError.OK)
                exit(-1);
        }
    );
}

@entry task test_i32_i32  (): void { return test_impl<int32     , int32       >("tcp://*:9888"); }
@entry task test_i16_i64  (): void { return test_impl<int16     , int64       >("tcp://*:9889"); }
@entry task test_i64_foo  (): void { return test_impl<int64     , common.foo  >("tcp://*:9890"); }
@entry task test_foo_bar  (): void { return test_impl<common.foo, common.bar  >("tcp://*:9891"); }
@entry task test_i32_iface(): void { return test_impl<int32     , common.iface>("tcp://*:9892"); }
@entry task test_i32_str  (): void { return test_impl<int32     , String      >("tcp://*:9893"); }
