/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
var is_done: bool = false;

function report_done()
{
    print("done\n");

    is_done = true;
}

function report_broken(ec: int32)
{
    print("broken at: \{ec}\n");

    exit(ec);
}

function do_wait()
{
    if (!is_done)
        async -> do_wait();
    else
        async -> exit(0);
}

task task_func(cond: bool)
{
    if (cond)
        return report_done();

    return cond ? report_broken(1) : report_done();

    flow -> {
        report_broken(2);
    }

    return report_broken(3);
}

@entry
task test_main_1() {
    async -> do_wait();
    async -> task_func(true);
}

@entry
task test_main_2() {
    async -> do_wait();
    async -> task_func(false);
}
