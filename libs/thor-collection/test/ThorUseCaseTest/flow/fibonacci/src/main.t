/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
task fib(n: int32): int32
{
    if (n == 0)
        return 0;

    if (n == 1)
        return 1;

    var f1: int32;
    var f2: int32;

    flow -> {
        f1 = fib(n - 1);
        f2 = fib(n - 2);
    }

    return f1 + f2;
}

@entry
task test_main()
{
    // fibonacci sequence
    //    n   | 0 | 1 | 2 | 3 | 4 | 5 | 6 |  7 |  8 |  9 | 10 | 11 |  12 |  13 | ...
    // --------------------------------------------------------------------------
    //  value | 0 | 1 | 1 | 2 | 3 | 5 | 8 | 13 | 21 | 34 | 55 | 89 | 144 | 233 | ...

    var value__0: int32; const expected__0: int32 =   0;
    var value__1: int32; const expected__1: int32 =   1;
    var value__2: int32; const expected__2: int32 =   1;
    var value__3: int32; const expected__3: int32 =   2;
    var value__4: int32; const expected__4: int32 =   3;
    var value__5: int32; const expected__5: int32 =   5;
    var value__6: int32; const expected__6: int32 =   8;
    var value__7: int32; const expected__7: int32 =  13;
    var value__8: int32; const expected__8: int32 =  21;
    var value__9: int32; const expected__9: int32 =  34;
    var value_10: int32; const expected_10: int32 =  55;
    var value_11: int32; const expected_11: int32 =  89;
    var value_12: int32; const expected_12: int32 = 144;
    var value_13: int32; const expected_13: int32 = 233;

    flow -> {
        value__0 = fib( 0);
        value__1 = fib( 1);
        value__2 = fib( 2);
        value__3 = fib( 3);
        value__4 = fib( 4);
        value__5 = fib( 5);
        value__6 = fib( 6);
        value__7 = fib( 7);
        value__8 = fib( 8);
        value__9 = fib( 9);
        value_10 = fib(10);
        value_11 = fib(11);
        value_12 = fib(12);
        value_13 = fib(13);
    }

    var failure_count: int32 = 0;

    print("value__0: \{value__0}\n");
    print("value__1: \{value__1}\n");
    print("value__2: \{value__2}\n");
    print("value__3: \{value__3}\n");
    print("value__4: \{value__4}\n");
    print("value__5: \{value__5}\n");
    print("value__6: \{value__6}\n");
    print("value__7: \{value__7}\n");
    print("value__8: \{value__8}\n");
    print("value__9: \{value__9}\n");
    print("value_10: \{value_10}\n");
    print("value_11: \{value_11}\n");
    print("value_12: \{value_12}\n");
    print("value_13: \{value_13}\n");

    if (value__0 != expected__0) { print("fib( 0) incorrect, expect \{expected__0} but got \{value__0}\n"); ++failure_count; }
    if (value__1 != expected__1) { print("fib( 1) incorrect, expect \{expected__1} but got \{value__1}\n"); ++failure_count; }
    if (value__2 != expected__2) { print("fib( 2) incorrect, expect \{expected__2} but got \{value__2}\n"); ++failure_count; }
    if (value__3 != expected__3) { print("fib( 3) incorrect, expect \{expected__3} but got \{value__3}\n"); ++failure_count; }
    if (value__4 != expected__4) { print("fib( 4) incorrect, expect \{expected__4} but got \{value__4}\n"); ++failure_count; }
    if (value__5 != expected__5) { print("fib( 5) incorrect, expect \{expected__5} but got \{value__5}\n"); ++failure_count; }
    if (value__6 != expected__6) { print("fib( 6) incorrect, expect \{expected__6} but got \{value__6}\n"); ++failure_count; }
    if (value__7 != expected__7) { print("fib( 7) incorrect, expect \{expected__7} but got \{value__7}\n"); ++failure_count; }
    if (value__8 != expected__8) { print("fib( 8) incorrect, expect \{expected__8} but got \{value__8}\n"); ++failure_count; }
    if (value__9 != expected__9) { print("fib( 9) incorrect, expect \{expected__9} but got \{value__9}\n"); ++failure_count; }
    if (value_10 != expected_10) { print("fib(10) incorrect, expect \{expected_10} but got \{value_10}\n"); ++failure_count; }
    if (value_11 != expected_11) { print("fib(11) incorrect, expect \{expected_11} but got \{value_11}\n"); ++failure_count; }
    if (value_12 != expected_12) { print("fib(12) incorrect, expect \{expected_12} but got \{value_12}\n"); ++failure_count; }
    if (value_13 != expected_13) { print("fib(13) incorrect, expect \{expected_13} but got \{value_13}\n"); ++failure_count; }

    exit(-failure_count);
}
