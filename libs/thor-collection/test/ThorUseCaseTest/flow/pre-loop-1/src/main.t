/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
var flags: Array<int32> = [
    2, 2, 2, 2, 2,
    2, 2, 2, 2, 2,
    2, 2, 2, 2, 2,
    2, 2, 2, 2, 2,
    2, 2, 2, 2, 2
];

function is_all_done()
{
    for (var flag in flags)
        if (flag != 0)
            return false;

    return true;
}

function dec(index: int32)
{
    --flags[index];
}

function do_wait()
{
    if (is_all_done())
        async -> exit(0);
    else
        async -> do_wait();
}

task task_func()
{
    for (var i = 0; i != flags.size(); ++i)
        dec(i);

    flow -> {
        dec( 0); dec( 1); dec( 2); dec( 3); dec( 4);
        dec( 5); dec( 6); dec( 7); dec( 8); dec( 9);
        dec(10); dec(11); dec(12); dec(13); dec(14);
        dec(15); dec(16); dec(17); dec(18); dec(19);
        dec(20); dec(21); dec(22); dec(23); dec(24);
    }
}

@entry
task test_main() {
    async -> do_wait  ();
    async -> task_func();
}
