/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
function do_exit_func_1(): void
{
    while (true)
    {
        exit(0);

        exit(-1);
    }

    exit(-2);
}

function do_exit_func_2(): void
{
    do_exit_func_1();

    exit(-3);
}

@entry
task do_exit_1(): void {
    while (true)
    {
        exit(0);

        exit(-1);
    }

    exit(-2);
}

@entry
task do_exit_2(): void {
    do_exit_func_1();

    exit(-3);
}

@entry
task do_exit_3(): int32 {
    async -> do_exit_1();

    return -4;
}

@entry
task do_exit_4(): int32 {
    async -> do_exit_func_2();

    return -4;
}

@entry
task do_exit_5(): int32 {
    async -> exit(0);

    return -4;
}
