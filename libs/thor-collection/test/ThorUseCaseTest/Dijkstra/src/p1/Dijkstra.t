/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
import thor.container;

function Dijkstra( matrix:thor.container.Vector<int32>, source:int32):void
{
        var numOfNode:int32;
        numOfNode = 20;
        var dist:thor.container.Vector<int32>;
        dist = new thor.container.Vector<int32>(); 
        var i:int32;
        var j:int32;
        for(j =0; j<numOfNode; ++j)
        {
               dist.pushBack(100000);
        } 
        var tmp:int32;
        var tmp2:int32;
        dist.set(source, 0);

        for(i =0; i < numOfNode; ++i)
        {
                for(j =0; j< numOfNode; ++j)
                {
                        tmp = matrix.get(i*numOfNode + j);  dist.get(i) ;
                        if(tmp > 0 )
                        { 
                                tmp2 = dist.get(i) ; 
                                tmp += tmp2;

                                if(dist.get(j) > tmp ) 
                                {
                                        dist.set(j,tmp);
                                }
                        }
                }
        }

        for(i =0; i < numOfNode; ++i)
                print(dist.get(i));

}
