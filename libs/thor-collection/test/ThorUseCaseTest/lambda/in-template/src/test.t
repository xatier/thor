/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
var count: int32 = 0;

function add_one_to_count(): void {
    count = count + 1;
}

function instantiate_one_level<T>(): void {
    var second = lambda(): void {
        add_one_to_count();
    };

    var first = lambda(): void {
        second();
    };
    first();
}

function instantiate_two_level<T>(): void {
    var first = lambda(): void {
        add_one_to_count();
        var second = lambda(): void {
            add_one_to_count();
        };
        second();
    };
    first();
}

function instantiate_three_level<T>(): void {
    var first = lambda(): void {
        add_one_to_count();
        var second = lambda(): void {
            add_one_to_count();
            var third = lambda(): void {
                add_one_to_count();
            };
            third();
        };
        second();
    };
    first();
}

@entry
task test(): void {

    var exit_success: int32 = 0;
    var exit_failure: int32 = -1;

    count = 0;
    instantiate_one_level<int32>();
    instantiate_one_level<float32>();
    if( count != 2 )
        exit(exit_failure);

    count = 0;
    instantiate_two_level<int32>();
    instantiate_two_level<float32>();
    if( count != 4 )
        exit(exit_failure);

    count = 0;
    instantiate_three_level<int32>();
    instantiate_three_level<float32>();
    if( count != 6 )
        exit(exit_failure);

    exit(exit_success);
}
