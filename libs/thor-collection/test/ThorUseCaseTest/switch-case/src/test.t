/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
@entry
task test1() : void
{
	var x : int32 = 10;
	var ret : int32 = -2;

	switch (x)
	{
	case 0: ret = -1;
	case 1: ret = -3;
	case 10: ret = 0;
	default: ret - -4;
	}

	exit(ret);
}

@entry
task test2() : void
{
	var x : int64 = 1;
	var r : int32 = -1;
	
	switch (x+1)
	{
	default: r = -3;
	case 1: r = -2;
	case 2: r = 0;
	}

	exit(r);
}

class Dummy {}

@entry
task test3() : void
{
    var g;

    switch (3)
    {
    case 3:
        g = new Dummy();
    }

    exit(g != null ? 0 : 1);
}

enum E { yes, no }

@entry
task test4() : void
{
    var e = E.no;
    switch(e)
    {
        case E.yes: exit(1);
        case E.no: exit(0);
    }
    exit(1);
}
