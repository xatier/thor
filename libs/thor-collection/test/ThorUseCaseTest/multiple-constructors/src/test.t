/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
class A
{
	public var value : int32 = 33;
	public var access : A = null;
	public function new(x : int32) : void {}
	public function new(x : float64) : void {}
}

@entry
task test1() : void
{
	var a1 = new A(10);
	var a2 = new A(10.1);

	if (a1.value != 33) exit(-1);
	if (a2.value != 33) exit(-2);
	if (a1.access != null) exit(-3);
	if (a2.access != null) exit(-4);

	exit(0);
}
