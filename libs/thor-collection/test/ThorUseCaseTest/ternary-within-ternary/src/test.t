/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
@entry
task test1() : void
{
	var x = 3;
	var z = ( (x == 4) ? (false) : (true) ) ?
				( 
					( (x == 3) ? (true) : (false) ) ? 
					( x == 4 ? 
						( x == 4 ? 8 : 9) 
						: 
						( x == 3 ? 98 : 99)
					) 
					: 
					( x == 4 ? 3 : 4 )
				) 
				:
				( 
					18
				);

	if (z != 98) exit(-1);
				
	exit(0);
}
