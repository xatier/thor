/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
@replicable
class Simple {
    public var a : int32;
    public var b : int64;
    public var c : int8;

    public function new() : void { }
    public function new( first:int32, second:int64, third:int8 ) : void {
        a = first;
        b = second;
        c = third;
    }

    public function equals( other:Simple ) : bool {
        return a == other.a && b == other.b && c == other.c;
    }
}

@replicable
class Mixed {
    public var a : int8;
    public var b : Simple;
    public var c : int64;
    public var d : String;
    public var e : int32;

    public function new() : void { }
    public function new( first:int8, second:Simple, third:int64, fourth:String,
                         fifth:int32 ) : void {
        a = first;
        b = second;
        c = third;
        d = fourth;
        e = fifth;
    }

    public function equals( other:Mixed ) : bool {
        return a == other.a && b.equals(other.b) && c == other.c && d == other.d && e == other.e;
        //                                                          ^^^^^^^^^^^^
        //                                                          expect 'null' so do not call method
    }
}

@replicable
class RefSelf
{
    public var a : int16;
    public var b : RefSelf;
    public var c : int32;

    public function new() : void { }
    public function new( first:int16, thirth:int32 ) : void {
        a = first;
        b = this;
        c = thirth;
    }

    public function validate( other:RefSelf ) : bool {
        return a == other.a &&
               other.b == other &&
               c == other.c;
    }
}

@replicable
class Circular
{
    public var a : Circular;

    public function new() : void {}
    public function new( other:Circular ) : void {
        a = other;
        other.a = this;
    }

    public static function validate( object:Circular ) : bool {
        return object.a.a == object;
    }
}

@entry
task test() : void
{
    var exit_failure : int32 = -1;
    var exit_success : int32 = 0;

    var success : bool = false;
    var encoder : ReplicationEncoder = null;
    var decoder : ReplicationDecoder = null;

    // test simple class
    encoder = new ReplicationEncoder();
    var simple : Simple = new Simple( 1, -2.0, 3 );
    if( !encoder.put( simple ) ) {
        print( "serialize Simple failed\n" ); exit(exit_failure);
    }

    decoder = new ReplicationDecoder( encoder );
    var simple2 : Simple = null;
    success, simple2 = decoder.get( simple2 );

    if( !success || !simple.equals(simple2) ) {
        print( "deserialize Simple failed\n" ); exit(exit_failure);
    }

    // test more complex class
    encoder = new ReplicationEncoder();
    var mixed : Mixed = new Mixed(
        -1, new Simple( 2, -3, 4 ), -5, null, 6
    );
    if( !encoder.put( mixed ) ) {
        print( "serialize Mixed failed\n" ); exit(exit_failure);
    }

    decoder = new ReplicationDecoder( encoder );
    var mixed2 : Mixed = null;
    success, mixed2 = decoder.get( mixed2 );
    if( !success || !mixed.equals(mixed2) ) {
        print( "deserialize Mixed failed\n" ); exit(exit_failure);
    }

    // test self reference type
    encoder = new ReplicationEncoder();
    var refself = new RefSelf(
        -1, 2
    );
    if( !encoder.put(refself) ) {
        print( "serialize RefSelf failed\n" ); exit(exit_failure);
    }

    decoder = new ReplicationDecoder( encoder );
    var refself2 : RefSelf = null;
    success, refself2 = decoder.get( refself2 );
    if( !success || !refself.validate(refself2) ) {
        print( "deserialize RefSelf failed\n" ); exit(exit_failure);
    }

    // test for cycle
    encoder = new ReplicationEncoder();
    var first : Circular = new Circular();
    var second : Circular = new Circular( first );
    if( !encoder.put(first) ) {
        print( "serialize Circular failed\n" ); exit(exit_failure);
    }

    decoder = new ReplicationDecoder( encoder );
    var circular : Circular = null;
    success, circular = decoder.get( circular );
    if( !success || !Circular.validate(circular) ) {
        print( "deserialize Circular failed\n" ); exit(exit_failure);
    }

    exit(exit_success);
}
