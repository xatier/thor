/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
import thor.lang;

@client
function func_on_client_js(a:int32, b:int32) : int32 {
    thor.lang.print("calling client js function 'func_on_client_js()'\n");
    return a > b ? a : b ;
}

@server
function min(a:int32, b:int32) : int32 {
    thor.lang.print(a);
    thor.lang.print(b);
    thor.lang.print("XXXXXXXXXXXXXX min i i\n");
    return a > b ? a : b ;
}

@export
function min(a:float32, b:float32) : float32 {
    thor.lang.print(a);
    thor.lang.print(b);
    thor.lang.print("XXXXXXXXXXXXXX min f f\n");
    return a > b ? a : b ;
}

@server
function max(a:int32, b:int32) : int32 {
    thor.lang.print(a);
    thor.lang.print(b);
    thor.lang.print("XXXXXXXXXXXXXX max f f\n");
    return a > b ? a : b ;
}

@server
function bbb(a:bool) : void {
    thor.lang.print(a);
    thor.lang.print("XXXXXXXXXXXXXX bbb\n");
}

@server
function i64(a:int64) : void {
    thor.lang.print(a);
    thor.lang.print("XXXXXXXXXXXXXX i64\n");
}
