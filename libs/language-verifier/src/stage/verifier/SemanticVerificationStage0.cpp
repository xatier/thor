/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include "language/tree/ASTNodeHelper.h"
#include "language/context/ParserContext.h"
#include "language/logging/LoggerWrapper.h"
#include "language/stage/verifier/SemanticVerificationStage0.h"
#include "language/stage/verifier/visitor/SemanticVerificationStageVisitor0.h"

namespace zillians { namespace language { namespace stage {

SemanticVerificationStage0::SemanticVerificationStage0() :
        dump_graphviz(false),
        dump_graphviz_dir(),
        keep_going(false)
{ }

SemanticVerificationStage0::~SemanticVerificationStage0()
{ }

const char* SemanticVerificationStage0::name()
{
    return "Semantic Verification Stage 0";
}

std::pair<shared_ptr<po::options_description>, shared_ptr<po::options_description>> SemanticVerificationStage0::getOptions()
{
    shared_ptr<po::options_description> option_desc_public(new po::options_description());
    shared_ptr<po::options_description> option_desc_private(new po::options_description());

    option_desc_public->add_options();

    for(auto& option : option_desc_public->options()) option_desc_private->add(option);

    option_desc_private->add_options();

    return std::make_pair(option_desc_public, option_desc_private);
}

bool SemanticVerificationStage0::parseOptions(po::variables_map& vm)
{
    dump_graphviz = vm["dump-graphviz"].as<bool>();
    if(vm.count("dump-graphviz-dir") > 0)
    {
        dump_graphviz_dir = vm["dump-graphviz-dir"].as<std::string>();
    }

    if(vm.count("mode-semantic-verify-0") > 0)
    {
        keep_going = true;
    }

    return true;
}

bool SemanticVerificationStage0::execute(bool& continue_execution)
{
    if(keep_going)
        continue_execution = true;

    if(!hasParserContext())
        return false;

    ParserContext& parser_context = getParserContext();

    if(parser_context.tangle)
    {
        if(dump_graphviz)
        {
            boost::filesystem::path p(dump_graphviz_dir);

            tree::ASTNodeHelper::visualize(getParserContext().tangle, p / "pre-semantic-0.dot");
        }

        LoggerWrapper::instance()->resetCounter();
        visitor::SemanticVerificationStageVisitor0 verifier;
        verifier.visit(*parser_context.tangle);
        verifier.applyCleanup();

        if(dump_graphviz)
        {
            boost::filesystem::path p(dump_graphviz_dir);

            tree::ASTNodeHelper::visualize(getParserContext().tangle, p / "post-semantic-0.dot");
        }

        return !(LoggerWrapper::instance()->hasError() || LoggerWrapper::instance()->hasFatal());
    }
    else
    {
        return false;
    }
}

} } }
