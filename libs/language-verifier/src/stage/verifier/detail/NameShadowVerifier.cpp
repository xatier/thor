/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <string>
#include <boost/foreach.hpp>
#include <boost/assert.hpp>

#include "language/logging/StringTable.h"
#include "language/stage/verifier/detail/NameShadowVerifier.h"
#include "language/tree/ASTNode.h"
#include "language/tree/basic/Block.h"
#include "language/tree/declaration/ClassDecl.h"
#include "language/tree/declaration/Declaration.h"
#include "language/tree/declaration/FunctionDecl.h"
#include "language/tree/declaration/TypenameDecl.h"
#include "language/tree/declaration/VariableDecl.h"
#include "language/tree/module/Package.h"

namespace zillians { namespace language { namespace stage { namespace visitor {

using namespace ::zillians::language::tree;

NameShadowVerifier::NameVarData::NameVarData(const std::wstring& name, ASTNode& owner, ASTNode& node) :
        name(name),
        owner(&owner),
        node(&node)
{
}

NameShadowVerifier::NameShadowVerifier() :
        pkg_stack(),
        cls_stack(),
        func_scope_stack(),
        enum_scope(NULL),
        block_stack(),
        name_statuses()
{
}

NameShadowVerifier::~NameShadowVerifier()
{
    BOOST_ASSERT(pkg_stack.empty());
    BOOST_ASSERT(cls_stack.empty());
    BOOST_ASSERT(func_scope_stack.empty());
    BOOST_ASSERT(enum_scope == NULL);
    BOOST_ASSERT(block_stack.empty());
    BOOST_ASSERT(name_statuses.empty());
}

void NameShadowVerifier::enter(Package& node)
{
    pkg_stack.push_back(&node);
}

void NameShadowVerifier::leave(Package& node)
{
    BOOST_ASSERT(!pkg_stack.empty() && "popping package from empty stack!");
    BOOST_ASSERT(pkg_stack.back() == &node && "package to be popped is not same as the recorded one!");

    getNameStatus<TagOwner>().erase(&node);

    pkg_stack.pop_back();
    name_statuses.erase(&node);
}

void NameShadowVerifier::enter(ClassDecl& node, const bool& follow_hierarchy)
{
    BOOST_ASSERT(cls_stack.empty() && "class stack should be empty before entering it!");

    if(follow_hierarchy)
    {
        followClassHierarchy(node);
    }

    cls_stack.push_back(&node);
}

void NameShadowVerifier::leave(ClassDecl& node)
{
    BOOST_ASSERT(!cls_stack.empty() && "popping class from empty stack!");
    BOOST_ASSERT(cls_stack.back() == &node && "class to be popped is not same as the recorded one!");

    if(isa<TemplatedIdentifier>(node.name))
    {
        getNameStatus<TagOwner>().erase(node.name);
    }

    BOOST_REVERSE_FOREACH(const auto cls, cls_stack)
    {
        getNameStatus<TagOwner>().erase(cls);
    }

    cls_stack.clear();
}

void NameShadowVerifier::enter(FunctionDecl& node)
{
    func_scope_stack.push(&node);
}

void NameShadowVerifier::leave(FunctionDecl& node)
{
    BOOST_ASSERT(!func_scope_stack.empty() && "popping function from empty stack!");
    BOOST_ASSERT(func_scope_stack.top() == &node && "function to be popped is not same as the recorded one!");

    if(isa<TemplatedIdentifier>(node.name))
    {
        getNameStatus<TagOwner>().erase(node.name);
    }

    getNameStatus<TagOwner>().erase(&node);

    func_scope_stack.pop();
}

void NameShadowVerifier::enter(Block& node)
{
    block_stack.push(&node);
}

void NameShadowVerifier::leave(Block& node)
{
    BOOST_ASSERT(!block_stack.empty() && "popping block from empty stack!");
    BOOST_ASSERT(block_stack.top() == &node && "block to be popped is not same as the recorded one!");

    getNameStatus<TagOwner>().erase(&node);

    block_stack.pop();
}

void NameShadowVerifier::enter(EnumDecl& node)
{
    BOOST_ASSERT(enum_scope == NULL && "Nested enumeration is not allowed");

    enum_scope = &node;
}

void NameShadowVerifier::leave(EnumDecl& node)
{
    BOOST_ASSERT(enum_scope == &node && "enumeration to be popped is not same as the recorded one!");

    getNameStatus<TagOwner>().erase(&node);

    enum_scope = NULL;
}

void NameShadowVerifier::enter(TypenameDecl& node)
{
    BOOST_ASSERT(isa<TemplatedIdentifier>(node.parent) && "incorrect parent of TyepnameDecl!");

    bool is_declaration = false;

    if(isa<FunctionDecl>(node.parent->parent))
    {
        is_declaration = true;
    }
    else if(isa<ClassDecl>(node.parent->parent))
    {
        is_declaration = true;
    }

    if(is_declaration)
    {
        ASTNode* owner = node.parent;

        checkShadow(node, owner);

        addName(node, owner);
    }
}

void NameShadowVerifier::leave(TypenameDecl& node)
{
}

void NameShadowVerifier::enter(VariableDecl& node)
{
    checkShadow(node);

    addName(node);
}

void NameShadowVerifier::leave(VariableDecl& node)
{
}

void NameShadowVerifier::checkShadow(Declaration& node, ASTNode* custom_owner/* = NULL*/)
{
    BOOST_ASSERT(isa<SimpleIdentifier>(node.name) && "only simple identifier is allowed to be here");

    const std::wstring name = node.name->toString();

    ASTNode* owner = custom_owner == NULL ? getOwner() : custom_owner;
    bool is_shadow_detected = false;

    BOOST_FOREACH(const NameVarData& data, getNameStatus<TagName>().equal_range(name))
    {
        if(data.owner != owner)
        {
            if(!is_shadow_detected)
            {
                LOG_MESSAGE(SHADOW_DECLARE, &node, _id(name));

                is_shadow_detected = true;
            }

            LOG_MESSAGE(SHADOW_DECLARE_PREV, data.node);
        }
    }
}

void NameShadowVerifier::followClassHierarchy(ClassDecl& node)
{
    ClassDecl* base_cls = NULL;

    if(node.base)
    {
        if(Type* cls_node = node.base->getCanonicalType())
        {
            base_cls = cls_node->getAsClassDecl();
            BOOST_ASSERT(base_cls != NULL && "class is not derived from class!");
        }
    }
    else
    {
        BOOST_ASSERT(cls_stack.empty() && "there is base class in class stack when pushing the most base one!");
    }

    if(base_cls)
    {
        followClassHierarchy(*base_cls);

        cls_stack.push_back(base_cls);

        BOOST_FOREACH(const auto var, base_cls->member_variables)
        {
            addName(*var);
        }
    }
}

void NameShadowVerifier::addName(Declaration& node, ASTNode* custom_owner/* = NULL*/)
{
    BOOST_ASSERT(isa<SimpleIdentifier>(node.name) && "only simple identifier is allowed to be here");

    const NameVarData data(node.name->toString(), custom_owner == NULL ? *getOwner() : *custom_owner, node);

    auto result = getNameStatus<TagIndex>().push_back(data);

    BOOST_ASSERT(result.second && "item is not inserted");
}

ASTNode* NameShadowVerifier::getOwner()
{
    if(!block_stack.empty())
    {
        return block_stack.top();
    }
    else if(!func_scope_stack.empty())
    {
        return func_scope_stack.top();
    }
    else if(enum_scope != NULL)
    {
        return enum_scope;
    }
    else if(!cls_stack.empty())
    {
        return cls_stack.back();
    }
    else if(!pkg_stack.empty())
    {
        return pkg_stack.back();
    }
    else
    {
        UNREACHABLE_CODE();

        return NULL;
    }
}

template<typename Tag>
typename NameShadowVerifier::NameStatusCollection::index<Tag>::type &NameShadowVerifier::getNameStatus()
{
    BOOST_ASSERT(!pkg_stack.empty() && "no package!");

    return name_statuses[pkg_stack.front()].get<Tag>();
}

} } } }
