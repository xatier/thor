/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <stack>
#include <functional>

#include "language/Architecture.h"
#include "language/logging/StringTable.h"
#include "language/logging/LoggerWrapper.h"
#include "language/tree/basic/TypeSpecifier.h"
#include "language/tree/declaration/Declaration.h"
#include "language/tree/declaration/ClassDecl.h"
#include "language/tree/expression/Expression.h"
#include "language/tree/ASTNodeHelper.h"
#include "language/stage/verifier/detail/ArchitectureVerifier.h"
#include "language/context/ConfigurationContext.h"

namespace zillians { namespace language { namespace stage { namespace visitor {

namespace {

const tree::Declaration* getDeclOf(const tree::ASTNode& node)
{
    return tree::cast_or_null<const tree::Declaration>(
        tree::ASTNodeHelper::getCanonicalSymbol(&node)
    );
}

bool isDifferentArches(const Architecture& lhs, const Architecture& rhs)
{
    auto intersection = lhs & rhs;

    return intersection == language::Architecture::zero();
}

} // anonymous namespace

using namespace ::zillians::language::tree;

ArchitectureVerifier::ArchitectureVerifier()
{
    if (hasConfigurationContext())
    {
        project_architecture = getConfigurationContext().global_config.manifest.arch;
    }

    parent_archs.push(language::Architecture::all());
}

bool ArchitectureVerifier::isArchDiffFromParent(const tree::Expression& expr) const
{
    const auto* declared = getDeclOf(expr);
    if (declared == nullptr)
        return false;

    return isArchDiffFromParent(*declared);
}

bool ArchitectureVerifier::isArchDiffFromProject(const tree::Declaration& decl) const
{
    const auto self_arch = decl.getActualArch();

    if (project_architecture.is_zero())
        return false;

    return isDifferentArches(self_arch, project_architecture);
}

bool ArchitectureVerifier::isArchDiffFromParent(const tree::Declaration& decl) const
{
    const auto self_arch = decl.getActualArch();

    return isDifferentArches(self_arch, getParentArchitecture());
}

void ArchitectureVerifier::reportErrorIfArchDiffFromProject(const tree::Declaration& decl) const
{
    if (isArchDiffFromProject(decl))
        LOG_MESSAGE(DECL_WITH_ARCH_NOT_APPEAR_IN_PROJECT_SETTING, &decl);
}

void ArchitectureVerifier::reportErrorIfCrossArchReference(const tree::Expression& expr) const
{
    if (isArchDiffFromParent(expr))
        LOG_MESSAGE(CROSS_REF_DIFFERENT_ARCH_SYMBOL, &expr);
}

void ArchitectureVerifier::reportErrorIfArchDiffFromParent(const tree::Declaration& decl) const
{
    if (isArchDiffFromParent(decl))
        LOG_MESSAGE(DECL_WITH_ARCH_NOT_APPEAR_IN_PARENT, &decl);
}

void ArchitectureVerifier::reportErrorIfAny(const tree::Declaration& decl) const
{
    if (!decl.isGlobal())
    {
        reportErrorIfArchDiffFromParent(decl);
        return;
    }

    // do not report error on global error functions
    if (const auto* function = cast<tree::FunctionDecl>(&decl))
    {
        if (function->isGlobalInit())
            return;
    }

    reportErrorIfArchDiffFromProject(decl);
}

void ArchitectureVerifier::reportErrorIfCrossArchReference(const tree::Declaration& attach, const tree::TypeSpecifier& specifier) const
{
    const auto* type = specifier.getCanonicalType();
    BOOST_ASSERT(type != nullptr && "can not get accosiated type from type specifier");

    if (type->isPrimitiveType())
        return;

    const auto* declared = type->getAsDecl();
    BOOST_ASSERT(declared != nullptr && "type had not been declared");

    if (isArchDiffFromParent(*declared))
        LOG_MESSAGE(CROSS_REF_DIFFERENT_ARCH_SYMBOL, &attach);
}

language::Architecture ArchitectureVerifier::getParentArchitecture() const
{
    return parent_archs.top();
}

void ArchitectureVerifier::verifyArchOfBaseAndInterfaces(const tree::ClassDecl& class_) const
{
    // verify base class
    if (class_.base)
        reportErrorIfCrossArchReference(class_, *class_.base);

    // verify interfaces
    for (const auto* interface : class_.implements)
        reportErrorIfCrossArchReference(class_, *interface);
}

ArchitectureVerifier::AutoArchPusher::AutoArchPusher(ArchitectureVerifier& verifier, const language::Architecture& current_arch)
    : verifier(verifier)
{
    verifier.parent_archs.push(current_arch);
}

ArchitectureVerifier::AutoArchPusher::~AutoArchPusher()
{
    verifier.parent_archs.pop();
}

} } } } // namespace zillians::language::stage::visitor
