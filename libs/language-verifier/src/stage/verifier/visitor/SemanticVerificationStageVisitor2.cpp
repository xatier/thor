/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include "language/logging/StringTable.h"
#include "language/logging/LoggerWrapper.h"
#include "language/tree/ASTNodeHelper.h"
#include "language/tree/visitor/GenericDoubleVisitor.h"
#include "language/stage/transformer/detail/AsyncHelper.h"
#include "language/stage/verifier/visitor/SemanticVerificationStageVisitor2.h"

using namespace zillians::language::tree;

namespace zillians { namespace language { namespace stage { namespace visitor {

void SemanticVerificationStageVisitor2::verifyNotTaskCall(CallExpr& node)
{
    ASTNode*const callee_node = ASTNodeHelper::getCanonicalSymbol(node.node);

    BOOST_ASSERT(callee_node != nullptr && "no callee");

    FunctionDecl*const callee_decl = cast<FunctionDecl>(callee_node);
    if (callee_decl == nullptr)
        return;

    if (callee_decl->is_task)
    {
        LOG_MESSAGE(TASK_CANNOT_BE_CALLED_DIRECTLY, &node);
        LOG_MESSAGE(TASK_CANNOT_BE_CALLED_DIRECTLY_INFO, callee_decl);
    }
}

SemanticVerificationStageVisitor2::SemanticVerificationStageVisitor2()
{
    REGISTER_ALL_VISITABLE_ASTNODE(verifyInvoker);
}

void SemanticVerificationStageVisitor2::verify(ASTNode& node)
{
    revisit(node);
}

void SemanticVerificationStageVisitor2::verify(ClassDecl& node)
{
    TemplatedIdentifier* tid = cast<TemplatedIdentifier>(node.name);
    if(tid && !tid->isFullySpecialized()) {
        return;
    }

    revisit(node);
}

void SemanticVerificationStageVisitor2::verify(FunctionDecl& node)
{
    TemplatedIdentifier* tid = cast<TemplatedIdentifier>(node.name);
    if(tid && !tid->isFullySpecialized()) {
        return;
    }

    revisit(node);
}

void SemanticVerificationStageVisitor2::verify(CallExpr& node)
{
    verifyNotTaskCall(node);

    revisit(node);
}

void SemanticVerificationStageVisitor2::verify(tree::AsyncBlock& node)
{
    detail::verify_async(node);

    revisit(node);
}

void SemanticVerificationStageVisitor2::applyCleanup()
{
    for(auto& cleaner : cleanup)
        cleaner();
    cleanup.clear();
}

} } } }
