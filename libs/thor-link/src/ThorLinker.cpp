/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include "language/toolchain/ThorLinker.h"
#include "language/stage/CompoundStage.h"
#include "language/stage/linker/BuildConfigGeneratorStage.h"
#include "language/stage/linker/ThorLinkerStage.h"
#include "language/stage/serialization/ASTSerializationStage.h"
#include "language/stage/serialization/ASTDeserializationStage.h"
#include "language/stage/generator/CppGeneratorStage.h"
#include "language/stage/generator/GeneratorDriverStage.h"
#include "language/stage/generator/LLVMGeneratorStage.h"
#include "language/stage/generator/LLVMDebugInfoGeneratorStage.h"
#include "language/stage/generator/LLVMBitCodeGeneratorStage.h"
#include "language/stage/generator/LLVMCleanupStage.h"
#include "language/stage/generator/CodeGeneratorStage.h"
#include "language/stage/serialization/BundleASTMergeStage.h"

using namespace zillians::language::stage;

namespace zillians { namespace language {

ThorLinker::ThorLinker() : stage::StageBuilder(true)
{
    using CommonLinkerStages = boost::mpl::vector<
            BuildConfigGeneratorStage,
            CppGeneratorStage,
            CompoundStage< GeneratorDriverStage, LLVMGeneratorStage, LLVMDebugInfoGeneratorStage, LLVMBitCodeGeneratorStage, CodeGeneratorStage, LLVMCleanupStage >,
            ThorLinkerStage,
            ASTSerializationStage
          >;

    using BundleASTLinkerStages = boost::mpl::push_front<CommonLinkerStages, BundleASTMergeStage>::type;
    using TangleASTLinkerStages = boost::mpl::push_front<CommonLinkerStages, ASTDeserializationStage>::type;

    addDefaultMode<TangleASTLinkerStages>();
    addMode<BundleASTLinkerStages>("mode-bundle-ast", "for bundle ast code generation.");
}

ThorLinker::~ThorLinker()
{ }

} }
