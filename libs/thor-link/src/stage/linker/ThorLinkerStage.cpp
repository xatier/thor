/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <cctype>

#include <memory>
#include <stdexcept>
#include <string>
#include <utility>
#include <vector>

#include <boost/algorithm/string/join.hpp>
#include <boost/algorithm/string/predicate.hpp>
#include <boost/algorithm/string/trim.hpp>
#include <boost/filesystem/path.hpp>
#include <boost/filesystem/operations.hpp>
#include <boost/iostreams/device/file_descriptor.hpp>
#include <boost/iostreams/stream.hpp>
#include <boost/program_options/errors.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/parsers.hpp>
#include <boost/program_options/value_semantic.hpp>
#include <boost/program_options/variables_map.hpp>
#include <boost/system/error_code.hpp>
#include <boost/range/adaptor/filtered.hpp>
#include <boost/range/adaptor/indirected.hpp>
#include <boost/range/algorithm_ext/erase.hpp>
#include <boost/range/as_literal.hpp>

#include <llvm/Config/config.h>
#if LLVM_VERSION_MAJOR >= 3 && LLVM_VERSION_MINOR >= 3
    #include <llvm/IR/Module.h>
    #include <llvm/IR/LLVMContext.h>
    #include <llvm/IRReader/IRReader.h>
#else
    #include <llvm/Module.h>
    #include <llvm/LLVMContext.h>
    #include <llvm/Support/IRReader.h>
#endif
#include <llvm/PassManager.h>
#include <llvm/Linker.h>
#include <llvm/Target/TargetMachine.h>
#include <llvm/Bitcode/BitstreamWriter.h>
#include <llvm/Bitcode/ReaderWriter.h>
#include <llvm/ADT/Triple.h>
#include <llvm/Support/PathV1.h>
#include <llvm/Support/ToolOutputFile.h>
#include <llvm/Support/SourceMgr.h>
#include <llvm/Support/TargetRegistry.h>
#include <llvm/Support/TargetSelect.h>
#include <llvm/Support/FormattedStream.h>
#include <llvm/Support/Host.h>
#include <llvm/Support/Program.h>

#include "utility/Filesystem.h"
#include "utility/Functional.h"
#include "utility/Process.h"

#include "language/Architecture.h"
#include "language/tree/ASTNode.h"
#include "language/tree/ASTNodeFactory.h"
#include "language/stage/linker/ThorLinkerStage.h"
#include "language/stage/serialization/detail/ASTSerializationHelper.h"
#include "language/stage/linker/visitor/CudaObjectSkeletonGeneratorVisitor.h"
#include "language/stage/generator/detail/CppSourceGenerator.h"
#include "language/ThorConfiguration.h"

namespace zillians { namespace language { namespace stage {

namespace {

std::string generate_dependence_include(const ThorBuildConfiguration& config)
{
    std::string result;
    for(auto& bundle : config.manifest.deps.bundles)
    {
        result += " -I" + (config.extract_bundle_path / bundle.name / "src").string();
    }
    return result;
}

std::string get_nvcc_dryrun_commands(
    const ThorBuildConfiguration&          config   ,
    const GeneratorContext::Configuration& target   ,
    const boost::filesystem::path&         dump_path)
{
    std::string command;

    const auto& nvcc = llvm::sys::Program::FindProgramByName("nvcc");
    if (nvcc.isEmpty())
        throw std::runtime_error("cannot locate path to nvcc");

    // TODO use a general way to dump dryrun result
    command += nvcc.str();
    command += " --cudart=shared -Xcompiler -fPIC -dc --dryrun";
    command += " -arch=" + target.arch.toString(";");
    command += " -o \"" + target.object_file + "\"";
    command += ' ' + config.manifest.name + ".cu";

    // includes
    command += " \"-I" + config.source_path.native() + "\"";
    for (const auto& bundle : config.manifest.deps.bundles)
        command += " \"-I" + (config.extract_bundle_path / bundle.name / "src").native() + "\"";

    command += " 2> \"" + dump_path.native() + '"';

    return std::move(command);
}

std::vector<std::string> dump_lines(const boost::filesystem::path& filepath)
{
    boost::iostreams::stream<boost::iostreams::file_descriptor_source> input(filepath);
    std::string                                                        line;
    std::vector<std::string>                                           lines;

    while (std::getline(input, line))
        lines.emplace_back(std::move(line));

    return std::move(lines);
}

std::vector<std::string> dryrun_nvcc(
    const ThorBuildConfiguration&          config,
    const GeneratorContext::Configuration& target)
{
    const auto& dryrun_result_file    = boost::filesystem::temp_directory_path() / boost::filesystem::unique_path();
    const auto& dryrun_cmd            = get_nvcc_dryrun_commands(config, target, dryrun_result_file);
    const auto& dryrun_result_deleter = invoke_on_destroy(
                                            [&dryrun_result_file]
                                            {
                                                boost::system::error_code ec;
                                                boost::filesystem::remove(dryrun_result_file, ec);
                                            }
                                        );

    if (const auto ret = zillians::system(dryrun_cmd))
        throw std::runtime_error("cannot dryrun nvcc");

    auto lines = dump_lines(dryrun_result_file);
    boost::remove_erase_if(lines, std::mem_fn(&std::string::empty));
    if (lines.empty())
        throw std::runtime_error("no dryrun result from nvcc");

    return std::move(lines);
}

std::pair<std::string /* arch */, std::string /* output */> find_arch_and_output_from_cicc_command(const std::string& command)
{
    namespace po = boost::program_options;

    auto segmented_cmds = po::split_unix(command);
    if (segmented_cmds.empty())
        throw std::runtime_error("empty command for cicc!?");

    segmented_cmds.erase(segmented_cmds.begin());

    std::string             arch_name;
    std::string             output_path;
    po::options_description desc("cicc commands");
    po::command_line_parser parser(segmented_cmds);

    desc.add_options()
        ("arch"    , po::value(&arch_name  )->required(), "architecture name")
        ("output,o", po::value(&output_path)->required(), "output path"      )
    ;

    po::variables_map vm;
    po::store(
         parser
        .style(po::command_line_style::default_style | po::command_line_style::allow_long_disguise)
        .options(desc)
        .allow_unregistered()
        .run(),
        vm
    );
    po::notify(vm);

    const auto& arch_prefix = boost::as_literal("compute_");
    if (!boost::starts_with(arch_name, arch_prefix))
        throw std::runtime_error("architecture for cicc should start with \"compute_\"");

    arch_name.erase(0, arch_prefix.size());

    return {std::move(arch_name), std::move(output_path)};
}

void hack_nvcc_dryrun_result(std::vector<std::string>& lines, const GeneratorContext::Configuration& target)
{
    // drop line prefix
    for (auto& line : lines)
    {
        const auto& prefix = boost::as_literal("#$ ");

        if (!boost::starts_with(line, prefix))
            throw std::runtime_error("unexpected line start of nvcc dryrun result");

        line.erase(0, prefix.size());

        boost::trim(line);
    }

    // remove the list of environment variables
    boost::remove_erase_if(
        lines,
        [](const std::string& line) -> bool
        {
            if (line.empty())
                return true;

            const auto& first_ch = line.front();

            if (std::isupper(first_ch) || first_ch == '_')
                return true;

            return false;
        }
    );

    for (auto& line : lines)
    {
        if (!boost::contains(line, "cicc"))
            continue;

        const auto& opts      = find_arch_and_output_from_cicc_command(line);
        const auto& arch_name = "sm_" + opts.first;
        const auto& output    = opts.second;

        if (arch_name != target.arch.toString(";"))
            throw std::runtime_error("unexpected architecture (" + arch_name + ") from cicc");

        line = "cp \"" + target.asm_file + "\" \"" + output + '"';
    }
}

bool device_compile_ptx(
    const ThorBuildConfiguration&          config,
    const GeneratorContext::Configuration& target)
{
    try
    {
        auto commands = dryrun_nvcc(config, target);

        hack_nvcc_dryrun_result(commands, target);

        for (const auto& command : commands)
            if (const auto ret = zillians::system(command))
                throw std::runtime_error("sub-command of nvcc failed: " + command);

        return true;
    }
    catch (const std::runtime_error& e)
    {
        std::cerr << "for " << target.arch.toString(";") << ", fail to device compile PTX code: " << e.what() << std::endl;
    }
    catch (const boost::program_options::error& e)
    {
        std::cerr << "for " << target.arch.toString(";") << ", fail to hack cicc command: " << e.what() << std::endl;
    }

    return false;
}

}

ThorLinkerStage::ThorLinkerStage() : verbose(false)
{
}

ThorLinkerStage::~ThorLinkerStage()
{ }

const char* ThorLinkerStage::name()
{
    return "linker_stage";
}

std::pair<shared_ptr<po::options_description>, shared_ptr<po::options_description>> ThorLinkerStage::getOptions()
{
    shared_ptr<po::options_description> option_desc_public(new po::options_description());
    shared_ptr<po::options_description> option_desc_private(new po::options_description());

    option_desc_public->add_options()
        ("verbose,v", "verbose mode");

    for(auto& option : option_desc_public->options()) option_desc_private->add(option);

    option_desc_private->add_options();

    return std::make_pair(option_desc_public, option_desc_private);
}

bool ThorLinkerStage::parseOptions(po::variables_map& vm)
{
    verbose = (vm.count("verbose") > 0);
    return true;
}

bool ThorLinkerStage::execute(bool& continue_execution)
{
    UNUSED_ARGUMENT(continue_execution);

    if(!buildNativeCodeX86()) return false;
    if(!buildNativeCodeCUDA()) return false;
    if(!linkObjects()) return false;

    if(true)
    {
        tree::ASTNodeHelper::visualize(getParserContext().tangle, getGeneratorContext().global_config.dump_graphviz_dir / "before_bundle_save.dot");
    }

    return true;
}


bool ThorLinkerStage::buildNativeCodeX86()
{
	GeneratorContext& generator_context = getGeneratorContext();

    // TODO: How about windows!?
    llvm::sys::Path native_compiler = llvm::sys::Program::FindProgramByName(THOR_CPP_NATIVE_COMPILER);
    if (native_compiler.isEmpty())
    {
        std::cerr << "Error: failed to locate " << THOR_CPP_NATIVE_COMPILER << std::endl;
        return false;
    }

	boost::filesystem::path current_working_dir = boost::filesystem::current_path();
	boost::filesystem::current_path(generator_context.global_config.project_path);

    for(auto* config : generator_context.target_config)
    {
        if(config->arch.is_not_x86())
    		continue;

    	std::string asm_file = config->asm_file;
    	std::string output_file;
    	{
    		boost::filesystem::path p = asm_file;
    		p.replace_extension(THOR_EXTENSION_OBJ);
    		output_file = p.string();
    	}

#ifdef __PLATFORM_WINDOWS__
		UNIMPLEMENTED_CODE();
#else
	    std::stringstream ss;

        // TODO: tempoarily disable assembler warning (xxx.s: Warning: stand-alone `data16' prefix)
        // From binutils: gas/config/tc-i386.c, it looks like the warning stands for the opcode is useless
        ss << " -Wa,-q";
		ss << " -c -shared -fPIC -o " + output_file;
		ss << " " << asm_file;

#endif
		if(shell(native_compiler.str(), ss.str()) != 0)
		{
			return false;
		}

		config->object_file = output_file;
    }

    boost::filesystem::current_path(current_working_dir);

    return true;
}

bool ThorLinkerStage::buildNativeCodeCUDA()
{
	GeneratorContext& generator_context = getGeneratorContext();

	bool has_any_cuda_build_target = false;
	for(auto* config : generator_context.target_config)
	{
	    if(config->arch.is_cuda())
		{
			has_any_cuda_build_target = true; break;
		}
	}

	if(!has_any_cuda_build_target)
		return true;

	// generate the CUDA skeleton
	if(true)
	{
		tree::Tangle* tangle = getParserContext().tangle;

		std::wostringstream ss;
		if(true)
		{
            std::wostringstream ss_hdr, ss_fwd;
            visitor::CudaObjectSkeletonGeneratorVisitor skeleton_generator(ss_hdr, ss_fwd);
            skeleton_generator.setForwardDeclarationOnly(true);
            skeleton_generator.visit(*tangle);

            static constexpr auto primitive_type_definitions =
                #include "thor/PrimitiveTypes.inc"
            ;

            ss << primitive_type_definitions << std::endl;
            ss << L"using thor::int8;"       << std::endl;
            ss << L"using thor::int16;"      << std::endl;
            ss << L"using thor::int32;"      << std::endl;
            ss << L"using thor::int64;"      << std::endl;
            ss << L"using thor::float32;"    << std::endl;
            ss << L"using thor::float64;"    << std::endl;

            ss << L"/***** begin included header *****/" << std::endl;
            ss << ss_hdr.str() << std::endl;
            ss << L"/***** end included header *****/" << std::endl;

			if(true)
			{
			    ss << L"/***** begin forward declaration *****/" << std::endl;
				ss << ss_fwd.str();
				ss << L"/***** end forward declaration *****/" << std::endl;
				ss << std::endl;
			}

			if(true)
			{
				std::wostringstream ss_dummy, ss_actual;
				visitor::CudaObjectSkeletonGeneratorVisitor skeleton_generator(ss_dummy, ss_actual);
				skeleton_generator.setForwardDeclarationOnly(false);
				skeleton_generator.visit(*tangle);

				ss << L"/***** begin dummy implementation *****/" << std::endl;
				ss << ss_actual.str();
				ss << L"/***** end dummy implementation *****/" << std::endl;
				ss << std::endl;
			}
		}

		// write the skeleton cu into file
		std::string cu_file;
		if(true)
		{
			cu_file = (generator_context.global_config.build_path / (generator_context.global_config.manifest.name + THOR_EXTENSION_CU)).string();

			std::wofstream cu_file_handle(cu_file);
			if(!cu_file_handle.is_open())
			{
				std::cerr << "Error: failed to open " << cu_file << "for writing" << std::endl;
				return false;
			}

			cu_file_handle << ss.str();
			cu_file_handle.close();
		}
	}

	// perform nvcc-compatible linking steps to generate final object file
	if(true)
	{
		boost::filesystem::path current_working_dir = boost::filesystem::current_path();
		boost::filesystem::current_path(generator_context.global_config.build_path);

		bool is_success = false;

#ifdef __PLATFORM_WINDOWS__
		UNIMPLEMENTED_CODE();
#else
        const auto& cuda_targets = generator_context.target_config
                                 | boost::adaptors::indirected
                                 | boost::adaptors::filtered([](const GeneratorContext::Configuration& target) { return target.arch.is_cuda(); })
                                 ;

        for (auto& target : cuda_targets)
        {
            const auto& arch         = target.arch;
            const auto& project_name = generator_context.global_config.manifest.name;
            const auto& output_name  = project_name + '.' + arch.toString(";") + THOR_EXTENSION_OBJ;

            target.object_file = (generator_context.global_config.build_path / output_name).native();
        }

        is_success = boost::algorithm::all_of(
            cuda_targets,
            std::bind(
                &device_compile_ptx,
                std::cref(generator_context.global_config),
                std::placeholders::_1
            )
        );
#endif

        if (!is_success)
            std::cerr << "Error: error while building nvcc GPU object code" << std::endl;

        // restore the current working directory
        boost::filesystem::current_path(current_working_dir);
        return is_success;
	}

	return true;
}

bool ThorLinkerStage::linkObjects()
{
    // TODO do the actual link only
    GeneratorContext& generator_context = getGeneratorContext();
    generator_context.target_config;

    // TODO: How about windows!?
    llvm::sys::Path cpp = llvm::sys::Program::FindProgramByName(THOR_CPP_NATIVE_COMPILER);
    llvm::sys::Path ar = llvm::sys::Program::FindProgramByName(THOR_NATIVE_ARCHIVER);

    if(cpp.isEmpty())
    {
        std::cerr << "Error: failed to locate " << THOR_CPP_NATIVE_COMPILER << std::endl;
        return false;
    }

    if(ar.isEmpty())
    {
        std::cerr << "Error: failed to locate " << THOR_NATIVE_ARCHIVER << std::endl;
        return false;
    }

	boost::filesystem::path current_working_dir = boost::filesystem::current_path();
	boost::filesystem::current_path(generator_context.global_config.project_path);

	// build x86/ARM shared object
    if(true)
    {
    	boost::filesystem::path output_file_path = generator_context.global_config.binary_output_path / (THOR_PREFIX_LIBRARY + generator_context.global_config.manifest.name + THOR_EXTENSION_SO);
    	std::string output_file = output_file_path.string();

    	std::stringstream ss;
#if defined (__PLATFORM_LINUX__) || defined (__PLATFORM_MAC__)
  	    // Prepare parameters to invoke gcc/cl. Use std::string to easily concate string
#ifdef __PLATFORM_MAC__
		boost::filesystem::path p1(output_file);
		std::string install_name = p1.filename().string();
		ss << " -undefined dynamic_lookup -dynamiclib -install_name ./bin/" << install_name;
#endif

		ss << " -shared -fPIC -o " << output_file;

#ifdef __PLATFORM_MAC__
		ss << " -L" << generator_context.global_config.sdk_lib_path.string();

		// by default, we will look for the libraries under the same folder
		ss << " -L" << generator_context.global_config.binary_output_path.string();
#else
		ss << " -Wl,-rpath=" << generator_context.global_config.sdk_lib_path.string();
		ss << " -L" << generator_context.global_config.sdk_lib_path.string();

		// by default, we will look for the libraries under the same folder
		ss << " -Wl,-rpath=./bin";
		ss << " -L" << generator_context.global_config.binary_output_path.string();
#endif

//    	    for (size_t i = 0; i < link_libraries.size(); i++)
//    	        args += " -l" + link_libraries[i];
//
//    	    // search path
//    	    for (size_t i = 0; i < link_search_paths.size(); i++)
//    	        args += " -L" + link_search_paths[i];

#ifdef __PLATFORM_MAC__
		// args += " -Wl,-rpath " + runtime_search_paths[i];
#else
//    	    for (size_t i = 0; i < runtime_search_paths.size(); i++)
//    	        args += " -Wl,-rpath=" + runtime_search_paths[i];
#endif
    	    // locate all object file under bin directory
#ifdef __PLATFORM_MAC__
		ss << " -Wl,-all_load";
#else
		ss << " -Wl,--whole-archive";
#endif

		for(auto* config : generator_context.target_config)
		{
		    if(config->arch.is_not_cuda())
			{
				if(!config->object_file.empty())
				{
					ss << " " << config->object_file;
				}
			}
		}

		std::string cpp_gen_temporary_file_name = "temporary_template_instantiation";
		boost::filesystem::path cpp_gen_obj_path = generator_context.global_config.build_path / (cpp_gen_temporary_file_name + THOR_EXTENSION_OBJ);
		if(boost::filesystem::exists(cpp_gen_obj_path))
		{
			ss << " " << cpp_gen_obj_path.string();
		}

//		std::vector<boost::filesystem::path> object_under_build = Filesystem::collect_files(generator_context.global_config.build_path, THOR_EXTENSION_OBJ);
//		foreach (i, object_under_build)
//		{
//			ss << " " << i->string();
//		}

		// input files
		for(auto& object : generator_context.global_config.manifest.deps.native_objects)
		{
		    if(object.arch.is_cpu())
			{
				boost::filesystem::path object_path(object.lpath);
				ss << " " << (object_path / (object.name)).string();
			}
		}

#ifdef __PLATFORM_MAC__
    	   // args += " -Wl,-noall_load";
#else
		ss << " -Wl,--no-whole-archive";
#endif

		// libraries from configuration
#ifdef __PLATFORM_MAC__
		ss << " -Wl,-all_load";
#else
		ss << " -Wl,--whole-archive";
#endif

		for (auto& library : generator_context.global_config.manifest.deps.native_libraries)
		{
		    if(library.arch.is_cpu())
			{
				ss << " -l" << library.name << " -L" << filterValidLinkerPath(library.lpath);
			}
		}

#ifdef __PLATFORM_MAC__
		//args += " -Wl,-noall_load";
#else
		ss << " -Wl,--no-whole-archive";
#endif

		for(auto& library : generator_context.global_config.manifest.deps.native_shared_libraries)
		{
		    if(library.arch.is_cpu())
			{
				if (library.rpath.length())
				{
#ifdef __PLATFORM_MAC__
					ss << " -l" << library.name << " -L" << filterValidLinkerPath(library.lpath);
#else
					ss << " -l" << library.name << " -L" << filterValidLinkerPath(library.lpath) << " -Wl,-rpath=" << library.rpath;
#endif
				}
				else
				{
					ss << " -l" << library.name << " -L" << filterValidLinkerPath(library.lpath);
				}
			}
		}

		// locate all the shared object under the project binary output path
		std::vector<boost::filesystem::path> so_under_bin = Filesystem::collect_files(generator_context.global_config.binary_output_path, THOR_EXTENSION_SO);
		for (auto& shared_library : so_under_bin)
		{
			auto stem = shared_library.stem();  // e.g. libfoo
			std::string symbol_name(stem.string(), 3);  // symbol_name = foo

			// skip myself XD
			if (symbol_name == generator_context.global_config.manifest.name) continue;

			BOOST_ASSERT(symbol_name.length() != 0 && "Well, please examine the share object file name, should start with 'lib'");
			ss << " -l" << symbol_name;
	//      args += i->string();
		}


#endif

#ifdef __PLATFORM_WINDOWS__
		UNIMPLEMENTED_CODE();
#endif

		if(shell(cpp.str(), ss.str()) != 0)
		{
			std::cerr << "Error: error while building shared object" << std::endl;
			return false;
		}
    }

    // create the CUDA linkable archive
    if(true)
    {
    	std::stringstream ss;
    	ss << " cr";

    	boost::filesystem::path output_archive_path = generator_context.global_config.binary_output_path / (THOR_PREFIX_LIBRARY + generator_context.global_config.manifest.name + "_cuda" + THOR_EXTENSION_LIB);
    	ss << " " << output_archive_path.string();

    	bool require_archive_for_cuda_linking = false;
		for(auto* config : generator_context.target_config)
		{
		    if(config->arch.is_cuda())
			{
				if(!config->object_file.empty())
				{
					ss << " " << config->object_file;
					require_archive_for_cuda_linking = true;
				}
			}
		}

		for (auto& native_object : generator_context.global_config.manifest.deps.native_objects)
		{
            if(native_object.arch.is_cuda())
			{
				boost::filesystem::path object_path(native_object.lpath);
				ss << " " << (object_path / (native_object.name)).string();
			}
		}

		for (auto& native_library : generator_context.global_config.manifest.deps.native_libraries)
		{
            if(native_library.arch.is_cuda())
			{
			    // TODO the i->lpath could be of semicolumn-delimited format, in which we should iterate each of them to locate the actual library
				// extract all enclosing objects
				boost::filesystem::path path_to_extract_object;
				if(true)
				{
					boost::filesystem::path lpath(native_library.lpath);
					boost::filesystem::path path_to_lib;
					if(lpath.empty())
					{
						// TODO we should search for default link path (LIBRARY_PATH)
						UNIMPLEMENTED_CODE();
					}
					else
					{
						if(lpath.is_absolute())
							path_to_lib = lpath / (THOR_PREFIX_LIBRARY + (native_library.name) + THOR_EXTENSION_LIB);
						else
							path_to_lib = boost::filesystem::current_path() / lpath / (THOR_PREFIX_LIBRARY + (native_library.name) + THOR_EXTENSION_LIB);
					}

					path_to_extract_object = generator_context.global_config.build_path / native_library.name;
					if(!extractArchive(path_to_lib, path_to_extract_object))
					{
						std::cerr << "Error: failed to extract library '" << (native_library.name) << "'" << std::endl;
						return false;
					}
				}

				ss << " " << (path_to_extract_object / (std::string("*") + THOR_EXTENSION_OBJ)).string();
			}
		}

		if(require_archive_for_cuda_linking)
		{
			if(shell(ar.str(), ss.str()) != 0)
			{
				std::cerr << "Error: error while building archive for CUDA linking" << std::endl;
				return false;
			}

		}
    }

    boost::filesystem::current_path(current_working_dir);

    return true;
}

bool ThorLinkerStage::extractArchive(const boost::filesystem::path& absolute_library_path, const boost::filesystem::path& extract_path)
{
	llvm::sys::Path ar = llvm::sys::Program::FindProgramByName(THOR_NATIVE_ARCHIVER);
    if(ar.isEmpty())
    {
        std::cerr << "Error: failed to locate " << THOR_NATIVE_ARCHIVER << std::endl;
        return false;
    }

	if(!boost::filesystem::exists(extract_path))
	{
		if(!boost::filesystem::create_directories(extract_path))
			return false;
	}

	// save the current path
	boost::filesystem::path current_path_backup = boost::filesystem::current_path();

	// switch to the path to extract objects (because ar can only extract to current directory
	boost::filesystem::current_path(extract_path);

	std::stringstream ss;
	ss << " x " << absolute_library_path.string();

	if(shell(ar.str(), ss.str()) != 0)
	{
		boost::filesystem::current_path(current_path_backup);
		return false;
	}

	// restore current path
	boost::filesystem::current_path(current_path_backup);

	return true;
}

std::string ThorLinkerStage::filterValidLinkerPath(const std::string& path)
{
    return boost::algorithm::join(
        StringUtil::tokenize(path, ";", false) | boost::adaptors::filtered([](const boost::filesystem::path& p){ // implicit conversion
            return boost::filesystem::exists(p) && boost::filesystem::is_directory(p);
        }),
        ";"
    );
}

int ThorLinkerStage::shell(const std::string& tool, const std::string& parameters)
{
	GeneratorContext& generator_context = getGeneratorContext();

	if(generator_context.global_config.verbose)
	{
		std::cerr << "[Link] invokes '" << tool << " " << parameters << "'" << std::endl;
	}

	std::string cmd = (tool + " " + parameters);
	return zillians::system(cmd);
}


} } }
