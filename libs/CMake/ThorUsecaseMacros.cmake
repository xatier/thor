#
# Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
#
# This file is part of Thor.
# Thor is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License, version 3,
# as published by the Free Software Foundation.
#
# Thor is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Thor.  If not, see <http://www.gnu.org/licenses/>.
#
# If you want to develop any commercial services or closed-source products with
# Thor, to adapt sources of Thor in your own projects without
# disclosing sources, purchasing a commercial license is mandatory.
#
# For more information, please contact Zillians, Inc.
# <thor@zillians.com>
#

INCLUDE(CMakeParseArguments)

INCLUDE(ThorTestMacros)
INCLUDE(ThorToolchainDefs)

MACRO(thor_add_usecase_test)
    SET(thor_add_usecase_test_switches         USE_MONITOR)
    SET(thor_add_usecase_test_one_value_opts   NAME ENTRY)
    SET(thor_add_usecase_test_multi_value_opts EXTRA_RUN_OPTS)

    CMAKE_PARSE_ARGUMENTS(THOR_ADD_USECASE_TEST
        "${thor_add_usecase_test_switches}"
        "${thor_add_usecase_test_one_value_opts}"
        "${thor_add_usecase_test_multi_value_opts}"
        ${ARGN}
    )

    IF(NOT THOR_ADD_USECASE_TEST_NAME)
        MESSAGE(SEND_ERROR "test name missing")
    ELSEIF(NOT THOR_ADD_USECASE_TEST_ENTRY)
        MESSAGE(SEND_ERROR "test entry missing")
    ELSE()
        SET(run_cmd "${THOR_DRIVER} run ${THOR_ADD_USECASE_TEST_ENTRY}")

        IF(THOR_ADD_USECASE_TEST_EXTRA_RUN_OPTS)
            SET(run_cmd "${run_cmd} ${THOR_ADD_USECASE_TEST_EXTRA_RUN_OPTS}")
        ENDIF()

        IF(THOR_ADD_USECASE_TEST_USE_MONITOR)
            SET(run_cmd "${THOR_MONITOR_SCRIPT} -c \"${run_cmd}\"")
        ENDIF()

        thor_add_test(
            NAME    ${THOR_ADD_USECASE_TEST_NAME}
            COMMAND ${CMAKE_COMMAND}
                    "-DBUILD_DIR=${CMAKE_CURRENT_BINARY_DIR}"
                    "-DSOURCE_DIR=${CMAKE_CURRENT_SOURCE_DIR}"
                    "-DTEST_NAME=${THOR_ADD_USECASE_TEST_NAME}"
                    "-DCMD_0:STRING=${THOR_DRIVER} build debug --verbose"
                    "-DCMD_1:STRING=${run_cmd}"
                    -P "${THOR_TEST_SCRIPT}"
        )

        UNSET(run_cmd)
    ENDIF()
ENDMACRO()
