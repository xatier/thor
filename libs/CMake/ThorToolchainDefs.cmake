#
# Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
#
# This file is part of Thor.
# Thor is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License, version 3,
# as published by the Free Software Foundation.
#
# Thor is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Thor.  If not, see <http://www.gnu.org/licenses/>.
#
# If you want to develop any commercial services or closed-source products with
# Thor, to adapt sources of Thor in your own projects without
# disclosing sources, purchasing a commercial license is mandatory.
#
# For more information, please contact Zillians, Inc.
# <thor@zillians.com>
#

set(THOR_BUNDLER       ${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/thor-bundle)
set(THOR_COMPILER      ${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/thor-compile)
set(THOR_DOC           ${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/thor-doc)
set(THOR_DRIVER        ${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/thorc)
set(THOR_LINKER        ${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/thor-link)
set(THOR_MAKE          ${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/thor-make)
set(THOR_STUBGENERATOR ${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/thor-stub)
set(THOR_VM            ${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/thor-vm)
set(THOR_IMPORTER      ${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/thor-import)
