#
# Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
#
# This file is part of Thor.
# Thor is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License, version 3,
# as published by the Free Software Foundation.
#
# Thor is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Thor.  If not, see <http://www.gnu.org/licenses/>.
#
# If you want to develop any commercial services or closed-source products with
# Thor, to adapt sources of Thor in your own projects without
# disclosing sources, purchasing a commercial license is mandatory.
#
# For more information, please contact Zillians, Inc.
# <thor@zillians.com>
#

# - Try to find the OptiX libraries
# Once done this will define
#
# OPTIX_FOUND - system has OptiX
# OPTIX_INCLUDE_DIR - the OptiX include directory
# OPTIX_LIBRARIES   - the OptiX libraries

MESSAGE(STATUS "Looking for OptiX...")

FIND_PATH(OPTIX_INCLUDE_DIR optix_cuda.h PATHS 
	/usr/include/ 
	/usr/local/include/
	${CUDA_INCLUDE_DIRS}
	${CMAKE_SOURCE_DIR}/dep/linux/optix/include/
	)

IF(CMAKE_SIZEOF_VOID_P EQUAL 8)
    FIND_LIBRARY(OPTIX_LIBRARIES_OPTIX NAMES optix PATHS
        ${CMAKE_SOURCE_DIR}/dep/linux/optix/lib64/
        )
    FIND_LIBRARY(OPTIX_LIBRARIES_OPTIXU NAMES optixu PATHS
        ${CMAKE_SOURCE_DIR}/dep/linux/optix/lib64/
        )
ELSE()
    FIND_LIBRARY(OPTIX_LIBRARIES_OPTIX NAMES optix PATHS
        ${CMAKE_SOURCE_DIR}/dep/linux/optix/lib/
        )
    FIND_LIBRARY(OPTIX_LIBRARIES_OPTIXU NAMES optixu PATHS
        ${CMAKE_SOURCE_DIR}/dep/linux/optix/lib/
        )
ENDIF()
	
IF(OPTIX_INCLUDE_DIR AND OPTIX_LIBRARIES_OPTIX AND OPTIX_LIBRARIES_OPTIXU)
	SET(OPTIX_FOUND 1)
	SET(OPTIX_LIBRARIES ${OPTIX_LIBRARIES_OPTIX} ${OPTIX_LIBRARIES_OPTIXU})
	IF(NOT OPTIX_FIND_QUIETLY)
		MESSAGE(STATUS "Found OptiX: include dir = ${OPTIX_INCLUDE_DIR}, libraries = ${OPTIX_LIBRARIES}")
	ENDIF()
ELSE()
	SET(OPTIX_FOUND 0 CACHE BOOL "OptiX not found")
	IF(OPTIX_FIND_REQUIRED)
    	MESSAGE(FATAL_ERROR "Could NOT find OptiX, error")
	ELSE()
		MESSAGE(STATUS "Could NOT find OptiX, disabled")
	ENDIF()
ENDIF()

MARK_AS_ADVANCED(OPTIX_INCLUDE_DIR)
MARK_AS_ADVANCED(OPTIX_LIBRARIES)

