#
# Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
#
# This file is part of Thor.
# Thor is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License, version 3,
# as published by the Free Software Foundation.
#
# Thor is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Thor.  If not, see <http://www.gnu.org/licenses/>.
#
# If you want to develop any commercial services or closed-source products with
# Thor, to adapt sources of Thor in your own projects without
# disclosing sources, purchasing a commercial license is mandatory.
#
# For more information, please contact Zillians, Inc.
# <thor@zillians.com>
#

# - Try to find the LibConfig libraries
# Once done this will define
#
# LIBCONFIG_FOUND - system has Config
# LIBCONFIG_INCLUDE_DIR - the Config include directory
# LIBCONFIG_LIBRARIES - Config library

IF(NOT WIN32)
    FIND_PATH(LIBCONFIG_INCLUDE_DIR libconfig.h++ PATHS
        /usr/include/
        )

    FIND_PATH(LIBCONFIG_LIBRARIES libconfig++.so 
        /usr/lib
	/usr/lib64
        )
    
    IF( LIBCONFIG_LIBRARIES )
        SET( LIBCONFIG_LIBRARIES ${LIBCONFIG_LIBRARIES}/libconfig++.so )
    ENDIF()
        
#    FIND_PROGRAM(LIBCONFIG_COMPILER thrift 
#        /usr/bin
#        /usr/local/bin
#        )
ENDIF()

IF(LIBCONFIG_INCLUDE_DIR AND LIBCONFIG_LIBRARIES)
    SET(LIBCONFIG_FOUND 1)
    IF(NOT LIBCONFIG_FOUND_QUIETLY)
        MESSAGE(STATUS "Found Libconfig: include = ${LIBCONFIG_INCLUDE_DIR}, libraries = ${LIBCONFIG_LIBRARIES}")
    ENDIF()
ELSE()
    SET(LIBCONFIG_FOUND 0 CACHE BOOL "LIBCONFIG not found")
    MESSAGE(STATUS "LIBCONFIG not found, disabled")
ENDIF()

MARK_AS_ADVANCED(LIBCONFIG_INCLUDE_DIR LIBCONFIG_LIBRARIES)
