#
# Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
#
# This file is part of Thor.
# Thor is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License, version 3,
# as published by the Free Software Foundation.
#
# Thor is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Thor.  If not, see <http://www.gnu.org/licenses/>.
#
# If you want to develop any commercial services or closed-source products with
# Thor, to adapt sources of Thor in your own projects without
# disclosing sources, purchasing a commercial license is mandatory.
#
# For more information, please contact Zillians, Inc.
# <thor@zillians.com>
#

# - Try to find the editline libraries
# Once done this will define
#
# EDITLINE_FOUND - system has editline
# EDITLINE_INCLUDE_DIR - the editline include directory
# EDITLINE_LIBRARY - editline library
#
# Zac: if editline not found, please make sure you have installed the package: libedit-dev
#   $ sudo apt-get install libedit-dev
#

FIND_PATH(EDITLINE_INCLUDE_DIR editline/readline.h)
FIND_LIBRARY(EDITLINE_LIBRARY NAMES edit) 

IF (EDITLINE_INCLUDE_DIR AND EDITLINE_LIBRARY)
   SET(EDITLINE_FOUND TRUE)
ENDIF (EDITLINE_INCLUDE_DIR AND EDITLINE_LIBRARY)

IF (EDITLINE_FOUND)
   IF (NOT EDITLINE_FIND_QUIETLY)
      MESSAGE(STATUS "Found editline: include = ${EDITLINE_INCLUDE_DIR}, libraries = ${EDITLINE_LIBRARY}")
   ENDIF (NOT EDITLINE_FIND_QUIETLY)
ELSE (EDITLINE_FOUND)
   MESSAGE(STATUS "editline not found, disabled")
   IF (EDITLINE_FIND_REQUIRED)
      MESSAGE(FATAL_ERROR "Could not find editline")
   ENDIF (EDITLINE_FIND_REQUIRED)
ENDIF (EDITLINE_FOUND)
