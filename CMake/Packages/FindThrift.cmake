#
# Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
#
# This file is part of Thor.
# Thor is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License, version 3,
# as published by the Free Software Foundation.
#
# Thor is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Thor.  If not, see <http://www.gnu.org/licenses/>.
#
# If you want to develop any commercial services or closed-source products with
# Thor, to adapt sources of Thor in your own projects without
# disclosing sources, purchasing a commercial license is mandatory.
#
# For more information, please contact Zillians, Inc.
# <thor@zillians.com>
#

# - Try to find the Thrift libraries
# Once done this will define
#
# THRIFT_FOUND - system has Thrift
# THRIFT_INCLUDE_DIR - the Thrift include directory
# THRIFT_LIBRARIES - Thrift library

IF(NOT WIN32)
    FIND_PATH(THRIFT_INCLUDE_DIR Thrift.h PATHS
        /usr/include/thrift
        /usr/local/include/thrift
        /opt/local/include/thrift
        )
    FIND_LIBRARY(THRIFT_LIBRARIES thrift)
    FIND_PROGRAM(THRIFT_COMPILER thrift)
ENDIF()

#IF(THRIFT_INCLUDE_DIR AND THRIFT_LIBRARIES AND THRIFT_LIBRARIES_FPIC AND THRIFT_COMPILER)
IF(THRIFT_INCLUDE_DIR AND THRIFT_LIBRARIES AND THRIFT_COMPILER)
    SET(THRIFT_FOUND 1)
    IF(NOT THRIFT_FOUND_QUIETLY)
        MESSAGE(STATUS "Found THRIFT: include = ${THRIFT_INCLUDE_DIR}, libraries = ${THRIFT_LIBRARIES}, thrift_compiler ${THRIFT_COMPILER}")
    ENDIF()
ELSE()
    SET(THRIFT_FOUND 0 CACHE BOOL "THRIFT not found")
    MESSAGE(STATUS "THRIFT not found, disabled")
ENDIF()

MARK_AS_ADVANCED(THRIFT_INCLUDE_DIR THRIFT_LIBRARIES)

MESSAGE(" thrift header - ${THRIFT_INCLUDE_DIR} ")
