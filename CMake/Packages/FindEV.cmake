#
# Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
#
# This file is part of Thor.
# Thor is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License, version 3,
# as published by the Free Software Foundation.
#
# Thor is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Thor.  If not, see <http://www.gnu.org/licenses/>.
#
# If you want to develop any commercial services or closed-source products with
# Thor, to adapt sources of Thor in your own projects without
# disclosing sources, purchasing a commercial license is mandatory.
#
# For more information, please contact Zillians, Inc.
# <thor@zillians.com>
#

# - Try to find the LibEv libraries
# Once done this will define
#
# EV_FOUND - system has liblog4cxx
# EV_INCLUDE_DIR - the liblog4cxx include directory
# EV_LIBRARIES - liblog4cxx library

MESSAGE(STATUS "Looking for EV...")

FIND_PATH(EV_INCLUDE_DIR ev.h PATHS 
	/usr/include/ 
	/usr/local/include/ 
	/usr/local/include/ev/ 
	)
	
FIND_LIBRARY(EV_LIBRARIES NAMES ev)

IF(EV_INCLUDE_DIR AND EV_LIBRARIES)
    SET(EV_FOUND 1)
    IF(NOT EV_FIND_QUIETLY)
        MESSAGE(STATUS "Found EV: libraries = ${EV_LIBRARIES}")
    ENDIF(NOT EV_FIND_QUIETLY)
ELSE(EV_INCLUDE_DIR AND EV_LIBRARIES)
    SET(EV_FOUND 0 CACHE BOOL "EV not found")
    MESSAGE(STATUS "Could NOT find EV, disabled")
ENDIF(EV_INCLUDE_DIR AND EV_LIBRARIES)

MARK_AS_ADVANCED(EV_INCLUDE_DIR EV_LIBRARIES)

