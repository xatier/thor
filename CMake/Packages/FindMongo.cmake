#
# Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
#
# This file is part of Thor.
# Thor is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License, version 3,
# as published by the Free Software Foundation.
#
# Thor is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Thor.  If not, see <http://www.gnu.org/licenses/>.
#
# If you want to develop any commercial services or closed-source products with
# Thor, to adapt sources of Thor in your own projects without
# disclosing sources, purchasing a commercial license is mandatory.
#
# For more information, please contact Zillians, Inc.
# <thor@zillians.com>
#

# - Try to find the Mongo libraries
# Once done this will define
#
# MONGO_FOUND - system has Mongo
# MONGO_INCLUDE_DIR - the Mongo include directory
# MONGO_LIBRARIES - Mongo library

IF(NOT WIN32)
    FIND_PATH(MONGO_INCLUDE_DIR mongo/client/dbclient.h PATHS
        ${CMAKE_SOURCE_DIR}/dep/linux/mongodb/
        )

    SET(MONGO_INCLUDE_DIR ${MONGO_INCLUDE_DIR} CACHE PATH "Mongo include directory" FORCE) # cache result

    FIND_LIBRARY(MONGO_LIBRARIES_MONGOCLIENT NAMES mongoclient PATHS
        ${CMAKE_SOURCE_DIR}/dep/linux/mongodb/mongodb-lib
        )
ENDIF()

IF(MONGO_INCLUDE_DIR AND MONGO_LIBRARIES_MONGOCLIENT)
    SET(MONGO_FOUND 1)
    SET(MONGO_LIBRARIES ${MONGO_LIBRARIES_MONGOCLIENT})
    IF(NOT MONGO_FIND_QUIETLY)
        MESSAGE(STATUS "Found MONGO: include = ${MONGO_INCLUDE_DIR}, libraries = ${MONGO_LIBRARIES}")
    ENDIF()
ELSE()
    SET(MONGO_FOUND 0 CACHE BOOL "MONGO not found")
    MESSAGE(STATUS "MONGO not found, disabled")
ENDIF()

MARK_AS_ADVANCED(MONGO_INCLUDE_DIR MONGO_LIBRARIES)
